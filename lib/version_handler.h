#ifndef VERSIONHANDLER_H
#define VERSIONHANDLER_H



class VersionHandler
{
public:
  VersionHandler();
  static bool isValid(const QString &version);
  static int isOlderThan(const QString &v1, const QString &vRef);
  static int64_t convertVerToVal(const QString &version);
};

#endif // VERSIONHANDLER_H
