#ifndef EMAIL_H
#define EMAIL_H





class EmailHandler
{
public:
  EmailHandler();

  static bool sendEmailWrapper(
    const QString &sender_,
    const QString &title,
    const QString &message,
    const QString &receiver);

  static bool send_mail(
    const QString &host,
    const QString &sender,
    const QString &pass,
    const QString &subject,
    const QString &message,
    const QString &receiver);

};

#endif // EMAIL_H
