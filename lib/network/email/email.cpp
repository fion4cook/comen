#include "stable.h"
#include "lib/machine/machine_handler.h"
#include "email.h"

EmailHandler::EmailHandler()
{

}

bool EmailHandler::sendEmailWrapper(
  const QString &sender_,
  const QString &title,
  const QString &message,
  const QString &receiver
    )
{
  CLog::log("send EmailWrapper args: sender(" + sender_ + ") receiver(" + receiver + ") title(" + title + ")" , "app", "trace");
  EmailSettings machine_public_email = CMachine::getPubEmailInfo();
  EmailSettings machine_private_email = CMachine::getPrivEmailInfo();

  QString sender, pass, host;

  if (machine_private_email.m_address == sender_) {
    sender = machine_private_email.m_address;
    pass = machine_private_email.m_password;
    host = machine_private_email.m_outgoing_mail_server;
  } else {
    sender = machine_public_email.m_address;
    pass = machine_public_email.m_password;
    host = machine_public_email.m_outgoing_mail_server;
  }
  return send_mail(host, sender, pass, title, message, receiver);
}

bool EmailHandler::send_mail(
  const QString &host,
  const QString &sender,
  const QString &pass,
  const QString &subject_,
  const QString &message,
  const QString &receiver
  )
{
  QString subject = subject_;
  if (CConsts::IS_DEVELOP_MODE)
    subject = "test";     //remove beforerelease

  // connect to poco;

//  let transporter = nodemailer.createTransport({
//    host: args.host,
//    service: "inbox",
//    auth: {
//        user: args.sender,
//        pass: args.pass
//    }
//  });
//  var mailOptions = {
//    from: args.sender,
//    to: receiver, // list of receivers
//    subject: subject, // Subject line
//    text: message
//  };

//  transporter.sendMail(mailOptions, function (err, info) {
//    if (err) {
//        console.error(err);
//        return ({ err: true, msg: err });
//    } else {
//        clog.app.info("Message sent: " + info.response);
//        return ({ err: false, msg: info.response });
//    };
//  });
  return true;
}
