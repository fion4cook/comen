#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H


class NetworkHandler
{
public:
  NetworkHandler();

  static bool iPush(
    const QString &title,
    const QString &message,
    const QString &sender,
    const QString &receiver);
};

#endif // NETWORKHANDLER_H
