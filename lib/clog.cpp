#include "clog.h"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>


namespace logging = boost::log;
namespace keywords = boost::log::keywords;

CLog CLog::s_instance;

void CLog::IinitLog()
{
  logging::register_simple_formatter_factory<logging::trivial::severity_level, char>("Severity");
  logging::add_file_log
  (
    keywords::file_name = "sample.log",
    keywords::format = "[%TimeStamp%] [%ThreadID%] [%Severity%] %Message%",
    logging::keywords::auto_flush = true
  );
  logging::add_common_attributes();
  CLog::Ilog(CLog::version());
}

void CLog::Ilog(
    const std::string &msg,
    const std::string &module,
    const std::string &level
  )
{
  //FIXME: for each module create a separate file
  //FIXME: messages higher than info must be written in a separate file -named errors -

  std::string fullMsg = module + " > " + msg;
  if (level=="fatal"){
    BOOST_LOG_TRIVIAL(fatal) << fullMsg;
  }else if (level=="error"){
    BOOST_LOG_TRIVIAL(error) << fullMsg;
  }else if (level=="warning"){
    BOOST_LOG_TRIVIAL(warning) << fullMsg;
  }else if (level=="info"){
    BOOST_LOG_TRIVIAL(info) << fullMsg;
  }else if (level=="debug"){
    BOOST_LOG_TRIVIAL(debug) << fullMsg;
  }else if (level=="trace"){
    BOOST_LOG_TRIVIAL(trace) << fullMsg;
  }
//    j = boost::log::trivial::severity_level::info;
}

void CLog::Ilog(
    const QString &msg,
    const std::string &module,
    const std::string &level
    )
{
  CLog::Ilog(
        msg.toStdString(),
        module,
        level
        );
}

void CLog::Ilog(
    const char *msg,
    const char *module,
    const char *level
    )
{
  CLog::Ilog(
        static_cast<std::string>(msg),
        static_cast<std::string>(module),
        static_cast<std::string>(level)
        );
}

std::string CLog::version(){
  //FIXME
  std::string v="Using Boost 1.65.1";
//    v = "Using Boost " + QString(BOOST_VERSION / 100000) + "."  // major version
//              BOOST_VERSION / 100 % 1000 + "."  // minor version
//              BOOST_VERSION % 100;  // patch level
  return v;
}
