#ifndef BLOCKUTILS_H
#define BLOCKUTILS_H

struct WrapDBObj
{
    QString sfVer = "";
    QString content = "";
};

class WrapRes : public GenRes
{
public:
    WrapRes() : GenRes() {
        sfVer = "0.0.0";
        content = "";
    };

    QString sfVer = "";
    QString content = "";

};

class BlockUtils
{
public:
    BlockUtils();
    static WrapRes wrapSafeContentForDB(const QString &content,
                                        const QString &sfVer = "0.0.0");

    static WrapRes unwrapSafeContentForDB(const QString &wrapped);
    static bool ifAncestorsAreValid(const QStringList &ancestors);
};

#endif // BLOCKUTILS_H
