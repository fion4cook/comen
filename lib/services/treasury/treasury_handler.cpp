#include "stable.h"

#include "treasury_handler.h"

const QString TreasuryHandler::stbl_treasury = "c_treasury";

TreasuryHandler::TreasuryHandler()
{

}

std::tuple<QString, QString> TreasuryHandler::getTreasureIncomesDateRange(const QString cDate)
{
  return CUtils::getACycleRange(cDate, CConsts::TREASURY_MATURATION_CYCLES);
}

std::tuple<QString, QString, MPAIValueT> TreasuryHandler::calcTreasuryIncomes(
  const QString cDate)
{

  // retrieve the total treasury incomes in last cycle (same as share cycle calculation)
  auto[minCreationDate, maxCreationDate] = getTreasureIncomesDateRange(cDate);

  QueryRes incomes = DbModel::sCustom(
    "SELECT SUM(tr_value) incomes_ FROM " + stbl_treasury + " WHERE tr_creation_date between \"" + minCreationDate + "\" AND \"" + maxCreationDate + "\" ",
    {"incomes_"});
  CLog::log("calc Treasury Incomes WHERE creation_date between (" + minCreationDate + "," + maxCreationDate + ") -> incomes: " + CUtils::dumpIt(incomes.records));
  MPAIValueT income_value = 0;
  if (incomes.records[0][0].toUInt() > 0)
    income_value = incomes.records[0][0].toUInt();

  return {
    minCreationDate,
    maxCreationDate,
    income_value};

}
