#ifndef TREASURYHANDLER_H
#define TREASURYHANDLER_H


class TreasuryHandler
{
public:
  TreasuryHandler();

  const static QString stbl_treasury;

  static std::tuple<QString, QString> getTreasureIncomesDateRange(const QString cDate = CUtils::getNow());
  static std::tuple<QString, QString, MPAIValueT> calcTreasuryIncomes(
  const QString cDate = CUtils::getNow());

};

#endif // TREASURYHANDLER_H
