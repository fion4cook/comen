#ifndef DEMOSHANDLER_H
#define DEMOSHANDLER_H



#include "lib/machine/machine_handler.h"
#include "lib/wallet/wallet_address_handler.h"

class Agora
{
public:
  QString m_title = "";
  QString m_doc_hash = "";  // hash of agora creator document
  QString m_agora_hash = "";    // agora id
  QString m_iname_hash = "";   // domain name(id) e.g. imagine/agora1/post1
  QString m_unique_hash = "";
  QString m_parent = "";
  QString m_description = "";
  QString m_tags = "";
  QString m_creation_date = "";
  QString m_last_modified = CUtils::getNow();
  QString m_creator = "";
  QString m_language = CConsts::DEFAULT_LANG;   // content language
  QString m_content_format_versaion = CConsts::DEFAULT_CONTENT_VERSION;

  Agora(const QVDicT &values){
    if (values.keys().size() > 0)
        setByDict(values);
  }
  bool setByDict(const QVDicT &values = {});

};

class DemosHandler
{
public:
  static const QString stbl_demos_agoras;
  static const QStringList stbl_demos_agoras_fields;
  static const QString stbl_demos_posts;

  DemosHandler();
  static bool insertAgoraInDB(const Agora &ago);
  static QueryRes searchInAgoras(
    const ClausesT &clauses,
    const QStringList &fields = stbl_demos_agoras_fields
    );
  static GenRes insertDemosPost(QVDicT &params);

};

#endif // DEMOSHANDLER_H
