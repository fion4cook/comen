#include "stable.h"
#include "lib/ccrypto.h"


class DemosHandler;

#include "lib/services/free_docs/demos/demos_handler.h"
#include "lib/services/free_docs/demos/init_contents/agoras_init_contents.h"


#include "initialize_agoras.h"

using std::vector;

AgoraInitializer::AgoraInitializer()
{

}

QHash<QString, QString> AgoraInitializer::map_title_to_ag_unique_hash = {};
bool AgoraInitializer::initAgoras()
{
  bool status = false;
  vector<InitAgora> def_agoras =
  {
    InitAgora("Beginners"),
    InitAgora("Development"),
    InitAgora("Economy"),
    InitAgora("Translation")
  };


  QString iname_hash = CCrypto::convertTitleToHash("imagine");
  QString creation_date = CUtils::getLaunchDate();

  for (InitAgora an_agora : def_agoras) {
    QString ag_hash = CCrypto::convertTitleToHash(an_agora.m_title);
    QString ag_doc_hash = ag_hash;   // a dummy doc-hash for initializing
    QString unique_hash = CCrypto::keccak256(iname_hash + "/" + ag_hash);
    bool status = DemosHandler::insertAgoraInDB(Agora(QVDicT
    {
      {"title", an_agora.m_title},
      {"doc_hash", ag_doc_hash},
      {"agora_hash", ag_hash},
      {"iname_hash", iname_hash},
      {"unique_hash", unique_hash},
      {"parent", ""},
      {"description", an_agora.m_description},
      {"tags", an_agora.m_tags},
      {"creation_date", creation_date},
      {"last_modified", creation_date},
      {"creator", CConsts::HU_INAME_OWNER_ADDRESS}
    }));
    map_title_to_ag_unique_hash.insert(an_agora.m_title, unique_hash);
    CLog::log(">>>>>>>>>>>>>>>>>>>>> map_title_to_ag_unique_hash " +unique_hash + " <- " + an_agora.m_title);
    if (!status)
    {
      return false;
    }
  }


  // insert sub-category
  def_agoras =
  {
    InitAgora("Before Start"),
    InitAgora("Contribute, Proposals, Earn And Shares")
  };
  status = insertSubCat(
    iname_hash,
    "Beginners",
    def_agoras
  );

  // insert sub-category
  def_agoras =
  {
    InitAgora("Before Start"),
    InitAgora("Pre Proposals and Discussions"),
    InitAgora("Security"),
    InitAgora("Core develop"),
    InitAgora("Interface Develop (Angular)"),
    InitAgora("Mobile Wallet"),
    InitAgora("3rd Party development")
  };
  status = insertSubCat(
    iname_hash,
    "Development",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }

  def_agoras =
  {
    InitAgora("Machine Buffer watcher")
  };
  status = insertSubCat(
    iname_hash,
    "3rd Party development",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }



  def_agoras =
  {
    InitAgora("Inflationary or Deflationary Money"),
    InitAgora("How to determine coins value")
  };
  status = insertSubCat(
    iname_hash,
    "Economy",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }



  def_agoras =
  {
    InitAgora("Comunication & Encryption Enhancement"),
    InitAgora("Plugins"),
    InitAgora("Wallet API"),
    InitAgora("Messenger improvement")
  };
  status = insertSubCat(
    iname_hash,
    "Core develop",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  def_agoras =
  {
    InitAgora("Plugin handler improvements"),
    InitAgora("Distributed Version Control Plugin implementation"),
    InitAgora("Distributed Wiki Plugin implementation"),
  };
  status = insertSubCat(
    iname_hash,
    "Plugins",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  def_agoras =
  {
    InitAgora("Design & Theme Improvement"),
    InitAgora("Multilingual"),
    InitAgora("Wallet Enhancement"),
  };
  status = insertSubCat(
    iname_hash,
    "Interface Develop (Angular)",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }



  def_agoras =
  {
    InitAgora("Logos, either imagine or PAI")
  };
  status = insertSubCat(
    iname_hash,
    "Design & Theme Improvement",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  def_agoras =
  {
    InitAgora("iPGP improvement")
  };
  status = insertSubCat(
    iname_hash,
    "Comunication & Encryption Enhancement",
    def_agoras
  );
  if (!status)
  {
    CLog::log("insertSubCat failed", "app", "error");
    return false;
  }


  // insert initial content of Agoras
  return initPosts();
}


bool AgoraInitializer::insertSubCat(
    const QString &iname_hash,
    const QString &parent_name,
    std::vector<InitAgora> &def_agoras
    )
{

  const QString creation_date = CUtils::getLaunchDate();

  QueryRes parent_info = DemosHandler::searchInAgoras(
    ClausesT
    {
      ModelClause("ag_iname_hash", iname_hash),
      ModelClause("ag_title", parent_name)
    }
  );

  QString parent_uniq_hash = parent_info.records[0].value("ag_unique_hash").toString();
  for (InitAgora an_agora : def_agoras)
  {
    QString ag_hash = CCrypto::convertTitleToHash(an_agora.m_title);
    QString ag_doc_hash = ag_hash;   // a dummy doc-hash for initializing
    QString unique_hash = CCrypto::keccak256(iname_hash + "/" + ag_hash);

    bool status = DemosHandler::insertAgoraInDB(Agora(QVDicT
    {
      {"title", an_agora.m_title},
      {"doc_hash", ag_doc_hash},
      {"agora_hash", ag_hash},
      {"iname_hash", iname_hash},
      {"unique_hash", unique_hash},
      {"parent", parent_uniq_hash},
      {"description", an_agora.m_description},
      {"tags", an_agora.m_tags},
      {"creation_date", creation_date},
      {"last_modified", creation_date},
      {"creator", CConsts::HU_INAME_OWNER_ADDRESS}
    }));
    if (!status)
    {
      CLog::log("insertSubCat faild" + parent_name + "-" + an_agora.m_title, "app", "fatal");
      return false;
    }
    map_title_to_ag_unique_hash.insert(
      parent_name + "-" + an_agora.m_title, unique_hash);
    CLog::log(">>>>>>>>>>>>>>>>>>>>> map_title_to_ag_unique_hash " +unique_hash + " <- " + an_agora.m_title);
  }
  return true;
}

bool AgoraInitializer::initPosts()
{
  QString creation_date = CUtils::getLaunchDate();
  // read all default post contents and insert in proper Agora
  QStringList topics = dbInitAgorasContent.keys();
  topics.sort();
  CLog::log(">>>>>>>>>>>>>>>>>>>>> map_title_to_ag_unique_hash " + CUtils::dumpIt(map_title_to_ag_unique_hash));
  for (QString a_topic : topics) {
    QString ap_opinion = dbInitAgorasContent.value(a_topic);
    QVDicT params
    {
      {"ap_ag_unique_hash", map_title_to_ag_unique_hash.value(a_topic)}, // map_title_to_ag_unique_hash.value(a_topic)},
      {"ap_doc_hash", CCrypto::keccak256(ap_opinion)},
      {"ap_opinion", ap_opinion},
      {"ap_reply", ""},
      {"ap_reply_point", 0},
      {"ap_creation_date", creation_date},
      {"ap_creator", CConsts::HU_INAME_OWNER_ADDRESS}
    };
    DemosHandler::insertDemosPost(params);
  }

  return true;
}
