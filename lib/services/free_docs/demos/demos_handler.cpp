#include "stable.h"
#include "lib/ccrypto.h"
#include "demos_handler.h"

using std::string;


bool Agora::setByDict(const QVDicT &values)
{
  if (values.value("title", "").toString() != "")
    m_title = values.value("title", "").toString();
  if (values.value("doc_hash", "").toString() != "")
    m_doc_hash = values.value("doc_hash", "").toString();
  if (values.value("agora_hash", "").toString() != "")
    m_agora_hash = values.value("agora_hash", "").toString();
  if (values.value("iname_hash", "").toString() != "")
    m_iname_hash = values.value("iname_hash", "").toString();
  if (values.value("unique_hash", "").toString() != "")
    m_unique_hash = values.value("unique_hash", "").toString();
  if (values.value("parent", "").toString() != "")
    m_parent = values.value("parent", "").toString();
  if (values.value("description", "").toString() != "")
    m_description = values.value("description", "").toString();
  if (values.value("tags", "").toString() != "")
    m_tags = values.value("tags", "").toString();
  if (values.value("creation_date", "").toString() != "")
    m_creation_date = values.value("creation_date", CUtils::getNow()).toString();
  if (values.value("last_modified", "").toString() != "")
    m_last_modified = values.value("last_modified", CUtils::getNow()).toString();
  if (values.value("creator", "").toString() != "")
    m_creator = values.value("creator", "").toString();
  if (values.value("language", "").toString() != "")
    m_language = values.value("language", CConsts::DEFAULT_LANG).toString();
  if (values.value("content_format_version", "").toString() != "")
    m_content_format_versaion = values.value("content_format_version", CConsts::DEFAULT_CONTENT_VERSION).toString();
  return true;

}


DemosHandler::DemosHandler()
{

}

const QString DemosHandler::stbl_demos_agoras = "c_demos_agoras";
const QStringList DemosHandler::stbl_demos_agoras_fields = {"ag_id", "ag_title", "ag_unique_hash", "ag_parent", "ag_iname_hash", "ag_hash", "ag_doc_hash", "ag_language", "ag_description", "ag_content_format_version", "ag_tags", "ag_creation_date", "ag_last_modified", "ag_creator", "ag_controlled_by_machine", "ag_mp_code"};
const QString DemosHandler::stbl_demos_posts = "c_demos_posts";

bool DemosHandler::insertAgoraInDB(const Agora &ago)
{

  if (ago.m_title == "")
  {
    CLog::log("m_title is empty", "app", "error");
    return false;
  }

  if (ago.m_creator == "")
  {
    CLog::log("ag_creator is empty", "app", "error");
    return false;
  }

  QString unique_hash = ago.m_unique_hash;
  if (unique_hash == "") {
    unique_hash = CCrypto::keccak256(ago.m_iname_hash + "/"  + ago.m_agora_hash);
  }

  QueryRes exist = DbModel::select(
    DemosHandler::stbl_demos_agoras,
    QStringList {"ag_unique_hash"},     // fields
    {ModelClause("ag_unique_hash", unique_hash)}
  );
  if (exist.records.size() > 0)
    return true;

  QString controlled_by_machine = CConsts::NO;
  QString mpCode = "";
  auto[status, wallet_controlled_addresses] = WalletAddressHandler::searchWalletAdress(
    QStringList(ago.m_creator),
    CMachine::getSelectedMProfile());
  Q_UNUSED(status);

  if (wallet_controlled_addresses.size() > 0) {
      controlled_by_machine = CConsts::YES;
      mpCode = wallet_controlled_addresses[0].value("wa_mp_code").toString();
  }

  QVDicT values {
    {"ag_title", ago.m_title},
    {"ag_doc_hash", ago.m_doc_hash},
    {"ag_hash", ago.m_agora_hash},
    {"ag_iname_hash", ago.m_iname_hash}, // the related domain
    {"ag_unique_hash", unique_hash},
    {"ag_parent", ago.m_parent},
    {"ag_language", ago.m_language},
    {"ag_description", ago.m_description},
    {"ag_content_format_version", ago.m_content_format_versaion},
    {"ag_tags", ago.m_tags},
    {"ag_creation_date", ago.m_creation_date},
    {"ag_last_modified", ago.m_last_modified},
    {"ag_creator", ago.m_creator},
    {"ag_controlled_by_machine", controlled_by_machine},
    {"ag_mp_code", mpCode},
  };
  DbModel::insert(
    DemosHandler::stbl_demos_agoras,
    values
  );

  return true;
}

QueryRes DemosHandler::searchInAgoras(
    const ClausesT &clauses,
    const QStringList &fields
    )
{
    QueryRes res = DbModel::select(
      DemosHandler::stbl_demos_agoras,
      fields,
      clauses);
     return res;
}

GenRes DemosHandler::insertDemosPost(QVDicT &params) {
   QString msg;

   if (params.value("ap_ag_unique_hash") == "") {
     msg = "empty ap_ag_unique_hash";
     CLog::log(msg, "app", "error");
     return { false, msg };
   }
   if (params.value("ap_creator") == "") {
     msg = "empty ap_creator";
     CLog::log(msg, "app", "error");
     return { false, msg };
   }

   QueryRes exist = DbModel::select(
      DemosHandler::stbl_demos_posts,
      {"ap_doc_hash"},
      {ModelClause("ap_doc_hash", params.value("ap_doc_hash"))}
   );
   if (exist.records.size() > 0)
       return { true };

  QVDicT values {
    {"ap_ag_unique_hash", params.value("ap_ag_unique_hash")},
    {"ap_doc_hash", params.value("ap_doc_hash")},
    {"ap_opinion", params.value("ap_opinion")},
    {"ap_attrs", params.value("ap_attrs", "")},
    {"ap_format_version", params.value("ap_format_version", "0.0.0")},
    {"ap_reply", params.value("ap_reply", "")},
    {"ap_reply_point", params.value("ap_reply_point", 0)},
    {"ap_creation_date", params.value("ap_reply_point", CUtils::getNow())},
    {"ap_creator", params.value("ap_creator")},
  };
  DbModel::insert(
    DemosHandler::stbl_demos_posts,
    values
  );

  return { true };
 }
