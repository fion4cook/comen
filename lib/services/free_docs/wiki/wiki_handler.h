#ifndef WIKIHANDLER_H
#define WIKIHANDLER_H



#include "init_contents/wikis_init_contents.h"
#include "lib/wallet/wallet_address_handler.h"

class Wiki
{
public:
  QString m_title = "";
  QString m_iname_hash = "";
  QString m_title_hash = "";
  QString m_doc_hash = "";
  QString m_unique_hash = "";
  QString m_language = CConsts::DEFAULT_LANG;
  QString m_content_format_versaion = CConsts::DEFAULT_CONTENT_VERSION;
  QString m_creation_date = "";
  QString m_last_modified = "";
  QString m_creator = "";
  QString m_content = "";

  Wiki(const QVDicT &values){
    if (values.keys().size() > 0)
        setByDict(values);
  }
  bool setByDict(const QVDicT &values = {});
};

class WikiHandler
{
public:
static QString stbl_wiki_pages;
static QString stbl_wiki_contents;

  WikiHandler();
  static bool initTmpWikiContent();
  static bool insertPage(const Wiki &wiki);

};

#endif // WIKIHANDLER_H
