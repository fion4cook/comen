#include "stable.h"
#include "lib/ccrypto.h"
#include "wiki_handler.h"


bool Wiki::setByDict(const QVDicT &values)
{
  if (values.value("title", "").toString() != "")
    m_title = values.value("title", "").toString();
  if (values.value("doc_hash", "").toString() != "")
    m_doc_hash = values.value("doc_hash", "").toString();
  if (values.value("iname_hash", "").toString() != "")
    m_iname_hash = values.value("iname_hash", "").toString();
  if (values.value("title_hash", "").toString() != "")
    m_title_hash = values.value("title_hash", "").toString();
  if (values.value("content", "").toString() != "")
    m_content = values.value("content", "").toString();
  if (values.value("unique_hash", "").toString() != "")
    m_unique_hash = values.value("unique_hash", "").toString();
  if (values.value("creation_date", "").toString() != "")
    m_creation_date = values.value("creation_date", CUtils::getNow()).toString();
  if (values.value("last_modified", "").toString() != "")
    m_last_modified = values.value("last_modified", CUtils::getNow()).toString();
  if (values.value("creator", "").toString() != "")
    m_creator = values.value("creator", "").toString();
  if (values.value("language", "").toString() != "")
    m_language = values.value("language", CConsts::DEFAULT_LANG).toString();
  if (values.value("content_format_version", "").toString() != "")
    m_content_format_versaion = values.value("content_format_version", CConsts::DEFAULT_CONTENT_VERSION).toString();
  return true;

}


// -  -  -  -  -  -  -  -  WikiHandler

QString WikiHandler::stbl_wiki_pages = "c_wiki_pages";
QString WikiHandler::stbl_wiki_contents = "c_wiki_contents";

WikiHandler::WikiHandler()
{

}

bool WikiHandler::initTmpWikiContent()
{
  QString creation_date = CUtils::getLaunchDate();
  QString inHash = CCrypto::convertTitleToHash("imagine");
  for (QString aPageTitle: InitWikiContent::s_wiki_initial_pages.keys())
  {

    QString wkHash = CCrypto::convertTitleToHash(aPageTitle);
    QString wkUniqueHash = CCrypto::keccak256(inHash + "/" + wkHash);
    insertPage(Wiki(QVDicT
    {
      {"title", aPageTitle},
      {"iname_hash", inHash},
      {"title_hash", wkHash},
      {"doc_hash", wkHash},
      {"unique_hash", wkUniqueHash},
      {"language", CConsts::DEFAULT_LANG},
      {"content_format_version", CConsts::DEFAULT_CONTENT_VERSION},
      {"creation_date", creation_date},
      {"last_modified", creation_date},
      {"creator", CConsts::HU_INAME_OWNER_ADDRESS},
      {"content", InitWikiContent::s_wiki_initial_pages[aPageTitle]}
    }));
  }

  // delegate permission to ALL to post on imagine domain
  // TODO:
  return true;
}

bool WikiHandler::insertPage(const Wiki &wiki)
{
   if (
       (wiki.m_title == "") ||
       (wiki.m_iname_hash == "") ||
       (wiki.m_doc_hash == "") ||
       (wiki.m_creator == "") ||
       (wiki.m_content == "")
   )
   {
     CLog::log("invalid wiki page info: " + wiki.m_title, "app", "fatal");
     return false;
   }

   QString wkp_title = CUtils::sanitizingContent(wiki.m_title);
   QString wkc_content = CUtils::sanitizingContent(wiki.m_content);

   QString title_hash = wkp_title;
   if (title_hash == "") {
     title_hash = CCrypto::convertTitleToHash(title_hash);
   }
   QString unique_hash = wiki.m_unique_hash;
   if (unique_hash == "") {
       unique_hash = CCrypto::keccak256(wiki.m_iname_hash + "/" + title_hash);
   }

   // check if exist newest version
  QueryRes exist = DbModel::select(
    stbl_wiki_pages,
    {"wkp_unique_hash"},
    {
      ModelClause("wkp_unique_hash", unique_hash),
      ModelClause("wkp_last_modified", wiki.m_last_modified, "'>=")
    }
  );
  if (exist.records.size() > 0)
  {
    CLog::log("already a newer version of wikipage(" + CUtils::hash16c(unique_hash) + ") exist in db");
    return true;
  }

  // check if same doc alredy recorded
  exist = DbModel::select(
    stbl_wiki_pages,
    {"wkp_unique_hash"},
    {
      ModelClause("wkp_doc_hash", wiki.m_doc_hash)
    }
  );
  if (exist.records.size() > 0)
  {
    CLog::log("wiki doc already recorded(" + CUtils::hash16c(wiki.m_doc_hash) + ") exist in db");
    return true;
  }

  QVDicT values
  {
    {"wkp_title", wiki.m_title},
    {"wkp_in_hash", wiki.m_iname_hash},
    {"wkp_title_hash", title_hash},
    {"wkp_doc_hash", wiki.m_doc_hash},
    {"wkp_unique_hash", unique_hash},
    {"wkp_language", wiki.m_language},
    {"wkp_format_version", wiki.m_content_format_versaion},
    {"wkp_creation_date", wiki.m_creation_date},
    {"wkp_last_modified", wiki.m_last_modified},
    {"wkp_creator", wiki.m_creator},
  };
  CLog::log("inserting a wiki page:" + CUtils::dumpIt(values));
  DbModel::insert(
    stbl_wiki_pages,
    values
  );

  // insert contents too
  values = {
    {"wkc_unique_hash", unique_hash},
    {"wkc_content", wkc_content}
  };
  CLog::log("inserting a wiki page content: " + CUtils::dumpIt(values));
  DbModel::insert(
    stbl_wiki_contents,
    values
  );
  return true;
}


