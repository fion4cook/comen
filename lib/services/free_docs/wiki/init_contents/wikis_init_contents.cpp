#include "wikis_init_contents.h"

QHash<QString, QString> InitWikiContent::s_wiki_initial_pages = {

  {
    "contributors",
    R"(
     in imagine, the contributor is who helped the system. either technical or non technical.
     who interested in imagine can propose her/his help and run a polling and the comunity decide to accept the proposal or not.
     if contributor's proposal be accepted the contributor became a share holder too and by this the contrinbutor have the voting rights for next 7 years
     and also get divivedent every 12 hours (one in 00:00:00 AM and the other in 12:00:00 PM) for next 7 years.
     contributor's share amount is calculated based a simlple stright forward formula.
     h = how many hours the contributed spend(or will spend) for that activity?
     l = what was the usefullness level of that activity?
     new shares = h * l
     and this share will be add to imagine's DNA for ever. means this user helped world this certain amount of shares.
     the help level can be between 1-12 and for now we assum all help in level 6.
     this document will be cmpleted soon. but for now the below graph will help you much.
     <br>
     [[File:accumulative-shares-of-alice.png|Accumulative shares of alice]]
     <br>
     [[File:accumulative-shares-of-contributors.png|Accumulative shares of all contributors]]
     <br>
    )"
  },


  {
    "development",
    R"(
    <div>
        there is a [[imagine/wiki/todos|big room]] for developers in evry aspect to join the project.
        and will be exponentially increase where your creativity add to on top of what already exist.
        just check the [[imagine/wiki/todos|TODOs list]] regularly.

        if you know Nodejs, expressjs, angular, most probably you can find something to do. and most important is you can
        help some non-technical friends and install imagine on their computer.
        YES, we need to help together. the friend can be an active contributor, if you dedicate a couple of hours to
        her/him.
        in addition if you already contributed and coded in some of these projects wikipedia, SMF or other foruom software,
        Jira, Git, theme & template designing then definitely you will participate in one or more projects of imagine.
        and if you are expert in blockchain you do even further. maybe start and preparing some documention and educational
        stuff, and later improving transaction & contracts, and adding planed features.
        or even beter, having brilliant ideas. let's implement it. you can implement new plugins. and if it enough good, can
        be added to core-code.
        and if your experties domain is mobile-app so start to make a nice powerfull wallet.
        probably the software architecture and database design need heavily refactor, definitely we will do it and always
        considering backward-compatibility.
        in such a way no one lose her/his coins and no (intentionally)fork will happend.
        but if someone wants to fork, it is ok too! althougt we suggest to add what she/he desired to existed imagine, or if
        you insist on fork, atleast wait until have a great robust software to relay on.
        if you are cryptographer or security expert, absolutely can help imagine. just take a look at code and make an
        improvment proposal and will be considered in high priority.
        depend on the case the proposal can be public or private. obviousley critical bugs or security issues MUST be
        reported privately to iName [[/messenger|"security"]].
        if you are not sure, just write a short message in [[/demos/development/security|related Agora]] and we will keep in touch.
    </div>
    <div>
        plugins: the most common way to develope project without touching core-code!
        if you know wordpress, must probably you can write some awesome plugins for imagine too.
        and lack of document is the big problem.
        there are some good hooks to start e.g. SASH_before_validate_normal_block, APSH_control_if_missed_block ...
        and more in code (imagine-server/plugin-handler/hooks-list.js)
        and obviously tons of feature request hooks will be add to imagine-core.
        feel free to request your needs in [[imagine/demos/Plugins|plugins general discussions]]
    </div>
    )"
  },


  {
    "faq",
    R"(
    <div>
        - I have not any PAIs, how can I get some?
        you can do something for imagine improvment and by that contribute become a share holder. every shareholder get paid every 12 hours based on hours of contribute.

    </div>

    <div>
        - how can I contribute?
        There is a long list of [[imagine/wiki/todos| TO DO]]s. take a look and find if there is something you can do?
        hopefully you will find somethig interset to do! after that you need to prepare a proposal and send it to network, in order to run a polling and all shareholders of
        imagine(including you) decide about your proposal. the proposal form is in address [[contributes, Contributes]], for more details read [[imagine/wiki/prepare%20a%20proposal|How to Preapare a proposal?]]


        I am not developer:
        crypto fans, researchers, economists, influencers, ... the key word is EDUCATION. crypto space needs education rather than speculation. most probably you know why you chose cryptocurency instead of fiat money and what are the pros and cons.
        please spread these to more person knowing about what are cryptocurency(including imagine) promises. write some good document, educational stauff, multimedia, etc and put it in your personal page(in web or imagine iNames), share it (in social media or imagine's social network, Demos, personal pages) and make a proposal about what you did. the comunity will approw your proposal.
        even beter, write your creative proposals and start to making world a better place.
        for the first time in human history we have a transparent public open ledger in which records every piece of humanity fait to help the world in DNA of bliss.
        this aspiring all to be a better person.
    </div>
    )"
  },


  {
    "home",
    R"(
    <div>
        imagine follows the [https://mediawiki.org MediaWiki] formatting.
        surly it must be implementd completely! [[imagine/wiki/TODOs|here]] is a list of what we can do.


        [[142c3bc70608df26ace66e287966b4953a96b427facbd5dc8d88bd8ac83a3310.jpg|nauture]]


    </div>

    UPDATE i_wiki_contents SET wkc_content ='<div>
        imagine follows the [https://mediawiki.org MediaWiki] formatting.
        surly it must be implementd completely! [[imagine/wiki/TODOs|here]] is a list of what we can do.


        [[File:142c3bc70608df26ace66e287966b4953a96b427facbd5dc8d88bd8ac83a3310.jpg|nauture]]

        [[File:60f2d0d7c59f5828947ce2b905f6ae90fc7e71795a451552d5f6180637338bdc.jpg|future]]


    </div>' WHERE wkc_unique_hash='20128937d67143a76812dfbfe2d9cbb137dd04895d488fa9e8572d252ef16c3d';

    )"
  },


  {
    "CIPs",
    R"(
    <div>
        Comen's Improvment Proposals, the idea comes from PIBs, same concept same policy.
    </div>
    )"
  },


  {
    "imagine",
    R"(
    <div>
        John Lennon
        Imagine there's no heaven
        It's easy if you try
        No hell below us
        Above us, only sky
        Imagine all the people living for today
        Ah


        Imagine there's no countries
        It isn't hard to do
        Nothing to kill or die for
        And no religion too
        Imagine all the people living life in peace


        You may say I'm a dreamer
        But I'm not the only one
        I hope some day you'll join us
        And the world will be as one

        Imagine no possessions
        I wonder if you can
        No need for greed or hunger
        A brotherhood of man
        Imagine all the people sharing all the world


        You may say I'm a dreamer
        But I'm not the only one
        I hope some day you'll join us
        And the world will live as one
    </div>
    )"
  },


  {
    "Comen network",
    R"(
    imagine's network in early days consists of one type element which are nodes. the laptops or personal computers whith a normal CPU power and HD capacity
    in mid time we will develope the mobile wallets and for long term we will have backing pools and also massive data backers.
    for know there are only light nodes which have all blockgraph's history.



    [[File:imagine-network.png|imagine network]]

    mobile wallets will be able to chose to connect whatever nodes she want. the mobile wallet can connect to more than one node in same time.
    the wallet find nodes by their [[imagine/wiki/inames|iName]]s
    eache node has different power, connectivity,... thus different transaction fee.
    mobile wallet can send same transaction to multiple nodes (in order to spread as fast as possible), obviousley paying multiple transactions fee too.

    )"
  },


  {
    "mail FAQ",
    R"(
    if you are here means you couldn't configure your email setting well or probably your email service provider
    (for example gmail) does not support your moachine connection.
    it could be also because of not fully implementation of imagine mailing sub system.
    hope fully we upgrade the code in order to have more reliability in system functionality.
    your machine may have problem in sending email or in receiving or both!
    you can do it manually. and since in early day of launching project there are few emails it will not a frustrating job!

    1. if your machine can not send email you can find emails to be sended in path imagine-server/temp-mailbox/outbox/
    the file name indicates the receiver. you just need to open the file, copy the content and past it in email and send to proper receiver.

    2. if your machine can not receive email, you need to copy the email body in a text file in imagine-server/temp-mailbox/inbox/. and the name of file isn't important.

    )"
  },


  {
    "mobile wallet",
    R"(
    <div>

        <div>mobile-wallet</div>


        <div>
            no inputs can not be spendable without manually confirmation by wallet owner, to defend against dust-attack or any type of privaicy & security issues.
        </div>

        <div>
            privacy & security is a MUST and DEFAULT feature in imagine Wallets
        </div>

        <div>
            the imagine mobile-wallet must have these features:
            - NATIVE coin-join(wasabi/samurai like) or even Knapsack(unequal inputs/outputs)
            - full support of imagine Demos features (e.g. rooms, pollings...)
            - Onion supported by default
            - because of the native multiSig and input-time-lock support in imagine addresses, user can easily add a time-locked address in every addresses which controls by wallet.
            and saves this time-locked key in a not so secure place(e.g. mailbox). since this special key can sign the funds but the network didn't accept thiese trnasactions until a certain time passed.
            so compromise this address has not dangerous.
            by the way in case of user losses his key, he always can use this time-locked-key to restoration funds(of course after expiring time-lock)
        </div>

        <div>
            bitcoin hd wallet improvment:

            - schnore signature to cover new features

            - instead sending some outputs to one input address, send each output to a seperate input:
            imagine wants to send 9 to another, currently it could be like

            inputs    output  change
                2-------|--4      1
                3-------|  3

            it will convert to

            inputs    output  change
                2---------2       1
                3---------3

            more details on BIP32 Reclaiming Financial Privacy With HD Wallets
            http://bitcoinism.blogspot.com/2013/07/reclaiming-financial-privacy-with-hd.html

            - coinjoin (wasabi like)
                https://wasabiwallet.io/
                https://bitcointalk.org/index.php?topic=279249.0
                https://bitcoin.stackexchange.com/questions/1823/what-is-the-maximum-size-of-a-transaction/35882#35882
                https://bitcoin.stackexchange.com/questions/57073/what-is-the-maximum-anonimity-set-of-a-coinjoin-transaction/57091
                https://github.com/nopara73/ZeroLink


        </div>

    </div>
    )"
  },


  {
    "page formatting",
    R"(
    imagine's wiki follows the exact rules of [[https://www.mediawiki.org/wiki/Help:Formatting|media wiki]].
    currently it has  implemente only [ [ ] ] for links and files.
    and very soon we (contributors) will complete it.
    if you know Javascript you can [[contributors|join to contribute]].

    )"
  },


  {
    "pledging and unpledging",
    R"(
    since in imagine every contributor has a (almost)regulare income for next 7 years, it could be a good fund to pledge it
    in order to get loan
    or buying some services and paying gradualy and so on.
    currently imagine supports peldeP which is use ful when you ask a loan to pay proposal's cost.

    the orther pledges will come in soon are:

    <div>
        <b>PledgeP</b>
        <br>Most common type of pledge in early launching imagine days, in which the new contributors do not have any PAI to
        apply a proposal. so they have to loan some PAIs, they can ask a loan and in return pledge the account in order to
        payback the loan. for detail about proposal & contribute check also [[wiki/imagine/faq, faq]]
        <br>disregarding proposal approved or not, the loan repayments will be cut from pledged account.

        <br>there are 3 ways to un-pledge an account
        <br>1. ByPledgee: The pledgee redeems account (pledgee Closes Pledge). it is most common way supposed to close
        pledge and redeem account.
        <br> this is most efficient way to avoid entire network engaging unnecessary calculation.
        <br>2. ByArbiter: in redeem contract can be add a BECH32 address as a person(or a group of persons) as arbiter. the
        contract can be closed by arbiter signatures too.
        <br> the arbiters inetercept in 2 cases. in case of denial of closing contract by pledgee or for the sake of useless
        process.
        <br> this method is also cost-less(in sence of network process load) and is a GOOD SAMPLE of trusting and not
        over-processing by entirenetwork
        <br> in other word, by giving rights to someone to be arbiter, and givin small wage, and involving them in a
        contract,
        <br> the arbiters MUST run a full node (and maybe a full-stack-virtual-machine to evaluate a contract) and the rest
        of networks doing nothing about this particulare contract
        <br> in this implementation even an infinit loop problem doesnot collapse entire network, and more over the
        contracts can be run as an OPTIONAL-PLUGIN on SOME machines
        <br> and for the rest of network, what is important is the final signature of arbiters, adn they only validate
        signatures in order to confirm the PAIs transformation/contract-closing...
        <br>3. ByPledger: in case of pledgee refuse to redeem account, pledger requests network to validate redeem clauses by
        calculate repayments and...
        <br> and finally vote to redeem (or not redeem) the account. this proces has cost, and must be paid by pledger.
        <br> althout this has cost for pledger, also has negative effect on pledgee(and potentially arbiters) reputation(if network redeem the
        account).
        <br>4. ByNetwork, every time the coinbase dividend toke place, every machines calculate automatically. this method
        is most costly till implementing an efficient way
        <br>either pledgee or arbiter can close contract before head and remit repayments
        <br>Note: there will be a reputation sub-system which calculate the risk of different pledgee and arbiters(which can be a group of persons)
        and at the end the pledger can manage the risk & pros & cons of each contract and also ceil & floor of the contract value.
        PledgeP Relations with other documents
        [[File:pledgep-relations.png|DNAProposal, Loan request & PledgeP Relations]]
    </div>

    <div>
        <b>PledgeSA</b>
        <br>self authorized pledge
        <br>in this class the pledger signs a contract to cut regularely(every 12 hours) from mentioned address(usually the
        shareholder
        address by which received incomes from share dvidend).
        <br>the diference is in this class all 3 parties(pledger, pledgee, arbiter) can close contract whenever they want.
        <br>this service is usefull for all type of push-money, subscription services e.g. monthly payments for T.V.
        streaming or even
        better DAILY payment for using services
        <br>by activating field "applyableAfter" the contract can be realy closed after certain minute of concluding it.
        <br>e.g. customer must use at least 3 month of service(in order to get discount), so pledger signs it with an extra
        tag which present the minutes of closing offset, and in time of closing contract the real close happened after
        certain minute which are mentioned befor.
        or can be calceled immediately if passed this certain time from signing time.
        <br>TODO: implement it since it costs couple of hours to implementing

    </div>

    <div>
        <b>PledgeG</b>
        <br>gneric pledge in which pledger can pledge an address(either has sufficent income from DNA or not) and get a loan
        <br>and by time payback the loan to same account, and pledgee takes from pledged account
        <br>of course there is no garanty to payback PAIs, excep reputation of pledger or her bussiness account which is
        pledged
        <br>TODO: implement it ASAP
    </div>

    <div>
        <b>PledgeL</b>
        <br>lending PAIs by pledging an account with SUFFICENT income
    </div>

    <div>

    </div>
    )"
  },


  {
    "pollings",
    R"(
    <div>
        there are too many different Voting systems in the world, non of them is designed for distributed systems!
        all are designed to perform in a certain and limited window-time(max 12 hours) and in certain places.
        the basci polling process in imagine is designed to run a distributed ellection and in long period.
        in order to reduce latenancy effect on the final result of polling basic profile works in 2 step.
        first positive, negative and abstain votes. and second phasde accepts only abstain and negative votes.
        I complete this later...
        [[File:basic-polling-phases.png| basic polling phases]]
    </div>
    )"
  },


  {
    "prepare a proposal",
    R"(
    the proposals are documents in which a contributor states what she/he can do for improvement of the imagine.
    or what she already did.
    and simply writes how many hours toke time that job and how important/usefull was that job?
    and at the end sets a time-window for polling about proposal. after finish the polling time,
    if the majority of imagine comunity approve the proposal,
    the proposer become a share holder or if the address already has some shares, the shares will be increased.
    the share value is equal to hours of contribute * usefulness of contribute.



    <div>

        There is a long list of [[imagine/wiki/todos| TO DO]]s. take a look and find if there is something you can do?
        hopefully you will find somethig interset to do!
        after that you need to prepare a proposal and send it to network, in order to run a polling and all shareholders of
        imagine(including you) decide about your proposal.
        the proposal form is in address [[contributes| Contributes]], and you first must complete form 1 (gray one)
        <br>
        [[File:create-a-draft-proposal.png|1. Create a Draft Proposal]].
        <br>
        and click on button "Prepare proposal". by clicking, the proposal will be add to your draft proposals.
        there is list of all your Draft proposals 2.(Blue one)
        <br>
        [[File:my-draft-proposals.png|2.my draft proposals]].
        <br>

        to send a proposal you need to spend some PAIs(in order to reduce spaming, this is a little high value).
        obviousely you have not this amount before head, so can ask a loan.
        if you have some PAIs you can "sign and pay cost..." and pay by yourself, otherwise click on button
        "Prepare Loan Request" and the form 3 will be displayed(green one)
        <br>
        [[File:request-loan-to-apply-for-polling.png|3. Request loan & apply for Polling]].
        <br>


        here you can change repayments count(by changing repayment value)
        and put inside pledgee the address of who you wana ask loan. by default it is filed by Hu's Address.
        by changing the numbers the repayments will be changed and you can see all in long list (green/gray one)
        <br>
        [[File:laon-repayments-details.png|4. Loan Repayment Details]].
        <br>

        <br>
        in form 3, at bottom you can click on "Sign Request" and now you have the signed loan request on your machine.
        the draft list presents the signe loan request(s)
        <b>
            [[File:my-draft-proposals-signed.png|2.my draft proposals]].
        </b>

        <br> now you need to send it whom you want to ask loan, there are 2 solution.
        first is sending directly by email address via form "2.my draft proposals" the default is Hu's email address but you
        can
        change it to who you want getting loan. this is absolutely private email.
        in case of losting email possibility, there is a [[imagine/demos/Pre Proposals and Discussions|Proposals and
        Discussions Room(Agora)]] you
        can publicly send your proposal over there
        and everybody can read your proposal but only pledgee that you signed can complete the request and send it to
        network.
        to sending this proposal you still need some small amount of PAIS.
        and if you do not have it you can send it by POW. in imagine POW is just a way to send messages and you can not earn
        PAIs by POW.

        <br>
        sending your proposal to pledgee means, she will sign your proposal and pay the proposal costs and push it to
        network.
        after placing proposal onchaine, the polling period starts immediately, and lasts for what you requested in form 1 (minimum 24 hours + 12 hour for negative votes).
        deatils about [[imagine/wiki/pollings|how basic polling works in imagine?]]
        and at the end if your proposal approved by majority of imagine comunity you will be a shareholder(or your share
        will be increased).


    </div>
    )"
  },


  {
    "reserved coins",
    R"(
    <div>
        The reserved coins are the coins which are issued at the time of creation a coinbase, but they are not released thus
        they are not spendable.
        for each coinbase block there are another 4 reserve blocks whith the same amount of "minted" coins. mintes is quoted
        because in coinbase class there are also treasury coins which are collected in treasury becase of diverse incomes.
        and the treasury income is not cloning to reserve blocks.
        so there are 4 reserved coins block corresponding to each coinbase block (which creates every 12 hours).
        These “anti-deflation reserve” blocks can/will be released in case of share holders of that particular
        block, voted to release it and reserve-time-lock is exceeded. For all 4 blocks there are different time-lock.
        First block, is releasable after 1 year of minting related coinbase block, second after 3 years, third after 6 years and
        forth after 11 years.
        to release a reserved block must run a polling and after that the votes indicate release should happend or not!
        this note is incomplete and will be more complete soon. maybe you can help to finish it.


        Request for Release Reserved Coins (reqRelRes) Relations
        [[File:reqrelres-relations.png|The ReqRelRes, Pollin & payer transactions]]
    </div>
    )"
  },


  {
    "support languages",
    R"(
    <div>
        Currently imagine supports some alnguages. a list of supported languages placed in file
        "imagine-server/web-interface/settings/languages.js". if you want help you can add more languages as mentioned in
        [[https://iso639-3.sil.org/code_tables/639/data/all|iso639-3]]. and send it back with a [[contributes|Proposal]].
        in such a way you will be a contributor and the languages list will be more complete.

    </div>
    )"
  },


  {
    "todos",
    R"(
    trx

    <div>
        <div>
            There is a big list of TODOs, some of them are specialized for cryptographers and some of them can be done by a
            simple-computer user (e.g. translate). and there are a wide range in between you can fit in.
            please check the list, surly you will find(or can suggest) something to do to bring happiness to the world.
        </div>

        <div>
            must complete BEFORE RELEASE
            - Cloned Trx and P4P trx, duoble-spending TESTs
            - TO TEST: GQL, invoke for entire DAG (for newly joined node)
            - TOTEST: proposer pays for proposal
            - TOTEST: release resereved coins
            - TOTEST: bind PGP to iName
            - maybe, bind by default the default key to iname(when it registered)
            - TOTEST: send msg to iName
            - TOTEST: iName(registering, collision handling, sending content, even multimeida, and presenting in personal
            page)
            - TOTEST: iname collision solving
            - TOTEST: Messenger (specialy sending encrypt message to iName which toke place on blockchain, improve it to not
            recording msg in blockchain space but relayed by entire network until reaching the propoer machine)
            - TOTEST: Demos & Agora (forum)

            -* TOTEST: booting a machine and sync
            -* TOTEST: too many tests for double-spend. clone, p4p tests

            ------------------------
            remove am pm from cycle
            utf8Decode
            - by default a PGP key must be bind to an iName
            - improve POW block to be able send msgs from Bech to Bech address
            - possiblity to send to iName by and bech32 address, betterpossibility to send msg between bech32 addresses
            getDocumentMerkleProof({blockHash:, docHash:});
            verifyDocumentMerkleProof({blockHash:, docHash:});
            - set a limit of POW blocks per hour.

            - notify version updating
            - arbitrary voting, can be created easily, low cost, for everything. e.g. imagine Logo, development planing,
            political polling... (SASH_custom_validate_polling)
            - implemet pay to iName which takes place on blockchain.
            - make a Black Holes to shoot some inappropriate iNames(e.g. racism, violence...) to be inaccessible forever
        </div>

        <div>
            bind ip to iName
        </div>

        <div>
            bind receive cost to iName. the sender must pay to you in order to be delivered her mail to you.
        </div>

        <div>
            improve content rendering/formatting based on media wiki formattings
            full support of all media wiki conventions
        </div>

        <div>
            Implementing Real Distributed Wiki based on mediawiki implementation
            todo: adding sign feature to wiki pages. in such a way viewers can find the most reliable pages(in case of
            adversary over-writes the qualified version, since everybody permited to edit every public pages).
        </div>

        <div>
            improvinge Demo & Agoras, in order to have nice graphic, folding for long texts,
            and most important re-order and restructure opinions in different ways e.g. most replyed(either positive or
            negative),
            extracting discussion main thread by folloing both cronological and reply.
            alongside ther discussions a nice pie chart statistics and sumup the discussion.
            all possible by a kind of plugin on top of existed structure and data
        </div>

        <div>
            adding optional new salt(or alter existed salt)
            in order to account owner add her name to address, and in such a way
            sender(by knowing the recepient name/nick nale/alias name... and hashing it to check)
            can be sure the address isn't type or transfer incorectly
        </div>

        <div>
            forum demos agora enhancing
            https://blog.discourse.org/2013/03/the-universal-rules-of-civilized-discourse/
        </div>
        <div>
            implement a bind-bitcoin-address to FleNS contract.
            it can accept also an HD-key to generate new pub-keys beside the wallet owner(which is iName owner too)
        </div>

        <div>
            adding discussion tab to wiki, in order to have an Agora(forum | chatroom | comment place... which are all same)
            for each page|topic in a certain language
        </div>

        <div>
            add pinedPost posibility to Agoras, to change first posts to better ones
        </div>

        <div>
            improve pay to iName in order to be interactive and
            payer ask from payee address to pay
            payee generates some fresh address(by HD Wallet) and sends to payer. The payer transfers PAIs to payee
            all process and comunications done in iPGP and encrypted and not visible for others and in background
        </div>

        <div>
            improve pay-to-iName to accept bitcoin payment(and maybe some other real usefull alt-coins).
            imagine node accepts a bitcoin payment to an iName, and returns the proper bitcoin address of an iName(even
            generates a bunch of address based on HD-wallets)
            and return to client wallet a new transaction in which the receiver addresses are replaced by real bitcoin
            address of recipient.
        </div>

        <div>
            TOO many security issue to not allowing upload harmful files, during send block to network, validating received
            blocks, forwarding them and also presenting the file content in browsers
        </div>

        <div>
            improve iName in which the owner can set a customized profile
            e.g. picture, webite, social network...
            ALL profile pieces MUST be optional and manageable by owner, in order to decide to share which parts with whom
            and be able to hide it again.
        </div>

        <div>
            educational plan to teach more tschnicals
        </div>

        <div>
            Optimizing plugin-based architecture for both server(node) and client(Angular & maybe React)
            more accurate hooks in all Active/Passive Synchronous/Asynchronous different modes
        </div>

        <div>
            adding alternate media-layer. e.g. SOCKET & IP
        </div>

        <div>
            improve iName regisre & Bind, in order to by registering an iName, automatically create a default iPGP and bind
            it to iName be label Default.
        </div>

        <div>
            improving server security and efficiency in order to each node can be connected by mobile wallet freely
            and earn a litle PAI by provide service to mobile wallets.
            each mobile wallet has to have a couple of different fullnode-sites url and optionally casn send transactions to
            some of them.
        </div>

        <div>
            Generalize imagine-DNA in order to make it available for every one who wants start an activity in distributed
            mode and give shares and dividend of that to contributors just like imagine-DNA.
        </div>

        <div>
            although multi-sig is native feature of imagine,
            BTW in some cases payee wants to be sure the payment MUST sign with a certain key(kSet).
            e.g if the payee accpet a pledged account as a payer of a service regulare subscribtion, and want to to
            subscriber not be able to cancel service in advance
            and he must use service for a shile(for example at least 6 month abounment).
            in this case payee accepts the only signature-pledged in which has an 6 month outputTimeLock.
            to solve this issue in this special type of SAPledge the transactioon includes also a certain uSet
        </div>

        <div>
            implementing customizable theme style css for entire site
        </div>

        <div>
            Too many security checks
            OWASP
        </div>

        <div>
            complete language lists, file: imagine-server/web-interface/settings/languages.js
        </div>

        <div>
            improve some method to save & backup private/public keys easily, even backup it onchain
            or encrypt all bunch of keys in a file and put on chain and divide the password between some trusted iNames(such
            as firends, partner, relatives, lawyer...)
            in case of lost key, user can re-genererate pwd from friends and decrypt the onchain file.
        </div>

        <div>
            Tons of tests (uniit, integrity & e2e)
        </div>

        <div>
            clients(full node or wallets) can comunicate eachother through iNames, specially they interactively can share
            paublic-keys in order to create multi-sig addresses.
            for example user1 and user2 send one public key each to user3, and she by adding her public-key, creates a 2 of
            3 signature address.
            and share it with 2 other. now all 3 have a same bech32 address in their wallet which the funds can be spend by
            2 signature of 3.
            obviously to signing the funds, this 3 users needs to pre-sign trx comunication and so forth. and finally
            sending to network.
            all these can be done simply and in background by iName infostructure. without users panic of all complexity.
        </div>

        <div>
            unittest, and efficiency for inputTimelock controls in BasicTx
        </div>

        <div>
            implementing outputTimelock control in BasicTx
        </div>

        <div>
            implementing pleger redeem account.(if pledgee refused doing so)
        </div>

        <div>
            improve neighbor-reputation plugin
        </div>

        <div>
            plugin for polling system, in order to watch a polling result instantly & online, and also promot to vote(accept
            or reject)
            <br>also tons of tests for polling
        </div>

        <div>
            implementing native block-explorer(DAG monitor) plugin
        </div>

        <div>
            implementing market prediction, Options, future contracts plugin
            since imagine is the only blockchain in which the backers have steady and predictable incomes for 7
            years(including reserved coins until 7+1+2+3+5=18 years),
            this income could be a very good security to many different type of reputation systems and Oracles.
            all aspect of smart contracts can be implemented easyly and bind to real-world with high degree of guaranty of
            good-treatment of actors in sistem.
            BTW, for more dynamic conditions it is possible to block imagine fund in an ethereum contract and use solidity
            to treat.
            in mid-term a bridge between imagine and ethreum to lock imagine incomes on imagine DAG and run smart-contracts
            on ethereum.
            in long-term, if it really needed impement our smart-contract platform(language, VM etc)
        </div>

        <div>
            implementing distributed issue tracking and project manager (Jira like) plugin for imagine eccosystem
        </div>

        <div>
            Later, improving IOT transactions to be more efficient than noraml transactions ( in sence of needed space &
            process)
        </div>

        <div>
            Bitcoin-like account Address & Transaction, in order to implement atomic swap between imagine & Bitcoin
        </div>

        <div>
            selecting an oen-source [[mobile-wallet|mobile-wallet]] project and customize it for imagine.
        </div>

        <div>
            since everything in imagine is designed to be modulare, so we can easily aggregate different behaviors in one
            eccosystem.
            in such a way the coins, can be used in BasicTx or can being transformed to different mode for example
            mimblewimble transactions or ZKP.
            in this case the same amount of PAIs are trading in mimble-mode and have the mimble charachteristic and
            everytime the owner of coins can transform it
            to Basic form or ZKP or ...
        </div>

        <div>
            boosting nodemailer to support more email client/servers
        </div>

        <div>
            adding new transfer medium as an optional plugin. in this case the plugin can be activated in order to broadcast
            the blocks(and GQL packets) in a efficient and alternate transformers
            such as direct IP/socket connections, or onion routing, maybe Dandelio...
            the plugin do not need to touch core code. it is enough to listen to hooks (SPSH_push_packet) to broadcast
            and in case of receiving new packet, calling hooks (SPSH_packet_ingress) will bring the packet to imagine core
            to parse & process it
        </div>

        <div>
            improve graphQL
        </div>

        <div>
            study for implementing other polling systems. either the way of voting or counting & presenting the result
        </div>

        <div>
            improve this version-patching system to be a real distributed versioning system with some handsfull commands
            (like commit, push...)
        </div>

        <div>
            fisibility study on RGB(colored coin) new generation implementing in imagine
            specialy offline transactions
        </div>

        <div>
            to support:
            The use of TLS in Censorship Circumvention
            uTLS as a wrapper for all censorship circumvention tools
        </div>

        <div>
            an app to automatically convert some old but gold mailing-lists (e.g. cryptography, cypherpunks, activists...)
            to new our Demos/Agoras in a modern form and presentation, in order to use the veteran's experiences and discussions.
            and also translate ALL to ALL languages.
        </div>

        <div>
            study on real-world needed contracts to implementing in imagine-core.
        </div>

        <div>
            develope PledgeG & PledgeL contracts as like as implemented PledgeP
            and later on developing PledgePZKP in respect of privacy
        </div>

        <div>
            the pledgeL is a loan system, user can pledge her income(which is essentialy an imagine Bech32 Address that has
            shares) in fron of get lon by PAIs
        </div>

        <div>
            improve pledgeL to transfer the PAIs immidiately to vendor's account, in this case pledgeL acts like a "pay over
            time" system in which user can buy a service/good/product
            and pay the cost over time with/without interest/late fees, penalties, and compounding interest,...
        </div>

        <div>
            I order to improve privacy in eccosystem, it needed to [[iPGP|improve PGP comunication between nodes]]. to
            support existed legacy GPG and web of trust
        </div>

        <div>
            adjust code to support multi language i18n
            then translate menu, label, content... ALL to ALL languages
        </div>

        <div>
            a class(dClass=VePoll Verify by Polling Result) of Basic transactions in which instead verify the signature of
            input owners, machines use the result of a generall polling
            and based on the result decide to transfer PAIs or reject the entire transaction.
            1. owner of funds will be used in trx signs the fund through a contract in which binds a polling.
            2. 3 documents(trx, polling & VePollContract) put on chain, and because of that the fund will be lock until end
            of polling period.
            3. after finish polling depends on the results the transaction will be valid or invalid

            P.S. same principals cn be implemented to do a Pledge/Unpledge(obviously for all class of pledges e.g. PledgeP,
            PledgeSA...)
            based on a polling.
            the polling participane can be ALL or be mentioned in VePollContract
        </div>

        <div>
            Implementing data-selling plugin(code, protocol...), in order to backers can be payed because of backing up mass
            data.
            using Universal Business Language UBL for nodes comunications
        </div>
        <div>
            Enabling Clone and P4P transactions
        </div>

        <div>
            refactor Routing in angular in order to having http://localhost:4200/imagine/... for all parts related to
            imagine
            and http://localhost:4200/iName1/... for all parts related to an hypothetical iName1
        </div>
    </div>
    )"
  },


  {
    "version control",
    R"(
    <div>
        In order to have a real distriuted system imagine has to have it's own distributed version control system.
        it not implementes yet, but the imagine updates are released by imagine's DAG and in free-document format.
        the update, essentially are patches you can download and add to your code regardles of what version control system yu are using (e.g. Fossil, Git, Subversion, ...)
        BTW we will imlement a new version control system which is customized for distributed environment soon.
        look at [[/imagine/demos/Distributed Version Control Plugin implementation|Distributed Version Control Plugin implementation]] for discussiona and planing
    </div>
    )"
  }

};

