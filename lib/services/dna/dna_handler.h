#ifndef DNAHANDLER_H
#define DNAHANDLER_H


struct Shareholder {
  QString account = "";
  uint64_t shares = 0;
};


class DNAHandler
{
public:
  static const QString stbl_dna_shares;

  DNAHandler();

  static std::tuple<QString, QString> getDNAActiveDateRange(QString cDate = "");
  static std::tuple<uint64_t, UI64DicT, QVector<Shareholder> > getSharesInfo(QString cDate = "");
  static GenRes insertAShare(QJsonObject &params);


};

#endif // DNAHANDLER_H
