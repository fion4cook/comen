#include "stable.h"

#include "dna_handler.h"

DNAHandler::DNAHandler()
{
}

const QString DNAHandler::stbl_dna_shares = "c_dna_shares";


GenRes DNAHandler::insertAShare(QJsonObject &params) {
  QueryRes exist = DbModel::select(
    DNAHandler::stbl_dna_shares,
    QStringList {"dn_doc_hash"},     // fields
    {ModelClause("dn_doc_hash", params.value("dn_doc_hash").toString())}
    );
  if (exist.records.size() > 0)
  {
    // maybe some updates
    return { false, "The DNA document (${utils.hash6c(dna.hash)}) is already recorded"};
  }


  CUtils::exitIfGreaterThanNow(params.value("m_doc_creation_date").toString());
  //      CUtils::exitIfGreaterThanNow(dna->m_approval_date);

  QVDicT values{
    {"dn_doc_hash", params.value("m_doc_hash").toString()},
    {"dn_shareholder", params.value("m_shareholder").toString()},
    {"dn_project_hash", params.value("m_project_hash").toString()},
    {"dn_help_hours", params.value("m_help_hours").toString()},
    {"dn_help_level", params.value("m_help_level").toString()},
    {"dn_shares", params.value("m_shares").toString()},
    {"dn_title", params.value("m_doc_title").toString()},
    {"dn_descriptions", params.value("m_doc_descriptions").toString()},
    {"dn_tags", params.value("m_doc_tags").toString()},
    {"dn_votes_y", params.value("m_votesY").toString()},
    {"dn_votes_a", params.value("m_votesA").toString()},
    {"dn_votes_n", params.value("m_votesN").toString()},
    {"dn_creation_date", params.value("m_block_creation_date").toString()}
  };

  DbModel::insert(
    stbl_dna_shares,    // table
    values, // values to insert
    true
  );

    return { true };
}

/**
 *
 * @param {*} _t
 * given time(DNA proposal approing time), it returns the range in which a share is valid
 * the active period starts from 7 years ago and finishes right at the end of previous cycle time
 * it means if your proposal have been approved in 2017-01-01 00:00:00, the owner can gain from 2017-01-01 12:00:00 to 2024-01-01 00:00:00
 */
std::tuple<QString, QString> DNAHandler::getDNAActiveDateRange(QString cDate)
{
  if(cDate == "")
    cDate = CUtils::getNow();

  auto[praticipate_from_date, praticipate_to_date] = CUtils::getACycleRange(
    cDate,
    CConsts::SHARE_MATURITY_CYCLE);

  if (CConsts::TIME_GAIN == 1)
  {
    praticipate_from_date = CUtils::yearsBefore(CConsts::CONTRIBUTION_APPRECIATING_PERIOD, praticipate_from_date);
  } else {
    praticipate_from_date = CUtils::minutesBefore(100 * CMachine::getCycleByMinutes(), praticipate_from_date);
  }
  return { praticipate_from_date, praticipate_to_date };
}

// TODO: since shares are counting for before 2 last cycles, so implementing a caching system will be much helpfull where we have millions of shareholders
std::tuple<uint64_t, UI64DicT, QVector<Shareholder> > DNAHandler::getSharesInfo(QString cDate)
{
  if(cDate == "")
    cDate = CUtils::getNow();

  // retrieve the total shares in last 24 hours, means -36 to -24 based on greenwich time
  // (Note: it is not the machine local time)
  // for examplie if a node runs this command on 7 May at 14 (in greenwich time)
  // the result will be the final state of DNA at 11:59:59 of 6 May.
  // it means the node calculate all shares which the creation date are less than 11:59:59  of 6 May
  // -------------< 11:59 of 6 May |         --- 24 hours ---        |12:00 of 7 May     --- 2 hours ---     14:00 of 7 May

  CLog::log("get Share info: calc shares for date(" + cDate + ")");

  auto[minCreationDate, maxCreationDate] = getDNAActiveDateRange(cDate);
  QString query = "SELECT dn_shareholder, SUM(dn_shares) sum_ FROM " + stbl_dna_shares;
  query += " WHERE dn_creation_date between \"" + minCreationDate + "\" AND \"" + maxCreationDate + "\" GROUP BY dn_shareholder ORDER BY sum_ DESC";
  CLog::log("Retrieve shares for range cDate(" + cDate + ") -> (" + minCreationDate + " " + maxCreationDate + ")");
  // let msg = `Retrieve shares: SELECT shareholder _shareholder, SUM(shares) _share FROM i_dna_shares WHERE creation_date between '${minCreationDate}' AND '${maxCreationDate}' GROUP BY _shareholder ORDER BY _share DESC `;
  QueryRes shares = DbModel::sCustom(
    query,
    {},
    2);

  // let shares = await model.getShares(cDate);
  uint64_t sumShares = 0;
  QVector<Shareholder> holdersOrderByShares {};
  UI64DicT share_amount_per_holder {};
  for (QVDicT aSHare: shares.records)
  {
    sumShares += aSHare["1"].toUInt();
    share_amount_per_holder[aSHare["0"].toString()] = aSHare["1"].toUInt();
    holdersOrderByShares.push_back(Shareholder {
      aSHare[0].toString(),
      share_amount_per_holder[aSHare["0"].toString()]
    });
  }
  return { sumShares, share_amount_per_holder, holdersOrderByShares };
}
