#ifndef INAMEHANDLER_H
#define INAMEHANDLER_H


class FleNS
{
public:
  QString m_doc_hash = "";
  QString m_iname = "";
  QString m_iname_hash = "";
  QString m_owner = "";
  QString m_doc_creation_date = "";
  QString m_block_creation_date = "";
  bool m_iname_is_setteled = false;

  FleNS(const QVDicT &values = {}){
    if (values.keys().size() > 0)
      setByDict(values);
  };
  bool setByDict(const QVDicT &values = {});

};


class BindingInfo {
public:
  bool m_status;
  QHash<QString, GRecordsT > m_binding_dict;
  QVDRecordsT m_records;
  QString m_msg;
  QVDicT m_map_address_to_profile = {};
  BindingInfo(
    const bool &status = false,
    const QHash<QString, GRecordsT> &binding_dict = {},
    const QVDRecordsT &records = {},
    const QString &msg = "",
    const QVDicT &map_address_to_profile = {}
    ): m_status(status), m_binding_dict(binding_dict), m_records(records),
  m_msg(msg), m_map_address_to_profile(map_address_to_profile){};
};


namespace iNameHandler
{

const QString stbl_iname_records = "c_iname_records";
const QString stbl_machine_iname_records = "c_machine_iname_records";
const QString stbl_iname_bindings = "c_iname_bindings";
const QString stbl_machine_iname_bindings = "c_machine_iname_bindings";

bool initINames();
QString normalizeIName(const QString inp);
QString generateINameHash(const QString &iname);
BindingInfo searchForINamesControlledByWallet(QString mp_code = "", const bool &needBindingsInfoToo = false);


// iname_register_handler.cpp
bool recordINameInDAG(const FleNS &flens);
BindingInfo searchRegisteredINames(const ClausesT &clauses, const bool &needBindingsInfoToo = false);
bool addToMachineControlled(const FleNS &flens);


// iname_binder.cpp
BindingInfo prepareBindingsDict(const ClausesT &clauses, const bool &needpgpKey = false);


// iname_messaging.cpp

};

#endif // INAMEHANDLER_H
