#include "reserved_inames.h"

ReservedINames::ReservedINames()
{

}


std::list<std::string> ReservedINames::s_reserved_inames =
{
  "about",
  "example",
  "chat",
  "contact",
  "demos",
  "faq",
  "forum",
  "home",
  "hu",
  "imagine",
  "localhost",
  "messenger",
  "security",
  "settings",
  "wiki",
  "misc",
  "contract",
  "contracts",
  "plugin",
  "plugins",
  "iname",
  "inames",
  "sample",
  "forum",
  "forums",
  "samples",
  "about",
  "home",
  "wallet",
  "wallets",
  "contributes",
  "contribute",
  "monitor",
  "contributors",
  "contributor",
};
