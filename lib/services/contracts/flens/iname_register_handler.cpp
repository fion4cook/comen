#include "iname_handler.h"




using namespace iNameHandler;

bool recordINameInDAG(const FleNS &flens)
{
  QString in_hash = generateINameHash(flens.m_iname);
  QVDicT values {
    {"in_doc_hash", flens.m_doc_hash},
    {"in_name", flens.m_iname},
    {"in_hash", flens.m_iname_hash},
    {"in_owner", flens.m_owner},
    {"in_register_date", flens.m_block_creation_date},
    {"in_is_settled", flens.m_iname_is_setteled ? CConsts::YES : CConsts::NO}
  };
  CLog::log("Registering iName(" + flens.m_iname + ") in DAG values:" + CUtils::dumpIt(values));
  DbModel::insert(
    stbl_iname_records,
    values
  );
  return addToMachineControlled(flens);
}

bool addToMachineControlled(const FleNS &flens)
{
  // if the iName is controlled by machine-wallet mProfiles, add it to local db too
  BindingInfo machineINames = searchForINamesControlledByWallet(CConsts::ALL);
  CLog::log("searchFor INamesControlledByWallet.records" + CUtils::dumpIt(machineINames.m_records), "app", "trace");
  CLog::log("searchFor INamesControlledByWallet.bindingDict" + CUtils::dumpIt(machineINames.m_binding_dict), "app", "trace");
  CLog::log("searchFor INamesControlledByWallet.mapAddressToProfile" + CUtils::dumpIt(machineINames.m_map_address_to_profile), "app", "trace");

  QStringList iname_hashes;
  for (QVDicT a_record: machineINames.m_records)
  {
    iname_hashes.append(a_record.value("in_hash").toString());
  }
    if (iname_hashes.contains(flens.m_iname_hash)) {
      QueryRes dbl = DbModel::select(
      stbl_machine_iname_records,
      {"imn_hash"},
      ClausesT {{"imn_hash", flens.m_iname_hash}}
    );
    if (dbl.records.size() == 0)
    {
      // insert
      QVDicT values {
        {"imn_mp_code", machineINames.m_map_address_to_profile.value(flens.m_owner)},
        {"imn_doc_hash", flens.m_doc_hash},
        {"imn_name", flens.m_iname},
        {"imn_hash", flens.m_iname_hash},
        {"imn_owner", flens.m_owner},
        {"imn_info", CUtils::getANullStringifyedJsonObj()},
        {"imn_register_date", flens.m_block_creation_date}
      };
      CLog::log("insert c_machine_iname_records.values" + CUtils::dumpIt(values), "app", "trace");
      DbModel::insert(
        stbl_machine_iname_records,
        values
      );
    }
  }
  return true;
}




BindingInfo searchRegisteredINames(const ClausesT &clauses, const bool &need_bindings_info_too)
{
  BindingInfo bInfo;
  QueryRes res = DbModel::select(
    stbl_iname_records,
    {},
    clauses,
    OrderT {{"in_name", "ASC"}}
  );
  if (res.records.size() == 0)
  {
    bInfo.m_status = true;
    bInfo.m_msg = "There is no registered iName!";
    return bInfo;
  }
  bInfo.m_records = res.records;

  if (!need_bindings_info_too)
  {
    bInfo.m_msg = QString("There are %1 registered iName!").arg(res.records.size());
    return bInfo;
  }

  return prepareBindingsDict(clauses, false);

}

