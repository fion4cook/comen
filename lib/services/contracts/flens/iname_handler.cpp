#include "stable.h"
#include "lib/ccrypto.h"

#include "reserved_inames.h"
#include "lib/wallet/wallet_address_handler.h"


#include "iname_handler.h"

bool FleNS::setByDict(const QVDicT &values)
{
  if (values.value("doc_hash", "").toString() != "")
    m_doc_hash = values.value("doc_hash", "").toString();
  if (values.value("iname", "").toString() != "")
    m_iname = values.value("iname", "").toString();
  if (values.value("iname_hash", "").toString() != "")
    m_iname_hash = values.value("iname_hash", "").toString();
  if (values.value("owner", "").toString() != "")
    m_owner = values.value("owner", "").toString();
  if (values.value("doc_creation_date", "").toString() != "")
    m_doc_creation_date = values.value("doc_creation_date", "").toString();
  if (values.value("block_creation_date", "").toString() != "")
    m_block_creation_date = values.value("block_creation_date", "").toString();
  if (values.value("iname_is_setteled", "").toString() != "")
    m_iname_is_setteled = values.value("iname_is_setteled", false).toBool();
  return true;
}

namespace iNameHandler
{

#include "iname_register_handler.cpp"
#include "iname_binder.cpp"
#include "iname_messaging.cpp"


QString normalizeIName(const QString inp)
{
   // it is tooo strict. later can be a more free even unicode and smiley domains
  //    return inp.replace(/([^a-z0-9-_@]+)/gi, '-').toLowerCase();
  QRegularExpression rx("([^a-z0-9-_@]+)");
  inp.toLower().replace(rx, "");
  return inp;
}

QString generateINameHash(const QString &iname)
{
  QString hash = CCrypto::keccak256(normalizeIName(iname));
  return hash;
}


bool initINames()
{

  // register iName imagine & hu
//  std::make_unique(reserved_inames);
  ReservedINames::s_reserved_inames.unique();
  ReservedINames::s_reserved_inames.sort();

  for (std::string aName : ReservedINames::s_reserved_inames) {
    QString iname_hash = generateINameHash(QString::fromStdString(aName));
    FleNS flens(QVDicT{
      {"doc_hash", iname_hash}, // dummy document hash
      {"iname_hash", iname_hash},
      {"iname", QString::fromStdString(aName)},
      {"owner", CConsts::HU_INAME_OWNER_ADDRESS},
      {"block_creation_date", CUtils::getLaunchDate()},
      {"iname_is_setteled", true}
    });
    recordINameInDAG(flens);
  }

  return true;

}


BindingInfo searchForINamesControlledByWallet(QString mp_code, const bool &need_bindings_info_too)
{
  if (mp_code == "")
    mp_code = CMachine::getSelectedMProfile();

  QVDRecordsT wallet_addresses = WalletAddressHandler::getAddressesListSync(mp_code, {"wa_address", "wa_mp_code"});
  QStringList addresses;
  QVDicT map_address_to_profile = {};
  for (QVDicT an_address_info : wallet_addresses) {
      addresses.append(an_address_info.value("wa_address").toString());
      map_address_to_profile.insert(an_address_info.value("wa_address").toString(), an_address_info.value("wa_mp_code").toString());
  }

  ClausesT clauses
  {
    {ModelClause("in_owner", addresses, "IN")}
  };
  BindingInfo res = searchRegisteredINames(clauses, need_bindings_info_too);

  res.m_map_address_to_profile = map_address_to_profile;
  return res;
}

};

