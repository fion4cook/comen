#include "iname_handler.h"





using namespace iNameHandler;

BindingInfo prepareBindingsDict(const ClausesT &clauses, const bool &needpgpKey)
{
  BindingInfo bInfo;
  QueryRes bindings = DbModel::select(
    stbl_iname_bindings,
    {"nb_doc_hash", "nb_in_hash", "nb_bind_type", "nb_conf_info", "nb_title", "nb_comment", "nb_status", "nb_creation_date"},
    clauses
  );
  if (bindings.records.size() == 0)
  {
    bInfo.m_status = true;
    bInfo.m_msg = "No binded pgp to iname exist";
    return bInfo;
  }

  bInfo.m_binding_dict = {};

  for (QVDicT aBind : bindings.records) {
    aBind.insert("nb_conf_info", CUtils::parseToJsonObj(aBind.value("nb_conf_info")));

    // lightening response
    if (aBind.value("nb_bind_type") == CConsts::BINDING_TYPES::IPGP) {
//      QString PGPHandle = CCrypto::keccak256(aBind.value("nb_conf_info").value("iPLabel").toString() + aBind.value("nb_creation_date").toString());
//      aBind.insert("PGPHandle", PGPHandle);

//      if (!needpgpKey) {
//        aBind.value("nb_conf_info").remove("iPPubKey");
//      }
    }

    QString nb_in_hash = aBind.value("nb_in_hash").toString();
    if (!bInfo.m_binding_dict.keys().contains(nb_in_hash))
      bInfo.m_binding_dict.insert(nb_in_hash, {});

    QString nb_bind_type = aBind.value("nb_bind_type").toString();
    if (!bInfo.m_binding_dict.value(nb_in_hash).keys().contains(nb_bind_type))
      bInfo.m_binding_dict[nb_in_hash].insert(nb_bind_type, {});
    bInfo.m_binding_dict[nb_in_hash][nb_bind_type].push_back(aBind);
  }

  return bInfo;
}
