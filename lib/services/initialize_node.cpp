#include "mainwindow.h"
#include "stable.h"

#include "initialize_node.h"

MainWindow* InitializeNode::s_mw;

InitializeNode::InitializeNode()
{

}

void InitializeNode::setMW(MainWindow* mw)
{
  s_mw = mw;
}

MainWindow* InitializeNode::getMW()
{
  return s_mw;
}

bool InitializeNode::refreshGUI()
{
//  s_mw->refreshGUI();
  return true;
}

GenRes InitializeNode::bootMachine()
{
  srand (time(NULL));

  int m_clone_id = CMachine::getAppCloneId();
  CLog::log(QString("Booting %1 ").arg(m_clone_id));

  GenRes res = CMachine::initMachine();
  if (res.status != true)
    return res;

//  if (![1, 4].includes(appCloneId))
//    machine.neighborHandler.addSeedNeighbors()
//  //TODO: start remove it BEFORERELEASE
//  if (appCloneId == 1) {
//    let issetted = model.sRead({
//        table: 'i_kvalue',
//        fields: ['kv_value'],
//        query: [
//            ['kv_key', 'setSampleMachines']
//        ]
//    });
//    if (issetted.length > 0)
//        return { err: false, msg: 'sample machines already setted' };
//    sampleMachines.setSampleMachines();
//    db.setAppCloneId(1);
//  }
//  //TODO: end remove it BEFORERELEASE
//  // initialize document & content
//  let initRes = initContentSetting.doInit();
//  if (initRes.err != false)
//    return initRes;
//  let doesSafelyInitialized = initContentSetting.doesSafelyInitialized();
//  if (doesSafelyInitialized.err != false) {
//    utils.exiter(`doesSafelyInitialized ${doesSafelyInitialized}`, 609);
//  }
//  return { err: false, msg: 'The machine fully initilized' };

  return { true};
}


bool InitializeNode::maybeInitDAG()
{
  // check if genesis block is created?
  QueryRes existedGenesisBlock = DbModel::select(
    "c_blocks",
    QStringList {"b_id", "b_hash"},     // fields
    {ModelClause("b_type", CConsts::BLOCK_TYPES::Genesis)}, //clauses
    OrderT {{"b_creation_date", "ASC"}, {"b_id", "ASC"}},   // order
    1   // limit
  );
  if (existedGenesisBlock.records.size() > 0)
  {
    return true;
  }

  bool status;

  // create Genisis Block
  GenesisBlock genesisBlock = GenesisBlock();
  genesisBlock.initGenesisBlock();

  // init Administrative Configurations History
  PollingHandler::initAdministrativeConfigurationsHistory();

  // init Polling Profiles
  PollingHandler::initPollingProfiles();

  // Initialize Agoras content
  status = AgoraInitializer::initAgoras();
  if (!status)
    CUtils::exiter("AgoraInitializer::initAgoras failed!", 908);

  // initialize registerd iNames
  status = iNameHandler::initINames();
  if (!status)
    CUtils::exiter("iNameHandler initINames failed!", 908);


  // initialize wiki pages
  WikiHandler::initTmpWikiContent();

  return doesSafelyInitialized();

}

bool InitializeNode::doesSafelyInitialized()
{
  // TODO implement it to controll if all intial document are inserted properly?
  return true;
}
