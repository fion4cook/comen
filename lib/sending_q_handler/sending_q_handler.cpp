#include "stable.h"
#include "lib/network/network_handler.h"
#include "sending_q_handler.h"

const QString SendingQHandler::stbl_sending_q = "c_sending_q";
const QStringList SendingQHandler::stbl_sending_q_fields = {"sq_id", "sq_type", "sq_code", "sq_title", "sq_sender", "sq_receiver", "sq_connection_type", "sq_payload"};
const QString SendingQHandler::stbldev_sending_q = "cdev_sending_q";

SendingQHandler::SendingQHandler()
{

}


CListListT SendingQHandler::preparePacketsForNeighbors(
    const QString &sqType,
    const QString &sqCode,
    const QString &sqPayload,
    const QString &sqTitle,
    const QStringList &sqReceivers,
    const QStringList &noReceivers,
    const bool &denayDoubleSendCheck
    )
{
  CLog::log("prepare PacketsForNeighbors args: ", "app", "trace");

  if (sqReceivers.size() > 0)
     CLog::log("targeted packet to " + CUtils::dumpIt(sqReceivers), "app", "trace");

  if (noReceivers.size() > 0 )
     CLog::log("no targeted packet to " + CUtils::dumpIt(noReceivers));

  QVDRecordsT neighbors = CMachine::get().getActiveNeighbors();
  if (sqReceivers.size() > 0)
  {
    // keep only requested neighbors
    QVDRecordsT selectedNeighbors;
    for (QVDicT neighbor: neighbors)
      if (sqReceivers.contains(neighbor.value("n_email").toString()))
        selectedNeighbors.push_back(neighbor);
    neighbors = selectedNeighbors;
  }

  if (noReceivers.size() > 0)
  {
     // keep only requested neighbors
     QVDRecordsT selectedNeighbors;
     for (QVDicT neighbor: neighbors)
         if (!noReceivers.contains(neighbor.value("n_email").toString()))
             selectedNeighbors.push_back(neighbor);
     neighbors = selectedNeighbors;
  }

  CLog::log("Finall Selected Neighbors= " + CUtils::dumpIt(neighbors), "app", "trace");

  if (neighbors.size() == 0)
  {
     CLog::log("There is no neighbore to send prepare Packets For Neighbors", "app", "trace");
     return {};
  }

  EmailSettings pub_email_info = CMachine::getPubEmailInfo();
  EmailSettings prive_email_info = CMachine::getPrivEmailInfo();

  CListListT packets;
  QString sender;
  for (QVDicT neighbor: neighbors)
  {
    QString receiver_pub_key = neighbor.value("n_pgp_public_key", "").toString();
    if (receiver_pub_key == "")
      continue;

//     let params = {
//         shouldSign: true,
//         shouldCompress: true,
//         message: args.sqPayload,
//     };
    QString sender_priv_key;
    QString connection_type = neighbor.value("n_connection_type", "").toString();
    QString receiver_email = neighbor.value("n_email", "").toString();
    if (connection_type == CConsts::PRIVATE)
    {
      sender = prive_email_info.m_address;
      sender_priv_key = prive_email_info.m_PGP_private_key;
    } else {
      sender = pub_email_info.m_address;
      sender_priv_key = pub_email_info.m_PGP_private_key;
    }

    QString key = QStringList {sqType, sqCode, sender, receiver_email}.join("");

    if (BroadcastLogger::listSentBloksIds().contains(key))
    {
      CLog::log("already send packet! " + key, "app", "error");
      if (!denayDoubleSendCheck)
        continue;
    }

    auto[pgp_status, emailBody] = CPGP::encryptPGP(sqPayload, sender_priv_key, receiver_pub_key);
    if(!pgp_status)
    {
      CLog::log("failed in encrypt PGP", "app", "error");
      continue;
    }
    emailBody = CUtils::breakByBR(emailBody);
    emailBody = CPGP::wrapPGPEnvelope(emailBody);

    // control output size
    if (static_cast<uint64_t>(emailBody.length()) > CConsts::MAX_BLOCK_LENGTH_BY_CHAR)
    {
      CLog::log("excedded max packet size for packet type(" + sqType + ") code(" + sqCode + ")", "app", "error");
      continue;
    }

    packets.append(
      QStringList{
        connection_type,
        sqTitle,
        sqType,
        sqCode,
        sender,
        receiver_email,
        emailBody   //sqPyload
    });


    BroadcastLogger::addSentBlock({
      {"lb_type", sqType},
      {"lb_code", sqCode},
      {"lb_title", sqTitle},
      {"lb_sender", sender},
      {"lb_receiver", receiver_email},
      {"lb_connection_type", connection_type}
    });
  }
  return packets;

  //TODO after successfull sending must save some part the result and change the email to confirmed
}


bool SendingQHandler::pushIntoSendingQ(
    const QString &sqType,
    const QString &sqCode,
    const QString &sqPayload,
    const QString &sqTitle,
    const QStringList &sqReceivers,
    const QStringList &noReceivers,
    const bool &denayDoubleSendCheck
    )
{
  CListListT packets = preparePacketsForNeighbors(
    sqType,
    sqCode,
    sqPayload,
    sqTitle,
    sqReceivers,
    noReceivers,
    denayDoubleSendCheck);

  CLog::log("prepare PacketsForNeighbors res packets: " + CUtils::dumpIt(packets));

  for (QStringList packet: packets)
  {
    CLog::log("inserting in '_sending_q' " + packet[2] +"-" + packet[3] + " for " + packet[5] + " " + packet[1]);
    QueryRes dblChk = DbModel::select(
      stbl_sending_q,
      {"sq_type", "sq_code"},
      {
        {"sq_type", packet[2]},
        {"sq_code", packet[3]},
        {"sq_sender", packet[4]},
        {"sq_receiver", packet[5]},
      });
    CLog::log("packet pushed to send(" + QString::number(dblChk.records.size()) + ") from " + packet[4] + " to " + packet[5] + " " + packet[2] + "(" + packet[3] + ")", "app", "trace");

    if (dblChk.records.size() == 0)
    {
      QVDicT values {
        {"sq_type", packet[2]},
        {"sq_code", packet[3]},
        {"sq_title", packet[1]},
        {"sq_sender", packet[4]},
        {"sq_receiver", packet[5]},
        {"sq_connection_type", packet[0]},
        {"sq_payload", packet[6]},
        {"sq_send_attempts", 0},
        {"sq_creation_date", CUtils::getNow()},
        {"sq_last_modified", CUtils::getNow()}
      };
      DbModel::insert(
        stbl_sending_q,
        values,
        false,
        false);

      if (CConsts::IS_DEVELOP_MODE)
      {
        QueryRes dblChk = DbModel::select(
          stbldev_sending_q,
          {"sq_type", "sq_code"},
          {
            {"sq_type", packet[2]},
            {"sq_code", packet[3]},
            {"sq_sender", packet[4]},
            {"sq_receiver", packet[5]}});

        if (dblChk.records.size() == 0)
          DbModel::insert(
            stbldev_sending_q,
            values,
            false,
            false);
      }
    }
  }
  return true;
}

QVDRecordsT SendingQHandler::fetchFromSendingQ(
  QStringList fields,
  ClausesT clauses,
  OrderT order)
{
  if (fields.size() == 0)
    fields = stbl_sending_q_fields;

  QueryRes cpackets = DbModel::select(
    stbl_sending_q,
    fields,
    clauses,
    order);
  return cpackets.records;
}


bool SendingQHandler::sendOutThePacket()
{
  QVDRecordsT cpackets = fetchFromSendingQ();
  if (cpackets.size() == 0)
  {
    CLog::log("No packet in sending q to Send", "app", "trace");
    return true;
  }

  // always pick the first pkt! TODO: maybe more intelligent solution needed
  QVDicT packet = cpackets[0];
  bool send_res = NetworkHandler::iPush(
    packet.value("sq_title").toString(),
    packet.value("sq_payload").toString(),
    packet.value("sq_sender").toString(),
    packet.value("sq_receiver").toString());

  // remove packet from sending queue
  if(send_res)
    rmoveFromSendingQ({
      {"sq_type", packet.value("sq_type")},
      {"sq_code", packet.value("sq_code")},
      {"sq_sender", packet.value("sq_sender")},
      {"sq_receiver", packet.value("sq_receiver")}});

  return true;
}

bool SendingQHandler::rmoveFromSendingQ(const ClausesT &clauses)
{
  DbModel::dDelete(
    stbl_sending_q,
    clauses);
  return true;
}

void SendingQHandler::recursivePullSendingQ()
{
    sendOutThePacket();
}
