#include "stable.h"
#include "coin.h"


//Coin::Coin(QObject *parent) : QObject(parent)
//{

//}

Coin::Coin(const QString& ref, uint64_t val) :
    m_coin_ref(ref),
    m_value(val)
{

}

//Coin::Coin(const Coin& other) :
//    QObject(other.parent())
//{
//    m_coin_ref = other.m_coin_ref;
//    m_value   = other.m_value;
//}

Coin& Coin::operator=(const Coin& rhs)
{
  if (&rhs != this)
  {
    m_coin_ref = rhs.m_coin_ref;
    m_value   = rhs.m_value;
  }

  return *this;
}

const QString& Coin::coinRef() const
{
    return m_coin_ref;
}

void Coin::setCoinRef(const QString &coinRef)
{
    m_coin_ref = coinRef;
}

uint64_t Coin::value() const
{
    return m_value;
}

void Coin::setValue(uint64_t value)
{
    if (value>MAX_COIN_VALUE)
        return;

    if (m_value!=value){
        m_value = value;
//        emit valueChanged();
    }
}
