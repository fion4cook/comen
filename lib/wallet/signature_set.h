#ifndef SIGNATURESET_H
#define SIGNATURESET_H

#include "constants.h"

class SignatureSet
{

public:
    QString m_sKey;
    QString m_pPledge = "N";
    QString m_pDelegate = "N";

    SignatureSet(const QString &sKey, const QString &pPledge=CConsts::NO, const QString &pDelegate=CConsts::NO);


};

#endif // SIGNATURESET_H
