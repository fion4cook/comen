#include "stable.h"
#include "wallet_address_handler.h"

WalletAddress::WalletAddress(UnlockDocument *unlock_doc, const QString &mp_code, const QString &title, const QString &creation_date)
{
  m_unlock_doc = new UnlockDocument();
  *m_unlock_doc = *unlock_doc;
  m_mp_code = mp_code; // machine profile code
  m_address = unlock_doc->m_account_address;
  m_title = title;
  m_creation_date = creation_date;
  if (m_creation_date == "")
    m_creation_date = CUtils::getNow();
}




// -  -  -  -  -  -  -  -  WalletAddressHandler

WalletAddressHandler::WalletAddressHandler()
{

}
const QString WalletAddressHandler::stbl_machine_wallet_addresses = "c_machine_wallet_addresses";
const QStringList WalletAddressHandler::stbl_machine_wallet_addresses_fields = {"wa_id", "wa_mp_code", "wa_address", "wa_title", "wa_detail", "wa_creation_date"};
const QString WalletAddressHandler::stbl_machine_wallet_funds = "c_machine_wallet_funds";


std::tuple<bool, QVDRecordsT> WalletAddressHandler::searchWalletAdress(
  const QStringList &addresses,
  const QString &mp_code,
  const QStringList &fields)
{
  ClausesT clauses;
  if (mp_code != CConsts::ALL)
    clauses.push_back({"wa_mp_code", mp_code});

  clauses.push_back(
    ModelClause("wa_address", addresses, "IN")
  );

  QueryRes res = DbModel::select(
    WalletAddressHandler::stbl_machine_wallet_addresses,
    fields,
    clauses);

  return {true, res.records};
}


QVDRecordsT WalletAddressHandler::getAddressesListSync(
    const QString &mp_code,
    const QStringList &fields,
    const bool &sum)
{
  ClausesT clauses{};
  if (mp_code != CConsts::ALL)
    clauses.push_back(ModelClause("wa_mp_code", mp_code));
  QueryRes addresses_info = DbModel::select(
    WalletAddressHandler::stbl_machine_wallet_addresses,
    fields,
    clauses
  );

  if (sum == false)
    return addresses_info.records;

//    let addressDict = {};
//    let nowT = utils.getNow();
//    let q = `select wf_address, SUM(wf_o_value), COUNT(*) FROM ${tableFunds} WHERE wf_mp_code=$1 AND wf_mature_date<$2 GROUP BY wf_address`;
//    let tmpRes = model.sQuery(q, [mpCode, nowT]);
//    if (Array.isArray(tmpRes))
//        tmpRes.forEach(aRow => {
//            addressDict[aRow.wf_address] = _.has(addressDict, aRow.wf_address) ? addressDict[aRow.wf_address] : {};
//            addressDict[aRow.wf_address] = {
//                'matSum': iutils.convertBigIntToJSInt(aRow.sum),
//                'matCount': aRow.count,
//            }
//        });
//    q = `select wf_address, SUM(wf_o_value), COUNT(*) FROM ${tableFunds} WHERE wf_mp_code=$1 AND wf_mature_date>=$2 GROUP BY wf_address`;
//    tmpRes = model.sQuery(q, [mpCode, nowT]);
//    if (Array.isArray(tmpRes))
//        tmpRes.forEach(aRow => {
//            addressDict[aRow.wf_address] = _.has(addressDict, aRow.wf_address) ? addressDict[aRow.wf_address] : {};
//            addressDict[aRow.wf_address]['unmatSum'] = iutils.convertBigIntToJSInt(aRow.sum);
//            addressDict[aRow.wf_address]['unmatCount'] = aRow.count;
//        });
//    // clog.app.info(addressDict);

//    for (let anAddrs of walletAddresses) {
//        anAddrs['matSum'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'matSum'))) ? addressDict[anAddrs.waAddress]['matSum'] : '';
//        anAddrs['matCount'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'matCount'))) ? addressDict[anAddrs.waAddress]['matCount'] : '';
//        anAddrs['unmatSum'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'unmatSum'))) ? addressDict[anAddrs.waAddress]['unmatSum'] : '';
//        anAddrs['unmatCount'] = ((_.has(addressDict, anAddrs.waAddress)) && (_.has(addressDict[anAddrs.waAddress], 'unmatCount'))) ? addressDict[anAddrs.waAddress]['unmatCount'] : '';
//    };
//    // clog.app.info(addresses);
//    return walletAddresses;

  return addresses_info.records;
}


bool WalletAddressHandler::insertAddress(const WalletAddress &w_address)
{
  QVDicT values {
    {"wa_mp_code", w_address.m_mp_code},
    {"wa_address", w_address.m_address},
    {"wa_title", w_address.m_title},
    {"wa_creation_date", w_address.m_creation_date},
    {"wa_detail", CUtils::serializeJson(w_address.m_unlock_doc->exportJson())}
  };
  CLog::log("Insert new address to machine wallet" + CUtils::dumpIt(values), "app", "trace");
  DbModel::insert(
    stbl_machine_wallet_addresses,
    values
  );
  return true;
}
