#ifndef COIN_H
#define COIN_H





class Coin
{
//  Q_OBJECT
//  Q_PROPERTY(uint64_t value READ value WRITE setValue NOTIFY valueChanged);

public:
  virtual ~Coin(){};
  Coin(const QString& ref, uint64_t val);
//  Coin(const Coin& other);

  Coin& operator=(const Coin& rhs);

  const QString& coinRef() const;
  void setCoinRef(const QString &coinRef);

  uint64_t value() const;
  void setValue(uint64_t value);

//signals:
//  void valueChanged();

private:
  QString m_coin_ref;
  uint64_t m_value;

};

#endif // COIN_H


