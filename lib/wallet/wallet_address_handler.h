#ifndef WALLETADDRESSHANDLER_H
#define WALLETADDRESSHANDLER_H

class UnlockDocument;


#include "lib/transactions/basic_transactions/signature_structure_handler/individual_signature.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_set.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_document.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"

class WalletAddress
{
public:
  QString m_mp_code = ""; // machine profile code
  QString m_address = "";
  QString m_title = "";
  UnlockDocument *m_unlock_doc;
  QString m_creation_date = "";

  WalletAddress();
  WalletAddress(
    UnlockDocument *unlock_doc,
    const QString &mp_code,
    const QString &title,
    const QString &creation_date = "");

};


class WalletAddressHandler
{
public:
  static const QString stbl_machine_wallet_addresses;
  static const QStringList stbl_machine_wallet_addresses_fields;
  static const QString stbl_machine_wallet_funds;

  WalletAddressHandler();
  static std::tuple<bool, QVDRecordsT> searchWalletAdress(
    const QStringList &addresses,
    const QString &mpCode,
    const QStringList &fields = WalletAddressHandler::stbl_machine_wallet_addresses_fields);

  static QVDRecordsT getAddressesListSync(const QString &mp_code,
                                   const QStringList &fields = {"wa_address"},
                                   const bool &sum = false);

  static bool insertAddress(const WalletAddress &w_address);
};

#endif // WALLETADDRESSHANDLER_H
