#include "stable.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/parsing_q_handler/parsing_q_handler.h"
#include "lib/dag/full_dag_handler.h"
#include "dag_message_handler.h"

const QString DAGMessageHandler::stbl_kvalue = "c_kvalue";

DAGMessageHandler::DAGMessageHandler()
{

}

bool DAGMessageHandler::setLastReceivedBlockTimestamp(
  const QString &bType,
  const QString &hash,
  const QString &receive_date)
{
  DbModel::upsert(
    stbl_kvalue,
    "kv_key",
    "LAST_RECEIVED_BLOCK_TIMESTAMP",
    {
      {"kv_value", CUtils::serializeJson({
        {"block_type", bType},
        {"hash", hash},
        {"receive_date", receive_date}
      })},
      {"kv_last_modified", CUtils::getNow()}
    });
  return true;
}

bool DAGMessageHandler::invokeDescendents(
  const bool &denay_double_send_check)
{
  // read latest recorded block in DAG
  auto[status, block_hash, block_creation_date] = DAG::getLatestBlock();
  Q_UNUSED(status);

  if (CUtils::timeDiff(block_creation_date).asMinutes > CMachine::getAcceptableBlocksGap())
  {
    // control if block's potentially descendent(s) exist in parsing q
    QVDRecordsT likeHashRes = ParsingQHandler::searchParsingQ(
      {
        {"pq_type", {CConsts::BLOCK_TYPES::Normal, CConsts::BLOCK_TYPES::Coinbase}, "IN"},
        {"pq_code", block_hash}
      },
      {"pq_type", "pq_code", "pq_payload"});

    // invoke network for block probably descendents
    QStringList existed_descendents_in_parsingQ = {};
    if (likeHashRes.size() > 0)
    {
      for (QVDicT wBlock: likeHashRes)
      {
        QJsonObject jBlock = CUtils::parseToJsonObj(wBlock.value("pq_payload").toString());
        // if the existed block in parsing q is descendent of block
        QStringList tmp = {};
        for(QJsonValueRef an_anc: jBlock.value("ancestors").toArray())
          tmp.push_back(an_anc.toString());
        if (tmp.contains(block_hash))
          existed_descendents_in_parsingQ.push_back(jBlock.value("blockHash").toString());
      }
    }
    if (existed_descendents_in_parsingQ.size() > 0)
    {
      // controling if the ancestors of descendent exist in local or not
      existed_descendents_in_parsingQ = CUtils::arrayUnique(existed_descendents_in_parsingQ);
      return blockInvokingNeeds(existed_descendents_in_parsingQ);
      // set prerequisities null and attemps zero in order to force machine parsing them

    } else {
      // Machine doesn't know about block descendents, so asks network
      return doInvokeDescendents(
        block_hash,
        block_creation_date,
        denay_double_send_check);

    }
  }
  return false;
}

bool DAGMessageHandler::doInvokeDescendents(
  const QString &block_hash,
  const QString &block_creation_date,
  const bool &denay_double_send_check)
{
  CLog::log("do Invoke Descendents args block_hash(" + block_hash + ") block_creation_date(" + block_creation_date + ") denay_double_send_check(" + CUtils::dumpIt(denay_double_send_check) + ")", "app", "trace");

  // if the last block which exists in DAG is older than 2 cycle time maybe efficient to call full-history
  if (block_creation_date < CUtils::minutesBefore(2 * CMachine::getCycleByMinutes()))
  {
    QString LastFullDAGDownloadResponse = KVHandler::getValue("LAST_FULL_DAG_DOWNLOAD_RESPONSE");
    if (LastFullDAGDownloadResponse == "")
    {
      KVHandler::upsertKValue("LAST_FULL_DAG_DOWNLOAD_RESPONSE", CUtils::minutesBefore(CMachine::getCycleByMinutes()));

    } else {
      if (CUtils::timeDiff(LastFullDAGDownloadResponse).asMinutes < 5)
      {
        CLog::log("less than 5 minutes ago invoked for full DAG", "app", "trace");
        return true;
      }
    }

    // TODO: improve it to not send full req to all neighbors
    auto[code, body] = FullDAGHandler::invokeFullDAGDlRequest(block_creation_date);
    CLog::log("invoke Full DAG Dl Request code(" + code + ") body" + body, "app", "trace");
    SendingQHandler::pushIntoSendingQ(
      CConsts::GQL,
      code,
      body,
      "Invoke Full DAG blocks after(" + block_creation_date + ")");

  } else {

    CLog::log("invoking for descendents of ${utils.hash6c(block_hash)}", "app", "trace");
    QJsonObject payload = {
      {"type", CConsts::MESSAGE_TYPES::DAG_INVOKE_DESCENDENTS},
      {"mVer", "0.0.0"},
      {"blockHash", block_hash}
    };
    QString payload_ = CUtils::serializeJson(payload);
    SendingQHandler::pushIntoSendingQ(
      CConsts::MESSAGE_TYPES::DAG_INVOKE_DESCENDENTS,
      block_hash, // sqCode
      payload_,
      "Invoke Descendents(" + CUtils::hash16c(block_hash) + ")",
      {},
      {},
      denay_double_send_check);
  }

  return true;
}

/**
* the method (going back in history) analyzes block(s) prerequisities and maybe invoke them
* @param {*} blockHash
* @param {*} level
*/
bool DAGMessageHandler::blockInvokingNeeds(
  QStringList block_hashes,
  uint level)
{


  QStringList next_level_block_hashes = {};
  QStringList missed_blocks = {};
  for (uint l = 0; l < level; l++)
  {
    // exists in DAG?
    QVDRecordsT existedInDAG = DAG::searchInDAG(
      {{"b_hash", block_hashes, "IN"}},
      {"b_hash"});
    if (existedInDAG.size() == block_hashes.size())
      continue; // all blocks are already recorded in local graph

    QStringList tmp;
    for(QVDicT a_row: existedInDAG)
      tmp.append(a_row.value("b_hash").toString());
    QStringList array_diff = CUtils::arrayDiff(block_hashes, tmp);

    // control if block exist in parsing_q
    for (auto looking_hash: array_diff)
    {
      QVDRecordsT existsInParsingQ = ParsingQHandler::searchParsingQ(
        {{"pq_code", looking_hash}},
        {"pq_code", "pq_payload"});

      if (existsInParsingQ.size() == 0)
      {
        missed_blocks.push_back(looking_hash);
      } else {
//        let ancestors = existsInParsingQ.map(x => JSON.parse(x.pqPayload).ancestors);
        QList<QStringList> ancestors;
        for(auto x: existsInParsingQ)
        {
          QJsonObject payloadJs = CUtils::parseToJsonObj(x.value("pq_payload").toString());
          QJsonArray ancsJS = payloadJs.value("ancestors").toArray();
          QStringList ancestors;
          for(auto y: ancsJS)
            ancestors.append(y.toString());
        }

        if (ancestors.size() == 0)
        {
          CLog::log("The block(" + CUtils::hash16c(looking_hash) + ") has no valid ancestors! " + CUtils::dumpIt(existsInParsingQ), "sec", "error");
          return false;
        }
        for(auto pckedAncestors: ancestors)
        {
          for(auto ancestor: pckedAncestors)
          {
            next_level_block_hashes.push_back(ancestor);
          }
        }
      }
    }
    block_hashes = CUtils::arrayUnique(next_level_block_hashes);
  }
  missed_blocks = CUtils::arrayUnique(missed_blocks);
  MissedBlocksHandler::addMissedBlocksToInvoke(missed_blocks);
//  recursiveMissedBlocksInvoker();
  return true;
}


void DAGMessageHandler::recursiveMissedBlocksInvoker()
{
  QString cycle = CUtils::getCoinbaseCycleStamp();
  CLog::log("ReMiBcInv cycle(" + cycle + ") called recursive MissedBlocks Invoker", "app", "trace");
  QStringList missed = MissedBlocksHandler::getMissedBlocksToInvoke(2);
  // listener.doCallAsync('APSH_control_if_missed_block');

  if (missed.size() > 0)
  {
    CLog::log("ReMiBcInv cycle(" + cycle + ") recursive Missed Blocks Invoker has " + missed.size() + " missed blocks(" + CUtils::dumpIt(missed) + ")", "app", "trace");
    for(QString a_missed: missed)
    {
      //check if not already exist in parsing q
      QVDRecordsT existsInParsingQ = ParsingQHandler::searchParsingQ(
        {{"pq_code", a_missed}},
        {"pq_type", "pq_code"});

      if (existsInParsingQ.size() == 0)
      {
        invokeBlock(a_missed);
        MissedBlocksHandler::increaseAttempNumber(a_missed);
      }
    }
  }
}

bool DAGMessageHandler::invokeBlock(const QString &block_hash)
{
  CLog::log("invoking for block(" + CUtils::hash16c(block_hash) + ")", "app", "trace");
  QJsonObject payload {
    {"type", CConsts::MESSAGE_TYPES::DAG_INVOKE_BLOCK},
    {"mVer", "0.0.0"},
    {"blockHash", block_hash}};
  QString serialized_payload = CUtils::serializeJson(payload);
  CLog::log("invoked for keaves (" + block_hash + ")", "app", "trace");

  bool status = SendingQHandler::pushIntoSendingQ(
    CConsts::MESSAGE_TYPES::DAG_INVOKE_BLOCK, // sqType
    block_hash,  // sqCode
    serialized_payload,
    "Invoke Block(" + CUtils::hash16c(block_hash) + ")");

  return status;
}
