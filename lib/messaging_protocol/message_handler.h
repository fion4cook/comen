#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H


class MessageHandler
{
public:
  MessageHandler();


  //  -  -  -  -  -  -  -  greeting codes
  static std::tuple<bool, QString, QString, QString, QString> createHandshakeRequest(const QString &connection_type, const QString &receiver_id);

  static std::tuple<bool, QString, QString, QString, QString> createNiceToMeetYou(
    const QString &connection_type,
    const QString &receiver_email,
    const QString &receiver_PGP_public_key);

  static std::tuple<bool, QString, QString, QString, QString> createHereIsNewNeighbor(
    const QString &connection_type,
    const QString &machine_email,
    const QString &machine_PGP_private_key,
    const QString &receiver_email,
    const QString &receiver_PGP_public_key,
    const QString &new_neighbor_email,
    const QString &new_neighbor_PGP_public_key);
};

#endif // MESSAGEHANDLER_H
