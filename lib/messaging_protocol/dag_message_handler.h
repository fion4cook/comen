#ifndef DAGMESSAGEHANDLER_H
#define DAGMESSAGEHANDLER_H


class DAGMessageHandler
{
public:
  DAGMessageHandler();

  const static QString stbl_kvalue;

  static bool setLastReceivedBlockTimestamp(
    const QString &bType,
    const QString &hash,
    const QString &receiveDate = CUtils::getNow());

  static bool invokeDescendents(
    const bool &denay_double_send_check = false);

  static bool doInvokeDescendents(
    const QString &block_hash,
    const QString &block_creation_date,
    const bool &denay_double_send_check = false);

  static bool blockInvokingNeeds(
    QStringList block_hashes,
    uint level = 7);

  static void recursiveMissedBlocksInvoker();
  static bool invokeBlock(const QString &block_hash);

};

#endif // DAGMESSAGEHANDLER_H
