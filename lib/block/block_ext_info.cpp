#include "stable.h"
#include "block_ext_info.h"


#include "lib/block_utils.h"




BlockExtInfo::BlockExtInfo()
{

}
QString BlockExtInfo::stbl_block_extinfos = "c_block_extinfos";

GenRes BlockExtInfo::insertToDB(const QString &serializedBextInfo,
                                const QString &blockHash,
                                const QString &creationDate)
{
    GenRes res;
    WrapRes safeBlock = BlockUtils::wrapSafeContentForDB(serializedBextInfo);

    QVDicT values;
    values.insert("x_block_hash", blockHash);
    values.insert("x_detail", safeBlock.content);
    values.insert("x_creation_date", creationDate);

    CLog::log("--- recording bExtInfo in DAG Block(" + CUtils::hash8c(blockHash)+")");
    DbModel::insert(
        BlockExtInfo::stbl_block_extinfos,     // table
        values, // values to insert
        true
    );

    res.status = true;
    return res;
}


std::tuple<bool, QJsonObject> BlockExtInfo::selectBExtInfosFromDB(const QString &blockHash)
{
  QueryRes bExtInfo = DbModel::select(
    BlockExtInfo::stbl_block_extinfos,
    QStringList {"x_block_hash", "x_detail"},     // fields
    {ModelClause("x_block_hash", blockHash)},
    {},
    1   // limit
  );
  if (bExtInfo.records.size() == 0) {
    return {
      false,
      CUtils::parseToJsonObj(QString(""))
    };
  }
  QVariant x_detail = bExtInfo.records[0].value("x_detail");
  QString serializedBextInfo = x_detail.toString();
  WrapRes safeBlock = BlockUtils::unwrapSafeContentForDB(serializedBextInfo);

  std::cout << "eeeeeeeeeeeeeeeee";

  QJsonObject JsonObj = CUtils::parseToJsonObj(safeBlock.content);

  return {
    true,
    JsonObj
  };

}
