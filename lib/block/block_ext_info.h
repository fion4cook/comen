#ifndef BLOCKEXTINFO_H
#define BLOCKEXTINFO_H



class BlockExtInfo
{
public:
    static QString stbl_block_extinfos;

    BlockExtInfo();
    static GenRes insertToDB(const QString &serializedBextInfo,
                             const QString &blockHash,
                             const QString &creationDate);

    static std::tuple<bool, QJsonObject>  selectBExtInfosFromDB(const QString &blockHash);

};

#endif // BLOCKEXTINFO_H
