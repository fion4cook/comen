#include "stable.h"
#include "lib/dag/normal_block/rejected_transactions_handler.h"
#include "lib/dag/society_rules/society_rules.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/documents_in_related_block/transactions/equations_controls.h"
#include "lib/transactions/basic_transactions/utxo/spent_coins_handler.h"
#include "lib/dag/super_control_til_coinbase_minting.h"
#include "coins_visibility_handler.h"

#include "transactions_in_related_block.h"

TransactionsInRelatedBlock::TransactionsInRelatedBlock()
{

}



std::tuple<bool, QStringList, QStringList, QSDicT, QV2DicT, QStringList> TransactionsInRelatedBlock::prepareBlockOverview(
  const Block *block)
{
  QStringList supported_P4P = {};
  QStringList trx_uniqueness = {};
  QStringList inputs_doc_hashes = {};
  QStringList block_used_coins = {};
  QSDicT map_coin_to_spender_doc = {};

  for (DocIndexT doc_inx = 0; doc_inx < block->m_documents.size(); doc_inx++)
  {
    Document * trx = block->m_documents[doc_inx];

    if (trx->m_doc_creation_date > block->m_block_creation_date)
    {
      CLog::log("The trx(" + CUtils::hash8c(trx->m_doc_hash) + ") is after block(" + CUtils::hash8c(block->m_block_hash) + ") creation-date!", "trx", "error");
      return {false, {}, {}, {}, {}, {}};
    }

    trx_uniqueness.append(trx->m_doc_hash);

    // extracting P4P (if exist)
    if ((trx->m_doc_type == CConsts::DOC_TYPES::BasicTx) && (trx->m_doc_class == CConsts::TRX_CLASSES::P4P))
    {
      if (!CConsts::SUPPORTS_P4P_TRANSACTION)
      {
        CLog::log("Network still doen't support P4P transactions. (" + CUtils::hash8c(trx->m_doc_hash) + ") in block(" + CUtils::hash8c(block->m_block_hash) + ")!", "trx", "error");
        return {false, {}, {}, {}, {}, {}};
      }
      if(trx->getRef() != "")
        supported_P4P.append(trx->getRef());
    }

    if (trx->trxHasInput() && !trx->trxHasNotInput())
    {
      for (TInput* input: trx->getInputs())
      {
        inputs_doc_hashes.append(input->m_transaction_hash);
        QString a_coin = CUtils::packCoinRef(input->m_transaction_hash, input->m_output_index);
        block_used_coins.append(a_coin);
        map_coin_to_spender_doc[a_coin] = trx->m_doc_hash;
      }
    }
  }

  // uniquness test
  if (trx_uniqueness.size() != CUtils::arrayUnique(trx_uniqueness).size())
  {
    CLog::log("Duplicating same trx in block body. block(" + CUtils::hash8c(block->m_block_hash) + ")!", "trx", "error");
    return {false, {}, {}, {}, {}, {}};
  }

  // control for using of rejected Transactions refLocs
  // in fact a refLoc can exist in table trx_utxo or not. if not, it doesn't matter whether exist in rejected trx or not.
  // and this controll of rejected trx is not necessary but it is a fastest way to discover a double-spend
  QVDRecordsT rejected_transactions = RejectedTransactionsHandler::searchRejTrx({{"rt_doc_hash", inputs_doc_hashes, "IN"}});
  if (rejected_transactions.size() > 0)
  {
    CLog::log("Useing rejected transaction's outputs in block(" + CUtils::hash8c(block->m_block_hash) + ")! rejected transactions:(" + CUtils::dumpIt(rejected_transactions) + ")!", "trx", "error");
    return {false, {}, {}, {}, {}, {}};
  }

  // control double spending in a block
  // because malisciuos user can use one ref in multiple transaction in same block
  if (block_used_coins.size() != CUtils::arrayUnique(block_used_coins).size())
  {
    CLog::log("Double spending same refs in a block(" + CUtils::hash8c(block->m_block_hash) + ")! ", "trx", "error");
    return {false, {}, {}, {}, {}, {}};
  }
  CLog::log("Block(" + CUtils::hash8c(block->m_block_hash) + ") has " + QString::number(block_used_coins.size())+ " inputs ", "trx", "trace");

  // it is a dictionary for all inputs either valid or invalid
  // it has 3 keys/values (utCoin, utOAddress, utOValue)
  QV2DicT used_coins_dict = {};
  // all inputs must be maturated, maturated means it passed at least 12 hours of creeating the outputs and now they are presented in table trx_utxos adn are spendable
  QStringList spendable_coins = {};
  if (block_used_coins.size() > 0)
  {
    // check if the refLocs exist in UTXOs?
    QVDRecordsT coins_info = UTXOHandler::getCoinsInfo(block_used_coins);
    if (coins_info.size() > 0)
    {
      for (QVDicT a_coin: coins_info)
      {
        spendable_coins.append(a_coin.value("ut_coin").toString());
        used_coins_dict[a_coin.value("ut_coin").toString()] = a_coin;
        // the block creation Date MUST be at least 12 hours after the creation date of reflocs
        if (block->m_block_creation_date < CUtils::minutesAfter(CMachine::getCycleByMinutes(), a_coin.value("ut_ref_creation_date").toString()))
        {
          CLog::log("The creation of coin(" + CUtils::shortCoinRef(a_coin.value("ut_coin").toString()) + ") is after usage in Block(" + CUtils::hash8c(block->m_block_hash) + ")! ", "trx", "error");
          return {false, {}, {}, {}, {}, {}};
        }
      }
    }
  }

  CLog::log("Block(" + CUtils::hash8c(block->m_block_hash) + ") has " + QString::number(spendable_coins.size()) + " maturated Inputs: " + spendable_coins.join(", "), "trx", "trace");

  // all inputs which are not in spendable coins, potentialy can be invalid
  QStringList block_not_matured_coins = CUtils::arrayDiff(block_used_coins, spendable_coins);
  if (block_not_matured_coins.size() > 0)
    CLog::log("In table trx_utxo at " + CUtils::getNowSSS() + " missed (" + block_not_matured_coins.join(", ") + ") inputs! probably is cloned transaction", "sec", "error");

  return
  {
    true,
    supported_P4P,
    block_used_coins,
    map_coin_to_spender_doc,
    used_coins_dict,
    block_not_matured_coins
  };
}

std::tuple<bool, QV2DicT, QV2DicT, bool, GRecordsT> TransactionsInRelatedBlock::considerInvalidCoins(
  const QString &blockHash,
  const QString &blockCreationDate,
  const QStringList &block_used_coins,
  QV2DicT used_coins_dict,
  QStringList maybe_invalid_coins,
  const QSDicT &map_coin_to_spender_doc)
{

  QV2DicT invalidCoinsDict {};  // it contains invalid coins historical creation info

  // retrieve all spent coins in last 5 days
  GRecordsT coinsInSpentTable = SpentCoinsHandler::makeSpentCoinsDict(block_used_coins);
  if (coinsInSpentTable.keys().size()> 0)
  {
    // the inputs which are already spended are invalid coins too
    maybe_invalid_coins = CUtils::arrayAdd(maybe_invalid_coins, coinsInSpentTable.keys());
    maybe_invalid_coins = CUtils::arrayUnique(maybe_invalid_coins);
  }

  if (maybe_invalid_coins.size() > 0)
  {
    CLog::log("maybe Invalid coins (either because of not matured or already spend): " + CUtils::dumpIt(maybe_invalid_coins), "trx", "error");
    invalidCoinsDict = DAG::getCoinsGenerationInfoViaSQL(maybe_invalid_coins);
    CLog::log("invalid Coins Dict: " + CUtils::dumpIt(invalidCoinsDict), "trx", "trace");

    // controll if all potentially invalid coins, have coin creation record in DAG history
    if (invalidCoinsDict.keys().size() != maybe_invalid_coins.size())
    {
      CLog::log("The block uses some un-existed inputs. may be machine is not synched. block(" + CUtils::hash8c(blockHash) + ")", "trx", "error");
      return {false, invalidCoinsDict, used_coins_dict, false, coinsInSpentTable};
    }

    /**
     * control if invalidity is because of using really unmatured outputs(which will be matured in next hours)?
     * if yes drop block
     */
    for (QString aCoin: invalidCoinsDict.keys())
    {
      bool is_matured = CUtils::isMatured(
        invalidCoinsDict[aCoin]["coinGenDocType"].toString(),
        invalidCoinsDict[aCoin]["coinGenCreationDate"].toString());
      if (!is_matured)
      {
        CLog::log("The block uses at least one unmaturated input: block(" + CUtils::hash8c(blockHash) + ") coin(" + aCoin +")", "trx", "error");
        return {false, invalidCoinsDict, used_coins_dict, false, coinsInSpentTable};
      }
    }
  }


  for (QString anInvalidCoin: invalidCoinsDict.keys())
  {
    // append also invalid refs to used coins dict
    used_coins_dict[anInvalidCoin] = QVDicT{
      {"utCoin", anInvalidCoin},
      {"utOAddress", invalidCoinsDict[anInvalidCoin]["coinGenOutputAddress"]},
      {"utOValue", invalidCoinsDict[anInvalidCoin]["coinGenOutputValue"]},
      {"utRefCreationDate", invalidCoinsDict[anInvalidCoin]["coinGenCreationDate"]}};

    /**
     * adding to spend-input-dictionary the invalid coins in current block too
     * in order to having a complete history & order of entire spent coins of the block
     */
    if (!coinsInSpentTable.keys().contains(anInvalidCoin))
      coinsInSpentTable[anInvalidCoin] = QVDRecordsT {};

    coinsInSpentTable[anInvalidCoin].push_back(QVDicT {
      {"spendDate", blockCreationDate},
      {"spendBlockHash", blockHash},
      {"spendDocHash", map_coin_to_spender_doc[anInvalidCoin]}});

  }


  // all spent_loc must exist in invalidCoinsDict
  QStringList tmp1 = invalidCoinsDict.keys();
  QStringList tmp2 = coinsInSpentTable.keys();
  if ((tmp1.size() != tmp2.size()) ||
    (CUtils::arrayDiff(tmp1, tmp2).size() > 0) ||
    (CUtils::arrayDiff(tmp2, tmp1).size()> 0))
  {
    QString msg = "finding invalidations messed up block(${utils.hash6c(blockHash)}) maybe Invalid Inputs: ";
    msg += "invalid Coins Dict ${utils.stringify(utils.objKeys(invalidCoinsDict))} coins In Spent Table:${utils.stringify(coinsInSpentTable)}";
    CLog::log(msg, "sec", "error");
    return {false, invalidCoinsDict, used_coins_dict, false, coinsInSpentTable};
  }

  bool isSusBlock = false;
  if (invalidCoinsDict.keys().size() > 0)
  {
    QString msg = "Some transaction inputs in block(${utils.hash6c(blockHash)}) are not valid ";
    msg += "these are duplicated inputs ${utils.stringify(utils.objKeys(invalidCoinsDict).map(x => iutils.shortCoinRef(x)))}";
    CLog::log(msg, "trx", "error");
    isSusBlock = true;
  }

  // apllying machine-POV-order to coinsInSpentTable as an order-attr
  for (QString aCoin: coinsInSpentTable.keys())
  {
    //looping on orders
    for (uint64_t inx = 0; inx < coinsInSpentTable[aCoin].size(); inx++)
      coinsInSpentTable[aCoin][inx].insert("spendOrder", static_cast<double>(inx));
  }


  return {
    true,
    invalidCoinsDict,
    used_coins_dict,
    isSusBlock,
    coinsInSpentTable};

}

/**
 * @brief TransactionsInRelatedBlock::validateTransactions
 * @param block
 * @param stage
 * @return <status, isSusBlock, doubleSpends>
 */
std::tuple<bool, bool, GRecordsT> TransactionsInRelatedBlock::validateTransactions(
  const Block *block,
  const QString &stage)
{

  if (block->m_block_ext_info.size() == 0)
  {
    CLog::log("Missed ext Info for Block(CUtils::hash8c(" + block->m_block_hash + ")!", "trx", "error");
    return {false, false, {}};
  }

  auto [
    status,
    supported_P4P,
    block_used_coins,
    map_coin_to_spender_doc,
    used_coins_dict,
    block_not_matured_coins
  ] = prepareBlockOverview(block); //let bOverview = this.
  if (!status)
    return {false, false, {}};

  QStringList maybe_invalid_coins = block_not_matured_coins;

  uint64_t localBlockDPCost = 0;
  uint64_t treasuryIncomes, backerIncomes;

  // let remoteBlockDPCostBacker = 0;
  uint64_t transactionMinimumFee = SocietyRules::getTransactionMinimumFee(block->m_block_creation_date);
  for (DocIndexT doc_inx = 0; doc_inx < block->m_documents.size(); doc_inx++)
  {
    Document *trx = block->m_documents[doc_inx];

    // do validate only transactions
    if (
      !Document::isBasicTransaction(trx->m_doc_type) &&
      !Document::isDPCostPayment(trx->m_doc_type)
    )
      continue;


    // DPCOst payment control
    if (trx->m_doc_type == CConsts::DOC_TYPES::DPCostPay)
    {
      auto [status, treasuryIncomes_, backerIncomes_] = trx->retrieveDPCostInfo(
        block->m_block_hash,
        block->m_backer);
      if (!status)
        return {false, false, {}};
      treasuryIncomes = treasuryIncomes_;
      backerIncomes = backerIncomes_;
    }


    // if extInfo & dExtHash are valid
    Document *temp_trx = DocumentFactory::create(CUtils::parseToJsonObj(trx->stringifyDoc()));
    if (!QStringList{CConsts::DOC_TYPES::DPCostPay}.contains(trx->m_doc_type))
    {
      if (!temp_trx->m_doc_ext_object_assigned)
      {
        temp_trx->m_doc_ext_object_assigned = true;
        temp_trx->m_doc_ext_object = block->getBlockExtInfoByDocIndex(doc_inx); // temp_trx->m_doc_ext_object = block->m_block_ext_info[doc_inx];
      }

      if (trx->m_doc_ext_hash != temp_trx->calcDocumentExtInfoRootHash())
      {
        CLog::log("dExtHash is different from local value! Block(CUtils::hash8c(" + block->m_block_hash + ")  trx(" + CUtils::hash8c(temp_trx->m_doc_hash) + ")!", "trx", "error");
        return {false, false, {}};
      }
    }
    delete temp_trx;


    uint64_t trxStatedDPCost = 0;
    if (supported_P4P.contains(trx->m_doc_hash))
    {
      CLog::log("The trx is supported by p4p trx. Block(" + CUtils::hash8c(block->m_block_hash) + ") trx(" + CUtils::hash8c(trx->m_doc_hash) + ") ", "trx", "info");
      // so we do not need to controll trx fee, because it is already payed

    } else if (QStringList {CConsts::DOC_TYPES::DPCostPay}.contains(trx->m_doc_type))
    {
      // this kind of transaction do not need to have trx-fee

    } else {
      if (!CConsts::SUPPORTS_CLONED_TRANSACTION && (trx->getDPIs().size() > 1))
      {
        CLog::log("The network still do not accept Cloned transactions!", "trx", "error");
        return {false, false, {}};
      }

      for (uint16_t aDPIndex: trx->getDPIs()) {
        if (trx->getOutputs()[aDPIndex]->m_address == block->m_backer)
        {
          trxStatedDPCost = trx->getOutputs()[aDPIndex]->m_value;
        }
      }
      if (trxStatedDPCost == 0)
      {
        CLog::log("At least one trx hasn't backer fee! Block(" + CUtils::hash8c(block->m_block_hash) + ") trx(" + CUtils::hash8c(trx->m_doc_hash) + ")", "trx", "error");
        return {false, false, {}};
      }

      if (trxStatedDPCost < transactionMinimumFee)
      {
        CLog::log("The backer fee is less than Minimum acceptable fee!! Block(" + CUtils::hash8c(block->m_block_hash) + ") trx(" + CUtils::hash8c(trx->m_doc_hash) + ") trxStatedDPCost(" + QString::number(trxStatedDPCost)+ ") < minimum fee(" + SocietyRules::getTransactionMinimumFee(block->m_block_creation_date) + ")", "trx", "error");
        return {false, false, {}};
      }

      auto[status, locallyRecalculateTrxDPCost] = trx->calcDocumentDataAndProcessCost(
        stage,
        block->m_block_creation_date);
      if (!status)
        return {false, false, {}};

      if (trxStatedDPCost < locallyRecalculateTrxDPCost)
      {
        CLog::log(
          "The backer fee is less than network values! Block(" +
          CUtils::hash8c(block->m_block_hash) + ") trx(" + CUtils::hash8c(trx->m_doc_hash) +
          ") trxStatedDPCost(" + QString::number(trxStatedDPCost)+ ") < network minimum fee(" +
          CUtils::sepNum(locallyRecalculateTrxDPCost) + ") mcPAIs", "trx", "error");
        return {false, false, {}};
      }
    }

    localBlockDPCost += trxStatedDPCost;
  }
  CLog::log("Backer Fees Sum = " + CUtils::sepNum(localBlockDPCost) + " for Block(" + CUtils::hash8c(block->m_block_hash) + ") ", "app", "info");

  // control if block total trx fees are valid
  uint64_t localBlockDPCostBacker = CUtils::CFloor(localBlockDPCost * CConsts::BACKER_PERCENT_OF_BLOCK_FEE) - SocietyRules::getBlockFixCost(block->m_block_creation_date);
  if (localBlockDPCostBacker != backerIncomes)
  {
    CLog::log(
      "The locally calculated backer fee is not what remote is! Block(" +
      CUtils::hash8c(block->m_block_hash) + ") localBlockDPCostBacker(" + CUtils::sepNum(localBlockDPCostBacker)+ ") backerIncomes(" +
      CUtils::sepNum(backerIncomes) + ") mcPAIs", "trx", "error");

    return {false, false, {}};
  }

  uint64_t localBlockDPCostTreasury = localBlockDPCost - localBlockDPCostBacker;
  if (localBlockDPCostTreasury != treasuryIncomes)
  {
    CLog::log(
      "The locally calculated treasury is not what remote is! Block(" +
      CUtils::hash8c(block->m_block_hash) + ") localBlockDPCostTreasury(" + CUtils::sepNum(localBlockDPCostTreasury)+ ") treasuryIncomes(" +
      CUtils::sepNum(treasuryIncomes) + ") mcPAIs", "trx", "error");
    return {false, false, {}};
  }

  GRecordsT SCUDS {};
  if (CConsts::SUPER_CONTROL_UTXO_DOUBLE_SPENDING)
  {
    /**
    * after being sure about secure and proper functionality of code, we can cut this controll in next monthes
    * finding the block(s) which are used these coins and already are registerg in DAG
    */
    auto[status, SCUDS] = SpentCoinsHandler::findCoinsSpendLocations(block_used_coins);
    if (!status)
      return {false, false, {}};

    if (SCUDS.keys().size() > 0)
    {
      CLog::log("SCUDS: SUPER_CONTROL_UTXO_DOUBLE_SPENDING found some double-spending with block(" + CUtils::hash8c(block->m_block_hash) + ") SCUDS.spendsDict: " + CUtils::dumpIt(SCUDS), "sec", "error");
      return {false, false, {}};
    }
  }

//  let SCUUCM = null;
//  if (CConsts::SUPER_CONTROL_UTXO_UNTIL_COINBASE_MINTING)
//  {
//    /**
//    * most paranoidic and pesimistic control of input validation
//    * for now I put this double-controll to also quality controll of the previuos-controls.
//    * this control is too costly, so it must be removed or optimized ASAP
//    */
//    SCUUCM = SuperControlTilCoinbaseMinting::trackingBackTheCoins(block);
//    // console.log(SCUUCM);
//    if (SCUUCM.err != false) {
//    msg = "SuperValidate, block(${CUtils::hash8c(block->m_block_hash)}) error message: ${utils.stringify(SCUUCM)}";
//    clog.trx.error(msg);
//    return { err: true, msg: SCUUCM.msg, shouldPurgeMessage: true }
//    } else {
//    clog.trx.info("SuperValidate, block(${CUtils::hash8c(block->m_block_hash)})'s inputs have confirmed path going back to coinbase");
//    }
//  }

  auto[status2, invalidCoinsDict, used_coins_dict_, isSusBlock, doubleSpends] = considerInvalidCoins(
    block->m_block_hash,
    block->m_block_creation_date,
    block_used_coins,
    used_coins_dict,
    maybe_invalid_coins,
    map_coin_to_spender_doc);
  if (!status2)
      return {false, false, doubleSpends};
  used_coins_dict = used_coins_dict_;


  bool equationRes = EquationsControls::validateEquation(
    block,
    used_coins_dict,
    invalidCoinsDict);
  if (!equationRes)
    return {false, false, doubleSpends};


  /**
  * control UTXO visibility in DAG history by going back throught ancestors
  * since the block can contains only UTXOs which are took palce in her hsitory
  * in oder words, they are in block's sibility
  */
  if (!isSusBlock && !CConsts::SUPER_CONTROL_UTXO_UNTIL_COINBASE_MINTING)
  {
    bool is_visible = CoinsVisibilityHandler::controlCoinsVisibilityInGraphHistory(
      block_used_coins,
      block->m_ancestors,
      block->m_block_hash);
    if (!is_visible)
      return {false, false, doubleSpends};
  }


  return {true, isSusBlock, doubleSpends};
}
