#ifndef EQUATIONSCONTROLS_H
#define EQUATIONSCONTROLS_H


class EquationsControls
{
public:
  EquationsControls();

  static bool validateEquation(
    const Block *block,
    const QV2DicT &usedCoinsDict,
    const QV2DicT &invalidCoinsDict);

};

#endif // EQUATIONSCONTROLS_H
