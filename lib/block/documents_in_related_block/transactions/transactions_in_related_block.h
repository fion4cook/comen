#ifndef TRANSACTIONSINRELATEDBLOCK_H
#define TRANSACTIONSINRELATEDBLOCK_H


class TransactionsInRelatedBlock
{
public:
  TransactionsInRelatedBlock();

  static std::tuple<bool, QStringList, QStringList, QSDicT, QV2DicT, QStringList> prepareBlockOverview(
    const Block *block);

  static std::tuple<bool, bool, GRecordsT> validateTransactions(
    const Block *block,
    const QString &stage);


  static std::tuple<bool, QV2DicT, QV2DicT, bool, GRecordsT> considerInvalidCoins(
    const QString &blockHash,
    const QString &blockCreationDate,
    const QStringList &block_used_coins,
    QV2DicT used_coins_dict,
    QStringList maybe_invalid_coins,
    const QSDicT &map_coin_to_spender_doc);
};

#endif // TRANSACTIONSINRELATEDBLOCK_H
