#include "stable.h"
#include "lib/block/document_types/document.h"
#include "equations_controls.h"

EquationsControls::EquationsControls()
{

}

bool EquationsControls::validateEquation(
  const Block *block,
  const QV2DicT &usedCoinsDict,
  const QV2DicT &invalidCoinsDict)
{

//  let validateRes;

//  // transaction details check
//  QHash<QString, uint64_t> inputsSum {};
//  QHash<QString, uint64_t> outputsSum {};
//  for (uint32_t docInx = 0; docInx < block->m_documents.size(); docInx++)
//  {
//    if (Document::isBasicTransaction(block->m_documents[docInx]->m_doc_type))
//    {

//      // signatures control
//      validateRes = trxSigValidator.trxSignatureValidator({
//      block,
//      docInx,
//      coinsDict: usedCoinsDict
//      });
//      if (validateRes.err != false) {
//      return validateRes
//      }

//      // control transaction hash
//      validateRes = validateGeneralRulsForTransaction(block.blockHash, block->m_documents[docInx]);
//      if (validateRes.err != false) {
//      return validateRes
//      }

//      // equation control
//      inputsSum[block->m_documents[docInx]->m_doc_hash] = 0;
//      outputsSum[block->m_documents[docInx]->m_doc_hash] = 0;
//      if (_.has(block->m_documents[docInx], 'inputs')) {
//      for (let input of block->m_documents[docInx].inputs) {
//      let aCoin = iutils.packCoinRef(input[0], input[1]);
//      if (_.has(usedCoinsDict, aCoin)) {
//      if (usedCoinsDict[aCoin].utOValue >= iConsts.MAX_INT_IN_JS) {
//      msg = `the transaction(${block->m_documents[docInx]->m_doc_hash}) in block(${utils.hash6c(block.blockHash)}) has input bigger than MAX_SAFE_INTEGER! ${utils.sepNum(output[1])}`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//      inputsSum[block->m_documents[docInx]->m_doc_hash] += usedCoinsDict[aCoin].utOValue;

//      } else {
//      /**
//      * trx uses already spent outputs! so try invalidCoinsDict
//      * probably it is a double-spend case, which will be decided after 12 hours, in importing step
//      * BTW ALL trx must have balanced equation (even duoble-spendeds)
//      */
//      if (_.has(invalidCoinsDict, aCoin)) {
//      if (iutils.convertBigIntToJSInt(invalidCoinsDict[aCoin].coinGenOutputValue >= iConsts.MAX_INT_IN_JS)) {
//      msg = `the transaction(${block->m_documents[docInx]->m_doc_hash}) in block(${utils.hash6c(block.blockHash)}) has inv-input bigger than MAX_SAFE_INTEGER! ${utils.sepNum(output[1])}`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//      inputsSum[block->m_documents[docInx]->m_doc_hash] += iutils.convertBigIntToJSInt(invalidCoinsDict[aCoin].coinGenOutputValue);
//      } else {
//      msg = `the input(${aCoin}) in block(${utils.hash6c(block.blockHash)}) absolutely missed! not in tables neither in DAG!`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//      }
//      }
//      }

//      if (_.has(block->m_documents[docInx], 'outputs')) {
//      for (let output of block->m_documents[docInx].outputs) {
//      if (output[1].toString() != output[1].toString().replace(/\D/g, '')) {
//      msg = `the transaction(${block->m_documents[docInx]->m_doc_hash}) in block(${utils.hash6c(block.blockHash)}) has not digit charecters! ${utils.microPAIToPAI(output[1])}`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//      if (output[1] == 0) {
//      msg = `the transaction(${utils.hash6c(block->m_documents[docInx]->m_doc_hash)}) in block(${utils.hash6c(block.blockHash)}) has zero output! ${utils.microPAIToPAI(output[1])}`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//      if (output[1] < 0) {
//      msg = `the transaction(${utils.hash6c(block->m_documents[docInx]->m_doc_hash)}) in block(${utils.hash6c(block.blockHash)}) has negative output! ${utils.microPAIToPAI(output[1])}`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//      if (parseInt(output[1]) >= iConsts.MAX_INT_IN_JS) {
//      msg = `the transaction(${block->m_documents[docInx]->m_doc_hash}) in block(${utils.hash6c(block.blockHash)}) has output bigger than MAX_SAFE_INTEGER! ${utils.sepNum(output[1])}`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//      outputsSum[block->m_documents[docInx]->m_doc_hash] += parseInt(output[1]);
//      }
//      }
//      clog.app.info(`inputsSum ${inputsSum[block->m_documents[docInx]->m_doc_hash]} = ${outputsSum[block->m_documents[docInx]->m_doc_hash]} output sum`);
//      clog.app.info(`inputsHash ${block->m_documents[docInx]->m_doc_hash} = ${block->m_documents[docInx]->m_doc_hash}`);
//      if (inputsSum[block->m_documents[docInx]->m_doc_hash] != outputsSum[block->m_documents[docInx]->m_doc_hash]) {
//      msg = `the transaction is not balanced! doc(${utils.hash6c(block->m_documents[docInx]->m_doc_hash)}) Block(${utils.hash6c(block.blockHash)})  input(${utils.microPAIToPAI6(inputsSum[block->m_documents[docInx]->m_doc_hash])}) != output(${utils.microPAIToPAI6(outputsSum[block->m_documents[docInx]->m_doc_hash])})`;
//      clog.sec.error(msg);
//      clog.trx.error(msg);
//      return { err: true, msg, shouldPurgeMessage: true }
//      }
//    }
//  }

  // calculate block total inputs & outputs
//  let blockInputsSum = utils.objKeys(inputsSum).map(x => inputsSum[x]).reduce((a, b) => a + b, 0);
//  let blockOutputsSum = utils.objKeys(outputsSum).map(x => outputsSum[x]).reduce((a, b) => a + b, 0);
//  if (blockInputsSum != blockOutputsSum) {
//  msg = `the block(${utils.hash6c(block.blockHash)}) has not balanced in/out! ${utils.microPAIToPAI6(blockInputsSum)} != ${utils.microPAIToPAI6(blockOutputsSum)}`;
//  clog.sec.error(msg);
//  clog.trx.error(msg);
//  return { err: true, msg, shouldPurgeMessage: true }
//  }
//  msg = `the block(${utils.hash6c(block.blockHash)}) input:${utils.microPAIToPAI6(blockInputsSum)} = output:${utils.microPAIToPAI6(blockOutputsSum)}`;
//  clog.trx.info(msg);


  return true;
}
