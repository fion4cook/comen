#include "stable.h"

#include "lib/dag/dag.h"
#include "floating_signature_block.h"

FloatingSignatureBlock::FloatingSignatureBlock()
{

}
/**
*
* @param {time} cDate
* the functions accepts a time and searchs for all floating signatures which are signed the prev coinbase block(either linked or not linked blocks)
*/
std::tuple<double, QStringList, QStringList> FloatingSignatureBlock::aggrigateFloatingSignatures(
  const QString &cDate)
{
  // retrieve prev cycle info
  auto[from_, to_] = CUtils::getCoinbaseRange(CUtils::getLaunchDate());
  Q_UNUSED(from_);
  if (CUtils::getNow() > to_)
  {
    auto[cycleStamp_, from_, to_, fromHour, toHour] = CUtils::getPrevCoinbaseInfo(cDate);
    Q_UNUSED(fromHour);
    Q_UNUSED(to_);
    Q_UNUSED(toHour);
//    clog.cb.info(`rerieve prevCoinbaseInfo ${cDate}=>${JSON.stringify(prevCoinbaseInfo)} `)

    // retrieve prev cycle coinbases
    QVDRecordsT prvCoinbaseBlocks = DAG::searchInDAG(
      {
        {"b_type", CConsts::BLOCK_TYPES::Coinbase},
        {"b_cycle", cycleStamp_},
        {"b_creation_date", from_, ">="}
      },
      {"b_hash"},
      {
        {"b_confidence", "DESC"},
        {"b_hash", "ASC"}
      });
    CLog::log("prvCoinbaseBlocks: " + CUtils::dumpIt(prvCoinbaseBlocks), "cb", "trace");
    QStringList prvCoinbaseBlocks_ {};
    for(QVDicT a_row: prvCoinbaseBlocks)
      prvCoinbaseBlocks_.append(a_row["b_hash"].toString());

    // retrieve all floating signature blocks which are created in prev cycle
    CLog::log("retrieve floating signatures for cycle(" + cycleStamp_ + ") from(" + from_ + ") ", "cb", "trace");

    QVDRecordsT fSWBlocks = DAG::searchInDAG(
      {
        {"b_type", CConsts::BLOCK_TYPES::FSign},
        {"b_cycle", cycleStamp_},
        {"b_creation_date", from_, ">=" }
      }, // TODO add a max Date to reduce results
      {"b_hash", "b_ancestors", "b_confidence", "b_backer"},
      {
        {"b_confidence", "DESC"},
        {"b_hash", "ASC"}});

    QStringList blockHashes = {};
    doubleDicT backers = {};
    for (QVDicT fSWBlock: fSWBlocks)
    {
      // drop float if it is not linked to proper coinbase block
      bool isLinkedToPropoerCB = false;
      QStringList tmpAncs = fSWBlock.value("b_ncestors").toString().split(",");
      for (QString anAnc: tmpAncs)
      {
        if (prvCoinbaseBlocks_.contains(anAnc))
          isLinkedToPropoerCB = true;
      }
      if (!isLinkedToPropoerCB)
          continue;

      backers[fSWBlock.value("b_backer").toString()] = fSWBlock.value("b_confidence").toFloat();
      blockHashes.append(fSWBlock.value("b_hash").toString());
    }
    double confidence = 0.0;
    for (QString bckr: backers.keys())
    {
      confidence += backers[bckr];
    }
    confidence = CUtils::iFloorFloat(confidence);

    return {
      confidence,
      blockHashes,
      backers.keys()};

  } else {
    // machine is in init cycle, so there is no floating signture
    QVDRecordsT genesis = DAG::searchInDAG({{"b_type", CConsts::BLOCK_TYPES::Genesis}});

    return {
      100.00,
      {genesis[0].value("b_hash").toString()}, // only the genesis block hash
      {}};
  }
}
