#ifndef FLOATINGSIGNATUREBLOCK_H
#define FLOATINGSIGNATUREBLOCK_H


class FloatingSignatureBlock
{
public:
  FloatingSignatureBlock();

  static std::tuple<double, QStringList, QStringList> aggrigateFloatingSignatures(
    const QString &cDate = CUtils::getNow());
};

#endif // FLOATINGSIGNATUREBLOCK_H
