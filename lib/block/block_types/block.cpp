#include "stable.h"

#include "lib/block/document_types/document_factory.h"

class Document;
class BSignal;
class DbHandler;


#include "lib/block/document_types/document.h"
#include "lib/block/document_ext_info_types/document_ext_info.h"
#include "lib/dag/dag.h"
#include "lib/dag/sceptical_dag_integrity_control.h"
#include "lib/block/block_types/block_coinbase/coinbase_utxo_handler.h"
#include "lib/dag/normal_block/normal_utxo_handler.h"
#include "lib/block/signal.h"
#include "lib/file_handler/file_handler.h"
#include "lib/block_utils.h"
#include "lib/block/block_ext_info.h"
#include "lib/dag/leaves_handler.h"
#include "lib/parsing_q_handler/parsing_q_handler.h"
#include "gui/c_gui.h"

#include "block.h"

//  -  -  -  Block Record
bool BlockRecord::setByRecordDict(const QVDicT &values)
{
  if (values.value("b_id", "").toString() != "")
    m_id = values.value("b_id", "").toUInt();
  if (values.value("b_hash", "").toString() != "")
    m_hash = values.value("b_hash", "").toString();
  if (values.value("b_type", "").toString() != "")
    m_type = values.value("b_type", "").toString();
  if (values.value("b_cycle", "").toString() != "")
    m_cycle = values.value("b_cycle", "").toString();
  if (values.value("b_confidence", "").toString() != "")
    m_confidence = values.value("b_confidence", "").toFloat();
  if (values.value("b_ext_root_hash", "").toString() != "")
    m_ext_root_hash = values.value("b_ext_root_hash", "").toString();
  if (values.value("b_docs_root_hash", "").toString() != "")
    m_docs_root_hash = values.value("b_docs_root_hash", "").toString();
  if (values.value("b_signals", "").toString() != "")
    m_signals = values.value("b_signals", "").toString();
  if (values.value("b_trxs_count", "").toString() != "")
    m_trxs_count = values.value("b_trxs_count", "").toUInt();
  if (values.value("b_docs_count", "").toString() != "")
    m_docs_count = values.value("b_docs_count", "").toUInt();
  if (values.value("b_ancestors_count", "").toString() != "")
    m_ancestors_count = values.value("b_ancestors_count", "").toUInt();
  if (values.value("b_ancestors", "").toString() != "")
    m_ancestors = values.value("b_ancestors", "").toString();
  if (values.value("b_descendents", "").toString() != "")
    m_descendents = values.value("b_descendents", "").toString();
  if (values.value("b_body", "").toString() != "")
    m_body = values.value("b_body", "").toString();
  if (values.value("b_creation_date", "").toString() != "")
    m_creation_date = values.value("b_creation_date", "").toString();
  if (values.value("b_receive_date", "").toString() != "")
    m_receive_date = values.value("b_receive_date", "").toString();
  if (values.value("b_confirm_date", "").toString() != "")
    m_confirm_date = values.value("b_confirm_date", "").toString();
  if (values.value("b_backer", "").toString() != "")
    m_backer = values.value("b_backer", "").toString();
  if (values.value("b_utxo_imported", "").toString() != "")
    m_utxo_imported = values.value("b_utxo_imported", "").toString();
  return true;
}

//  -  -  -  Block

const QString Block::stbl_block_extinfos = "c_block_extinfos";
/**
 * @brief Block::setByReceivedJsonDoc
 * @param obj
 * @return
 * converts a JSon object(based on parsing text stream) to a standard c++ object
 */
bool Block::setByJsonObj(const QJsonObject &obj)
{
  QStringList object_keys = obj.keys();

  if (obj.value("net").toString() != "")
    m_net = obj.value("net").toString();

  if (obj.value("bVer").toString() != "")
    m_block_version = obj.value("bVer").toString();

  if (obj.value("bType").toString() != "")
    m_block_type = obj.value("bType").toString();

  if (obj.value("descriptions").toString() != "")
    m_block_descriptions = obj.value("descriptions").toString();

  if (obj.value("blockLength").toString() != "")
    m_block_length = static_cast<BlockLenT>(CUtils::convertPaddedStringToInt(obj.value("blockLength").toString()));

  if (object_keys.contains("confidence"))
    m_block_confidence = obj.value("confidence").toDouble();

  if (obj.value("blockHash").toString() != "")
    m_block_hash = obj.value("blockHash").toString();

  if (obj.value("ancestors").toArray().size() > 0)
    m_ancestors = CUtils::convertJSonArrayToStringList(obj.value("ancestors").toArray());

    if (obj.value("signals").toArray().size() > 0)
    m_signals = obj.value("signals").toArray();

  if (obj.value("creationDate").toString() != "")
    m_block_creation_date = obj.value("creationDate").toString();

  if (obj.value("docsRootHash").toString() != "")
    m_docs_root_hash = obj.value("docsRootHash").toString();

  if (object_keys.contains("docs"))
    createDocuments(obj.value("docs"));

  if (obj.value("cycle").toString() != "")
    m_cycle = obj.value("cycle").toString();

  if (obj.value("backer").toString() != "")
    m_backer = obj.value("backer").toString();

  return true;
}

Block::Block()
{
}

Block::~Block()
{
  // delete documents
  for(Document *d: m_documents)
    delete d;
}

void Block::extractHashableParts()
{
}

void Block::doHashObject()
{
}

QString Block::stringifyBExtInfo() const
{
    return "";
}

QString Block::getBlockHashableString() const
{
  return "";
}

QString Block::calcBlockHash() const
{
  return "";
}

QJsonArray Block::exportDocumentsToJSon() const
{
  QJsonArray documents {};
  for(auto a_doc: m_documents)
  {
    documents.push_back(a_doc->exportToJson());
  }
  return documents;
}

QJsonObject Block::exportBlockToJSon() const
{
  QJsonObject out {
    {"ancestors", QVariant::fromValue(m_ancestors).toJsonArray()},
    {"bType", m_block_type},
    {"bVer", m_block_version},
    {"creationDate", m_block_creation_date},
    {"net", m_net},
    {"docsRootHash", m_docs_root_hash},
    {"blockLength", CUtils::paddingDocLength(m_block_length)},
    {"blockHash", m_block_hash},
    {"signals", m_signals},
    {"docs", exportDocumentsToJSon()}
  };
  return out;
}

BlockLenT Block::calcBlockLength(const QJsonObject &block_obj) const
{
  return CUtils::serializeJson(block_obj).length();
}

void asyncDoImportCoinbase_(QString cDate = "")
{
  CLog::log("asyncDoImport Before" );
  std::this_thread::sleep_for (std::chrono::seconds(1000));
  CLog::log("asyncDoImport midle" );
  CoinbaseUTXOHandler::importCoinbasedUTXOs(cDate);
  CLog::log("asyncDoImport after" );
}

void asyncDoImportNormal_(QString cDate = "")
{
  CLog::log("Normal asyncDoImport Before" );
  std::this_thread::sleep_for (std::chrono::seconds(1000));
  CLog::log("Normal asyncDoImport midle" );
  NormalUTXOHandler::importNormalBlockUTXOs(cDate);
  CLog::log("Normal asyncDoImport after" );
}

std::tuple<bool, bool> Block::handleReceivedBlock() const
{
  return {false, false};
}

std::tuple<bool, bool, GRecordsT> Block::validateBlock(const QString &stage) const
{
  Q_UNUSED(stage);
  return {false, false, {}};
}

QString Block::dumpBlock() const
{
  return "dumpBlock not implemented!";
}

bool Block::addBlockToDAG() const
{

  GenRes res;
  CLog::log("receive_date: " + m_block_receive_date, "app");

  // duplicate check
  QueryRes existed_blocks = DbModel::select(
    "c_blocks",
    {"b_hash"},     // fields
    {{"b_hash", m_block_hash}}, //clauses
    {{"b_creation_date", "ASC"}, {"b_id", "ASC"}},   // order
    1   // limit
  );
  if (existed_blocks.records.size() > 0)
    return true;

  // block create date control
  CUtils::exitIfGreaterThanNow(m_block_creation_date);


  // save hard copy of blocks(timestamped by receive date) to have backup
  // in case of curruptions in DAG or bootstrp the DAG, machine doesn't need to download again entire DAG
  // you can simply copy files from ~/backup-dag to folder ~/temporary/inbox
  if (CConsts::DO_HARDCOPY_DAG_BACKUP)
  {
    FileHandler::write(
      CConsts::HD_PATHES::BACKUP_DAG,
      CUtils::getNowSSS() + "_" + m_block_type + "_" + m_block_hash + ".txt",
      CUtils::serializeJson(exportBlockToJSon())
      );
  }

  //TODO: implementing atomicity(transactional) either in APP or DB

  // prepare safe content for recording in DB
  WrapRes safeBlock = BlockUtils::wrapSafeContentForDB(CUtils::serializeJson(exportBlockToJSon()));

  // insert into DB
  QVDicT values {
    {"b_hash", m_block_hash},
    {"b_type", m_block_type},
    {"b_confidence", m_block_confidence},
    {"b_body", safeBlock.content},
    {"b_docs_root_hash", m_docs_root_hash},
    {"b_ext_root_hash", m_b_ext_root_hash},
    {"b_signals", CUtils::serializeJson(m_signals)},
    {"b_trxs_count", 0},
    {"b_docs_count", static_cast<DocIndexT>(m_documents.size())},
    {"b_ancestors", CUtils::serializeJson(m_ancestors)},
    {"b_ancestors_count", static_cast<BlockAncestorsCountT>(m_ancestors.size())},
    {"b_descendents", CUtils::serializeJson(m_descendents)},
    {"b_creation_date", m_block_creation_date},
    {"b_receive_date", m_block_receive_date},
    {"b_confirm_date", m_block_confirm_date},
    {"b_cycle", m_cycle},
    {"b_backer", m_backer},
    {"b_utxo_imported", CConsts::NO}};

  CLog::log("--- recording block in DAG Block(" + CUtils::hash8c(m_block_hash)+")");
  DbModel::insert(
    "c_blocks",     // table
    values, // values to insert
    true
  );

  // recording block ext Info (if exist)
  QString bExtInfo = stringifyBExtInfo();
  if (bExtInfo != "")
  {
    GenRes insRes = BlockExtInfo::insertToDB(bExtInfo, m_block_hash, m_block_creation_date);
  }

  // adjusting leave blocks
  LeavesHandler::removeFromLeaveBlocks(m_ancestors);
  LeavesHandler::addToLeaveBlocks(m_block_hash, m_block_creation_date, m_block_type);

  // insert block signals
  BSignal::logSignals(*this);


  if (m_documents.size() > 0)
  {
    for (DocIndexT doc_inx=0; doc_inx<m_documents.size(); doc_inx++)
    {
      //FIXME: implement suspicious docs filtering!

      Document* aDoc = m_documents[doc_inx];

      bool res = aDoc->applyDocImpact1(*this);

      // connect documents and blocks
      aDoc->mapDocToBlock(m_block_hash, doc_inx);

    }
  }


  // update ancestor's descendent info
  DAG::appendDescendents(m_ancestors, {m_block_hash});


  // sceptical_dag_integrity_controls
  GenRes scepRes = ScepticalDAGIntegrityControl::insertNewBlockControlls(m_block_hash);
  if (scepRes.status != true)
  {
    CLog::log("error in sceptical Data Integrity Check: block(" + CUtils::hash8c(m_block_hash) + ") ", "app", "error");
    return false;
  }


  // FIXME: run it if machine is in sync mode so try to import UTXOs ASAP
  // if (machine.isInSyncProcess()) {
  QString cDate = CUtils::minutesAfter(CMachine::getCycleByMinutes() * 2, m_block_creation_date);
  if (cDate > CUtils::getNow())
      cDate = CUtils::getNow();
  if (m_block_type == CConsts::BLOCK_TYPES::Coinbase)
  {
      // dummy async calling
     std::thread(asyncDoImportCoinbase_, cDate).detach();
  }
  else if (m_block_type == CConsts::BLOCK_TYPES::Normal)
  {
    // dummy async calling
    std::thread(asyncDoImportNormal_, cDate).detach();
  }

  if (m_block_type != CConsts::BLOCK_TYPES::Genesis)
    CGUI::signalUpdateBlocks();

  return true;
}


bool Block::postAddBlockToDAG() const
{
  // remove perequisity, if any block in parsing Q was needed to this block

  ParsingQHandler::removePrerequisites(m_block_hash);

  /**
  * sometimes (e.g. repayback blocks which can be created by delay and causing to add block to missed blocks)
  * we need to doublecheck if the block still is in missed blocks list and remove it
  */
  MissedBlocksHandler::removeFromMissedBlocks(m_block_hash);

  /**
  * inherit UTXO visibilities of ancestors of newly DAG-added block
  * current block inherits the visibility of it's ancestors
  * possibly first level ancestors can be floating signatures(which haven't entry in table trx_utxos),
  * so add ancestors of ancestors too, in order to being sure we keep good and reliable history in utxos
  */
  if (!QStringList {CConsts::BLOCK_TYPES::FSign, CConsts::BLOCK_TYPES::FVote}.contains(m_block_type))
  {
    QStringList ancestors = m_ancestors;
    ancestors = CUtils::arrayAdd(ancestors, DAG::getAncestors(ancestors));
    ancestors = CUtils::arrayAdd(ancestors, DAG::getAncestors(ancestors));
    ancestors = CUtils::arrayUnique(ancestors);
    UTXOHandler::inheritAncestorsVisbility(
      ancestors,
      m_block_creation_date,
      m_block_hash);
  }


  /**
  * remove from i_trx_utxo the the entries which are visible by blocks that are not "leave" any more and
  * they are placed 4 level backward(or older) in history
  * since it is a optional process to lightening DB loadness, it could be done for 8 level previous too
  */
  QStringList ancestors = DAG::getAncestors(m_ancestors, 8);
  if (ancestors.size()> 0)
  {
    // to be sure the visibility creation date is 2 cycles older than block creation date
    auto[minCreationDate, max_] = CUtils::getCbUTXOsDateRange(m_block_creation_date);
    Q_UNUSED(max_);
    QVDRecordsT wBlocks = DAG::searchInDAG(
      {{"b_hash", ancestors, "IN"},
      {"b_creation_date", minCreationDate, "<"}},
      {"b_hash"});

    if (wBlocks.size()> 0)
    {
      ancestors = QStringList {};
      for(QVDicT row: wBlocks)
        ancestors.append(row.value("b_hash").toString());
      if (ancestors.size() > 0)
      {
        CLog::log("removing visible_by " + ancestors.join(", "), "app", "trace");
        UTXOHandler::removeVisibleOutputsByBlocks(ancestors);
      }
    }
  }

  return true;
}



bool Block::createDocuments(const QJsonValue &documents)
{
  auto docs = documents.toArray();
  for(QJsonValueRef a_doc: documents.toArray())
  {
    Document* d = DocumentFactory::create(a_doc.toObject()); //Block* block =
    m_documents.push_back(d);
  }
  return true;
}

/**
 * @brief Block::getBlockExtInfos
 * @param blockHash
 * @return <status, extInfoExist?, extinfoJsonObj>
 */
std::tuple<bool, bool, QJsonObject> Block::getBlockExtInfos(const QString &blockHash)
{
  QJsonObject bExtInfo;
  QueryRes res = DbModel::select(
    stbl_block_extinfos,
    {"x_block_hash"},
    {{"x_block_hash", blockHash}});
  if (res.records.size() != 1)
  {
    CLog::log("get Block Ext Infos: the block(" + CUtils::hash8c(blockHash) + ") has not ext Info", "app", "trace");
    return {true, false, bExtInfo};
  }
  WrapRes safeBlock = BlockUtils::unwrapSafeContentForDB(res.records[0].value("x_detail").toString());
  bExtInfo = CUtils::parseToJsonObj(safeBlock.content);
  return {true, true, bExtInfo};
}

std::tuple<bool, bool, QJsonObject> Block::getBlockExtInfos()
{
  return Block::getBlockExtInfos(m_block_hash);
}

QJsonObject Block::getBlockExtInfoByDocIndex(const DocIndexT &document_index) const
{
  return m_block_ext_info[document_index];
}


/**
 *
 * @param {*} args
 * do a groupping and some general validations on entire documents of a Block
 * TODO: maybe enhance it to use memory buffer
 */

BlockGrouppedDocuments Block::groupDocsOfBlock(const QString &stage) const
{
  BlockGrouppedDocuments grouppedRes {false, stage, {}, {}, {}, {}, {}, {}, {}, {}, this};


  if (m_documents.size() == 0)
    return grouppedRes;


  for (DocIndexT doc_inx = 0; doc_inx < m_documents.size(); doc_inx++)
  {
    Document *aDoc = m_documents[doc_inx];
    grouppedRes.m_docByHash[aDoc->m_doc_hash] = aDoc;

    if ((aDoc->m_doc_creation_date > m_block_creation_date) || (aDoc->m_doc_creation_date > CUtils::getNow()))
    {
      CLog::log("Block has document with creationdate after block-creationDate! stage(" + stage + "), block(" + CUtils::hash8c(m_block_hash) + ") Doc(" + CUtils::hash8c(aDoc->m_doc_hash) + ")", "app", "error");
      return grouppedRes;
    }
    grouppedRes.m_docIndexByHash[aDoc->m_doc_hash] = doc_inx;

    if (!CUtils::isValidVersionNumber(aDoc->m_doc_version))
    {
      CLog::log("invalid dVer in group Docs Of Block for doc(" + CUtils::hash8c(aDoc->m_doc_hash) + ")", "sec", "error");
      return grouppedRes;
    }

    // length control
    Document *tmpDoc = DocumentFactory::create(CUtils::parseToJsonObj(aDoc->stringifyDoc()));
    if (!tmpDoc->m_doc_ext_object_assigned)
    {
      tmpDoc->m_doc_ext_object_assigned = true;
      tmpDoc->m_doc_ext_object = getBlockExtInfoByDocIndex(doc_inx);
    }

    if ((tmpDoc->m_doc_type != CConsts::DOC_TYPES::DPCostPay) &&
        (tmpDoc->m_doc_length != static_cast<DocLenT>(tmpDoc->stringifyDoc().length())))
    {
      QString msg = "The doc stated dLen is not same as real length. stage(" + stage + ") doc type(" + tmpDoc->m_doc_type + "/" + CUtils::hash8c(tmpDoc->m_doc_hash) + ") stated dLen(" + QString::number(aDoc->m_doc_length) + "), ";
      msg += " real length(" + QString::number(tmpDoc->stringifyDoc().length()) + ")";
      CLog::log(msg, "sec", "error");
      return grouppedRes;
    }


    if (!grouppedRes.m_grpdDocuments.keys().contains(aDoc->m_doc_type))
      grouppedRes.m_grpdDocuments[aDoc->m_doc_type] = {};
    grouppedRes.m_grpdDocuments[aDoc->m_doc_type].push_back(aDoc);

    if (aDoc->getRef() != "")
    {
      if (Document::canBeACostPayerDoc(aDoc->m_doc_type))
      {
        grouppedRes.m_trxDict[aDoc->m_doc_hash] = aDoc;
        grouppedRes.m_mapTrxHashToTrxRef[aDoc->m_doc_hash] = aDoc->getRef();
        grouppedRes.m_mapTrxRefToTrxHash[aDoc->getRef()] = aDoc->m_doc_hash;
      } else {
        grouppedRes.m_mapReferencerToReferenced[aDoc->m_doc_hash] = aDoc->getRef();
        grouppedRes.m_mapReferencedToReferencer[aDoc->getRef()] = aDoc->m_doc_hash;
      }
    }
  }

  QStringList payedRefs1 = grouppedRes.m_mapTrxRefToTrxHash.keys();
  QStringList payedRefs2;
  for(QString key: grouppedRes.m_mapTrxHashToTrxRef.keys())
    payedRefs2.append(grouppedRes.m_mapTrxHashToTrxRef[key]);

  for (Document *aDoc: m_documents)
  {
    if (!Document::isNoNeedCostPayerDoc(aDoc->m_doc_type))
    {
      // there must be a transaction to pay for this document
      if (!payedRefs1.contains(aDoc->m_doc_hash) || !payedRefs2.contains(aDoc->m_doc_hash))
      {
        if ((aDoc->m_doc_type == CConsts::DOC_TYPES::CPost) && (aDoc->m_doc_class == CConsts::CPOST_CLASSES::DMS_Post))
        {
          if ((aDoc->getNonce() == "") || (m_block_creation_date > "2021-01-01 00:00:00"))
          {
            CLog::log("The document DMS_Post has not Nonce & not payed by no transaction. stage(" + stage + ") document(" + CUtils::hash8c(aDoc->m_doc_hash) + ") ", "sec", "error");
            return grouppedRes;
          }
        } else {
          CLog::log("The document is not payed by no transaction. stage(" + stage + ") document(" + CUtils::hash8c(aDoc->m_doc_hash) + ") ", "sec", "error");
          return grouppedRes;
        }
      }
    }
  }

  if (grouppedRes.m_mapTrxRefToTrxHash.keys().size() != grouppedRes.m_mapTrxHashToTrxRef.keys().size())
  {
    CLog::log("transaction count and ref count are different! stage(" + stage + ") mapTrxRefToTrxHash: " + CUtils::dumpIt(grouppedRes.m_mapTrxRefToTrxHash) + " mapTrxHashToTrxRef: " + CUtils::dumpIt(grouppedRes.m_mapTrxHashToTrxRef) + " ", "sec", "error");
    return grouppedRes;
  }

  for (QString aRef: grouppedRes.m_mapTrxRefToTrxHash.keys())
  {
    if (!grouppedRes.m_trxDict.keys().contains(grouppedRes.m_mapTrxRefToTrxHash[aRef]))
    {
      CLog::log("missed some1 transaction to support referenced documents. stage(" + stage + ") trxDict: " + CUtils::dumpIt(grouppedRes.m_trxDict) + " mapTrxRefToTrxHash: " + CUtils::dumpIt(grouppedRes.m_mapTrxRefToTrxHash) + " ", "sec", "error");
      return grouppedRes;
    }
  }
  if (CUtils::arrayDiff (grouppedRes.m_mapTrxHashToTrxRef.keys(), grouppedRes.m_trxDict.keys()).size() != 0)
  {
    CLog::log("missed some transaction, to support referenced documents. stage(" + stage + ") trxDict: " + CUtils::dumpIt(grouppedRes.m_trxDict) + " mapTrxRefToTrxHash: " + CUtils::dumpIt(grouppedRes.m_mapTrxRefToTrxHash) + " ", "sec", "error");
    return grouppedRes;
  }
  QStringList docsHashes = grouppedRes.m_docByHash.keys();
  for (QString aRef: grouppedRes.m_mapTrxRefToTrxHash.keys())
  {
    if (!docsHashes.contains(aRef))
    {
      CLog::log("missed some transaction, to support referenced documents. stage(" + stage + ") aRef(" + CUtils::hash8c(aRef) + ") ", "sec", "error");
      return grouppedRes;
    }
  }

  if (grouppedRes.m_docIndexByHash.keys().size() != m_documents.size())
  {
    CLog::log("There is duplicated doc.hash in block. stage(" + stage + ") block(" + CUtils::hash8c(m_block_hash) + ") ", "sec", "error");
    return grouppedRes;
  }

  return grouppedRes;
//  {
//    true,
//    trxDict,
//    docByHash,
//    grpdDocuments,
//    docIndexByHash,
//    mapTrxHashToTrxRef,
//    mapTrxRefToTrxHash,
//    mapReferencedToReferencer,
//    mapReferencerToReferenced
//  };
}
