#ifndef NORMALBLOCK_H
#define NORMALBLOCK_H


class NormalBlock : public Block
{
public:
  NormalBlock(const QJsonObject &obj);
  bool setByJsonObj(const QJsonObject &obj) override;
  std::tuple<bool, bool> handleReceivedBlock() const override;
  std::tuple<bool, bool, GRecordsT> validateBlock(const QString &stage) const override;
  QString dumpBlock() const override;

  bool validateGeneralRulsForNormalBlock() const;

  QString getBlockHashableString() const override;
  QString calcBlockHash() const override;
  QJsonObject exportBlockToJSon() const override;
  BlockLenT calcBlockLength(const QJsonObject &block_obj) const override;

  QString stringifyFloatingVotes() const;

};

#endif // NORMALBLOCK_H
