#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block_utils.h"
#include "lib/block/documents_in_related_block/transactions/transactions_in_related_block.h"
#include "lib/block/document_types/document.h"
#include "lib/block/block_types/block.h"
#include "normal_block.h"


NormalBlock::NormalBlock(const QJsonObject &obj)
{
  setByJsonObj(obj);
}


bool NormalBlock::setByJsonObj(const QJsonObject &obj)
{
  Block::setByJsonObj(obj);


  // custom settings for Genesis block

  return true;
}

QString NormalBlock::dumpBlock() const
{
  // firsdt call parent dump
  QString out = Block::dumpBlock();

  // then child dumpping
  out += "\n in child";
  return out;
}


QString NormalBlock::stringifyFloatingVotes() const
{
  // process m_floating_votes (if exist)
  return "";
}


QString NormalBlock::getBlockHashableString() const
{
  // in order to have almost same hash! we sort the attribiutes alphabeticaly
  QString hashable_block = "{";
  hashable_block += "\"ancestors\":\"" + m_ancestors.join(",") + "\",";
  hashable_block += "\"backer\":\"" + m_backer + "\",";
  hashable_block += "\"blockLength\":\"" + CUtils::paddingDocLength(m_block_length) + "\",";
  hashable_block += "\"bType\":\"" + m_block_type + "\",";
  hashable_block += "\"creationDate\":\"" + m_block_creation_date + "\",";
  hashable_block += "\"descriptions\":\"" + m_block_descriptions + "\",";
  hashable_block += "\"docsRootHash\":\"" + m_docs_root_hash + "\",";  // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
  hashable_block += "\"bExtHash\":\"" + m_b_ext_root_hash + "\",";  // note that we do not put the segwits directly in block hash, instead using segwits-merkle-root-hash
  hashable_block += "\"fVotes\":\"" + stringifyFloatingVotes() + "\",";
  hashable_block += "\"net\":\"" + m_net + "\",";
  hashable_block += "\"signals\":\"" + CUtils::serializeJson(m_signals) + "\",";
  hashable_block += "\"bVer\":\"" + m_block_version + "\"}";
  return hashable_block;
}

QJsonObject NormalBlock::exportBlockToJSon() const
{

}

BlockLenT NormalBlock::calcBlockLength(const QJsonObject &block_obj) const
{
  return Block::calcBlockLength(block_obj);
}


QString NormalBlock::calcBlockHash() const
{
  QString hashable_block = getBlockHashableString();

  // clonedTransactionsRootHash: block.clonedTransactionsRootHash, // note that we do not put the clonedTransactions directly in block hash, instead using clonedTransactions-merkle-root-hash

  CLog::log("The NORMAL! block hashable: " + hashable_block + "\n", "app", "trace");
  return CCrypto::keccak256(hashable_block);
}

bool NormalBlock::validateGeneralRulsForNormalBlock() const
{

  if (!CUtils::isValidDateForamt(m_block_creation_date))
  {
    CLog::log("Invalid m_block_creation_date(" + m_block_creation_date + ")!", "sec", "error");
    return false;
  }

  if (CUtils::isGreaterThanNow(m_block_creation_date))
  {
    CLog::log("Block whith future creation date is not acceptable(" + m_block_creation_date + ") not Block(" + m_block_hash + ")!", "sec", "error");
    return false;
  }

  // docRootHash control
  QStringList doc_hashes = {};
  for(Document* a_doc: m_documents)
    doc_hashes.append(a_doc->m_doc_hash);
  auto[docsRootHash, final_verifies, version, levels, leaves] = CMerkle::generate(doc_hashes);
  Q_UNUSED(final_verifies);
  Q_UNUSED(version);
  Q_UNUSED(levels);
  Q_UNUSED(leaves);
  if (m_docs_root_hash != docsRootHash)
  {
    CLog::log("Mismatch block DocRootHash. localy calculated(" + CUtils::hash8c(docsRootHash) +") remote(" + CUtils::hash8c(m_docs_root_hash) + ")", "sec", "error");
    return false;
  }

  if (!CUtils::isValidHash(m_block_hash))
  {
    CLog::log("Invalid block Hash(" + CUtils::hash8c(m_block_hash) +")", "sec", "error");
    return false;
  }

  // re-calculate block hash
  QString re_calc_block_hash = calcBlockHash();
  if (re_calc_block_hash != m_block_hash)
  {
    CLog::log("Mismatch blockHash. localy calculated(" + CUtils::hash8c(re_calc_block_hash) +") remote(" + CUtils::hash8c(m_block_hash) + ")", "sec", "error");
    return false;
  }

  // Block length control
  QString stringyfied_block = "FIXME";// stringify();
  if (stringyfied_block.length() != m_block_length)
  {
    CLog::log("Mismatch block lenght Block(" + CUtils::hash8c(m_block_hash) +") localy calculated length(" + stringyfied_block.length() + ") remote length(" + m_block_length + ")", "sec", "error");
    return false;
  }

  return true;
}

/**
 * @brief NormalBlock::validateBlock
 * @param stage
 * @return {status, isSusBlock, doubleSpends}
 */
std::tuple<bool, bool, GRecordsT> NormalBlock::validateBlock(const QString &stage) const
{

//   let hookValidate = listener.doCallSync('SASH_before_validate_normal_block', args);


   CLog::log("xxxxxxxxxxxx validate Normal Block xxxxxxxxxxxxxxxxxxxx", "app", "trace");
   CLog::log("\n\n\n" + dumpBlock(), "app", "trace");
   // clog.app.info(blockPresenter(block));


  bool is_valid = validateGeneralRulsForNormalBlock();
  if (!is_valid)
    return {false, false, {}};

   auto[status, isSusBlock, doubleSpends] = TransactionsInRelatedBlock::validateTransactions(this, stage);
   if (!status)
      return {false, false, {}};


  BlockGrouppedDocuments grpd_docs = groupDocsOfBlock(stage);
   if (!grpd_docs.m_status)
   {
     return {false, false, {}};
//       grpdRes.shouldPurgeMessage = true;
   }

   QStringList dTyps = grpd_docs.m_grpdDocuments.keys();
   dTyps.sort();
   CLog::log("Block(" +CUtils::hash6c(m_block_hash) + ") docs types(" + CUtils::dumpIt(dTyps), "app", "info");

   // control if each trx is referenced to only one Document?
   QStringList tmpTrxs;
   for(QString  key: grpd_docs.m_mapTrxRefToTrxHash.keys())
     tmpTrxs.append(grpd_docs.m_mapTrxRefToTrxHash[key]);

   if (tmpTrxs.size() != CUtils::arrayUnique(tmpTrxs).size())
   {
     CLog::log("invalid block! same transaction is used as a ref for different docs! Block(" +CUtils::hash6c(m_block_hash) + ") mapTrxRefToTrxHash(" + CUtils::dumpIt(grpd_docs.m_mapTrxRefToTrxHash), "sec", "error");
     return {false, false, {}};
   }

//   ToBeeValidate validateParams {
//     stage,
//     trxDict,
//     mapTrxHashToTrxRef,
//     mapTrxRefToTrxHash,
//     mapReferencerToReferenced,
//     mapReferencedToReferencer,
//     docByHash,
//     grpdDocuments,
//     docIndexByHash,
//     block
//   };

   // TODO: important! currently the order of validating documents of block is important(e.g. polling must be validate before proposals and pledges)
   // improve the code and remove this dependency

//   /**
//    * validate polling request(if exist)
//    */
//   let pollingsValidateRes = pollingsInRelatedBlock.validatePollings(grpd_docs);
//   if (pollingsValidateRes.err != false) {
//       return pollingsValidateRes;
//   }

//   /**
//    * validate requests for administrative polling(if exist)
//    */
//   let admPollingValidateRes = admPollingsInRelatedBlock.validateAdmPollings(validateParams);
//   if (admPollingValidateRes.err != false) {
//       return admPollingValidateRes;
//   }

//   /**
//    * validate reqRelRes request(if exist)
//    * TODO: move it to validateAdmPollings
//    */
//   let reserveCoinsValidateRes = reqRelRessInRelatedBlock.validateReqRelRess(validateParams);
//   if (reserveCoinsValidateRes.err != false) {
//       return reserveCoinsValidateRes;
//   }

//   /**
//    * validate vote-ballots (if exist)
//    */
//   let ballotsValidateRes = ballotsInRelatedBlock.validateBallots(validateParams);
//   if (ballotsValidateRes.err != false) {
//       return ballotsValidateRes;
//   }

//   /**
//    * validate proposals (if exist)
//    */
//   let proposalsValidateRes = proposalsInRelatedBlock.validateProposals(validateParams);
//   if (proposalsValidateRes.err != false) {
//       return proposalsValidateRes;
//   }

//   /**
//    * validate pledges (if exist)
//    */
//   let pledgesValidateRes = pledgeInRelatedBlock.validatePledges(validateParams);
//   if (pledgesValidateRes.err != false) {
//       return pledgesValidateRes;
//   }

//   /**
//    * validate close pledges (if exist)
//    */
//   let closePledgesValidateRes = closePledgeInRelatedBlock.validateClosePledges(validateParams);
//   if (closePledgesValidateRes.err != false) {
//       return closePledgesValidateRes;
//   }

//   /**
//    * validate iNames (if exist)
//    */
//   let iNamesValidateRes = iNamesInRelatedBlock.validateINames(validateParams);
//   if (iNamesValidateRes.err != false) {
//       return iNamesValidateRes;
//   }

//   /**
//    * validate bind-iNames (if exist)
//    */
//   let iNameBindsValidateRes = iNameBindsInRelatedBlock.validateINameBinds(validateParams);
//   if (iNameBindsValidateRes.err != false) {
//       return iNameBindsValidateRes;
//   }

//   /**
//    * validate msg-to-iNames (if exist)
//    */
//   let iNameMsgsValidateRes = iNameMsgsInRelatedBlock.validateINameMsgs(validateParams);
//   if (iNameMsgsValidateRes.err != false) {
//       return iNameMsgsValidateRes;
//   }

//   /**
//    * validate free-docs (if exist)
//    */
//   let freeDocsValidateRes = freeDocsInRelatedBlock.validateFreeDocs(validateParams);
//   if (freeDocsValidateRes.err != false) {
//       return freeDocsValidateRes;
//   }


   // validate...

   CLog::log("--- confirmed normal block(" + CUtils::hash8c(m_block_hash) + ")");


//   hookValidate = listener.doCallSync('SASH_validate_normal_block', block);
//   if (_.has(hookValidate, 'err') && (hookValidate.err != false)) {
//       return hookValidate;
//   }

   return {
     true,
     isSusBlock,
     doubleSpends
   };

}

/**
* @brief NormalBlock::handleReceivedBlock
* @return <status, should_purge_record>
*/
std::tuple<bool, bool> NormalBlock::handleReceivedBlock() const
{
  CLog::log("******** handle Received Normal Block(" + CUtils::hash8c(m_block_hash)+ ")", "app", "trace");

  if ((m_block_version == "") || !CUtils::isValidVersionNumber(m_block_version))
  {
    CLog::log("missed bVer normal Block(" + CUtils::hash8c(m_block_hash)+ ") bVer(" + m_block_version+ ")", "app", "error");
    return {false, true};
  }

  auto[status, isSusBlock, doubleSpends] = validateBlock(CConsts::STAGES::Validating);

  CLog::log("Received a block of type(" + m_block_type + ") block(" +CUtils::hash8c(m_block_hash) + "), validation result: isSusBlock(" + CUtils::dumpIt(isSusBlock) + ") doubleSpends(" +CUtils::dumpIt(doubleSpends) + ")", "app", "trace");
  if (status) {
    // maybe do something more! e.g. calling reputation system hooks via zmq
    return {false, true};
  }

//  let confidence = DNAHandler.getAnAddressShares(block.backer, block.creationDate).percentage;
//  //TODO: prepare a mega query to run in atomic transactional mode

//  // add block to DAG(refresh leave blocks, remove from utxos, add to utxo12...)
//  // insert the block in i_blocks as a confirmed block
//  dagHandler.addBH.addBlockToDAG({ block, confidence });
//  dagHandler.addBH.postAddBlockToDAG({ block });

//  // remove used UTXOs
//  utxoHandler.removeUsedCoinsByBlock(block);
//  // log spend TxOs
//  let cDate = utils.getNow();
//  // if machine is in sync mode, we send half a cycle after creationdate to avoid deleting all spend records in table "trx_spend"
//  if (machine.isInSyncProcess())
//  cDate = block.creationDate;
//  spentCoinsHandler.markAsSpentAllBlockInputs({
//  block,
//  cDate
//  });

//  // broadcast block to neighbors
//  if (dagHandler.isDAGUptodated()) {
//  let pushRes = sendingQ.pushIntoSendingQ({
//  sqType: block.bType,
//  sqCode: block.blockHash,
//  sqPayload: utils.stringify(block),
//  sqTitle: `Broadcasting confirmed normal block(${utils.hash6c(block.blockHash)})`,
//  })
//  clog.app.info(`BCNB pushRes: ${utils.stringify(pushRes)}`);


//  if (_.has(validateRes, 'isSusBlock') && (validateRes.isSusBlock != null)) {
//  let susVoteRes = fVHandler.createFVoteBlock({
//  cDate,
//  uplink: block.blockHash,
//  bCat: iConsts.FLOAT_BLOCKS_CATEGORIES.Trx,
//  voteData: {
//  doubleSpends: validateRes.doubleSpends
//  }
//  });

//  if (susVoteRes.err != false) {
//  clog.app.error(`\n\nFailed on generating susVote: ${utils.stringify(susVoteRes)}\n`);
//  return { err: false, shouldPurgeMessage: true };
//  }

//  console.log(`\n\nBroadcasting susVote block(${utils.hash6c(block.blockHash)}): ${utils.stringify(susVoteRes)}\n`);
//  clog.app.info(`\n\nBroadcasting susVote block(${utils.hash6c(block.blockHash)}): ${utils.stringify(susVoteRes)}\n`);
//  let pushBVRes = sendingQ.pushIntoSendingQ({
//  sqType: susVoteRes.block.bType,
//  sqCode: susVoteRes.block.blockHash,
//  sqPayload: utils.stringify(susVoteRes.block),
//  sqTitle: `Broadcasting susVote block$(${utils.hash6c(block.blockHash)})`,
//  })
//  clog.app.info(`susVote pushRes: ${utils.stringify(pushBVRes)}`);
//  }
//  return { err: false, shouldPurgeMessage: true };

//  } else if (machine.isInSyncProcess()) {
//  if (_.has(validateRes, 'isSusBlock') && (validateRes.isSusBlock != null))
//  clog.app.info(`machine in sync mode found a sus block ${utils.hash6c(block.blockHash)}`);
//  return { err: false, shouldPurgeMessage: true };

//  }

  return {false, false};
}
