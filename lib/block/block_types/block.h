#ifndef BLOCK_H
#define BLOCK_H

#include "stable.h"


typedef QHash<QString, Document*> DocDicT;
typedef QHash<QString, QVector<Document*> > DocDicVecT;

/**
 * @brief The BlockRecord class
 * it contains information about a block(m_body) and some meta information of block
 */
class BlockRecord
{
public:
  uint64_t m_id = 0;
  QString m_hash = ""; // block root hash
  QString m_type = ""; // block type (genesis/coinbase/normal)
  QString m_cycle = ""; // the coin base cycle
  double m_confidence = 0.0; // if the block is coinbase it denots to percentage of share of signers
  QString m_ext_root_hash = ""; // it was ext_infos_root_hash segwits/zippedInfo... root hashes
  QString m_docs_root_hash = ""; // it was docs_root_hash documents root hash
  QString m_signals = ""; // block signals
  uint64_t m_trxs_count = 0; // transaction counts
  uint64_t m_docs_count = 0; // documents counts
  uint64_t m_ancestors_count = 0; // ancestors counts
  QString m_ancestors = ""; // comma seperated block ancestors
  QString m_descendents = ""; // comma seperated block descendents
  QString m_body = ""; // stringify json block full body
  QString m_creation_date = ""; // the block creation date which stated by block creator
  QString m_receive_date = ""; // the block receive date in local, only for statistics
  QString m_confirm_date = ""; // the block confirmation date in local node
  QString m_backer = ""; // the BECH32 address of who got paid because of creating this block
  QString m_utxo_imported = ""; // does UTXO imported to i_trx_utxo table?

  BlockRecord(const QVDicT &values = {}){
    if (values.keys().size() > 0)
      setByRecordDict(values);
  };
  bool setByRecordDict(const QVDicT &values = {});

};

class Block;
class BlockGrouppedDocuments{
public:
  bool m_status = false;
  QString m_stage = "";
  QSDicT m_mapTrxHashToTrxRef = {};
  QSDicT m_mapTrxRefToTrxHash = {};
  QHash<QString, Document*> m_trxDict = {};
  QSDicT m_mapReferencerToReferenced = {};
  QSDicT m_mapReferencedToReferencer = {};
  QHash<QString, Document*> m_docByHash = {};
  DocDicVecT m_grpdDocuments = {};
  UI16DicT m_docIndexByHash = {};
  const Block* m_block;
};

class Block
{

public:
  Block();
  Block(const QJsonObject &obj){ setByJsonObj(obj); };
  virtual ~Block();

  static const QString stbl_block_extinfos;

  static std::tuple<bool, bool, QJsonObject> getBlockExtInfos(const QString &blockHash);
  std::tuple<bool, bool, QJsonObject> getBlockExtInfos();

  QString m_net = "im";
  QString m_block_descriptions = "";
  BlockLenT m_block_length = 0;
  QString m_block_hash = "0000000000000000000000000000000000000000000000000000000000000000";
  QString m_block_type = "";
  QString m_block_version = "0.0.0";
  QStringList m_ancestors = {};
  QStringList m_descendents = {};
  QJsonArray m_signals = {};
  QString m_backer = ""; // the address which is used to collect transaction-fees
  double m_block_confidence = 0.0; // the sahre percentage of block backer (the node who creatd the block)
  QString m_block_creation_date = "";
  QString m_block_receive_date = "";
  QString m_block_confirm_date = "";
  QString m_cycle = "";
  QString m_descriptions = "";
  QString m_docs_root_hash = "0000000000000000000000000000000000000000000000000000000000000000";
  QString m_b_ext_root_hash = "";
  std::vector<Document *> m_documents = {}; // different type of documents which are inherited from Document
  std::vector<QJsonObject> m_block_ext_info = {}; // fixed different type of objects
  std::vector<QString> m_floating_votes = {}; // TODO: to be implemented later


  //  -  -  -  base class methods
  QJsonObject getBlockExtInfoByDocIndex(const DocIndexT &document_index) const;

  BlockGrouppedDocuments groupDocsOfBlock(const QString &stage) const;
  /**
   * @brief createDocuments
   * makes a loop for JSon documents and converts them to C++ Document object
   * @param documents
   * @return
   */
  bool createDocuments(const QJsonValue &documents);


  //  -  -  -  virtual methods
  virtual bool setByJsonObj(const QJsonObject &obj);

  virtual QString getBlockHashableString() const;
  virtual QString calcBlockHash() const;
  virtual QJsonObject exportBlockToJSon() const;
  virtual BlockLenT calcBlockLength(const QJsonObject &block_obj) const;

  virtual void extractHashableParts();
  virtual void doHashObject();
  virtual QString stringifyBExtInfo() const;

  virtual std::tuple<bool, bool> handleReceivedBlock() const;
  virtual std::tuple<bool, bool, GRecordsT> validateBlock(const QString &stage) const;
  virtual QString dumpBlock() const;


  bool addBlockToDAG() const;
  bool postAddBlockToDAG() const;

  QJsonArray exportDocumentsToJSon() const;


};

#endif // BLOCK_H
