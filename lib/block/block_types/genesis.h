#ifndef GENESIS_H
#define GENESIS_H

#include <QJsonObject>

class Block;
#include "block.h"
#include "lib/block/document_types/dna_proposal.h"
#include "lib/services/dna/dna_handler.h"
#include "lib/services/polling/polling_handler.h"

class GenesisBlock : public Block
{
public:
    QString m_genesisBlock =
    {R"(
        {"net":"im","descriptions":"Imagine all the people sharing all the world","blockLength":"0001051","blockHash":"7a2e58190452d3764afd690ffd13a1360193fdf30f932fc1b2572e834b72c291","bType":"Genesis","bVer":"0.0.0","ancestors":[],"signals":[],"backer":"","confidence":100,"creationDate":"2020-02-02 00:00:00","docsRootHash":"fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396","docs":[{"dType":"DNAProposal","dClass":"General","dVer":"0.0.7","hash":"fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396","title":"fair effort, fair gain, win win win","descriptions":"imagine's contributors time & effort is recorded here","tags":"initialize, DNA","projectHash":"58be4875eaa3736f60622e26bda746fb81812e8d7ecad50a2c3f97f0605a662c","creationDate":"2020-02-01 00:00:00","approvalDate":"2020-02-01 00:00:00","shareholder":"im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl","helpHours":1000000,"helpLevel":1,"shares":1000000,"votesY":1000000,"votesN":0,"votesA":0,"votingLongevity":24,"pollingProfile":"Basic"}],"fVotes":[]}
    )"};

    GenesisBlock();
    GenesisBlock(const QJsonObject &obj){ setByJsonObj(obj); };
    bool setByJsonObj(const QJsonObject &obj) override;
    void initGenesisBlock();
    void extractHashableParts() override;
    QString calcBlockHash() const override;
    QJsonObject exportBlockToJSon() const override;
    void initShares();
    QString stringifyGenisis();     // the stringyfy... functions are a kind of customized ordered serialization of Json object. in order to being complient with javascript Json stringify.
};

#endif // GENESIS_H
