#include "stable.h"

#include "block.h"
#include "genesis.h"
#include "block_normal/normal_block.h"
#include "block_coinbase/coinbase_block.h"

#include "block_factory.h"

BlockFactory::BlockFactory()
{

}


Block* BlockFactory::create(const QJsonObject &obj)
{
  QString block_type = obj.value("bType").toString();
  if (block_type == CConsts::BLOCK_TYPES::Normal)
  {
    return new NormalBlock(obj);

  }
  else if (block_type == CConsts::BLOCK_TYPES::Coinbase)
  {
    Block *b{new CoinbaseBlock(obj)};
    return b;

  }
  else if (block_type == CConsts::BLOCK_TYPES::Genesis)
  {
    return new GenesisBlock(obj);
  }

  return new Block(obj);
}
