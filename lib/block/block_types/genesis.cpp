#include "genesis.h"

#include "lib/ccrypto.h"
#include "lib/clog.h"
#include "lib/dag/dag.h"
#include "lib/block/block_types/block.h"

GenesisBlock::GenesisBlock()
{
    m_block_descriptions = "Imagine all the people sharing all the world";
    m_block_confidence = 100.0;

//    m_net = "im",
//    m_descriptions = "Imagine all the people sharing all the world",
//    m_block_length = 0,
//    m_block_hash = "0000000000000000000000000000000000000000000000000000000000000000",
//    m_block_type = iConsts.BLOCK_TYPES.Genesis,
//    m_block_version = "0.0.0",
//    m_ancestors = [],
//    m_signals = [],
//    m_backer = "", // the address which is used to collect transaction-fees
//    m_block_confidence = 100.00, // the sahre percentage of block backer
//    m_creationDate = iConsts.getLaunchDate(),
//    m_docs_root_hash = "",
//    m_documents = [],
//    m_floating_votes = [], // floating votes(e.g. coinbase confirmations, floating Votes, cloneTransaction...)

}

bool GenesisBlock::setByJsonObj(const QJsonObject &obj)
{
  Block::setByJsonObj(obj);

  // custom settings for Genesis block
  m_block_type = CConsts::BLOCK_TYPES::Genesis;

  return true;
}

void GenesisBlock::initGenesisBlock()
{
  m_block_confidence = 99.99;
  m_block_type = CConsts::BLOCK_TYPES::Genesis;
  m_block_creation_date = CConsts::LAUNCH_DATE;
  m_signals = {};
  m_documents = {};

  DNAProposal* dna_doc = new DNAProposal();
  dna_doc->m_doc_title = "fair effort, fair gain, win win win";
  dna_doc->m_doc_descriptions = "imagine's contributors time & effort is recorded here";
  dna_doc->m_doc_tags = "initialize, DNA";
  dna_doc->m_doc_creation_date = "2020-02-01 00:00:00";
  dna_doc->m_block_creation_date = "2020-02-01 00:00:00";
  dna_doc->m_approval_date = "2020-02-01 00:00:00";
  dna_doc->m_shareholder = "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl";
  dna_doc->m_help_hours = static_cast<uint64_t>(1000000);
  dna_doc->m_help_level = 1;
  dna_doc->m_shares = dna_doc->m_help_hours * dna_doc->m_help_level;
  dna_doc->m_votesY = static_cast<uint64_t>(1000000);
  dna_doc->m_votesA = 0;
  dna_doc->m_votesN = 0;
  dna_doc->m_voting_longevity = 24;
  dna_doc->m_polling_profile = "Basic";
  dna_doc->setHash();
  m_documents.push_back(dna_doc);

//    dnaProposalDoc = DNAHandler.getInitialdna_doc(); // add initialise DNA to genesis block
  QString dnaShareDoc =
  {R"(
   {
   "dType":"DNAProposal",
   "dClass":"General",
   "dVer":"0.0.7",
   "hash":"fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396",
   "title":"fair effort, fair gain, win win win",
   "descriptions":"imagine's contributors time & effort is recorded here",
   "tags":"initialize, DNA",
   "projectHash":"58be4875eaa3736f60622e26bda746fb81812e8d7ecad50a2c3f97f0605a662c",
   "creationDate":"2020-02-01 00:00:00",
   "approvalDate":"2020-02-01 00:00:00",
   "shareholder":"im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl",
   "helpHours":1000000,
   "helpLevel":1,
   "shares":1000000,
   "votesY":1000000,
   "votesN":0,
   "votesA":0,
   "votingLongevity":24,
   "pollingProfile": "Basic"
   }
  )"};

  QString dnaShareDocSer =
  {R"({"creationDate":"2020-02-01 00:00:00","descriptions":"imagine's contributors time & effort is recorded here","dType":"DNAProposal","dVer":"0.0.7","helpHours":1000000,"helpLevel":1,"projectHash":"58be4875eaa3736f60622e26bda746fb81812e8d7ecad50a2c3f97f0605a662c","shareholder":"im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl","tags":"initialize, DNA","title":"fair effort, fair gain, win win win","shares":1000000,"hash":"fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396"})"};
  m_docs_root_hash = dna_doc->m_doc_hash; // "fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396";
  m_block_hash = "7a2e58190452d3764afd690ffd13a1360193fdf30f932fc1b2572e834b72c291";
  m_backer = "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl";

  GenRes r = addBlockToDAG();
  // initShares
  // set initial shares
  initShares();

}

void GenesisBlock::extractHashableParts()
{

//    static extractDNARegDocHashableParts(doc) {
//        let hashable = {
//            creationDate: doc.creationDate,
//            descriptions: doc.descriptions,
//            dType: doc.dType,
//            dVer: doc.dVer,
//            helpHours: doc.helpHours,
//            helpLevel: doc.helpLevel,
//            net: doc.net,
//            projectHash: doc.projectHash,
//            shareholder: doc.shareholder,
//            tags: doc.tags,
//            title: doc.title,
//        };
//        hashable.shares = hashable.helpHours * hashable.helpLevel;
//        return hashable;
//    }


//    let hashable = DNAHandler.extractDNARegDocHashableParts(doc);
//            return crypto.keccak256(JSON.stringify(hashable));

    calcBlockHash();
}


void GenesisBlock::initShares()
{

  const QString startVotingDate = CUtils::minutesBefore(5 * CMachine::getCycleByMinutes(), CConsts::LAUNCH_DATE);

  Document* initialProposalDocPtr = m_documents.front();
  QString proposalDocHash = initialProposalDocPtr->m_doc_hash;

  // update proposal status in DB
  QVDicT proposalUpdValues;
  proposalUpdValues.insert("pr_start_voting_date", startVotingDate);
  proposalUpdValues.insert("pr_conclude_date", CConsts::LAUNCH_DATE);
  proposalUpdValues.insert("pr_approved", CConsts::YES);
  DNAProposal::updateProposal(
    proposalUpdValues,
    {ModelClause("pr_hash", proposalDocHash)}
    );

  // conclude the polling
  QVDicT pollingUpdValues;
  pollingUpdValues.insert("pll_start_date", startVotingDate);
  pollingUpdValues.insert("pll_status", CConsts::CLOSE);
  pollingUpdValues.insert("pll_ct_done", CConsts::YES);
  PollingHandler::updatePolling(
    pollingUpdValues,
    {ModelClause("pll_ref", proposalDocHash)}
    );


  // also insert in db DNA initiate shares
  // create stock recording document
  QJsonObject params = initialProposalDocPtr->exportJson();
  DNAHandler::insertAShare(params);

}

QJsonObject GenesisBlock::exportBlockToJSon() const
{
  return QJsonObject {};
}

QString GenesisBlock::calcBlockHash() const
{
    QString ss = "";
    ss = CCrypto::keccak256(ss);
    CLog::log("Hashed str: " + ss, "app", "debug");
    return "";
}

QString GenesisBlock::stringifyGenisis()
{
    QString dnaShareDocSer = {R"(
{
  "net": "im",
  "descriptions": "Imagine all the people sharing all the world",
  "blockLength": "0001051",
  "blockHash": "7a2e58190452d3764afd690ffd13a1360193fdf30f932fc1b2572e834b72c291",
  "bType": "Genesis",
  "bVer": "0.0.0",
  "ancestors": [],
  "signals": [],
  "backer": "",
  "confidence": 100,
  "creationDate": "2020-02-02 00:00:00",
  "docsRootHash": "fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396",
  "docs": [
      {
          "dType": "DNAProposal",
          "dClass": "General",
          "dVer": "0.0.7",
          "hash": "fb20e4323d695db7728eabcf3a44a1c0516d23362622fa3093e7cf887ef88396",
          "title": "fair effort, fair gain, win win win",
          "descriptions": "imagine's contributors time & effort is recorded here",
          "tags": "initialize, DNA",
          "projectHash": "58be4875eaa3736f60622e26bda746fb81812e8d7ecad50a2c3f97f0605a662c",
          "creationDate": "2020-02-01 00:00:00",
          "approvalDate": "2020-02-01 00:00:00",
          "shareholder": "im1xpjkywf48yckgepcvdnrgdrx8qurgdeevf3kyenyv9snvve5v5ung9axujl",
          "helpHours": 1000000,
          "helpLevel": 1,
          "shares": 1000000,
          "votesY": 1000000,
          "votesN": 0,
          "votesA": 0,
          "votingLongevity": 24,
          "pollingProfile": "Basic"
      }
  ],
  "fVotes": []
}
    )"};
    return dnaShareDocSer;
}

//QString GenesisBlock::stringifySignals() const
//{
//    // in order to create signals, process this member m_signals

//    QString sig = {R"(
//        [
//                {
//                    "sig": "clientVersion",
//                    "ver": "0.0.14"
//                },
//                {
//                    "sig": "iNReputation",
//                    "ver": "0.0.0"
//                }
//            ]
//        )"};
//    sig = CUtils::removeTabNL(sig);
//    sig = CUtils::removeDblSpaces(sig);
//    return sig;
//}

