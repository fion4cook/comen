#include "stable.h"

class Document;

#include "lib/utils/cmerkle.h"
#include "lib/dag/leaves_handler.h"
#include "lib/services/treasury/treasury_handler.h"
#include "lib/block/document_types/document.h"
#include "lib/block/document_types/document_factory.h"
#include "lib/block/block_types/block_floating_signature/floating_signature_block.h"
#include "coinbase_issuer.h"

static QJsonObject coinbaseBlockVersion0 {

    // coud be imagine test net (it) or main net (im)
  {"net", "im"},

  {"bVer", "0.0.0"},
  {"bType", CConsts::BLOCK_TYPES::Coinbase},
  {"descriptions", "null"},
    // coud be imagine test net (it) or main net (im)
  {"blockLength", "0000000"}, // seialized block size by byte (char). this number is also a part of block root hash
  {"confidence", 0.0}, // aggregating of all floating signatures which are created in previous cycle and linked to previous coinbase block

    // miner trx-fee address is implicitly is in coinbase transaction, the email address of node or it's public-key I am not sure it is usefull or not
    // or even compromise some un-necessary informations for adversary
    // miners publick key, has many uses such as open-pgp comunicating to other miners via email to broadcasting the block
    // minerKey: "02e90f689391564243f22082996b75e41ba832733fb25810c576d0cf1cb83f1b54", // dab5b531d6366b298d143175bf47b2ddf671813bd493c9b32dc235102a77a596

    // root hash of all doc hashes
    // it is maked based on merkle tree root of transactions, segwits, wikis, SNSs, SSCs, DVCs, ...
  {"blockHash", "null"},

    // the structure in which contain signature of shareholders(which are backers too) and
    // by their sign, they confirm the value of shares & DAG screen-shoot on that time.
    // later this confirmation will be used in validating the existance of a "sus-block" at the time of that cycle of coinbase

    // a list of ancestors blocks, these ancestor's hash also recorded in block root hash
    // if a block linked to an ancestors block, it must not liks to the father of that block
    // a <--- b <---- c
    // if the block linked to b, it must not link to a
    // the new block must be linked to as many possible as previous blocks (leave blocks)
    // maybe some discount depends on how many block you linked as an ancester!
    // when a new block linke t another block, it MUST not linked to that's block ancester

  {"ancestors", QJsonArray {}},

    // a list of descendent blocks. this list can not be exist when the node receive block.
    // the node update these information whenever receive new childs blocks. and in sequence node send this information ( if exist)

    // a list of coming feature to signaling whether the node supports or not
  {"signals", QJsonArray {}}, // e.g. ["mimblewimble", "taproot"]

    // creation time timestamp, it is a part of block root-hash
    // it also used to calculate spendabality of an output. each output is not spendable befor passing 12 hours of creation time.
    // creation date must be greater than all it's ancesters
  {"creationDate", ""}, // 'yyyy-mm-dd hh:mm:ss'

    // the time in which block is received in node, this time is valid only on local node and not published
    // receiveDate: null,


  {"docsRootHash", ""}, //the hash root of merkle tree of transactions
  {"docs", QJsonArray {}}

};

CoinbaseIssuer::CoinbaseIssuer()
{

}


// it seems we do not need the big number module at all
MPAIValueT CoinbaseIssuer::calcPotentialMicroPaiPerOneCycle(QString year_)
{
  if (year_ == "")
    year_ = CUtils::getCurrentYear();

  auto year = year_.toUInt();
  uint halvingCycleNumber = CUtils::CFloor((year - CConsts::LAUNCH_YEAR) / CConsts::HALVING_PERIOD);
  MPAIValueT one_cycle_max_mili_PAIs = pow(2, (CConsts::COIN_ISSUING_INIT_EXPONENT - halvingCycleNumber));
  return one_cycle_max_mili_PAIs;
}


uint8_t CoinbaseIssuer::calculateReleasableCoinsBasedOnContributesVolume(const uint64_t &total_shares)
{
  uint64_t contributes = CUtils::CFloor((static_cast<double>(total_shares) * 100) / CConsts::WORLD_POPULATION);
  uint8_t releseable_percentage = 1;
  for (uint8_t target: CConsts::MAP_CONTRIBUTE_AMOUNT_TO_MINTING_PERCENTAGE.keys())
  {
    if (contributes >= target)
    {
      releseable_percentage = CConsts::MAP_CONTRIBUTE_AMOUNT_TO_MINTING_PERCENTAGE[target];
    }
  }

  if (releseable_percentage > 100)
    releseable_percentage = 100;
  return releseable_percentage;
}

std::tuple<MPAIValueT, MPAIValueT, uint64_t, UI64DicT> CoinbaseIssuer::calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore(
  const QString &cDate)
{
  MPAIValueT one_cycle_max_mili_PAIs = calcPotentialMicroPaiPerOneCycle(cDate.split('-')[0]);
  auto[total_shares, share_amount_per_holder, holdersOrderByShares_] = DNAHandler::getSharesInfo(cDate);
  Q_UNUSED(holdersOrderByShares_);

  double releseable_percentage = static_cast<double>(calculateReleasableCoinsBasedOnContributesVolume(total_shares)) / 100;
  MPAIValueT oneCycleIssued = CUtils::CFloor(releseable_percentage * one_cycle_max_mili_PAIs);
  return { one_cycle_max_mili_PAIs, oneCycleIssued, total_shares, share_amount_per_holder };
}

QJsonObject CoinbaseIssuer::getCoinbaseTemplateObject()
{
  QJsonObject Jdoc {
    {"hash", ""},
    {"dType", CConsts::DOC_TYPES::Coinbase},
    {"dVer", "0.0.0"},
    {"cycle", ""}, // 'yyyy-mm-dd am' / 'yyyy-mm-dd pm'
    {"treasuryFrom", ""}, // incomes from date
    {"treasuryTo", ""}, // incomes to date
    {"treasuryIncomes", 0}, // incomes value
    {"mintedCoins", 0},
    {"outputs", QJsonArray{}}};
  return Jdoc;
}

/**
 *
 * @param {the time for which cycle is calculated} cycle
 *
 * coinbase core is only shares and dividends,
 * so MUST BE SAME IN EVERY NODES.
 * the inputs are newly minted coins and treasury incomes
 *
 */
QJsonObject CoinbaseIssuer::createCBCore(
  QString cycle,
  const QString &mode)
{
  CLog::log("create CBCore cycle(" + cycle +") mode(" + mode +")", "cb", "info");
  QString cDate = "";
  QString mintingYear = "";
  if (cycle == "")
  {
    cDate = CUtils::getNow();
    cycle = CUtils::getCoinbaseCycleStamp();
    mintingYear = cDate.split(" ")[0].split("-")[0];

  } else {
    if (CConsts::TIME_GAIN == 1)
    {
      // normally the cycle time is 12 hours
      cDate = cycle;

    } else {
      // here is for test net in which the cycle time is accelerated to have a faster block generator(even 2 minutes)
      uint64_t minutes = cycle.split(" ")[1].toUInt() * CMachine::getCycleByMinutes();
      QString minutes_ = CUtils::convertMinutesToHHMM(minutes);
      cDate = cycle.split(" ")[0] + " " + minutes_ + ":00";
    }
    mintingYear = cycle.midRef(0, 4).toString();
  }

  auto[block_creation_date, to_] = CUtils::getCoinbaseRangeByCycleStamp(cycle);
  Q_UNUSED(to_);
  QJsonObject coinbase_document = getCoinbaseTemplateObject();

  coinbase_document["cycle"] = cycle;

  auto[fromDate, toDate, incomes] = TreasuryHandler::calcTreasuryIncomes(cDate);
  CLog::log("coinbase cDate(" + cDate + ") treasury incomes(" + CUtils::sepNum(incomes) + ") micro PAIs from Date(" + fromDate + ") toDate(" + toDate + ")", "cb", "info");

  coinbase_document["treasuryIncomes"] = QVariant::fromValue(incomes).toJsonValue();
  coinbase_document["treasuryFrom"] = fromDate;
  coinbase_document["treasuryTo"] = toDate;

  // create coinbase outputs
  QHash<MPAIValueT, QHash<QString, TmpHolder> > tmpOutDict = {};
  QStringList holders = {};
  QList<MPAIValueT> dividends {};

  /**
  *
  minted: 2,251,799,813.685248
  burned:
  share1:    21,110,874.142979
  share2:       985,017.380061
  share3:       422,068.382528
  share4:            38.230606
  */
  auto[one_cycle_max_mili_PAIs, oneCycleIssued, total_shares, share_amount_per_holder] = calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore(
    block_creation_date);

  CLog::log("share_amount_per_holder: " + CUtils::dumpIt(share_amount_per_holder));
  coinbase_document["mintedCoins"] = QVariant::fromValue(oneCycleIssued).toJsonValue();

  MPAIValueT cycleCoins = coinbase_document["treasuryIncomes"].toDouble() + coinbase_document["mintedCoins"].toDouble();
  CLog::log("DNA cycle sum minted coins+treasury(" + CUtils::sepNum(cycleCoins) + " micro PAIs) . Cycle Max Coins(" + CUtils::sepNum(one_cycle_max_mili_PAIs) + " micro PAIs)", "cb", "info");

  for(QString aHolder: share_amount_per_holder.keys())
  {
    uint64_t aShare= share_amount_per_holder[aHolder];
    MPAIValueT dividend = CUtils::CFloor((CUtils::iFloorFloat(aShare / total_shares) * cycleCoins));

    holders.append(aHolder);
    dividends.append(dividend);
    if (!tmpOutDict.keys().contains(dividend))
        tmpOutDict[dividend] = QHash<QString, TmpHolder> {};
    tmpOutDict[dividend][aHolder] = TmpHolder {aHolder, dividend};

  };
  // in order to have unique hash for coinbase block (even created by different backers) sort it by sahres desc, addresses asc
  dividends = CUtils::listUnique(dividends);
  dividends = CUtils::intReverseSort(dividends);
  holders.sort();
  std::reverse(holders.begin(), holders.end());

  QJsonArray outputs {};
  for (MPAIValueT dividend: dividends)
  {
    for (QString holder: holders)
    {
      if ((tmpOutDict.keys().contains(dividend)) && (tmpOutDict[dividend].keys().contains(holder)))
      {
        QJsonArray output_arr {
          tmpOutDict[dividend][holder].holder,
          QVariant::fromValue(tmpOutDict[dividend][holder].dividend).toDouble()
        };
        outputs.append(output_arr);
      }
    }
  }
  coinbase_document["outputs"] = outputs;

  Document* doc = DocumentFactory::create(coinbase_document);
  coinbase_document["hash"] = doc->m_doc_hash; // trxHashHandler.doHashTransaction(trx)
  delete doc;

  QJsonObject Jblock = coinbaseBlockVersion0;
  Jblock["cycle"] = cycle;
  Jblock["docs"] = QJsonArray {coinbase_document};
  auto[root, verifies, version, levels, leaves] = CMerkle::generate({coinbase_document["hash"].toString()});
  Q_UNUSED(verifies);
  Q_UNUSED(version);
  Q_UNUSED(levels);
  Q_UNUSED(leaves);
  Jblock["docsRootHash"] = root;
  Jblock["creationDate"] = block_creation_date;

  return Jblock;
}

/**
 * 
 * @param {*} cycle 
 * 
 * although the coinbase core is only shares (which are equal in entire nodes)
 * but the final coinbase block consists of also ancestors links (which are participating in block hash)
 * so it could be possible different nodes generate different coinbaseHash for same blocks.
 * 
 */
std::tuple<bool, QJsonObject> CoinbaseIssuer::doGenerateCoinbaseBlock(
  const QString &cycle,
  const QString &mode)
{
  CLog::log("do GenerateCoinbaseBlock cycle(" + cycle + ") mode(" + mode + ")", "cb", "info");
  auto[cycleStamp, from_, to_, fromHour, toHour] = CUtils::getCoinbaseInfo("", cycle);
  Q_UNUSED(cycleStamp);
  Q_UNUSED(fromHour);
  Q_UNUSED(toHour);

  QJsonObject Jblock = createCBCore(cycle, mode);
  
  // connecting to existed leaves as ancestors
  QJsonObject leaves = LeavesHandler::getLeaveBlocks(from_);
  QStringList leaves_hashes = leaves.keys();
  leaves_hashes.sort();
  CLog::log("do GenerateCoinbaseBlock retrieved cbInfo: from_(" + from_ +") to_(" + to_ +")", "cb", "info");
  CLog::log("do GenerateCoinbaseBlock retrieved leaves from kv: cycle(" + cycle +") leaves_hashes(" + leaves_hashes.join(", ") +") leaves(" + CUtils::dumpIt(leaves) +")", "cb", "info");

  auto[confidence_, blockHashes_, backers_] = FloatingSignatureBlock::aggrigateFloatingSignatures();
  Q_UNUSED(backers_);
//  clog.cb.info(`locally created block's confidence: ${JSON.stringify(floatingSignatures)}`);
  Jblock["confidence"] = confidence_;
  leaves_hashes = CUtils::arrayUnique(CUtils::arrayAdd(leaves_hashes, blockHashes_));
  
  // if requested cycle is current cycle and machine hasn't fresh leaves, so can not generate a CB block
  if ((mode == CConsts::STAGES::Creating) && (leaves_hashes.size() == 0) && (cycle == CUtils::getCoinbaseCycleStamp()))
  {
    if (mode == CConsts::STAGES::Creating)
    {
      CLog::log("generating new CB in generating mode faild!! leaves(" + leaves_hashes.join(",") + ")", "sb", "info");
    } else {
      CLog::log("strange error generating new CB faild!! mode(" + mode + ") mode(" + leaves_hashes.join(",") + ") ", "sb", "error");
    }
    return {false, Jblock};
  }
  
  Jblock["ancestors"] = QVariant(leaves_hashes).toJsonArray();
  // block.signals  = iutils.getMachineSignals();   // since it is not going to broadcost, so do not need the signals 
  CLog::log("do GenerateCoinbaseBlock block.ancestors: " + leaves_hashes.join(","), "cb", "info");
  
  
  // if the backer is also a shareholder (probably with large amount of shares), 
  // would be more usefull if she also signs the dividends by his private key as a shareholder 
  // and sends also her corresponding publick key
  // this signature wil be used for 2 reson
  // 1. anti rebuild DAG by adverseries
  // 2. prevent fake sus-blocks to apply to network
  // signing block by backer private key (TODO: or delegated private key for the sake of security)
  


  // clog.app.info(blockPresenter(block));
  return {true, Jblock};
}
