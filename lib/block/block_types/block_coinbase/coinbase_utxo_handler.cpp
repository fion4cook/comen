#include "stable.h"
#include "coinbase_utxo_handler.h"


CoinbaseUTXOHandler::CoinbaseUTXOHandler()
{

}


void CoinbaseUTXOHandler::recursiveImportUTXOs()
{
  importCoinbasedUTXOs(CUtils::getNow());
}


void CoinbaseUTXOHandler::importCoinbasedUTXOs(const QString &cDate)
{
  CLog::log("importCoinbasedUTXOs " + cDate);
  CLog::log("importCoinbasedUTXOs");
  CLog::log("importCoinbasedUTXOs");
  CLog::log("importCoinbasedUTXOs");
  CLog::log("importCoinbasedUTXOs");
  CLog::log("importCoinbasedUTXOs");
  auto tid = std::this_thread::get_id();
  std::cout<< std::endl << "this_thread importCoinbasedUTXOs " << tid << std::endl;

  //FIXME: complete implementation

}

//TODO some uintteasts need
// every coinbased incomes will be spendable after 2 cycle and right after starting 3rd cycle
QString CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate(QString cDate)
{
  if (cDate == "")
    cDate = CUtils::getNow();

  QString matureDate = CUtils::minutesAfter(CConsts::COINBASE_MATURATION_CYCLES * CMachine::getCycleByMinutes(), cDate);
  auto[fromm_, to_] = CUtils::getCoinbaseRange(matureDate);
  Q_UNUSED(to_);
  return fromm_;
}
