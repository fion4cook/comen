#ifndef COINBASEISSUER_H
#define COINBASEISSUER_H


struct TmpHolder {
  QString holder = "";
  MPAIValueT dividend = 0;
};

class CoinbaseIssuer
{
public:
  CoinbaseIssuer();
  static uint8_t calculateReleasableCoinsBasedOnContributesVolume(const uint64_t &sumShares);
  static QJsonObject getCoinbaseTemplateObject();
  static MPAIValueT calcPotentialMicroPaiPerOneCycle(QString year = "");
  static std::tuple<MPAIValueT, MPAIValueT, uint64_t, UI64DicT> calcDefiniteReleaseableMicroPaiPerOneCycleNowOrBefore(
    const QString &cDate = CUtils::getNow());

  static QJsonObject createCBCore(
    QString cycle,
    const QString &mode = CConsts::STAGES::Creating);

  static std::tuple<bool, QJsonObject> doGenerateCoinbaseBlock(
    const QString &cycle,
    const QString &mode = CConsts::STAGES::Creating);

};

#endif // COINBASEISSUER_H
