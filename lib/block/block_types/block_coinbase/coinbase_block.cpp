#include "stable.h"

#include "lib/ccrypto.h"
#include "lib/block/block_types/block_coinbase/coinbase_issuer.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/block/block_types/block_factory.h"
#include "coinbase_block.h"

CoinbaseBlock::CoinbaseBlock(const QJsonObject &obj)
{
  setByJsonObj(obj);
}


bool CoinbaseBlock::setByJsonObj(const QJsonObject &obj)
{
  Block::setByJsonObj(obj);

  // maybe drived class assignings

//  m_block_hash = calcBlockHash();

  return true;
}


/**
* @brief CoinbaseBlock::handleReceivedBlock(
* @return <status, should_purge_record>
*/
std::tuple<bool, bool> CoinbaseBlock::handleReceivedBlock() const
{

  auto[status, should_purge_record] = validateCoinbaseBlock();
  CLog::log("Received validate CoinbaseBlock result: status(" + CUtils::dumpIt(status) + ") should_purge_record(" + CUtils::dumpIt(should_purge_record) + ")", "cb", "trace");
  if (!status) {
    // do something (e.g. bad reputation log for sender neighbor)
    return {false, true};
  }

  CLog::log("dummy log pre broadcating CoinbaseBlock: " + CUtils::serializeJson(exportBlockToJSon()), "cb", "trace");

  addBlockToDAG();

  postAddBlockToDAG();


  // broadcast block to neighbors
  if (CUtils::isInCurrentCycle(m_block_creation_date))
  {
    bool pushRes = SendingQHandler::pushIntoSendingQ(
      CConsts::BLOCK_TYPES::Coinbase,
      m_block_hash,
      CUtils::serializeJson(exportBlockToJSon()),
      "Broadcasting the confirmed coinbase block(" + CUtils::hash8c(m_block_hash) + ") in current cycle");

    CLog::log("coinbase pushRes(" + CUtils::dumpIt(pushRes) + ")");
  }

  return {true, true};
}


/**
* @brief CoinbaseBlock::validateCoinbaseBlock
* @return <status, should_purge_record>
*/
std::tuple<bool, bool> CoinbaseBlock::validateCoinbaseBlock() const
{

  auto[cycleStamp, from_, to_, fromHour, toHour] = CUtils::getCoinbaseInfo("", m_cycle);
  Q_UNUSED(cycleStamp);
  Q_UNUSED(fromHour);
  Q_UNUSED(toHour);
  CLog::log("\nValidate Coinbase Block(" + CUtils::hash8c(m_block_hash) + ") cycle:(" + m_cycle + ") from:(" + from_ + ") to:(" + to_ + ")", "cb", "info");
//  let res = { err: true, shouldPurgeMessage: true, msg: '', sender: sender };


  // in case of synching, we force machine to (maybe)conclude the open pollings
  // this code should be somewhere else, because conceptually it has nothing with coinbase flow!
//  if (CMachine::isInSyncProcess())
//    doPollingConcludeTreatment({
//        cDate: remoteCB.creationDate
//    });

  auto[status, local_regenerated_coinbase] = CoinbaseIssuer::doGenerateCoinbaseBlock(
    m_cycle,
    CConsts::STAGES::Regenerating);
  Q_UNUSED(status);

  local_regenerated_coinbase["confidence"] = m_block_confidence;
  local_regenerated_coinbase["backer"] = m_backer;

  Block* tmp_block = BlockFactory::create(local_regenerated_coinbase);
  tmp_block->m_signals = m_signals;
  tmp_block->m_block_hash = tmp_block->calcBlockHash();
  local_regenerated_coinbase["blockHash"] = tmp_block->m_block_hash;
  QJsonObject tmp_Jblock = tmp_block->exportBlockToJSon();
//  tmp_Jblock["blockLength"] = CUtils::paddingDocLength(CUtils::serializeJson(tmp_Jblock).length());
  CLog::log("dummy dumping after calculating it's length(serialized): " + CUtils::serializeJson(tmp_Jblock) , "cb", "info");
  delete tmp_block;

  local_regenerated_coinbase["blockLength"] = CUtils::paddingDocLength(CUtils::serializeJson(local_regenerated_coinbase).length());
  CLog::log("dummy dumping local_regenerated_coinbase beofr calculating it's length(serialized): " + CUtils::serializeJson(local_regenerated_coinbase) , "cb", "info");
  CLog::log("dummy dumping local_regenerated_coinbase beofr calculating it's length(object): " + CUtils::dumpIt(local_regenerated_coinbase) , "cb", "info");

  if (tmp_Jblock.value("blockHash").toString() == "")
  {
    CLog::log("big failllllllll 1 . Regenerated coinbase in local has no hash! " + CUtils::dumpIt(tmp_Jblock), "cb", "error");
    return {false, true};
  }

  if (tmp_Jblock.value("blockHash").toString() != m_block_hash)
  {
    CLog::log("big failllllllll2 in Regenerating coinbase in local, has differetnt hash! " + CUtils::dumpIt(tmp_Jblock), "cb", "error");
    return {false, true};
  }

  if (tmp_Jblock.value("docsRootHash").toString() != m_docs_root_hash)
  {
    QString msg = "Discrepancy in docsRootHash locally created coinbase block(" + CUtils::hash8c(tmp_Jblock.value("docsRootHash").toString());
    msg += ") and remote(" + CUtils::hash8c(m_docs_root_hash) + ") ";
    CLog::log(msg, "cb", "error");
    CLog::log("Remote block" + dumpBlock(), "cb", "error");
    CLog::log("Local regenrated block" + CUtils::dumpIt(tmp_Jblock), "cb", "error");
    return {false, true};
  }

  CLog::log("remoteConfidence(" + QString::number(m_block_confidence)+ ")", "cb", "info");
  CLog::log("Valid Coinbase block has received. Block(" + m_block_hash + ")", "cb", "info");
  return {true, true};
}

QString CoinbaseBlock::getBlockHashableString() const
{
  // in order to have almost same hash! we sort the attribiutes alphabeticaly
  QString hashable_block = "{";
  hashable_block += "\"ancestors\":" + CUtils::serializeJson(QVariant::fromValue(m_ancestors).toJsonArray()) + ",";

  // putting blockLength in conbase blockHash is missed!
  // in next upgrade we must put block length in block hash as well
  if(m_block_version > "0.0.0")
    hashable_block += "\"blockLength\":\"" + CUtils::paddingDocLength(m_block_length) + "\",";

  hashable_block += "\"bType\":\"" + m_block_type + "\",";
  hashable_block += "\"bVer\":\"" + m_block_version + "\",";
  hashable_block += "\"confidence\":" + QString("%1").arg(m_block_confidence) + ",";
  hashable_block += "\"creationDate\":\"" + m_block_creation_date + "\",";
  hashable_block += "\"cycle\":\"" + m_cycle + "\",";
  if(m_descriptions != "")
    hashable_block += "\"descriptions\":\"" + m_descriptions + "\",";
  hashable_block += "\"net\":\"" + m_net + "\",";
  hashable_block += "\"docsRootHash\":\"" + m_docs_root_hash + "\"}";  // note that we do not put the docsHash directly in block hash, instead using docsHash-merkle-root-hash
  return hashable_block;
}

QString CoinbaseBlock::calcBlockHash() const
{
  QString hashable_block = getBlockHashableString();

  // clonedTransactionsRootHash: block.clonedTransactionsRootHash,
  // note that we do not put the clonedTransactions directly in block hash,
  // instead using clonedTransactions-merkle-root-hash

  CLog::log("The Coinbase! block hashable: " + hashable_block + "\n", "app", "trace");
  return CCrypto::keccak256(hashable_block);
}

QJsonObject CoinbaseBlock::exportBlockToJSon() const
{
  QJsonObject out = Block::exportBlockToJSon();
  out.insert("confidence", m_block_confidence);
  out.insert("cycle", m_cycle);
  out["blockLength"] = CUtils::paddingDocLength(calcBlockLength(out));
  return out;
}

BlockLenT CoinbaseBlock::calcBlockLength(const QJsonObject &block_obj) const
{
  // in order to support backwardcompatibility with DAG history
  // for coinbase block with version under 0.0.1 some cumstomizations needed
  if(m_block_version == "0.0.0")
  {
    QJsonObject tmp = block_obj;
    tmp.remove("signal");
    tmp.remove("blockLength");
    tmp.remove("confidence");
    return CUtils::serializeJson(tmp).length();
  }
  return CUtils::serializeJson(block_obj).length();
}
