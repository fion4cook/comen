#ifndef COINBASEBLOCK_H
#define COINBASEBLOCK_H


class CoinbaseBlock : public Block
{
public:
  CoinbaseBlock(const QJsonObject &obj);

  bool setByJsonObj(const QJsonObject &obj) override;
  std::tuple<bool, bool> handleReceivedBlock() const override;

  QString getBlockHashableString() const override;
  QString calcBlockHash() const override;
  QJsonObject exportBlockToJSon() const override;
  BlockLenT calcBlockLength(const QJsonObject &block_obj) const override;

  //  -  -  -  not drived methods
  std::tuple<bool, bool> validateCoinbaseBlock() const;

};


#endif // COINBASEBLOCK_H
