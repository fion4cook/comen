#ifndef DOCUMENTFACTORY_H
#define DOCUMENTFACTORY_H


class DocumentFactory
{
public:
  DocumentFactory();
  static Document* create(const QJsonObject &obj);
};

#endif // DOCUMENTFACTORY_H
