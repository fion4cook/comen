#include "stable.h"

#include "document.h"
#include "lib/dag/society_rules/society_rules.h"
#include "basic_tx_document.h"


BasicTxDocument::BasicTxDocument(const QJsonObject &obj)
{
  setByJsonObj(obj);
}

BasicTxDocument::~BasicTxDocument()
{
  deleteInputs();
  deleteOutputs();
}



bool BasicTxDocument::setByJsonObj(const QJsonObject &obj)
{
  Document::setByJsonObj(obj);

  return true;
}

QString BasicTxDocument::stringifyDoc() const
{
  return "";
}

QString BasicTxDocument::calcDocumentExtInfoRootHash() //calcTrxExtRootHash()
{
  QStringList hashes = {};
  for (QJsonValueRef anExtInfo: m_doc_ext_object)
  {
//    hashes.append(anExtInfo.toObject().value("").toString()));
    //hashes.push(iutils.doHashObject(anExtInfo));
  }

  auto[docsRootHash, final_verifies, version, levels, leaves] = CMerkle::generate(hashes);
  Q_UNUSED(final_verifies);
  Q_UNUSED(version);
  Q_UNUSED(levels);
  Q_UNUSED(leaves);
  return docsRootHash;
}

std::tuple<bool, uint64_t> BasicTxDocument::calcDocumentDataAndProcessCost(
  const QString &stage,
  QString cDate,
  const uint32_t &extra_length) //calcTrxDPCost
{
  if(cDate == "")
    cDate =CUtils::getNow();

  DocLenT dLen = m_doc_length;

  if (stage == CConsts::STAGES::Creating)
    dLen += extra_length + CConsts::TRANSACTION_PADDING_LENGTH;

  if (stage == CConsts::STAGES::Validating)
  {
    if (dLen != static_cast<DocLenT>(stringifyDoc().length()))
    {
      CLog::log("The trx len and local re-calc len are not same! stage(" + stage + ") remoteLen(" + QString::number(dLen) + ") local Len(" + QString::number(stringifyDoc().length()) + ") trx(" + CUtils::hash8c(m_doc_hash) + ")", "trx", "error");
      return {false, 0};
    }
  } else {
      if (dLen < static_cast<DocLenT>(stringifyDoc().length()))
      {
        CLog::log("The trx len and local re-calc len are not same! stage(" + stage + ") remoteLen(" + QString::number(dLen) + ") local Len(" + QString::number(stringifyDoc().length()) + ") trx(" + CUtils::hash8c(m_doc_hash) + ")", "trx", "error");
        return {false, 0};
      }
  }

  if (m_doc_class == CConsts::TRX_CLASSES::P4P)
    dLen = dLen * getDPIs().size();  // the transaction which new transaction is going to pay for

  uint64_t theCost =
    dLen *
    SocietyRules::getBasePricePerChar(cDate) *
    SocietyRules::getDocExpense(m_doc_type, dLen, m_doc_class, cDate);

  if (stage == CConsts::STAGES::Creating)
    theCost = theCost * CMachine::getMachineServiceInterests(
      m_doc_type,
      m_doc_class,
      dLen,
      extra_length,
      getDPIs().size());

  return {true, trunc(theCost) };
}

bool BasicTxDocument::deleteInputs()
{
  for (TInput* an_inptu: m_inputs)
    delete an_inptu;
  return true;
}

bool BasicTxDocument::deleteOutputs()
{
  for (TOutput* an_output: m_outputs)
    delete an_output;
  return true;
}

QJsonObject BasicTxDocument::exportToJson() const
{
  CUtils::exiter("exportToJson is n't implement for basic trx Base class", 0);
  return QJsonObject {};
}

std::tuple<bool, QJsonArray> BasicTxDocument::exportOutputsToJson() const
{
  CUtils::exiter("export OutputsToJson is n't implement for BasicTxDocument Base class", 0);
  return {false, QJsonArray {}};
}

bool BasicTxDocument::applyDocImpact1(const Block &block) const
{
  return false;
}


QString BasicTxDocument::getRef()
{
  return m_ref;
}

QVector<OutputIndexT> BasicTxDocument::getDPIs()
{
  return m_data_and_process_payment_indexes;
}

QString BasicTxDocument::getNonce()
{
  return "";  // only free doc (by POW) can have nonce and m_nonce; member
}

QVector<TInput*> BasicTxDocument::getInputs()
{
  return m_inputs;
}

QVector<TOutput*> BasicTxDocument::getOutputs()
{
  return m_outputs;
}

std::tuple<bool, uint64_t, uint64_t> BasicTxDocument::retrieveDPCostInfo(
  const QString &block_hash,
  const QString &backer)
{

  /**
  * the block cost payment transaction is a document that always has to has no input and 2 outputs.
  * 0. TP_DP   Treasury Payment Data & Process Cost
  * 1. backer fee
  */
  if (
    (m_outputs.size() != 2) ||
    (m_outputs[0]->m_address != "TP_DP") ||
    (m_outputs[1]->m_address != backer))
  {
    CLog::log("Invalid treasury payment in block(" + CUtils::hash8c(block_hash) + ") doc(" + CUtils::hash8c(m_doc_hash) + ")! ", "trx", "error");
    return {false, 0, 0};
  }

  if (static_cast<DocLenT>(stringifyDoc().length()) > CConsts::MAX_DPCostPay_DOC_SIZE)
  {
    CLog::log("Invalid treasury payment doc length in block(" + CUtils::hash8c(block_hash) + ") doc(" + CUtils::hash8c(m_doc_hash) + ")! ", "trx", "error");
    return {false, 0, 0};
  }

  uint64_t treasuryIncomes = m_outputs[0]->m_value;
  uint64_t backerIncomes = m_outputs[1]->m_value;

  return { true, treasuryIncomes, backerIncomes };
}
