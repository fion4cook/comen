#ifndef ARBITRARYDOC_H
#define ARBITRARYDOC_H

#include "lib/block/block_types/block.h"

class ArbitraryDoc
{
public:
  ArbitraryDoc();
  virtual bool applyDocImpact1(const Block &block) const;
};

#endif // ARBITRARYDOC_H
