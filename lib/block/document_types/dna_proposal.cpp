#include <stable.h>
#include "dna_proposal.h"

DNAProposal::DNAProposal()
{
  m_doc_type = CConsts::DOCUMENT_TYPES::DNAProposal;
  m_doc_version = "0.0.7";
}

const QString DNAProposal::stbl_dna_shares = "c_dna_shares";
const QString DNAProposal::stbl_machine_draft_proposals = "c_machine_draft_proposals";
const QString DNAProposal::stbl_proposals = "c_proposals";


QString DNAProposal::stringifyDoc() const
{

  QStringList docSegments = {
    QString::fromStdString({R"({)"}),

    QString::fromStdString({R"("creationDate":")"}),
    m_doc_creation_date,

    QString::fromStdString({R"(","descriptions":")"}),
    m_doc_descriptions,

    QString::fromStdString({R"(","dType":"DNAProposal","dVer":")"}),
    m_doc_version,

    QString::fromStdString({R"(","helpHours":)"}),
    QString("%1").arg(m_help_hours),

    QString::fromStdString({R"(,"helpLevel":)"}),
    QString("%1").arg(m_help_level),

//    QString::fromStdString({R"(","net":")"}),
//    QString::fromStdString(m_net),

    QString::fromStdString({R"(,"projectHash":")"}),
    m_project_hash,

    QString::fromStdString({R"(","shareholder":")"}),
    m_shareholder,

    QString::fromStdString({R"(","tags":")"}),
    m_doc_tags,

    QString::fromStdString({R"(","title":")"}),
    m_doc_title,

    QString::fromStdString({R"(","shares":)"}),
    QString("%1").arg(m_shares),

    QString::fromStdString({R"(})"})

  };

  QString customStringified = docSegments.join("");
  CLog::log("custom Stringified Document: " + customStringified);
  return customStringified;
}

bool DNAProposal::applyDocImpact1(const Block &block) const
{
  // recording block in DAG means also starting voting period for proposals inside block(if exist)
  // it implicitly leads to create a new polling and activate it in order to collect shareholders opinion about proposal
  CLog::log("Try to activate Proposal Polling(" + CUtils::hash8c(m_doc_hash) + ") Block(" + CUtils::hash8c(block.m_block_hash) + ")");  //  activatePollingForProposal(block);

  // record in c_proposals (i_proposal)
  CLog::log("record A New Proposal args: ");
  QueryRes dbl = DbModel::select(
    DNAProposal::stbl_machine_draft_proposals,
    QStringList {"pr_hash"},
    {ModelClause("pr_hash", m_doc_hash)} //clauses
  );
  if (dbl.records.size()> 0) {
    QString msg = "try to double insert existed proposal " + CUtils::hash8c(m_doc_hash);
    CLog::log(msg, "sec", "error");
  }
  uint64_t helpLevel = static_cast<uint64_t>(m_help_level);
  QVDicT values;
  values.insert("pr_hash", m_doc_hash);
  values.insert("pr_type", m_doc_type);
  values.insert("pr_class", m_doc_class);
  values.insert("pr_version", m_doc_version);
  values.insert("pr_title", m_doc_title);
  values.insert("pr_descriptions", m_doc_descriptions);
  values.insert("pr_tags", m_doc_tags);
  values.insert("pr_project_id", m_project_hash);
  values.insert("pr_help_hours", QString("%1").arg(m_help_hours));
  values.insert("pr_help_level", QString("%1").arg(helpLevel));
  values.insert("pr_voting_longevity", QString("%1").arg(m_voting_longevity));
  values.insert("pr_polling_profile", m_polling_profile);
  values.insert("pr_cotributer_account", m_shareholder);
  values.insert("pr_start_voting_date", block.m_block_creation_date);
  values.insert("pr_conclude_date", QString::fromStdString(""));
  values.insert("pr_approved", CConsts::NO);
  DbModel::insert(
    DNAProposal::stbl_proposals,
    values
  );


  // create a new polling
  QJsonObject params = QJsonObject
  {
    {"dType", CConsts::DOCUMENT_TYPES::Polling},
    {"dClass", m_polling_profile}, // default is iConsts.POLLING_PROFILE_CLASSES.Basic.ppName
    {"dVer", m_doc_version},
    {"ref", m_doc_hash},
    {"refType", CConsts::POLLING_REF_TYPE::Proposal},
    {"refClass", CConsts::PLEDGE_CLASSES::PledgeP},
    {"startDate", block.m_block_creation_date},
    {"longevity", QJsonValue(QString("%1").arg(m_voting_longevity))},
    {"creator", m_shareholder},
    {"comment", "Polling for proposal(" + CUtils::hash6c(m_doc_hash) + "), " + m_doc_title + " "},
    {"status", CConsts::OPEN}
  };

  bool res = PollingHandler::autoCreatePollingForProposal(params);
  return res;
}

GenRes DNAProposal::insertDocShares()
{
  QueryRes exist = DbModel::select(
    DNAProposal::stbl_dna_shares,
    QStringList {"dn_doc_hash"},     // fields
    {ModelClause("dn_doc_hash", m_doc_hash)}
    );
  if (exist.records.size() > 0)
  {
    // maybe some updates
    return
    {
      false,
      "The DNA document (${utils.hash6c(dna.hash)}) is already recorded"
    };
  }

  return { false };
}

QJsonObject DNAProposal::exportJson()
{
  QJsonObject params = QJsonObject
  {
    {"m_doc_hash", m_doc_hash},
    {"m_doc_tags", m_doc_tags},
    {"m_project_hash", m_project_hash},
    {"m_doc_title", m_doc_title},
    {"m_doc_descriptions", m_doc_descriptions},
    {"m_approval_date", m_approval_date},
    {"m_shareholder", m_shareholder}, // creator
    {"m_help_hours", QString::number(m_help_hours)},
    {"m_help_level", QString::number(m_help_level)},
    {"m_shares", QString::number(m_shares)},
    {"m_votesY", QString::number(m_votesY)},
    {"m_votesA", QString::number(m_votesA)},
    {"m_votesN", QString::number(m_votesN)},
    {"m_voting_longevity", QString::number(m_voting_longevity)},
    {"m_polling_profile", m_polling_profile},
    {"m_doc_creation_date", m_doc_creation_date},
    {"m_block_creation_date", m_block_creation_date},


  //    {"comment", QJsonValue(QString::fromStdString("Polling for proposal(" + CUtils::hash6c(proposal.m_doc_hash) + "), " + proposal.m_doc_title + " "))},
  //    {"status", QJsonValue(QString::fromStdString(CConsts::OPEN))}
    };
  return params;
}

GenRes DNAProposal::updateProposal(
    const QVDicT &values,
    const ClausesT &clauses,
    const bool &is_transactional)
{
  DbModel::update(
    DNAProposal::stbl_proposals,
    values, // update values
    clauses,  // update clauses
    is_transactional
    );

  return { true };
}


