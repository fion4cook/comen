#ifndef RPDOCDOCUMENT_H
#define RPDOCDOCUMENT_H


class RpDocDocument : public Document
{
public:
  RpDocDocument(const QJsonObject &obj);
  bool setByJsonObj(const QJsonObject &obj) override;
};

#endif // RPDOCDOCUMENT_H
