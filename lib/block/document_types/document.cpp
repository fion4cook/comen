#include "stable.h"

#include "lib/ccrypto.h"

class Block;
//class DNAHandler;
class DbHandler;

#include "lib/block/block_types/block.h"
#include "document.h"

const QString Document::stbl_docs_blocks_map = "c_docs_blocks_map";


Document::~Document()
{

}

QString Document::calcDocumentExtInfoRootHash()
{
  return "";
}

bool Document::setByJsonObj(const QJsonObject &obj)
{
  QStringList object_keys = obj.keys();

  if (obj.value("hash").toString() != "")
    m_doc_hash = obj.value("hash").toString();

  if (obj.value("dType").toString() != "")
    m_doc_type = obj.value("dType").toString();

  if (obj.value("dClass").toString() != "")
    m_doc_class = obj.value("dClass").toString();

  if (obj.value("dVer").toString() != "")
    m_doc_version = obj.value("dVer").toString();

//  if (obj.value("m_doc_hash").toString() != "")
//    m_doc_hash = obj.value("m_doc_hash").toString();


//  QString m_doc_title = "";
//  QString m_doc_descriptions = "";
//  QString m_doc_tags = "";
//  QString m_doc_creation_date = "";
//  QString m_block_creation_date = "";

  return true;
}

QString Document::stringifyDoc() const
{
  return "";
}

QString Document::getRef()
{
  CUtils::exiter("m_ref is n't implement for Document Base class", 0);
  return "";
}

bool Document::deleteInputs()
{
  CUtils::exiter("deleteInputs is n't implement for Document Base class", 0);
  return false;
}

bool Document::deleteOutputs()
{
  CUtils::exiter("deleteOutputs is n't implement for Document Base class", 0);
  return false;
}

std::tuple<bool, QJsonArray> Document::exportOutputsToJson() const
{
  CUtils::exiter("export OutputsToJson is n't implement for Document Base class", 0);
  return {false, QJsonArray {}};
}

QJsonObject Document::exportToJson() const
{
  QJsonObject document {
    {"hash", m_doc_hash},
    {"dType", m_doc_type},
    {"dVer", m_doc_version}
  };

  // maybe add outputs
  auto[has_output, Joutputs] = exportOutputsToJson();
  if(has_output)
      document["outputs"] = Joutputs;

  return document;
}

QVector<OutputIndexT> Document::getDPIs()
{
  CUtils::exiter("m data_and_process_payment_indexes is n't implement for Base class", 0);
  return {};
}

QString Document::getNonce()
{
  CUtils::exiter("m_nonce is n't implement for Base class", 0);
  return "";
}

QVector<TInput*> Document::getInputs()
{
  CUtils::exiter("m_inputs isn't implement for Base class", 0);
  return {};
}

QVector<TOutput*> Document::getOutputs()
{
  CUtils::exiter("m_outputs isn't implement for Base class", 0);
  return {};
}

std::tuple<bool, uint64_t, uint64_t> Document::retrieveDPCostInfo(
  const QString &block_hash,
  const QString &backer)
{
  return {false, 0, 0};
}

QString Document::getDocHashableString()
{

}

QString Document::calcDocHash()
{
//  QString customStringified = stringifyDoc();
//  QString hash = CCrypto::keccak256(customStringified);
//  CLog::log("hash " + hash);
//  return hash;
  return "";
}

void Document::setHash()
{
  m_doc_hash = calcDocHash();
}

/**
 * @brief Document::applyDocImpact1
 * depends on the document type, the node does some impacts on database
 * e.g. records a FleNS in DB
 */
bool Document::applyDocImpact1(const Block &block) const
{
  Q_UNUSED(block);
  return true;
}

QJsonObject Document::exportJson()
{
  QJsonObject r;
  return r;
}

std::tuple<bool, uint64_t> Document::calcDocumentDataAndProcessCost(
  const QString &stage,
  QString cDate,
  const uint32_t &extraLength)
{
  Q_UNUSED(stage);
  Q_UNUSED(cDate);
  Q_UNUSED(extraLength);
  return {false, 0};
}

GenRes Document::applyDocImpact2()
{
  return
  {
    false
  };
}


/**
 * @brief Document::mapDocToBlock
 * inserts a row in table "c_docs_blocks_map" to connect documents to block
 */
void Document::mapDocToBlock(const QString &blockHash, const uint32_t &docInx)
{

  QVDicT values {
    {"dbm_block_hash", blockHash},
    {"dbm_doc_index", docInx}, //QString("%1").arg(docInx)
    {"dbm_doc_hash", m_doc_hash},
    {"dbm_last_control", CUtils::getNow()}};

  CLog::log("--- connecting Doc(" + CUtils::hash8c(m_doc_hash) + ") to Block(" + CUtils::hash8c(blockHash) + ")");
  DbModel::insert(
      Document::stbl_docs_blocks_map,     // table
      values, // values to insert
      true
  );

}

bool Document::setDocumentOutputs(const QJsonValue &obj)
{
  return false;
}

bool Document::trxHasInput(const QString &document_type)
{
  return QStringList {
    CConsts::DOC_TYPES::BasicTx,
    CConsts::DOC_TYPES::RpDoc
  }.contains(document_type);
}

bool Document::trxHasInput()
{
  return Document::trxHasInput(m_doc_type);
}

bool Document::trxHasNotInput(const QString &document_type)
{
  return QStringList {
    CConsts::DOC_TYPES::Coinbase,
    CConsts::DOC_TYPES::DPCostPay,
    CConsts::DOC_TYPES::RlDoc
  }.contains(document_type);
}

bool Document::trxHasNotInput()
{
  return Document::trxHasNotInput(m_doc_type);
}

bool Document::isBasicTransaction(const QString &dType)
{
  // Note: the iConsts.DOC_TYPES.Coinbase  and iConsts.DOC_TYPES.DPCostPay altough are transactions, but have special tretmnent
  // Note: the iConsts.DOC_TYPES.RpDoc altough is a transaction, but since is created directly by node and based on validated coinbase info, so does not need validate
  return QStringList{CConsts::DOC_TYPES::BasicTx}.contains(dType);
}

bool Document::isDPCostPayment(const QString &dType)
{
  return QStringList{CConsts::DOC_TYPES::DPCostPay}.contains(dType);
}

bool Document::canBeACostPayerDoc(const QString &dType)
{
  return QStringList{CConsts::DOC_TYPES::BasicTx}.contains(dType);
}

/**
 * The documents which do not need another doc to pay their cost.
 * instead they can pay the cost of another docs
 *
 * @param {} dType
 */
bool Document::isNoNeedCostPayerDoc(const QString &dType)
{
  return (QStringList {
    CConsts::DOC_TYPES::BasicTx,
    CConsts::DOC_TYPES::DPCostPay,
    CConsts::DOC_TYPES::RpDoc,
    CConsts::DOC_TYPES::RlDoc
  }.contains(dType));
}
