#include "stable.h"

#include "document.h"
#include "lib/ccrypto.h"
#include "coinbase_document.h"


CoinbaseDocument::CoinbaseDocument(const QJsonObject &obj)
{
  setByJsonObj(obj);
   m_doc_hash = calcDocHash();
}


CoinbaseDocument::~CoinbaseDocument()
{
  deleteOutputs();
}

bool CoinbaseDocument::setByJsonObj(const QJsonObject &obj)
{

  Document::setByJsonObj(obj);

  if (obj.value("cycle").toString() != "")
    m_doc_cycle = obj.value("cycle").toString();

  if (obj.value("treasuryFrom").toString() != "")
    m_treasury_from = obj.value("treasuryFrom").toString();

  if (obj.value("treasuryTo").toString() != "")
    m_treasury_to = obj.value("treasuryTo").toString();

  if (obj.value("mintedCoins").toDouble() > 0)
    m_minted_coins = obj.value("mintedCoins").toDouble();

  if (obj.value("treasuryIncomes").toDouble() > 0)
      m_treasury_incomes = obj.value("treasuryIncomes").toDouble();

  auto ooo = obj.value("outputs");
  if (obj.value("outputs").toArray().size() > 0)
      setDocumentOutputs(obj.value("outputs"));

  //  QVector<TOutput> m_outputs = {};

  //  QString  = CConsts::DOCUMENT_TYPES::customDoc;
  //  QString m_doc_class = CConsts::GENERAL;
  //  QString  = CConsts::DEFAULT_DOCUMENT_VERSION;
  //  QString m_doc_title = "";
  //  QString m_doc_descriptions = "";
  //  QString m_doc_tags = "";
  //  QString m_doc_creation_date = "";
  //  QString m_block_creation_date = "";

  return true;
}

bool CoinbaseDocument::setDocumentOutputs(const QJsonValue &obj)
{
  QJsonArray outputs = obj.toArray();
  for(QJsonValueRef an_output: outputs)
  {
    QJsonArray oo = an_output.toArray();
    TOutput *o  = new TOutput({oo[0].toString(), static_cast<MPAIValueT>(oo[1].toDouble())});
    m_outputs.push_back(o);
  }
  return true;
}

QString CoinbaseDocument::getDocHashableString()
{
  QString hahsableTrx = "{";
//  hahsableTrx += "\"creationDate\":\"" + m_doc_creation_date + "\",";
  hahsableTrx += "\"cycle\":\"" + m_doc_cycle + "\",";
  if(m_doc_class != "")
    hahsableTrx += "\"dClass\":\"" + m_doc_class + "\",";
  hahsableTrx += "\"dType\":\"" + m_doc_type + "\",";
  hahsableTrx += "\"dVer\":\"" + m_doc_version + "\",";
  hahsableTrx += "\"mintedCoins\":" + QString::number(m_minted_coins) + ",";
  QStringList outputs = {};
  for(auto an_output: m_outputs)
    outputs.append("[\"" + an_output->m_address + "\"," + QString::number(an_output->m_value) + "]");
  hahsableTrx += "\"outputs\":[" + outputs.join(",") + "],";
  hahsableTrx += "\"treasuryFrom\":\"" + m_treasury_from + "\",";
  hahsableTrx += "\"treasuryIncomes\":" + QString::number(m_treasury_incomes) + ",";
  hahsableTrx += "\"treasuryTo\":\"" + m_treasury_to + "\"";
  hahsableTrx += "}";
  return hahsableTrx;

//  return { needPure: false, hashables: this.extractHParts_coinbase(trx) };
//  let hashInfo = this.extractTransactionHashableParts(trx);
//  QString hash = CCrypto::keccak256Dbl(hahsableTrx);    // NOTE: absolutely using double hash for more security
//  if (hashInfo.needPure) {
//    // generate deterministic part of trx hash
//    let pureHash = this.getPureHash(trx);
//    // console.log('pureHash', pureHash);
//    if (vHandler.isNewerThan(trx.dVer, '0.0.0')) {
//        hash = [pureHash.toString().substr(32), hash.toString().substr(32)].join('');
//    } else {
//        hash = [pureHash.toString().substr(0, 32), hash.toString().substr(0, 32)].join('');
//    }
//  }
//  return hash;
}

QString CoinbaseDocument::calcDocHash()
{
  QString to_be_hased_string = getDocHashableString();
  CLog::log("\nHashable string for coinbase block: " + to_be_hased_string, "app", "trace");
  QString hash = CCrypto::keccak256Dbl(to_be_hased_string); // NOTE: absolutely using double hash for more security
//  QString customStringified = stringifyDoc();
//  QString hash = CCrypto::keccak256(customStringified);
//  CLog::log("hash " + hash);
//  return hash;
  return hash;
}

bool CoinbaseDocument::deleteInputs()
{
  return true;
}

bool CoinbaseDocument::deleteOutputs()
{
  for (TOutput* an_output: m_outputs)
    delete an_output;
  return true;
}

QJsonObject CoinbaseDocument::exportToJson() const
{
  QJsonObject document = Document::exportToJson();

  document["cycle"] = m_doc_cycle;
  document["treasuryFrom"] = m_treasury_from;
  document["treasuryTo"] = m_treasury_to;
  document["treasuryIncomes"] = QVariant::fromValue(m_treasury_incomes).toDouble();
  document["mintedCoins"] = QVariant::fromValue(m_minted_coins).toDouble();
  return document;
}

std::tuple<bool, QJsonArray> CoinbaseDocument::exportOutputsToJson() const
{
  QJsonArray outputs {};
  if(m_outputs.size() ==0)
    return {false, outputs};

  for(auto an_output: m_outputs)
    outputs.push_back(QJsonArray {
      an_output->m_address,
      QVariant::fromValue(an_output->m_value).toDouble()});

  return {true, outputs};
}

bool CoinbaseDocument::applyDocImpact1(const Block &block) const
{
  Q_UNUSED(block);
  // coinbase documents haven't first impact functionalities
  return true;
}
