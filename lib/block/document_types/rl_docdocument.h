#ifndef RLDOCDOCUMENT_H
#define RLDOCDOCUMENT_H


class RlDocDocument : public Document
{
public:
  RlDocDocument(const QJsonObject &obj);
  bool setByJsonObj(const QJsonObject &obj) override;
};

#endif // RLDOCDOCUMENT_H
