#ifndef BASICTXDOCUMENT_H
#define BASICTXDOCUMENT_H


class BasicTxDocument : public Document
{
public:
  BasicTxDocument(const QJsonObject &obj);
  ~BasicTxDocument();
  bool setByJsonObj(const QJsonObject &obj) override;
  QString stringifyDoc() const override;
  QString calcDocumentExtInfoRootHash() override;  // calcTrxExtRootHash
  std::tuple<bool, uint64_t> calcDocumentDataAndProcessCost(
    const QString &stage,
    QString cDate = "",
    const uint32_t &extraLength = 0) override;  // calcTrxExtRootHash

  std::tuple<bool, uint64_t, uint64_t> retrieveDPCostInfo(
    const QString &block_hash,
    const QString &backer) override;

  QString getRef() override;
  bool deleteInputs() override;
  bool deleteOutputs() override;
  QJsonObject exportToJson() const override;
  QVector<OutputIndexT> getDPIs() override;
  QString getNonce() override;
  QVector<TInput*> getInputs() override;
  QVector<TOutput*> getOutputs() override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;
  bool applyDocImpact1(const Block &block) const override;


  QString m_ref = ""; // reference of document that the transaction pay for it
  QVector<TInput*> m_inputs = {};
  QVector<TOutput*> m_outputs = {};
  QVector<OutputIndexT> m_data_and_process_payment_indexes = {};  // dPIs

};

#endif // BASICTXDOCUMENT_H
