#include "stable.h"

#include "document.h"
#include "rl_docdocument.h"

RlDocDocument::RlDocDocument(const QJsonObject &obj)
{
  setByJsonObj(obj);
}


bool RlDocDocument::setByJsonObj(const QJsonObject &obj)
{
  Document::setByJsonObj(obj);

  return true;
}
