#ifndef DNAPROPOSAL_H
#define DNAPROPOSAL_H



class ProposalHandler;
//class ProposalsIRB;

#include "document.h"
#include "lib/services/polling/polling_handler.h"

class DNAProposal : public Document
{
public:
  static const QString stbl_dna_shares;
  static const QString stbl_machine_draft_proposals;
  static const QString stbl_proposals;

  QString m_project_hash = "58be4875eaa3736f60622e26bda746fb81812e8d7ecad50a2c3f97f0605a662c";
//  QString m_net = "";
  QString m_approval_date = "";
  QString m_shareholder = "";
  uint64_t m_help_hours = 0;
  uint64_t m_help_level = 0;
  uint64_t m_shares = 0;
  uint64_t m_votesY = 0;
  uint64_t m_votesA = 0;
  uint64_t m_votesN = 0;
  uint64_t m_voting_longevity = 24;
  QString m_polling_profile = "Basic";

  DNAProposal();

  static GenRes updateProposal(const QVDicT &values, const ClausesT &clauses, const bool &is_transactional = false);

  GenRes insertDocShares();
  QString stringifyDoc() const override;
  bool applyDocImpact1(const Block &block) const override;
  QJsonObject exportJson() override;
//  virtual GenRes applyDocImpact2() override;
};

#endif // DNAPROPOSAL_H
