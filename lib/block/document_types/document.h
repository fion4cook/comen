#ifndef DOCUMENT_H
#define DOCUMENT_H

#include "stable.h"

class Block;
class Document
{
public:
  Document(){};
  Document(const QJsonObject &obj){ setByJsonObj(obj); };
  virtual ~Document();

  static const QString stbl_docs_blocks_map;

//  Block m_containterBlock;
  //  -  -  -  common members
  QString m_doc_hash = "0000000000000000000000000000000000000000000000000000000000000000";
  QString m_doc_type = "";
  QString m_doc_class = "";
  QString m_doc_version = CConsts::DEFAULT_DOCUMENT_VERSION;
  QString m_doc_title = "";
  QString m_doc_descriptions = "";
  QString m_doc_tags = "";
  QString m_doc_creation_date = "";
  QString m_block_creation_date = "";
  QString m_doc_ext_hash = "";
  DocLenT m_doc_length = 0;
  bool m_doc_ext_object_assigned = false;
  QJsonObject m_doc_ext_object = {};


  //  -  -  -  static methods
  static bool isBasicTransaction(const QString &dType);
  static bool isDPCostPayment(const QString &dType);
  static bool canBeACostPayerDoc(const QString &dType);
  static bool isNoNeedCostPayerDoc(const QString &dType);
  //  -  -  -  virtual methods
  /**
   * @brief stringifyDoc
   * creates an ordered JSon object and serialize it, for the sake of uniqness of hash of the document
   * @return
   */
  virtual QString stringifyDoc() const;

  virtual QString getDocHashableString();
  virtual QString calcDocHash();

  virtual void setHash();
  virtual bool applyDocImpact1(const Block &block) const;
  virtual QJsonObject exportJson();
  virtual GenRes applyDocImpact2();
  virtual bool setByJsonObj(const QJsonObject &obj);
  virtual QString calcDocumentExtInfoRootHash();
  virtual std::tuple<bool, uint64_t> calcDocumentDataAndProcessCost(
    const QString &stage,
    QString cDate = "",
    const uint32_t &extraLength = 0);

  virtual std::tuple<bool, uint64_t, uint64_t> retrieveDPCostInfo(
    const QString &block_hash,
    const QString &backer);

  virtual QString getRef();
  virtual bool deleteInputs();
  virtual bool deleteOutputs();
  virtual QJsonObject exportToJson() const;
  virtual std::tuple<bool, QJsonArray> exportOutputsToJson() const;
  virtual QVector<OutputIndexT> getDPIs();
  virtual QString getNonce();
  virtual QVector<TInput*> getInputs();
  virtual QVector<TOutput*> getOutputs();
  virtual bool setDocumentOutputs(const QJsonValue &obj);

  //  -  -  -  base class methods
  bool trxHasInput();
  static bool trxHasInput(const QString &document_type);

  bool trxHasNotInput();
  static bool trxHasNotInput(const QString &document_type);

  void mapDocToBlock(const QString &blockHash, const uint32_t &docInx);

};

#endif // DOCUMENT_H
