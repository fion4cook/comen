#include "stable.h"
#include "document.h"
#include "basic_tx_document.h"
#include "coinbase_document.h"
#include "rp_docdocument.h"
#include "rl_docdocument.h"

#include "document_factory.h"

DocumentFactory::DocumentFactory()
{

}

Document* DocumentFactory::create(const QJsonObject &obj)
{
  QString doc_type = obj.value("dType").toString();
  if (doc_type == CConsts::DOCUMENT_TYPES::BasicTx)
  {
    return new BasicTxDocument(obj);
  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::Coinbase)
  {
    return new CoinbaseDocument(obj);

  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::RpDoc)
  {
    return new RpDocDocument(obj);
  }
  else if (doc_type == CConsts::DOCUMENT_TYPES::RlDoc)
  {
    return new RlDocDocument(obj);
  }

  return new Document(obj);
}
