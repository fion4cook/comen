#include "stable.h"

#include "document.h"
#include "rp_docdocument.h"



RpDocDocument::RpDocDocument(const QJsonObject &obj)
{
  setByJsonObj(obj);
}


bool RpDocDocument::setByJsonObj(const QJsonObject &obj)
{
  Document::setByJsonObj(obj);

  return true;
}
