#ifndef COINBASEDOCUMENT_H
#define COINBASEDOCUMENT_H


class CoinbaseDocument : public Document
{
public:
  CoinbaseDocument(const QJsonObject &obj);
  ~CoinbaseDocument();
  bool setByJsonObj(const QJsonObject &obj) override;
  bool setDocumentOutputs(const QJsonValue &obj) override;
  QString getDocHashableString() override;
  QString calcDocHash() override;
  bool deleteInputs() override;
  bool deleteOutputs() override;
  QJsonObject exportToJson() const override;
  std::tuple<bool, QJsonArray> exportOutputsToJson() const override;
  bool applyDocImpact1(const Block &block) const override;

  QString m_doc_cycle = "";
  QString m_treasury_from = "";
  QString m_treasury_to = "";
  uint64_t m_minted_coins = 0;
  uint64_t m_treasury_incomes = 0;

  QVector<TOutput*> m_outputs = {};

};

#endif // COINBASEDOCUMENT_H
