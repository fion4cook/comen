#ifndef BSIGNAL_H
#define BSIGNAL_H

class Block;

#include <QJsonObject>

#include "lib/block/block_types/block.h"
#include "lib/database/db_model.h"

class BSignal
{
public:
  static const QString stbl_signals;

  BSignal();
  static void logSignals(const Block &block);

};

#endif // BSIGNAL_H
