#include "signal.h"


BSignal::BSignal()
{

}

const QString BSignal::stbl_signals = "c_signals";

void BSignal::logSignals(const Block &block)
{
  for (QJsonValue aSignal_ : block.m_signals)
  {
    QJsonObject aSignal = aSignal_.toObject();
    QString sigKey = aSignal.value("sig").toString();
    QString sigVal;
    if (aSignal.keys().contains("val"))
    {
      auto tmpSigVal = aSignal.value("val");
      if (tmpSigVal.isObject())
      {
        sigVal = CUtils::serializeJson(tmpSigVal.toObject());

      }
      else if (tmpSigVal.isString())
      {
        sigVal = tmpSigVal.toString();

      }
    }
    else
    {
      // backward compatibility
      sigVal = aSignal.value("ver").toString();
    }

    QVDicT values{
      {"sig_signaler", block.m_backer},
      {"sig_block_hash", block.m_block_hash},
      {"sig_key", sigKey},
      {"sig_value", sigVal},
      {"sig_creation_date", block.m_block_creation_date}};

    DbModel::insert(
      BSignal::stbl_signals,     // table
      values, // values to insert
      true
    );
  }
}
