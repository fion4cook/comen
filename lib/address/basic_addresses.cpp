#include "stable.h"
#include "lib/ccrypto.h"
#include "address_handler.h"



std::tuple<bool, UnlockDocument> CAddress::createANewBasicAddress(const QString &signature_mod, const QString &signature_version)
{

// return   mpSettings.backerTitle = address.title
//  mpSettings.backerDetail = address.detail
//  mpSettings.backerAddress = address.address

  QString signature_type = CConsts::SIGNATURE_TYPES::Basic; // by default is two of three (m of n === m/n)
  CLog::log("creating a new signature_type:signature_mod " + signature_type + ":" + signature_mod, "app", "info");
  QVDicT  privContainer;
  QVDicT pubToPrvMap;
  uint16_t mSignaturesCount = signature_mod.split("/")[0].toUInt();
  uint16_t nSignaturesCount = signature_mod.split("/")[1].toUInt();
  QHash<QString, IndividualSignature> individuals_signing_sets;
//    trxUnlockMerkle;

  for (uint32_t i = 0; i < nSignaturesCount; i++) {
    auto[status, prvKey, pubKey] = CCrypto::ECDSAGenerateKeyPair();
    if (!status)
    {
      CLog::log("Couldn't create Basic ECDSA key pair", "app", "info");
      return { false, UnlockDocument() };
    }

    pubToPrvMap[pubKey] = prvKey;
    IndividualSignature a_sign_set(pubKey);//

    a_sign_set.m_signer_id = QString("%1").arg(random());
    individuals_signing_sets.insert(a_sign_set.m_signer_id, a_sign_set);
  }

  QVDicT options{
    {"signature_type", signature_type},
    {"signature_version", signature_version},
    {"customSalt", "PURE_LEAVE"}
  };

  UnlockDocument trxUnlockMerkle = SignatureStructureHandler::createCompleteUnlockSets(
        individuals_signing_sets,
        mSignaturesCount,
        options
        );


//       for (let anUnlockerSet of trxUnlockMerkle.uSets) {
//       privContainer[anUnlockerSet.salt] = QStringList{};
//           for (let aSignSet of anUnlockerSet.sSets)
//               privContainer[anUnlockerSet.salt].push(pubToPrvMap[aSignSet.sKey]);

//           // test unlock structure & signature
//           let isValidUnlock = mOfNHandler.validateSigStruct({
//               address: trxUnlockMerkle.accountAddress,
//               uSet: anUnlockerSet
//           });
//           if (isValidUnlock) {
//               clog.trx.info(`new Bassic address ${iutils.shortBech(trxUnlockMerkle.accountAddress)} created & tested successfully`);
//           } else {
//               console.error('Curropted Basic address created!');
//               console.error(trxUnlockMerkle);
//               utils.exiter(trxUnlockMerkle, 33);
//           }
//       }
//       trxUnlockMerkle.privContainer = privContainer;
//       // console.log(`\n trxUnlockMerkle: ${utils.stringify(trxUnlockMerkle)}\n`);

//       // validate signature of new address
//       let signMsg = CCrypto::convertTitleToHash('Imagine all the people living life in peace').substring(0, iConsts.SIGN_MSG_LENGTH);
//       let fetchedPrivContainer = trxUnlockMerkle.privContainer;
//       for (let aUSet of trxUnlockMerkle.uSets) {
//           for (let inx = 0; inx < aUSet.sSets.length; inx++) {
//               let aSignature = crypto.signMsg(signMsg, fetchedPrivContainer[aUSet.salt][inx]);
//               let verifyRes = crypto.verifySignature(signMsg, aSignature, aUSet.sSets[inx].sKey);
//               if (verifyRes != true) {
//                   let msg = `Curropted Basic address created signature!? ${utils.stringify(trxUnlockMerkle)}`
//                   utils.exiter(msg, 423);
//               }
//           }
//       }

//       let res = {
//           address: trxUnlockMerkle.accountAddress,
//           title: `${signature_type}:${signature_mod}`,
//           detail: trxUnlockMerkle
//       }
//       return res;
       return {true, UnlockDocument()};
}
