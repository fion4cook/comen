#ifndef PERMUTATIONHANDLER_H
#define PERMUTATIONHANDLER_H

#include <QVector>
#include <QHash>
#include <QStringList>


#include "lib/clog.h"



struct PremInfo
{
  uint count = 0;
  QStringList variety;
};


class PermutationHandler
{
public:
  QStringList m_elements;
  bool m_shouldBeUnique = true;
  QList<QStringList> m_permutations = {};
  QStringList m_permutationsStringify = {};
  QHash<QString, PremInfo>  m_test_analyze;

  PermutationHandler(
      QStringList elements,
      uint32_t subset_count = 0,
      bool shouldBeUnique = true,
      QList<QStringList> premutions = {},
      QStringList  premutionsStringify = {}
  );
  void heapP(QStringList values = {});
  void recursiveHeapP(QStringList values, uint32_t subset_count, int size = -1);
  void printArr(QStringList values, uint32_t subset_count);
  void testAnalyze(QList<QStringList> premutations = {});

};

#endif // PERMUTATIONHANDLER_H
