#include "cutils.h"



#include "cutils_dumpers.cpp"


TimeDiff::TimeDiff(
  const uint64_t &asYears_,
  const uint64_t &years_,
  const uint64_t &asMonths_,
  const uint64_t &months_,
  const uint64_t &asDays_,
  const uint64_t &days_,
  const uint64_t &asHours_,
  const uint64_t &hours_,
  const uint64_t &asMinutes_,
  const uint64_t &minutes_,
  const uint64_t &asSeconds_,
  const uint64_t &seconds_
  )
{
  asYears = asYears_;
  years = years_;
  asMonths = asMonths_;
  months = months_;
  asDays = asDays_;
  days = days_;
  asHours = asHours_;
  hours = hours_;
  asMinutes = asMinutes_;
  minutes = minutes_;
  asSeconds = asSeconds_;
  seconds = seconds_;
}

bool TimeDiff::operator ==(const TimeDiff &obj)
{
  if (
     (asYears != obj.asYears) ||
     (years != obj.years) ||
     (asMonths != obj.asMonths) ||
     (months != obj.months) ||
     (asDays != obj.asDays) ||
     (days != obj.days) ||
     (asHours != obj.asHours) ||
     (hours != obj.hours) ||
     (asMinutes != obj.asMinutes) ||
     (minutes != obj.minutes) ||
     (asSeconds != obj.asSeconds) ||
     (seconds != obj.seconds)
     )
  {
    return false;
  }
  return true;
}

bool TimeDiff::operator !=(const TimeDiff &obj)
{
  return !(*this==obj);
}







typedef long double longDouble;

std::tuple <longDouble, longDouble, longDouble, longDouble> CUtils::calcLog(
  longDouble xValue,
  uint64_t range,
  uint64_t exp)
{
  longDouble hundredPercent = log(pow(range, exp));
  longDouble yValue = (xValue >= range) ? 0 : log(pow((range - xValue), exp));
  longDouble gain = iFloorFloat((yValue * 100) / hundredPercent);

//  if ((gain == Number.POSITIVE_INFINITY || gain == Number.NEGATIVE_INFINITY))
//  {gain = Number.MAX_VALUE;}

  longDouble revGain = (1 / gain);
//  if ((revGain == Number.POSITIVE_INFINITY || revGain == Number.NEGATIVE_INFINITY))
//  {revGain = Number.MAX_VALUE;}

  return {
      xValue,
      yValue,
      gain,
      revGain
  };
}

QString CUtils::paddingDocLength(const int &value, const int &neededLen)
{
  return paddingDocLength(QString("%1").arg(value), neededLen);
}

QString CUtils::paddingDocLength(const QString &value, const int &neededLen)
{
  return value.rightJustified(neededLen, '0');
}

double CUtils::customFloorFloat(double number, uint8_t percision)
{
  double per = pow(10, percision) * 1.0;
  return trunc(number * per) / per;
}

QString CUtils::lightRandom(const int &max)
{
  srand (time(NULL));
  return QString::number(random() * max);
}

double CUtils::iFloorFloat(double number)
{
  return customFloorFloat(number, 11);
}


double CUtils::CFloor(double v)
{
  return floor(v);
}

QStringList CUtils::chunkString(const QString &str, const uint16_t &chunckSize=2)
{
  QStringList out;
  for (int i = 0; i < str.length(); i += chunckSize) {
    QString s = str.midRef(i, chunckSize).toString();
    out.push_back(s);
  }
  return out;
}

template<typename T>
QList<T> CUtils::chunkIt(T values, uint64_t chunk_size)
{
  QList<T> out;
//  T lines;
  uint64_t chunks_count = trunc(values.size()/chunk_size);
  for (uint64_t i=0; i<chunks_count; i++)
  {
    uint64_t end_index = (((i+1) * chunk_size) < values.size()) ? ((i+1) * chunk_size) : values.size()-1;
    T a_chunk(values.begin() + (i * chunk_size), values.begin() + end_index);
    out.append(a_chunk);
  }
  return out;
  // fill
//  std::size_t const half_size = lines.size() / 2;
//  T split_lo(lines.begin(), lines.begin() + half_size);
//  T split_hi(lines.begin() + half_size, lines.end());

}

QVector<QStringList> CUtils::chunkStringList(QStringList values, uint64_t chunk_size)
{
  QVector<QStringList> out;

  if ((values.size()==0) || (chunk_size==0))
    return out;

  if (static_cast<uint64_t>(values.size()) <= chunk_size)
  {
    out.push_back(values);
    return out;
  }
  uint64_t chunks_count = trunc(values.size()/chunk_size);
  for (uint64_t i=0; i<chunks_count+1; i++)
  {
    uint64_t end_index = (((i+1) * chunk_size) < static_cast<uint64_t>(values.size())) ? ((i+1) * chunk_size) : values.size();
    QStringList a_chunk(values.begin() + (i * chunk_size), values.begin() + end_index);
    if (a_chunk.size()>0)
      out.push_back(a_chunk);
  }

  return out;
  // fill
//  std::size_t const half_size = lines.size() / 2;
//  T split_lo(lines.begin(), lines.begin() + half_size);
//  T split_hi(lines.begin() + half_size, lines.end());

}


QString CUtils::hash4c(const QString &s)
{
  return s.midRef(0,4).toString();
}

QString CUtils::hash6c(const QString &s)
{
    return s.midRef(0,6).toString();
}

QString CUtils::hash8c(const QString &s)
{
    return s.midRef(0,8).toString();
}

QString CUtils::hash16c(const QString &s)
{
    return s.midRef(0,16).toString();
}

QString CUtils::hash32c(const QString &s)
{
    return s.midRef(0, 32).toString();
}

QString CUtils::hash64c(const QString &s)
{
    return s.midRef(0, 64).toString();
}

QString CUtils::shortBech16(const QString &s)
{
    return s.midRef(0, 3).toString() + s.midRef(46).toString();
}

QString CUtils::removeNewLine(QString s)
{
  return s.replace(QRegExp("\n"), "");

//    QString str = static_cast<QString>(s);
//    str.erase(std::remove(str.begin(), str.end(), "\n"), str.end());
//    return str;
}

QString CUtils::replaceNewLineByBackN(QString s)
{
  return s.replace(QRegExp("\n"), "\\n");
//    QString str = static_cast<QString>(s);
//    str.erase(std::remove(str.begin(), str.end(), "\n"), str.end());
//    return str;
}

QString CUtils::removeTab(QString s)
{
  return s.replace(QRegExp("\t"), "");
//    QString str = static_cast<QString>(s);
//    str.erase(std::remove(str.begin(), str.end(), "\t"), str.end());
//    return str;
}

QString CUtils::removeTabNL(const QString &s)
{
    QString str = removeNewLine(s);
    str = removeTab(str);
    return str;
}

QString CUtils::removeDblSpaces(QString s)
{
  return s.replace(QRegExp("[' ']{2,}"), " ");

//    std::string str = s.toStdString();
//    str = boost::regex_replace(str, boost::regex("[' ']{2,}"), " ");
//    return QString::fromStdString(str);
}



QString CUtils::breakByBR(const QString &content, const uint16_t &size)
{
  QStringList chunks = chunkString(content, size);
  QString lineBR = CConsts::MESSAGE_TAGS::iPGPEndLineBreak + CConsts::MESSAGE_TAGS::iPGPStartLineBreak;
  QString out = chunks.join(lineBR);
  out = CConsts::MESSAGE_TAGS::iPGPStartLineBreak + out + CConsts::MESSAGE_TAGS::iPGPEndLineBreak;
  return out;
}


std::string s(QString content)
{
  return content.toStdString();
}

QString CUtils::stripBR(QString  content)
{
  if (content.contains("("))
  {
    content.replace(QString("\n"), QString(""));
    content.replace(QString("\r"), QString(""));
    QString outs = "";
    QStringList chunks = content.split("<br>");
    for (QString a_chunk: chunks)
    {
      int open_p = a_chunk.indexOf("(");
      int close_p = a_chunk.indexOf(")");
      QString ach = a_chunk.midRef(open_p+1, close_p - open_p - 1).toString();
      outs += ach;
    }
    return outs.simplified();
  } else {
    return "";
  }
}

bool CUtils::isValidHash(QString s)
{
  if (stripNonHex(s) != s)
    return false;


  // TODO add some more control such as length control,...
  return true;
}

QString CUtils::stripNonHex(QString s)
{
//  return s.replace(/[^0-9a-fA-F]/gi, '');
  return s.replace(QRegularExpression("[^0-9a-fA-F]"), "");
}

QString CUtils::sepNum(uint64_t number)
{
  // TODO: implement it to seperate every 3 digit with "."
//  QRegularExpression re("(?=(\d{3})+(?!\d))");
//  return QString::number(number).replace(//g, ",")
  return QString::number(number);
}

BlockLenT CUtils::convertPaddedStringToInt(QString s)
{
  while(s.midRef(0, 1).toString() == "0")
    s = s.midRef(1).toString();
  return s.toDouble();
}


QStringList CUtils::convertJSonArrayToQStringList(const QJsonArray &arr)
{
  QStringList out = {};
  for(QJsonValue an_elm: arr)
    out.append(an_elm.toString());
  return out;
}

QJsonArray CUtils::parseToJsonArr(const QString &serialized)
{
  QJsonDocument d = QJsonDocument::fromJson(serialized.toUtf8());
  return d.array();
}

QJsonObject CUtils::parseToJsonObj(const QString &serialized)
{
  QJsonDocument d = QJsonDocument::fromJson(serialized.toUtf8());
  QJsonObject JsonObj = d.object();
  return JsonObj;
}

QJsonObject CUtils::parseToJsonObj(const QVariant &serialized)
{
    return parseToJsonObj(serialized.toString());
}

QString CUtils::serializeJson(const QJsonObject &values)
{
    QJsonDocument doc(values);
    QString serializedJson(doc.toJson(QJsonDocument::Compact));
    return serializedJson;
}

QString CUtils::serializeJson(const QStringList &values)
{
  QJsonArray JsArr;
  for(QString elm: values)
    JsArr.push_back(elm);
  return serializeJson(JsArr);
}

QString CUtils::serializeJson(const QJsonArray &values)
{
   QJsonDocument doc(values);
   QString serializedJson(doc.toJson(QJsonDocument::Compact));
   return serializedJson;
}

QString CUtils::getANullStringifyedJsonObj()
{
  QJsonObject jsonObj {};
  QJsonDocument doc(jsonObj);
  QString serializedJson(doc.toJson(QJsonDocument::Compact));
  return serializedJson;
}


QString CUtils::sanitizingContent(const QString &str)
{
  // TODO: implement it ASAP to support html sanitization and other stuff
  return str;
}

QStringList CUtils::convertJSonArrayToStringList(const QJsonArray &arr)
{
  QStringList out;
  for (QJsonValue a_value: arr)
  {
    out.append(a_value.toString());
  }
  return out;
}




// - - - - - - time functions - - - - -

QString CUtils::getNow()
{
    return QDateTime::currentDateTimeUtc().toString("yyyy-MM-dd HH:mm:ss");
}

QString CUtils::getNowSSS()

{
    return QDateTime::currentDateTimeUtc().toString("yyyy-MM-dd HH:mm:ss.zzz");
}

QString CUtils::getCurrentYear()
{
  return QDateTime::currentDateTimeUtc().toString("yyyy");
}

bool CUtils::isValidDateForamt(const QString &cDate)
{
  return true;  // TOD: implement a control on format "yyyy-MM-dd HH:mm:ss"
}

QString CUtils::getLaunchDate()
{
//    if (this.LD_SETTED === undefined) {
//        let launchDate;
//        if ((this.TIME_GAIN == 1) || (this.FORCED_LAUNCH_DATE != null)) {
//            launchDate = this.FORCED_LAUNCH_DATE
//        } else {
//            launchDate = getMoment().subtract({ 'minutes': IMG_CONSTS.getCycleByMinutes() - 1 }).format('YYYY-MM-DD HH:mm:ss')
//        }
//        this.setLaunchDate(launchDate)
//    }
    return CConsts::LAUNCH_DATE;
}

bool CUtils::isGreaterThanNow(const QString &cDate) {
    if (cDate > getNow()) {
        return true;
    }
    return false;
}

void CUtils::exitIfGreaterThanNow(const QString &cDate){
    if (isGreaterThanNow(cDate)) {
        std::cout << std::endl << "exit on Greater Than Now Date Fails";
        exit(0);
    }
}

TimeDiff CUtils::timeDiff(const QString &fromT, const QString &toT)
{
  TimeDiff res;
  QDateTime startT = QDateTime::fromString(fromT, "yyyy-MM-dd HH:mm:ss");
  QDateTime endT = QDateTime::fromString(toT, "yyyy-MM-dd HH:mm:ss");

  qint64 diffByDay = startT.daysTo(endT);
  qint64 diffBySec = startT.secsTo(endT);
  QString timeDiffStr = QString("%1").arg(startT.secsTo(endT));

  res.asSeconds = diffBySec;  // entire gap by seconds
  res.asMinutes = round(res.asSeconds/60);
  res.asHours = round(res.asMinutes/60);
  res.asDays = diffByDay;
  res.asMonths = round(res.asDays/30);  //FIXME: more test
  res.asYears = round(res.asMonths/12);  //FIXME: more test

  res.days = res.asDays % 30; //FIXME: more test
  res.hours = res.asHours % 24; //FIXME: more test
  res.minutes = res.asMinutes % 60; //FIXME: more test
  res.seconds = res.asSeconds % 60; //FIXME: more test

  // FIXME: implement the other missed properties

  return res;
}

//TimeDiff timeDiff(const QString &fromT, const QString &toT)
//{
//  return timeDiff(fromT.toStdString(), toT.toStdString());
//}

QString CUtils::minutesBefore(const uint64_t &back_in_time_by_minutes, const QString &cDate) {
  uint64_t since_epoch;
  if (cDate == "")
  {
    since_epoch = QDateTime::currentDateTimeUtc().toSecsSinceEpoch();
  }
  else
  {
    QDateTime dt = QDateTime::fromString(cDate, "yyyy-MM-dd HH:mm:ss");
    since_epoch = dt.toSecsSinceEpoch();
  }
  since_epoch -= (back_in_time_by_minutes * 60);
  QDateTime res = QDateTime::fromSecsSinceEpoch(since_epoch);
  return res.toString("yyyy-MM-dd HH:mm:ss");
}

QString CUtils::minutesAfter(const uint64_t &forward_in_time_by_minutes, const QString &cDate) {
  uint64_t since_epoch;
  if (cDate == "")
  {
    since_epoch = QDateTime::currentDateTimeUtc().toSecsSinceEpoch();
  }
  else
  {
    QDateTime dt = QDateTime::fromString(cDate, "yyyy-MM-dd HH:mm:ss");
    since_epoch = dt.toSecsSinceEpoch();
  }
  since_epoch += (forward_in_time_by_minutes * 60);
  QDateTime res = QDateTime::fromSecsSinceEpoch(since_epoch);
  return res.toString("yyyy-MM-dd HH:mm:ss");
}

QString CUtils::yearsBefore(const uint64_t &backInTimesByYears, const QString &cDate)
{
  uint64_t since_epoch;
  if (cDate == "")
  {
    since_epoch = QDateTime::currentDateTimeUtc().toSecsSinceEpoch();
  }
  else
  {
    QDateTime dt = QDateTime::fromString(cDate, "yyyy-MM-dd HH:mm:ss");
    since_epoch = dt.toSecsSinceEpoch();
  }
  since_epoch -= (backInTimesByYears * 31536000);  // 365Days * 24Hours * 60Minutes * 60Seconds
  QDateTime res = QDateTime::fromSecsSinceEpoch(since_epoch);
  return res.toString("yyyy-MM-dd HH:mm:ss");
}

QString CUtils::yearsAfter(const uint64_t &forwardInTimesByYears, const QString &cDate)
{
  uint64_t since_epoch;
  if (cDate == "")
  {
    since_epoch = QDateTime::currentDateTimeUtc().toSecsSinceEpoch();
  }
  else
  {
    QDateTime dt = QDateTime::fromString(cDate, "yyyy-MM-dd HH:mm:ss");
    since_epoch = dt.toSecsSinceEpoch();
  }
  since_epoch += (forwardInTimesByYears * 31536000);  // 365Days * 24Hours * 60Minutes * 60Seconds
  QDateTime res = QDateTime::fromSecsSinceEpoch(since_epoch);
  return res.toString("yyyy-MM-dd HH:mm:ss");
}

QString CUtils::getCoinbaseCycleStamp(QString cDate)
{
  if (cDate == "")
    cDate = getNow();

  QString day = cDate.split(" ")[0];

  if (CConsts::TIME_GAIN == 1)
  {
    return (day + " " + getCoinbaseCycleNumber(cDate));

  } else {
    return (day + " " + getCoinbaseCycleNumber(cDate).rightJustified(3, '0'));

  }
}

QString CUtils::getCoinbaseCycleNumber(QString cDate)
{
  uint minutes;
  if (cDate == "")
  {
    minutes = getNowByMinutes();
  } else {
    QStringList minutesDtl = cDate.split(" ")[1].split(":");
    minutes = (minutesDtl[0].toUInt() * 60) + minutesDtl[1].toUInt();
  }
  QString cycleNumber;
  if (CConsts::TIME_GAIN == 1)
  {
    cycleNumber = isAmOrPm(minutes);
  } else {
    cycleNumber = QString::number(static_cast<uint>(minutes / getCycleByMinutes()));
  }
  return cycleNumber;
}

uint CUtils::getNowByMinutes()
{
  QStringList minutesDtl = getNow().split(" ")[1].split(":");
  uint nowByMinutes = (minutesDtl[0].toUInt() * 60) + minutesDtl[1].toUInt();
  return nowByMinutes;
}

QString CUtils::isAmOrPm(const uint &minutes)
{
  if (minutes >= 720)
    return "12:00:00";
  return "00:00:00";
}

uint CUtils::getCycleByMinutes()
{
  return (CConsts::TIME_GAIN == 1) ? CConsts::STANDARD_CYCLE_BY_MINUTES : CConsts::TIME_GAIN;
}

uint CUtils::getCycleCountPerDay()
{
  return static_cast<uint>(24 * 60 / getCycleByMinutes());
}


/**
 * @brief CUtils::getCycleElapsedByMinutes
 * @param cDate
 * @return
 * renamed from getCoinbaseAgeByMinutes to getCycleElapsedByMinutes
 */
uint CUtils::getCycleElapsedByMinutes(QString cDate)
{
  if (cDate == "")
    cDate = getNow();
  auto[cycle_start_time, cycle_end_time] = getACycleRange(cDate);
  return timeDiff(cycle_start_time, cDate).asMinutes;
}

/**
*
* @param {*} cDate reference date
* @param {*} backByCycle is a number to indicate how old the cycle before you need
*/
std::tuple<QString, QString> CUtils::getACycleRange(
  QString cDate,
  uint backByCycle,
  uint forwardByCycle)
{
  if (cDate == "")
    cDate = getNow();

  if (CConsts::TIME_GAIN == 1)
  {
    // one extra step to resolve +- summer time
    uint h_ = (cDate.split(" ")[1].split(":")[0]).toUInt();
    QString h;
    if (h_ >= 12)
    {
      h = "18:00:00";
    } else {
      h = "06:00:00";
    }
    cDate = cDate.split(" ")[0] + " " + h;
  }

  QString minCreationDate;
  if (forwardByCycle == 0)
  {
    minCreationDate = minutesBefore(backByCycle * getCycleByMinutes(), cDate);
//    minCreationDate = Moment(cDate, "YYYY-MM-DD HH:mm:ss").subtract({"minutes": backByCycle * iConsts.getCycleByMinutes()});
  } else {
    minCreationDate = minutesAfter(forwardByCycle * getCycleByMinutes(), cDate);
//    minCreationDate = Moment(cDate, "YYYY-MM-DD HH:mm:ss").add({"minutes": forwardByCycle * iConsts.getCycleByMinutes()});
  }

  QString day = minCreationDate.split(" ")[0];  //format("YYYY-MM-DD");
  QStringList time_details = minCreationDate.split(" ")[1].split(":");
  uint minutes = (time_details[0].toUInt() * 60) + time_details[1].toUInt();
  uint startMinute = trunc(minutes / getCycleByMinutes()) * getCycleByMinutes();
  uint endMinute = startMinute + getCycleByMinutes() - 1;
  return {
    day + " " + convertMinutesToHHMM(startMinute) + ":00",
    day + " " + convertMinutesToHHMM(endMinute) + ":59"
  };
}


QString CUtils::convertMinutesToHHMM(uint minutes)
{
  return QString::number(trunc(minutes / 60)).rightJustified(2, '0') + ':' + QString::number(minutes % 60).rightJustified(2, '0');
  //  return _.padStart(trunc(minutes / 60), 2, '0') + ':' + _.padStart((minutes % 60), 2, '0');
}

std::tuple<QString, QString> CUtils::getCoinbaseRange(const QString &cDate)
{
  return getACycleRange(cDate);
}


std::tuple<QString, QString> CUtils::getCbUTXOsDateRange(QString cDate)
{
  return getACycleRange(cDate, CConsts::COINBASE_MATURATION_CYCLES);
}

/**
 * @brief CUtils::getCoinbaseInfo
 * @param cDate
 * @param cycle
 * @return {cycleStamp, from_, to_, fromHour, toHour}
 */
std::tuple<QString, QString, QString, QString, QString> CUtils::getCoinbaseInfo(
  QString cDate,
  QString cycle)
{
  if (cDate != "")
  {
    auto[from_, to_] = getCoinbaseRange(cDate);
    return {
      getCoinbaseCycleStamp(cDate),
      from_, to_,
      from_.split(' ')[1], to_.split(' ')[1]  // fromHour, toHour
    };

  } else if (cycle != "")
  {
    auto[from_, to_] = getCoinbaseRangeByCycleStamp(cycle);
    return {
      cycle,
      from_, to_,
      from_.split(' ')[1], to_.split(' ')[1]  // fromHour, toHour
    };

  }
}

/**
 * @brief CUtils::getPrevCoinbaseInfo
 * @param cDate
 * @return {cycleStamp, from_, to_, fromHour, toHour}
 */
std::tuple<QString, QString, QString, QString, QString> CUtils::getPrevCoinbaseInfo(const QString &cDate)
{
  auto[from_, to_] = getACycleRange(cDate, 1);
  Q_UNUSED(to_);
  return getCoinbaseInfo(from_);
}


std::tuple<QString, QString> CUtils::getCoinbaseRangeByCycleStamp(const QString &cycle)
{
  QStringList cycleDtl = cycle.split(" ");
  if (cycleDtl[1] == "00:00:00")
  {
    return {cycleDtl[0] + " 00:00:00", cycleDtl[0] + " 11:59:59"};

  } else if (cycleDtl[1] == "12:00:00"){
    return {cycleDtl[0] + " 12:00:00", cycleDtl[0] + " 23:59:59"};

  } else {
    QString cDate = minutesAfter(cycleDtl[1].toUInt() * getCycleByMinutes(), cycleDtl[0] + " 00:00:01");
    return getCoinbaseRange(cDate);

  }
}








//  -  -  -  -  -  -  - array functions
/**
 *
 * @param {superset}
 * @param {subset}
 * output = superset - subset
 *
 */
QStringList CUtils::arrayDiff(const QStringList &superset, const QStringList &subset)
{
  QStringList reminedValues = {};
  for(QString element: superset)
    if (!subset.contains(element))
      reminedValues.append(element);
  return reminedValues;
}

QStringList CUtils::arrayAdd(const QStringList &arr1, const QStringList &arr2)
{
  QStringList out = arr1;
  for (QString elm: arr2)
    out.append(elm);
  return out;
}

QStringList CUtils::arrayUnique(QStringList arr)
{
  arr.removeDuplicates();
  return arr;
}

QList<MPAIValueT> CUtils::listUnique(const QList<MPAIValueT> &l)
{
//  return std::unique (l.begin(), l.end());
  QSet<MPAIValueT> ss(l.begin(), l.end());
  QList<MPAIValueT> ll(ss.begin(), ss.end());
  return ll;
}

QList<MPAIValueT> CUtils::intReverseSort(QList<MPAIValueT> l)
{
  std::sort (l.begin(), l.begin());
  std::reverse(l.begin(), l.end());

  return l;

//  QStringList padded_numbers = {};
//  for(MPAIValueT a_number: arr)
//  {
//    padded_numbers.append(QString::number(a_number).rightJustified(17, '0'));
//  }
//  padded_numbers.sort();
//  std::reverse(padded_numbers.begin(), padded_numbers.end());

//  QList<MPAIValueT> out {};
//  for(QString a_number: padded_numbers)
//  {
//    out.push_back(static_cast<MPAIValueT>(a_number.toDouble()));
//  }
//  return out;
}

QString CUtils::normalizeCommaSeperatedStr(QString str)
{
  if (str == "")
    return "";

  QStringList elms = str.split(",");
  QStringList new_elms {};
  for(QString elm: elms)
    if(elm != "")
      new_elms.append(elm);
  return ("," + new_elms.join(","));
}


//  -  -  -  math wrappers
MPAIValueT CUtils::truncMPAI(const float value)
{
  return trunc(value);
}

MPAIValueT CUtils::truncMPAI(const double value)
{
  return trunc(value);
}



// - - - - - - legacy iutils part - - - - -

int CUtils::getAppCloneId()
{
  return 0; // FIXME: get this value from MainWindow
}

bool CUtils::isValidVersionNumber(const QString &ver)
{
    if (ver == "")
        return false;

    QStringList ver_segments = ver.split(".");
    if (ver_segments.size() != 3)
        return false;

    for (QString aSeg: ver_segments)
    {
        if (QString::number(aSeg.toUInt()) != aSeg)
            return false;
    }

    return true;
}

// FIXME: remove all exit points and resolve it in serenity
void CUtils::exiter(const QString &msg, const uint8_t &number)
{
  CLog::log(msg, "app", "fatal");
  CLog::log(QString("%1").arg(number), "app", "fatal");
  exit(number);
}

void CUtils::exiter(const QString &msg, const int &number)
{
  CLog::log(msg, "app", "fatal");
  CLog::log(QString("%1").arg(number), "app", "fatal");
  exit(number);
}

QString CUtils::packCoinRef(const QString &ref_trx_hash, const uint16_t &output_index)
{
  return QStringList {ref_trx_hash, QString::number(output_index)}.join(":");
}

std::tuple<QString, OutputIndexT> CUtils::unpackCoinRef(const QString &coin)
{
  QStringList segments = coin.split(":");
  return {segments[0], static_cast<OutputIndexT>(segments[1].toUInt())};
}

QString CUtils::shortCoinRef(const QString &coin)
{
  return QStringList{hash16c(coin.split(":")[0]), coin.split(":")[1]}.join(':');
}

std::tuple<QString, QString> CUtils::unpackCoinSpendLoc(const QString &spend_location)
{
  QStringList segments = spend_location.split(":");
  return {segments[0], segments[1]};
}

uint8_t CUtils::getMatureCyclesCount(const QString &document_type)
{
  QHash<QString, uint8_t> cycle_map {
    {CConsts::DOC_TYPES::BasicTx, 1},
    {CConsts::DOC_TYPES::DPCostPay, 1},

    {CConsts::DOC_TYPES::Coinbase, 2},
    {CConsts::DOC_TYPES::RpDoc, 2},
    {CConsts::DOC_TYPES::RlDoc, 2}};

  if(cycle_map.keys().contains(document_type))
    return cycle_map[document_type];

  CLog::log("Invalid document_type in 'get Mature Cycles Count'! " + document_type, "app", "error");
  return 0;
}

bool CUtils::isMatured(
  const QString &docType,
  const QString &coinCreationDate,
  const QString &cDate)
{
  uint8_t matureCycles = getMatureCyclesCount(docType);
  if (matureCycles == 0)
    return false;

  // control maturity
  if (
    // (spendBlock.bType != iConsts.BLOCK_TYPES.RpBlock) &&
    (CUtils::timeDiff(coinCreationDate, cDate).asMinutes < (matureCycles * getCycleByMinutes()))
  )
  {
    QString msg = "${level}.is Matured: block(${utils.hash6c(spendBlock.blockHash)} ${cDate}) ";
    msg += "uses an output(${refDoc.dType} ${utils.hash6c(refDoc.hash)})  ${coinCreationDate}) ";
    msg += "before being maturated by ${matureCycles} cycles";
    CLog::log(msg, "sec", "error");
    return false;
  }

  return true;
}

bool CUtils::isInCurrentCycle(const QString &cDate)
{
  auto[from_, to_] = getCoinbaseRange();
  Q_UNUSED(to_);
  return (cDate >= from_);
}
