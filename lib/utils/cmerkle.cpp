#include "stable.h"
#include "lib/ccrypto.h"
#include "cmerkle.h"

namespace CMerkle
{


//static merklePresenter(m) {
//    let out = `Root: ${m.root} \nLeaves: ${m.levels} \nLevels: ${m.leaves} \nProofs:`;

//    let inx = 0;
//    _.forOwn(m.proofs, function (value, key) {
//        out += `\n\tleave(${inx}): ${key} => ` + ((utils._notNil(value.m_left_hash)) ? `\n\t\tleftHsh: ${value.m_left_hash}` : ``)
//        out += `\n\t\thashes: `
//        value.m_proofs.forEach(element => {
//            out += element + ` `
//        });
//        inx += 1;
//    });
//    return out;
//}

QString getRootByAProve(QString leave, QStringList proveHashes, QString lHash, QString inputType, QString hashAlgorithm)
{
  if (inputType == "string")
  {
    leave = doHashANode(leave, hashAlgorithm);
  }
  QString proof = "";
  if (lHash != "")
  {
    proof = doHashANode(lHash + leave, hashAlgorithm);
  }
  else
  {
    proof = leave;
  }

  if (proveHashes.size() > 0)
  {
    for (QString element : proveHashes){
      QString pos = element.mid(0, 1);
      QString val = element.mid(2);
      if (pos == "r")
      {
        proof = doHashANode(proof + val, hashAlgorithm);
      }
      else
      {
        proof = doHashANode(val + proof, hashAlgorithm);
      }
    }
  }
  return proof;
}

QString doHashANode(const QString &node_value, const QString &hash_algorithm)
{
  if (hash_algorithm == "keccak256")
  {
    return CCrypto::keccak256(node_value);
  }
  if (hash_algorithm == "noHash")
  {
    return node_value;
  }
  if (hash_algorithm == "aliasHash")
  {
    return "h(" + node_value + ")";
  }

  CLog::log("Invalid hash for merkle!");
  exit(888);
}

std::tuple<QString, MNodesMapT, QString, int, int> generate(QStringList elms, QString inputType, QString hashAlgorithm, QString version)
{
  MNodesMapT p;
  if (elms.size() == 0)
  {
    return {
      "", //root
      p, //proofs
      version,
      0,  //levels
      0  //leaves
    };
  }

  if (elms.size() == 1)
    {
      return {
        (inputType == "hashed") ? elms[0] : doHashANode(elms[0], hashAlgorithm),  //root
        p, // proofs
        version,
        1,  //levels
        1  // leaves
      };
    }

  auto[root, verifies, leaves, levels] = innerMerkle(elms, inputType, hashAlgorithm);

  // let verifies;
  MNodesMapT final_verifies;
  QStringList keys = verifies.keys();
  keys.sort();  // FIXME: the right order is reverse sort, also consider the version 0.1.0 in order to solve "fake right leave" problem
  for (QString key : keys)
  {
    MerkleNodeData a_proof = verifies[key];
    final_verifies[key.midRef(2).toString()].m_proofs = a_proof.m_proof_keys;
    final_verifies[key.midRef(2).toString()].m_left_hash = a_proof.m_left_hash;
  }
  return {
      root,
      final_verifies,
      version,
      levels,
      leaves
    };
};


std::tuple<QString, MNodesMapT, int, int> innerMerkle(QStringList elms, QString inputType, QString hashAlgorithm, QString version)
{
  if (inputType == "string")
  {
    QStringList hashedElms {};
    for (QString element : elms)
    {
      hashedElms.push_back(doHashANode(element, hashAlgorithm));
    }
    elms = hashedElms;
  }

  MNodesMapT verifies {};
  int leaves = elms.size();
  int level = 0;
  QString parent;
  QString lKey, rKey;
  QString lChild, rChild;
  while (level < 1000000) {
    level += 1;
    if (elms.size() == 1)
      return
      {
        elms[0], //root:
        verifies,
        leaves,
        level
      };

    if (elms.size() % 2 == 1)
    {
      // adding parity right fake hash
      QString theHash = elms[elms.size() - 1];
      if (version != "0.0.0")
        theHash = "FRL:" + theHash;

      elms.push_back(theHash);
    }

    QVector<QStringList> chunks = CUtils::chunkStringList(elms, 2);
    elms = QStringList {};
    for (QStringList chunk : chunks) {
      parent = doHashANode(chunk[0] + chunk[1], hashAlgorithm);
      elms.push_back(parent);
      if (level == 1)
      {
        QString lKey = "l_" + chunk[0];
        QString rKey = "r_" + chunk[1];
//        QStringList proofKeys = QStringList {};
        verifies.insert(lKey, MerkleNodeData {
            QStringList {},   // proofKeys
            parent,
            ""    //  lHash
          }
        );
        verifies.insert(rKey, MerkleNodeData {
            QStringList {},   // proofKeys
            parent,
            chunk[0]  //  lHash
          }
        );
        verifies[lKey].m_proof_keys.append("r." + chunk[1]);

      }
      else
      {
        // find alter parent cild
        for (QString key: verifies.keys())
        {
          MerkleNodeData value = verifies.value(key);
            if (chunk[0] == value.m_parent) {
              verifies[key].m_proof_keys.append("r." + chunk[1]);
              verifies[key].m_parent = parent;
            }
            if (chunk[1] == value.m_parent) {
              verifies[key].m_proof_keys.append("l." + chunk[0]);
              verifies[key].m_parent = parent;
            }
//            lChild = _.find(verifies, { parent: chunk[0] })

        }
      }
    }
  }

  return
  {
    "", //root:
    MNodesMapT{},
    0,
    0
  };

}



}



