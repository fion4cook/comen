#ifndef CUtils_H
#define CUtils_H

#include <math.h>

#include <stdint.h>
#include <algorithm>
#include <string>
#include <regex>

#include <boost/config.hpp>
#include <boost/regex.hpp>

#include <QString>
#include <QVector>
#include <QStringList>
#include <QJsonObject>
#include <QJsonDocument>

#include "lib/vector.h"

//#include "vector.h"
#include <qdebug.h>
#include <qdatetime.h>

#include "constants.h"
#include "lib/clog.h"

class UnlockSet;
class IndividualSignature;
class UnlockDocument;
class CMachine;
class Document;


#include "lib/database/db_model.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"
#include "lib/machine/machine_handler.h"
#include "lib/wallet/wallet_address_handler.h"
#include "lib/dag/normal_block/normal_utxo_handler.h"
#include "lib/block/block_types/block_coinbase/coinbase_utxo_handler.h"

class TimeDiff
{
public:
  uint64_t asYears = 0;
  uint64_t years = 0;
  uint64_t asMonths = 0;
  uint64_t months = 0;
  uint64_t asDays = 0;
  uint64_t days = 0;
  uint64_t asHours = 0;
  uint64_t hours = 0;
  uint64_t asMinutes = 0;
  uint64_t minutes = 0;
  uint64_t asSeconds = 0;
  uint64_t seconds = 0;

  TimeDiff(
    const uint64_t &asYears_ = 0,
    const uint64_t &years_ = 0,
    const uint64_t &asMonths_ = 0,
    const uint64_t &months_ = 0,
    const uint64_t &asDays_ = 0,
    const uint64_t &days_ = 0,
    const uint64_t &asHours_ = 0,
    const uint64_t &hours_ = 0,
    const uint64_t &asMinutes_ = 0,
    const uint64_t &minutes_ = 0,
    const uint64_t &asSeconds_ = 0,
    const uint64_t &seconds_ = 0
  );
  bool operator ==(const TimeDiff &obj);
  bool operator !=(const TimeDiff &obj);

};



typedef long double longDouble;

class CUtils
{
public:

  CUtils(){};

  static double customFloorFloat(double number, uint8_t percision = 11);
  static double iFloorFloat(double number);
  static double CFloor(double v);
  static std::tuple <longDouble, longDouble, longDouble, longDouble> calcLog(
    longDouble xValue,
    uint64_t range,
    uint64_t exp = 17);
  static QString lightRandom(const int &max=100);


  static QString getLaunchDate();
  static bool isGreaterThanNow(const QString &cDate);
  static void exitIfGreaterThanNow(const QString &cDate);

  static QString dumpIt(const bool &status);
  static QString dumpIt(const QStringList &values, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<QStringList> &rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVDicT &values, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const UI64DicT &values, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QList<QVDicT> &rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVDRecordsT &rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, QHash<QString, QVDRecordsT>> &groupped_rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<QVector<QVDicT> > &rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  template<typename T>
  QList<T> chunkIt(T values, uint64_t chunk_size=2);
  static QVector<QStringList> chunkStringList(QStringList values, uint64_t chunk_size);
  static QStringList chunkString(const QString &content, const uint16_t &size);
  static QString dumpIt(const UnlockSet &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<UnlockSet> &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const UnlockDocument &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const IndividualSignature &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QVector<IndividualSignature> &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QV2DicT &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const CListListT &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, QString> &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QJsonValue &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QJsonObject &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const JRecordsT &rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QJsonArray &rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const GRecordsT &groupped_rows, const QString &prefix_tabs = CConsts::DUMPER_INDENT);
  static QString dumpIt(const QHash<QString, Document*> &custom_data, const QString &prefix_tabs = CConsts::DUMPER_INDENT);

  static QString paddingDocLength(const int &value, const int &neededLen = 7);
  static QString paddingDocLength(const QString &value, const int &neededLen = 7);

  static QString hash4c(const QString &s);
  static QString hash6c(const QString &s);
  static QString hash8c(const QString &s);
  static QString hash16c(const QString &s);
  static QString hash32c(const QString &s);
  static QString hash64c(const QString &s);
  static QString shortBech16(const QString &s);

  static QString removeNewLine(QString s);
  static QString replaceNewLineByBackN(QString s);
  static QString removeTab(QString s);
  static QString removeTabNL(const QString &s);
  static QString removeDblSpaces(QString s);
  static QString breakByBR(const QString &content, const uint16_t &size = 100);
  static QString stripBR(QString  content);

  static QJsonObject parseToJsonObj(const QString &serialized = "{}");
  static QJsonObject parseToJsonObj(const QVariant &serialized);
  static QJsonArray parseToJsonArr(const QString &serialized);
  static QString serializeJson(const QJsonObject &values);
  static QString serializeJson(const QJsonArray &values);
  static QString serializeJson(const QStringList &values);
  static QString getANullStringifyedJsonObj();
  static QString sanitizingContent(const QString &str);
  static QStringList convertJSonArrayToStringList(const QJsonArray &arr);

  static QString getNow();
  static QString getNowSSS();
  static QString getCurrentYear();
  static bool isValidDateForamt(const QString &cDate);

  static TimeDiff timeDiff(const QString &fromT, const QString &toT = getNow());
  //TimeDiff timeDiff(const QString &fromT, const QString &toT = QString::fromStdString(getNow()));
  static QString minutesBefore(const uint64_t &backInTimesByMinutes, const QString &cDate = "");
  static QString minutesAfter(const uint64_t &forwardInTimesByMinutes, const QString &cDate = "");
  static QString yearsBefore(const uint64_t &backInTimesByYears, const QString &cDate = "");
  static QString yearsAfter(const uint64_t &forwardInTimesByYears, const QString &cDate = "");
  static QString getCoinbaseCycleStamp(QString cDate = "");
  static uint getNowByMinutes();
  static QString getCoinbaseCycleNumber(QString cDate = "");
  static QString isAmOrPm(const uint &minutes);
  static uint getCycleByMinutes();
  static uint getCycleCountPerDay();
  static std::tuple<QString, QString> getACycleRange(
    QString cDate = getNow(),
    uint backByCycle = 0,
    uint forwardByCycle = 0);
  static QString convertMinutesToHHMM(uint minutes);
  static uint getCycleElapsedByMinutes(QString cDate = "");
  static std::tuple<QString, QString> getCoinbaseRange(const QString &cDate = getNow());
  static std::tuple<QString, QString> getCbUTXOsDateRange(QString cDate = getNow());
  static std::tuple<QString, QString> getCoinbaseRangeByCycleStamp(const QString &cycle);
  static std::tuple<QString, QString, QString, QString, QString> getCoinbaseInfo(
    QString cDate = "",
    QString cycle = "");
  static std::tuple<QString, QString, QString, QString, QString> getPrevCoinbaseInfo(const QString &cDate = getNow());

  static bool isValidHash(QString s);
  static QString stripNonHex(QString s);
  static QString sepNum(uint64_t number);
  static BlockLenT convertPaddedStringToInt(QString s);

  static QStringList convertJSonArrayToQStringList(const QJsonArray &arr);
  static QStringList arrayDiff(const QStringList &superset, const QStringList &subset);
  static QStringList arrayAdd(const QStringList &arr1, const QStringList &arr2);
  static QStringList arrayUnique(QStringList arr);
  static QList<MPAIValueT> listUnique(const QList<MPAIValueT> &l);
  static QList<MPAIValueT> intReverseSort(QList<MPAIValueT> l);
  static QString normalizeCommaSeperatedStr(QString str) ;

//  -  -  -  math wrappers
  static MPAIValueT truncMPAI(const float value);
  static MPAIValueT truncMPAI(const double value);

// - - - - - - legacy iutils part - - - - -

  static int getAppCloneId();
  static bool isValidVersionNumber(const QString &ver);
  static void exiter(const QString &msg, const uint8_t &number = 0);
  static void exiter(const QString &msg, const int &number);
  static QString packCoinRef(const QString &ref_trx_hash, const uint16_t &output_index);

  /**
   * @brief unpackCoinRef
   * @param coin
   * @return {doc_hash, ouput_index}
   */
  static std::tuple<QString, OutputIndexT> unpackCoinRef(const QString &coin);

  static QString shortCoinRef(const QString &coin);
  static std::tuple<QString, QString> unpackCoinSpendLoc(const QString &refLoc);

  static uint8_t getMatureCyclesCount(const QString &document_type);
  static bool isMatured(
    const QString &docType,
    const QString &coinCreationDate,
    const QString &cDate = CUtils::getNow());

  static bool isInCurrentCycle(const QString &cDate = getNow());

};

#endif // namespace CUtils_H
