#include "stable.h"
#include "lib/ccrypto.h"
#include "block_utils.h"

BlockUtils::BlockUtils()
{

}

WrapRes BlockUtils::wrapSafeContentForDB(const QString &content, const QString &sfVer)
{
    /**
     * to make a safe string to insert in db, jus convert it to base64
     */
    WrapRes res;
    if (sfVer == "0.0.0")
    {
        QString b64 = CCrypto::base64Encode(content);
        QJsonObject jsonObj
        {
            {"sfVer", sfVer},
            {"content", b64},
        };
        res.status = true;
        res.content = CUtils::serializeJson(jsonObj);
        CLog::log("Safe Wrapped Content: " + res.content, "app");
        return res;
    }
    else
    {
        QString msg = "unknown sfVer version: " + sfVer;
        CLog::log(msg, "app", "error");
        res.status = false;
        res.msg = msg;
        return res;
    }
}

WrapRes BlockUtils::unwrapSafeContentForDB(const QString &wrapped)
{
  WrapRes res;
  QJsonObject JsonObj = CUtils::parseToJsonObj(wrapped);

  //    QJsonValue vvv = JsonOb.value("content");
  QString content = JsonObj.value("content").toString();
  QString sfVer = JsonObj.value("sfVer").toString();

  if (sfVer == "0.0.0")
  {
    content = CCrypto::base64Decode(content);
  }
  res.sfVer = sfVer;
  res.content = content;
  res.status = true;

  //    QJsonValue sSetsList = unserObj.value(QString("sSets"));
  //    qDebug() << sSetsList;
  //    QJsonObject item = sSetsList.toObject();
  //    qDebug() << ("QJsonObject of description: ") << item;



  return res;
}


bool BlockUtils::ifAncestorsAreValid(const QStringList &ancestors)
{
  // TODO: since the address is in hex base, add hex char controll
  for (QString an_ancestor: ancestors)
  {
  if ((an_ancestor == "") || (an_ancestor.length() != 64))
    return false;
  }
  return true;
}
