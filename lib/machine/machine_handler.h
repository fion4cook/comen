#ifndef CMACHINE_H
#define CMACHINE_H

class CUtils;


#include "lib/transactions/basic_transactions/signature_structure_handler/individual_signature.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_set.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/unlock_document.h"
#include "lib/transactions/basic_transactions/signature_structure_handler/general_structure.h"

#include "lib/address/address_handler.h"
#include "lib/wallet/wallet_address_handler.h"
#include "lib/k_v_handler.h"
#include "lib/dag/dag.h"

//class MainWindow;
//#include "mainwindow.h"

class EmailSettings
{
public:
  QString m_address = "abc@def.gh";
  QString m_password = "";
  QString m_income_IMAP = "993";
  QString m_income_POP3 = "995";
  QString m_incoming_mail_server = "";
  QString m_outgoing_mail_server = "";
  QString m_outgoing_SMTP = "465";
  QString m_fetching_interval_by_minute = "5";  // it depends on smtp server, but less than 5 minute is useless
  QString m_PGP_private_key = "";
  QString m_PGP_public_key = "";

  EmailSettings()
  {}
  void importJson(const QJsonObject &obj);
  QJsonObject exportJson();
};

class MPSetting
{
public:
  MPSetting();

  EmailSettings m_public_email;
  EmailSettings m_private_email;

  QString m_machine_alias = "imagineNode";
  UnlockDocument *m_backer_detail{};
  QString m_language = CConsts::DEFAULT_LANG;
  QString m_term_of_services = CConsts::NO;
  QJsonArray m_already_presented_neighbors = {};

  void importJson(const QJsonObject &obj);
  QJsonObject exportJson();

};

class MachineProfile
{
public:
  QString m_mp_code = "";
  QString m_mp_name = "";
  QString m_mp_last_modified = "";
  MPSetting m_mp_settings;
  QJsonObject m_this_json;

  // provides a new template profile
  MachineProfile();
  // retrieve profile from DB and sets to object
  MachineProfile(const QString &mp_code);
  // set passed JSon object to itself
  MachineProfile(const QJsonObject &profile);

  void importJson(const QJsonObject &profile);
  QJsonObject exportJson();

};


class CMachine
{


public:
  CMachine(const CMachine&) = delete;

  const static QString stbl_machine_profiles;
  const static QString stbl_machine_neighbors;

  MachineProfile m_profile;
//  static MainWindow* getMW(){ return get().m_mw; };


  static CMachine &get(){return s_instance;};

  // machine_handler.cpp
  GenRes initDefaultProfile();
  static GenRes initMachine(){return get().IinitMachine();}

//  static bool setClone(const uint8_t &clone_id, MainWindow* mw){return get().IsetClone(clone_id, mw);}
  static bool setClone(const uint8_t &clone_id){return get().IsetClone(clone_id);}
  static bool createFolders(){return get().IcreateFolders();}

  static QString getBackerAddress(){return get().IgetBackerAddress();}
  static uint8_t getAppCloneId(){return get().IgetAppCloneId();}
  static MachineProfile getProfile(){return get().IgetProfile();}
  static EmailSettings getPubEmailInfo(){return get().IgetPubEmailInfo();}
  static EmailSettings getPrivEmailInfo(){return get().IgetPrivEmailInfo();}
  static bool setPublicEmailAddress(const QString & v){return get().IsetPublicEmailAddress(v);}
  static bool setPublicEmailInterval(const QString & v){return get().IsetPublicEmailInterval(v);}
  static bool setPublicEmailIncomeHost(const QString & v){return get().IsetPublicEmailIncomeHost(v);}
  static bool setPublicEmailPassword(const QString & v){return get().IsetPublicEmailPassword(v);}
  static bool setPublicEmailIncomeIMAP(const QString & v){return get().IsetPublicEmailIncomeIMAP(v);}
  static bool setPublicEmailIncomePOP(const QString & v){return get().IsetPublicEmailIncomePOP(v);}
  static bool setPublicEmailOutgoingSMTP(const QString & v){return get().IsetPublicEmailOutgoingSMTP(v);}
  static bool setPublicEmailOutgoingHost(const QString & v){return get().IsetPublicEmailOutgoingHost(v);}
  static bool setTermOfServices(const QString & v){return get().IsetTermOfServices(v);}
  static bool saveSettings(){return get().IsaveSettings();}
  static QJsonObject getLastSyncStatus(){return get().IgetLastSyncStatus();};
  static bool isInSyncProcess(const bool &forceToControlBasedOnDAGStatus = false){return get().IisInSyncProcess(forceToControlBasedOnDAGStatus);}
  bool loadSelectedProfile();
  static QString getSelectedMProfile();
  static uint64_t getCycleByMinutes();
  static uint64_t getParsingQGap();
  static uint64_t getCoinbaseImportGap();
  static uint64_t getHardDiskReadingGap();
  static uint64_t getNBUTXOsImportGap();
  static uint32_t getAcceptableBlocksGap();
  static uint32_t getBlockInvokeGap();
  static uint64_t getSendingQGap();

  static double getMachineServiceInterests(
    const QString &dType,
    const QString &dClass = "",
    const DocLenT &dLen = 0,
    const DocLenT &extra_length = 0,
    const int &supported_P4P_trx_count = 1)
  {
      return get().IgetMachineServiceInterests(
        dType,
        dClass,
        dLen,
        extra_length,
        supported_P4P_trx_count);
  }


  //  -  -  -  -  -  machine_backup.cpp


  //  -  -  -  -  -  machine_services_interests.cpp


  //  -  -  -  -  -  neighbors handler

  static QVDRecordsT getNeighbors(
    const QString &neighbor_type = "",
    const QString &connection_status = "",
    const QString &mp_code = getSelectedMProfile(),
    const QString &n_id = "",
    const QString &n_email = ""){ return get().IgetNeighbors(neighbor_type, connection_status, mp_code, n_id, n_email); };

  static QVDRecordsT getActiveNeighbors(const QString &mp_code = getSelectedMProfile());

  static std::tuple<bool, QString> addANewNeighbor(
    const QString &email,
    const QString &connection_type,
    const QString &public_key = "",
    const QString &mp_code = getSelectedMProfile(),
    const QString &is_active = CConsts::YES,
    const QJsonObject &neighbor_info = QJsonObject(),
    QString creation_date = "");

  static bool handshakeNeighbor(const QString &n_id, const QString &connection_type);

  static std::tuple<bool, bool> parseHandshake(
    const QString &sender_email,
    const QJsonObject &message,
    const QString &connection_type);

  static bool floodEmailToNeighbors(
    const QString &email,
    QString PGP_public_key = ""){ return get().IfloodEmailToNeighbors(email, PGP_public_key); };

  static bool setAlreadyPresentedNeighbors(const QJsonArray &already_presented_neighbors){ return get().IsetAlreadyPresentedNeighbors(already_presented_neighbors); }

  static QJsonArray getAlreadyPresentedNeighbors(){ return get().IgetAlreadyPresentedNeighbors(); }

  static bool deleteNeighbors(
    const QString &n_id,
    const QString &connection_type,
    const QString &mp_code = getSelectedMProfile()){return get().IdeleteNeighbors(n_id, connection_type, mp_code);}

  static std::tuple<bool, bool> parseNiceToMeetYou(
    const QString &sender_email,
    const QJsonObject &message,
    const QString &connection_type);

private:
  CMachine(){};
  static CMachine s_instance;
//  MainWindow* m_mw;

  uint8_t m_clone_id = 0;
  bool m_machine_is_loaded = false;
  bool m_db_is_connected = false;

  GenRes IinitMachine();
  QString IgetBackerAddress();
  bool IsetPublicEmailAddress(const QString & v);
  bool IsetPublicEmailInterval(const QString & v);
  bool IsetPublicEmailIncomeHost(const QString & v);
  bool IsetPublicEmailPassword(const QString & v);
  bool IsetPublicEmailIncomeIMAP(const QString & v);
  bool IsetPublicEmailIncomePOP(const QString & v);
  bool IsetPublicEmailOutgoingSMTP(const QString & v);
  bool IsetPublicEmailOutgoingHost(const QString & v);
  bool IsetTermOfServices(const QString & v);
  bool IsaveSettings();
  bool ImaybeAddSeedNeighbors();
  QJsonObject IgetLastSyncStatus();
  bool IinitLastSyncStatus();
  static bool IisInSyncProcess(const bool &forceToControlBasedOnDAGStatus = false);
  static double IgetMachineServiceInterests(
    const QString &dType,
    const QString &dClass = "",
    const DocLenT &dLen = 0,
    const DocLenT &extra_length = 0,
    const int &supported_P4P_trx_count = 1);

  uint8_t IgetAppCloneId(){
    return m_clone_id;
  }

  bool IsetClone(const uint8_t &clone_id) //, MainWindow* mw
  {
//    m_mw = mw;  //MainWindow* mw
    m_clone_id = clone_id;
    return true;
  }

  bool IcreateFolders();

  MachineProfile IgetProfile(){
    return m_profile;
  }

  EmailSettings IgetPubEmailInfo(){return m_profile.m_mp_settings.m_public_email;}
  EmailSettings IgetPrivEmailInfo(){return m_profile.m_mp_settings.m_private_email;}


  //  -  -  -  -  -  neighbors handler
  QVDRecordsT IgetNeighbors(
    const QString &neighbor_type = "",
    const QString &connection_status = "",
    const QString &mp_code = getSelectedMProfile(),
    const QString &n_id = "",
    const QString &n_email = "");

  bool IfloodEmailToNeighbors(
    const QString &email,
    QString PGP_public_key = "");

  QJsonArray IgetAlreadyPresentedNeighbors(){ return m_profile.m_mp_settings.m_already_presented_neighbors; }
  bool IsetAlreadyPresentedNeighbors(const QJsonArray &already_presented_neighbors){ m_profile.m_mp_settings.m_already_presented_neighbors = already_presented_neighbors; return true; }
  bool IdeleteNeighbors(
    const QString &n_id,
    const QString &connection_type,
    const QString &mp_code = getSelectedMProfile());


};

#endif // CMACHINE_H
