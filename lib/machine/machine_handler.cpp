#include "stable.h"
#include "machine_handler.h"


//  -  -  -  EmailSettings
QJsonObject EmailSettings::exportJson()
{
  return QJsonObject
  {
    {"m_address", m_address},
    {"m_password", m_password},
    {"m_income_IMAP", m_income_IMAP},
    {"m_income_POP3", m_income_POP3},
    {"m_incoming_mail_server", m_incoming_mail_server},
    {"m_outgoing_mail_server", m_outgoing_mail_server},
    {"m_outgoing_SMTP", m_outgoing_SMTP},
    {"m_fetching_interval_by_minute", m_fetching_interval_by_minute}, // it depends on smtp server, but less than 5 minute is useless
    {"m_PGP_private_key", m_PGP_private_key},
    {"m_PGP_public_key", m_PGP_public_key}
  };
}

void EmailSettings::importJson(const QJsonObject &obj)
{
  m_address = obj.value("m_address").toString();
  m_password = obj.value("m_password").toString();
  m_income_IMAP = obj.value("m_income_IMAP").toString();
  m_income_POP3 = obj.value("m_income_POP3").toString();
  m_incoming_mail_server = obj.value("m_incoming_mail_server").toString();
  m_outgoing_mail_server = obj.value("m_outgoing_mail_server").toString();
  m_outgoing_SMTP = obj.value("m_outgoing_SMTP").toString();
  m_fetching_interval_by_minute = obj.value("m_fetching_interval_by_minute").toString();
  m_PGP_private_key = obj.value("m_PGP_private_key").toString();
  m_PGP_public_key = obj.value("m_PGP_public_key").toString();
}




//  -  -  -  MPSetting
MPSetting::MPSetting()
{

}

void MPSetting::importJson(const QJsonObject &obj)
{
  m_machine_alias = obj.value("m_machine_alias").toString();
  m_language = obj.value("m_language").toString();
  m_term_of_services = obj.value("m_term_of_services").toString();
  m_machine_alias = obj.value("m_machine_alias").toString();
  m_already_presented_neighbors = obj.value("m_already_presented_neighbors").toArray();
  m_backer_detail = new UnlockDocument();
  m_backer_detail->importJson(obj.value("m_backer_detail").toObject());
  m_public_email.importJson(obj.value("m_public_email").toObject());
  m_private_email.importJson(obj.value("m_private_email").toObject());
}

QJsonObject MPSetting::exportJson()
{
  return QJsonObject
  {
    {"m_machine_alias", m_machine_alias},
    {"m_language", m_language},
    {"m_term_of_services", m_term_of_services},
    {"m_machine_alias", m_machine_alias},
    {"m_backer_detail", m_backer_detail->exportJson()},
    {"m_public_email", m_public_email.exportJson()},
    {"m_private_email", m_private_email.exportJson()},
    {"m_already_presented_neighbors", m_already_presented_neighbors}
  };
}






//  -  -  -  MachineProfile

MachineProfile::MachineProfile()
{

}

MachineProfile::MachineProfile(const QString &mp_code)
{
  QueryRes prf = DbModel::select(
    CMachine::stbl_machine_profiles,
    {"mp_code", "mp_name", "mp_settings"},
    {{"mp_code", mp_code}}
  );
  if (prf.records.size() == 1)
  {
    QJsonObject profile = CUtils::parseToJsonObj(prf.records[0]["mp_settings"]);
    importJson(profile);
  }
}

MachineProfile::MachineProfile(const QJsonObject &profile)
{
  importJson(profile);
}

void MachineProfile::importJson(const QJsonObject &obj)
{
  m_mp_code = obj.value("m_mp_code").toString();
  m_mp_name = obj.value("m_mp_name").toString();
  m_mp_last_modified = obj.value("m_mp_last_modified").toString();
  m_mp_settings.importJson(obj.value("m_mp_settings").toObject());
}

QJsonObject MachineProfile::exportJson()
{
  m_this_json = QJsonObject
  {
    {"m_mp_code", m_mp_code},
    {"m_mp_name", m_mp_name},
    {"m_mp_last_modified", m_mp_last_modified},
    {"m_mp_settings", m_mp_settings.exportJson()}
  };
  return m_this_json;
}






#include "machine_backup.cpp"
#include "machine_neighbor.cpp"
#include "machine_services_interests.cpp"


/**
 * the machine can have 1 or more diffrent profile(s)
 * each profile has it's own public/private email & public/private iPGP key pairs & it's neightbors set &
 * and it's wallet addresses and
 * machine_onchain_contracts
 * and maybe kvalue
 *
 * adding profile field to all tables
 * machine_tmp_documents, machine_buffer_documents
 * machine_neighbors, machine_wallet_addresses, machine_wallet_funds
 * machine_draft_proposals, machine_used_utxos, machine_ballots, machine_draft_pledges
 *
 *   // the status be booting, synching, ready
  // booting: when nodes starts to connecting to network for first time
  // synching: when latest confirmed blocks are created before 12 hours ago
  // ready: the node has some confirmed blocks created in last 12 hour in his locl DB
  // status: constants.NODE_IS_BOOTING,
  // lastConfirmedBlockDate: IMAGINE_LAUNCH_DATE,

  // machine email setting
  // each node has 2 email addreess, public & private to resist against the spamming...
  // TODO: maybe machine have to have ability to have more than one email to comunicate to prevent against any censorship

 */






// -  -  -  -  -  -  CMachine  -  -  -  -

CMachine CMachine::s_instance;
const QString CMachine::stbl_machine_profiles = "c_machine_profiles";
const QString CMachine::stbl_machine_neighbors = "c_machine_neighbors";

//CMachine& CMachine::get()
//{
//  return s_instance;
//}

GenRes CMachine::initDefaultProfile()
{

  m_profile = MachineProfile(CConsts::DEFAULT);
  if (m_profile.m_mp_code == CConsts::DEFAULT)
    return { true, "The Default profile Already initialized" };

  // initializing default valuies and save it

  m_profile.m_mp_code = CConsts::DEFAULT;
  m_profile.m_mp_name = CConsts::DEFAULT;
  m_profile.m_mp_last_modified = CUtils::getNow();

  {
    // initializing email PGP pair keys (it uses native node.js crypto lib. TODO: probably depricated and must refactor to use new one)
    auto[status, private_PGP, public_PGP] = CCrypto::nativeGenerateKeyPairSync();
      if (!status)
        return {false, "Couldn't creat RSA key pairs (for private channel)"};
    m_profile.m_mp_settings.m_private_email.m_PGP_private_key = private_PGP;
    m_profile.m_mp_settings.m_private_email.m_PGP_public_key = public_PGP;
  }
  {
    // initializing email PGP pair keys (it uses native node.js crypto lib. TODO: probably depricated and must refactor to use new one)
    auto[status, private_PGP, public_PGP] = CCrypto::nativeGenerateKeyPairSync();
      if (!status)
        return {false, "Couldn't creat RSA key pairs (for public channel)"};
    m_profile.m_mp_settings.m_public_email.m_PGP_private_key = private_PGP;
    m_profile.m_mp_settings.m_public_email.m_PGP_public_key = public_PGP;
  }


  auto[status, unlock_doc] = CAddress::createANewAddress(
    CConsts::SIGNATURE_TYPES::Strict,
    "2/3",
    "0.0.1");
  if (!status)
    return {false, "Couldn't creat ECDSA key pairs (for public channel)"};

  m_profile.m_mp_settings.m_backer_detail = new UnlockDocument();
  m_profile.m_mp_settings.m_backer_detail = &unlock_doc;
  m_profile.m_mp_settings.m_machine_alias = "node-" + CUtils::hash6c(CCrypto::keccak256(unlock_doc.m_account_address));

  IsaveSettings();

  // set selected profile=default
  DbModel::upsert(
    "c_kvalue",
    "kv_key",
    "SELECTED_PROFILE",
    QVDicT
    {
      {"kv_value", m_profile.m_mp_code},
      {"kv_last_modified", m_profile.m_mp_last_modified}
    }
  );

  // add backer address to wallet as well
  auto[status2, addresses] = WalletAddressHandler::searchWalletAdress(
    QStringList{m_profile.m_mp_settings.m_backer_detail->m_account_address},
    getSelectedMProfile(),
    {"wa_address"});
  Q_UNUSED(status2);

  if (addresses.size() == 0)
    WalletAddressHandler::insertAddress(WalletAddress (
      m_profile.m_mp_settings.m_backer_detail,
      CConsts::DEFAULT,   // mp code
      "Backer Address (" +
      m_profile.m_mp_settings.m_backer_detail->m_unlock_sets[0].m_signature_type + " " +
      m_profile.m_mp_settings.m_backer_detail->m_unlock_sets[0].m_signature_ver + ")"
    ));

  ImaybeAddSeedNeighbors();

  return { true, "The Default profile initialized" };
}




GenRes CMachine::IinitMachine()
{

//  if (m_machine_is_loaded)
//    return true;

  // control DataBase
  if (!m_db_is_connected)
  {
    auto[status, msg] = DbHandler::initDb();
    m_db_is_connected = status;
    if (!status)
      CLog::log(msg);
  }


  GenRes res = initDefaultProfile();
  if (res.status != true)
      return res;

  // load machine settings
  loadSelectedProfile();

  m_machine_is_loaded = true;

  return true;
}

bool CMachine::IcreateFolders()
{
  if(CConsts::HD_ROOT_PATHE != "")
    if(!QDir(CConsts::HD_ROOT_PATHE).exists())
      QDir().mkdir(CConsts::HD_ROOT_PATHE);

  if(!QDir(CConsts::HD_PATHES::HD_FILES).exists())
    QDir().mkdir(CConsts::HD_PATHES::HD_FILES);

  QString inbox = CConsts::HD_PATHES::INBOX;
  if(getAppCloneId() > 0)
    inbox += QString::number(getAppCloneId());
  if(!QDir(inbox).exists())
    QDir().mkdir(inbox);

  QString outbox = CConsts::HD_PATHES::OUTBOX;
  if(getAppCloneId() > 0)
    outbox += QString::number(getAppCloneId());
  if(!QDir(outbox).exists())
    QDir().mkdir(outbox);

  QString backup_dag = CConsts::HD_PATHES::BACKUP_DAG;
  if(getAppCloneId() > 0)
    backup_dag += QString::number(getAppCloneId());
  if(!QDir(backup_dag).exists())
    QDir().mkdir(backup_dag);


  return true;
}

QString CMachine::IgetBackerAddress()
{
  return m_profile.m_mp_settings.m_backer_detail->m_account_address;
}

bool CMachine::loadSelectedProfile()
{
  QueryRes selected_prof = DbModel::select(
    "c_kvalue",
    QStringList {"kv_value"},     // fields
    {ModelClause("kv_key", "SELECTED_PROFILE")}
  );
  if (selected_prof.records.size() != 1) {
      return false;
  }


  MachineProfile mp(selected_prof.records[0]["kv_value"].toString());
  m_profile = mp;
  return true;
}

QString CMachine::getSelectedMProfile()
{
  QueryRes res = DbModel::select(
    "c_kvalue",
    {"kv_value"},
    {{"kv_key", "SELECTED_PROFILE"}}
  );
  // console.log('resresresresres', res);
  if (res.records.size() != 1) {
    CLog::log("NoooOOOOOOOOOOOOOOooooooooooooo profile!", "app", "fatal");
    exit(345);
  }
  return res.records[0].value("kv_value").toString();
}

// - - - - - - cycle functions - - - - -
uint64_t CMachine::getCycleByMinutes()
{
  if (CConsts::TIME_GAIN == 1){
    return CConsts::STANDARD_CYCLE_BY_MINUTES;
  }
  return CConsts::TIME_GAIN;
}

uint64_t CMachine::getParsingQGap()
{
  return 1900;

  uint64_t gap_by_second;
  if (CConsts::TIME_GAIN == 1)
  {
    // live
    if (isInSyncProcess())
    {
        gap_by_second = 5; // every 3 second check parsing q
    } else {
        gap_by_second = 63; // every 1 minutes check parsing q
    }
  } else {
    //develop
    if (isInSyncProcess())
    {
        gap_by_second = CConsts::TIME_GAIN / 5;
    } else {
        gap_by_second = CConsts::TIME_GAIN / 1;
    }
  }
  CLog::log("parsing Q Gap every " + QString::number(gap_by_second) + " second");
  return gap_by_second;
}

uint64_t CMachine::getCoinbaseImportGap()
{
  return 900;
  return (getCycleByMinutes() * 60) / 13;
}

uint64_t CMachine::getNBUTXOsImportGap()
{
  return 900;
  uint64_t gap_by_seconds;
  if (CConsts::TIME_GAIN == 1)
  {
    // live mode
    if (isInSyncProcess())
    {
        gap_by_seconds = CConsts::STANDARD_CYCLE_BY_MINUTES / 10;
    } else {
        gap_by_seconds = CConsts::STANDARD_CYCLE_BY_MINUTES / 4;
    }
  } else {
    // develope mode
    if (isInSyncProcess())
    {
        gap_by_seconds = CConsts::TIME_GAIN * 3;
    } else {
        gap_by_seconds = CConsts::TIME_GAIN * 6;
    }
  }

  return gap_by_seconds;
}

uint64_t CMachine::getHardDiskReadingGap()
{
  return 900;
  if (isInSyncProcess())
  {
    if (CConsts::TIME_GAIN == 1)
      return 20; // every 20 seconds check read a file from inbox folder (if exists)
    return CConsts::TIME_GAIN / 2; // it is testing ambianet value

  } else {
    if (CConsts::TIME_GAIN == 1)
        return 120; // every 2 minutes check read a file from inbox folder (if exists)
    return CConsts::TIME_GAIN / 1; // it is testing ambianet value

  }
}

uint64_t CMachine::getSendingQGap()
{
  return 900;
  uint64_t gap_by_seconds;
  if (CConsts::TIME_GAIN == 1)
  {
    // live
    if (isInSyncProcess())
    {
      gap_by_seconds = 30; // every 1 minutes send to sending q
    } else {
      gap_by_seconds = 100; // every 5 minutes send to sending q
    }
  } else {
    //develop
    if (isInSyncProcess())
    {
      gap_by_seconds = CConsts::TIME_GAIN / 2;
    } else {
      gap_by_seconds = CConsts::TIME_GAIN;
    }
  }
  CLog::log("sending Q fetch Gap = " + QString::number(gap_by_seconds), "app", "trace");
  return gap_by_seconds;
}


bool CMachine::IsetPublicEmailAddress(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_address = v;
  return true;
}

bool CMachine::IsetPublicEmailInterval(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_fetching_interval_by_minute = v;
  return true;
}

bool CMachine::IsetPublicEmailIncomeHost(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_incoming_mail_server = v;
  return true;
}

bool CMachine::IsetPublicEmailPassword(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_password = v;
  return true;
}

bool CMachine::IsetPublicEmailIncomeIMAP(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_income_IMAP = v;
  return true;
}

bool CMachine::IsetPublicEmailIncomePOP(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_income_POP3 = v;
  return true;
}

bool CMachine::IsetPublicEmailOutgoingSMTP(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_outgoing_SMTP = v;
  return true;
}

bool CMachine::IsetPublicEmailOutgoingHost(const QString & v)
{
  m_profile.m_mp_settings.m_public_email.m_outgoing_mail_server = v;
  return true;
}

bool CMachine::IsetTermOfServices(const QString & v)
{
  m_profile.m_mp_settings.m_term_of_services = v;
  return true;
}

bool CMachine::IsaveSettings()
{
  QString serialized_setting = CUtils::serializeJson(m_profile.exportJson());
  QVDicT values {
//    {"mp_code", m_profile.m_mp_code},
    {"mp_name", m_profile.m_mp_name},
    {"mp_settings", serialized_setting},
    {"mp_last_modified", m_profile.m_mp_last_modified}
  };
  DbModel::upsert(
      CMachine::stbl_machine_profiles,
      "mp_code",
      m_profile.m_mp_code,
      values,
      true);

  return true;
}


bool CMachine::ImaybeAddSeedNeighbors()
{
  addANewNeighbor("matbit@secmail.pro", CConsts::PUBLIC);
  return true;
}


QJsonObject CMachine::IgetLastSyncStatus()
{
  QString lastSyncStatus = KVHandler::getValue("LAST_SYNC_STATUS");
  if (lastSyncStatus == "")
  {
    get().IinitLastSyncStatus();
    lastSyncStatus = KVHandler::getValue("LAST_SYNC_STATUS");
  }
  return CUtils::parseToJsonObj(lastSyncStatus);
}

bool CMachine::IinitLastSyncStatus()
{
    QJsonObject lastSyncStatus {
      {"isInSyncMode", "Unknown"},
      {"lastTimeMachineWasInSyncMode",
        CUtils::minutesBefore(CMachine::getCycleByMinutes() * 2)},
      {"checkDate", CUtils::minutesBefore(CMachine::getCycleByMinutes())},
      {"lastDAGBlockCreationDate", "Unknown"}
    };
    return KVHandler::upsertKValue(
      "LAST_SYNC_STATUS",
      CUtils::serializeJson(lastSyncStatus));
}


/**
* if the creationdate of latest recorded block in DAG is older than 2 cycle, so machine must go to synching mode
* @param {*} args
*/
bool CMachine::IisInSyncProcess(const bool &forceToControlBasedOnDAGStatus)
{
  QString lastSyncStatus = KVHandler::getValue("LAST_SYNC_STATUS");
  if (lastSyncStatus == "")
  {
   get().IinitLastSyncStatus();
   QString lastSyncStatus = KVHandler::getValue("LAST_SYNC_STATUS");
  }
  QJsonObject lastSyncStatusObj = CUtils::parseToJsonObj(lastSyncStatus);

  uint64_t cycleByMinutes = CMachine::getCycleByMinutes();
  // control if the last status-check is still valid (is younger than 30 minutes?= 24 times in a cycle)
  if (!forceToControlBasedOnDAGStatus &&
    (lastSyncStatusObj.value("checkDate").toString() > CUtils::minutesBefore((cycleByMinutes / 24))))
  {
    return (lastSyncStatusObj.value("lastDAGBlockCreationDate").toString() < CUtils::minutesBefore(2 * cycleByMinutes));
  }
  else
  {
    // re-check graph info & update status-check info too
    auto[status, blockRecord] = DAG::getLatestBlockRecord();
    if (status)
        CUtils::exiter("No block in DAG exit!!", 111);

    bool is_in_sync_process = (blockRecord.m_creation_date < CUtils::minutesBefore(2 * cycleByMinutes));

    lastSyncStatusObj.insert("isInSyncMode", is_in_sync_process ? "Y" : "N");
    lastSyncStatusObj.insert("checkDate", CUtils::getNow());
    lastSyncStatusObj.insert("lastDAGBlockCreationDate", blockRecord.m_creation_date);
    if (is_in_sync_process)
      lastSyncStatusObj.insert("lastTimeMachineWasInSyncMode", CUtils::getNow());
    KVHandler::upsertKValue("LAST_SYNC_STATUS", CUtils::serializeJson(lastSyncStatusObj));
    return is_in_sync_process;
  }
}


// it means maximum how long we suppose some nodes creae a new block(except coinbase block)
uint32_t CMachine::getAcceptableBlocksGap()
{
  uint32_t gapByMinutes;
  if (CConsts::TIME_GAIN == 1)
  {
    // live
    gapByMinutes = isInSyncProcess() ? 11 : 71;
  } else {
    // devel
    gapByMinutes = isInSyncProcess() ? (uint32_t)(CConsts::TIME_GAIN / 0.15) : (uint32_t)(CConsts::TIME_GAIN / 0.5);
  }

  CLog::log("acceptable block gap By Minutes(" + QString::number(gapByMinutes) + ") ", "app", "trace");
  return gapByMinutes;
}

uint32_t CMachine::getBlockInvokeGap()
{
      return 500;
  if (isInSyncProcess())
  {
    return (CConsts::TIME_GAIN == 1) ? CConsts::STANDARD_CYCLE_BY_MINUTES / 500 : CConsts::TIME_GAIN / 20;
  } else {
    return (CConsts::TIME_GAIN == 1) ? CConsts::STANDARD_CYCLE_BY_MINUTES / 120 : CConsts::TIME_GAIN / 9;
  }
}









