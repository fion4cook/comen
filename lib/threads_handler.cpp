#include "stable.h"
#include "lib/file_buffer_handler/file_buffer_handler.h"
#include "lib/messaging_protocol/dag_message_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "threads_handler.h"

ThreadsHandler::ThreadsHandler()
{

}

void launchEmailPoper(std::string cDate = "")
{
  CLog::log("Email Poper stuff..." );
  std::this_thread::sleep_for (std::chrono::seconds(CConsts::GAP_EMAIL_POP));
  launchEmailPoper(cDate);
//  CLog::log("Normal asyncDoImport midle" );
//  NormalUTXOHandler::importNormalBlockUTXOs(cDate);
//  CLog::log("Normal asyncDoImport after" );
}

void launchSmartPullFromParsingQ()
{
  CLog::log("Smart Pull From Parsing Queue..." );
  std::this_thread::sleep_for (std::chrono::seconds(CMachine::getParsingQGap()));
  ParsingQHandler::recursiveSmartPullFromParsingQ();
  launchSmartPullFromParsingQ();
}

void launchImportUTXOsFromNormalBlocks()
{
  CLog::log(" Import UTXOs From Normal Blocks..." );
  std::this_thread::sleep_for (std::chrono::seconds(CMachine::getNBUTXOsImportGap()));
  NormalUTXOHandler::recursiveImportUTXOs();
  launchImportUTXOsFromNormalBlocks();
}

void recursiveMissedBlocksInvoker()
{
  CLog::log(" Import UTXOs From Normal Blocks..." );
  std::this_thread::sleep_for (std::chrono::seconds(CMachine::getBlockInvokeGap()));
  DAGMessageHandler::recursiveMissedBlocksInvoker();
  recursiveMissedBlocksInvoker();
}

void recursivePullSendingQ()
{
  CLog::log(" Import UTXOs From Normal Blocks..." );
  std::this_thread::sleep_for (std::chrono::seconds(CMachine::getSendingQGap()));
  SendingQHandler::recursivePullSendingQ();
  recursivePullSendingQ();
}


void launchImportUTXOsFromCoinbaseBlocks()
{
  CLog::log(" Import UTXOs From Normal Blocks..." );
  std::this_thread::sleep_for (std::chrono::seconds(CMachine::getCoinbaseImportGap()));
  CoinbaseUTXOHandler::recursiveImportUTXOs();
  launchImportUTXOsFromCoinbaseBlocks();
}


void launchHardCopyReading(std::string cDate = "")
{
  CLog::log("launchHardCopyReading..." );
  std::this_thread::sleep_for (std::chrono::seconds(CMachine::getHardDiskReadingGap()));
  FileBufferHandler::readAndParseHardDiskInbox();
  launchHardCopyReading(cDate);
}


void launchThreads_()
{
  std::this_thread::sleep_for (std::chrono::seconds(60));

  {
    // ingress cpackets and parsing
    if (CConsts::EMAIL_IS_ACTIVE)
      std::thread(launchEmailPoper, "").detach();

    // read messages from hard drive
    std::this_thread::sleep_for (std::chrono::seconds(5));
    FileBufferHandler::maybeBootDAGFromBundle();
    std::thread(launchHardCopyReading, "").detach();

    // read messages from database queue
    std::this_thread::sleep_for (std::chrono::seconds(5));
    std::thread(launchSmartPullFromParsingQ).detach();

  }

  {
    // coin importing

    // import new minted coins
    std::this_thread::sleep_for (std::chrono::seconds(5));
    std::thread(launchImportUTXOsFromCoinbaseBlocks).detach();

    // import UTXOs
    std::this_thread::sleep_for (std::chrono::seconds(5));
    std::thread(launchImportUTXOsFromNormalBlocks).detach();
  }

  // missed blocks
  std::this_thread::sleep_for (std::chrono::seconds(5));
  std::thread(recursiveMissedBlocksInvoker).detach();

  {
    // output cpackets

    // fetching sending queue
    std::this_thread::sleep_for (std::chrono::seconds(5));
    std::thread(recursivePullSendingQ).detach();

  }

}


void ThreadsHandler::launchThreads()
{
  std::thread(launchThreads_).detach();
}

