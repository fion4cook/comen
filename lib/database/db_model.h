#ifndef DBMODEL_H
#define DBMODEL_H

#include <QVariant>

#include "constants.h"
#include "lib/utils/cutils.h"
#include "lib/clog.h"
#include "lib/database/db_handler.h"


struct QueryElements{
  QString   m_clauses;
  QVDicT     m_values;
  QString   m_order;
  QString   m_limit;
};

class OrderModifier
{
public:
  QString m_field;
  QString m_order;

  OrderModifier(const QString &field, const QString &order);
};

class ModelClause
{
public:
  QString m_clause_operand = "=";
  QString m_fieldName;
  QVariant m_fieldSingleValue;
  QStringList m_fieldMultiValues;

  ModelClause(const QString &fieldName, const QVariant &fieldValue);
  ModelClause(const QString &fieldName, const QVariant &fieldValue, const QString &clause_operand);
  ModelClause(const QString &fieldName, const QStringList &fieldValues, const QString &clause_operand = "IN");

};

class ModelParams
{
public:
  QString m_table;
  ClausesT m_clauses;
  QStringList m_fields;   // the * is not valid for fields, and fields must be listed before head
  OrderT m_order = {};
  int m_limit = 0;

  ModelParams(
    const QString &table,
    const QStringList &fields,
    const ClausesT &clauses = {},
    const OrderT &order={{}},
    const int &limit=0);

};

struct PTRRes
{
  QString complete_query;
  QueryElements qElms;
};

struct QueryRes
{
  bool status = false;
  QVDRecordsT records;
};

class DbModel
{
public:
  static QVector<QString> s_single_operands;

  DbModel();

  static PTRRes prepareToSelect(
    const QString &table,
    const QStringList &fields,
    const ClausesT &clauses = {},
    const OrderT &order = {},
    const int &limit = 0);

  static QueryRes select(
    const QString &table,
    const QStringList &fields,
    const ClausesT &clauses = {},
    const OrderT &order = {},
    const int &limit = 0,
    const bool &is_transactional = false,
    const bool &do_log = true);

  static PTRRes prepareToDelete(
    const QString &table,
    const ClausesT &clauses);

  static QueryRes dDelete(
    const QString &table,
    const ClausesT &clauses,
    const bool &is_transactional = false,
    const bool &do_log = true);

  static QueryElements clauseQueryGenerator(
    const ClausesT &clauses = {},
    const OrderT &order = {},
    const int &limit = 0);

  static bool insert(
    const QString &table,
    const QVDicT &values,
    const bool &is_transactional = false,
    const bool &do_log = true);

  static QueryRes sCustom(
    const QString &complete_query = "",
    const QStringList &fields = {},
    const int &field_count = 0,
    const QVDicT &to_bind_values = {},
    const bool &is_transactional = false,
    const bool &do_log = true);

  static QueryRes sQuery(
    const QString &complete_query,
    const ClausesT &clauses,
    const QStringList &fields = {},
    const QVDicT &upd_values = {},
    const bool &is_transactional = false,
    const bool &do_log = true);

  static bool upsert(
    const QString &table,
    const QString &controledField,
    const QString &controledValue,
    const QVDicT &values,
    const bool &is_transactional = false,
    const bool &do_log = true);

  static bool update(
    const QString &table,
    const QVDicT &update_values,
    const ClausesT &update_clauses = {},
    const bool &is_transactional = false,
    const bool &do_log = true);

  static std::tuple<QString, QStringList> prepareToUpdate(
    const QString &table,
    const QVDicT &values,
    const ClausesT &clauses);

  static bool ClauseContains(ClausesT &clauses, const QString &filedName);   //legacy queryHas
};

#endif // DBMODEL_H
