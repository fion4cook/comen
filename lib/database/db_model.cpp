#include "db_model.h"

OrderModifier::OrderModifier(const QString &field, const QString &order)
{
  m_field = field;
  m_order = order;
}

ModelClause::ModelClause(const QString &fieldName, const QVariant &fieldValue, const QString &clause_operand)
{
  m_fieldName = fieldName;
  m_fieldSingleValue = fieldValue;
  m_clause_operand = clause_operand;
}

ModelClause::ModelClause(const QString &fieldName, const QVariant &fieldValue)
{
  m_fieldName = fieldName;
  m_fieldSingleValue = fieldValue;
}

ModelClause::ModelClause(const QString &fieldName, const QStringList &fieldValues, const QString &clause_operand)
{
  m_fieldName = fieldName;
  m_fieldSingleValue = "";
  m_fieldMultiValues = fieldValues;
  m_clause_operand = clause_operand;
}

QVector<QString> DbModel::s_single_operands = {"=", "<", ">", "<=", "=<", ">="};

DbModel::DbModel()
{
}


QueryRes DbModel::select(
  const QString &table,
  const QStringList &fields,
  const ClausesT &clauses,
  const OrderT &order,
  const int &limit,
  const bool &is_transactional,
  const bool &do_log)
{
  PTRRes r = prepareToSelect(table, fields, clauses, order, limit);
  return DbModel::sQuery(r.complete_query, clauses, fields, {}, is_transactional, do_log);//, lockDb, log);
}

QueryRes DbModel::dDelete(
  const QString &table,
  const ClausesT &clauses,
  const bool &is_transactional,
  const bool &do_log)
{
  PTRRes r = prepareToDelete(table, clauses);
  return DbModel::sQuery(r.complete_query, clauses, {});//, lockDb, log);
}

PTRRes DbModel::prepareToSelect(
  const QString &table,
  const QStringList &fields,
  const ClausesT &clauses,
  const OrderT &order,
  const int &limit)
{
  QueryElements qElms = clauseQueryGenerator(clauses, order, limit);
  QString complete_query = "SELECT " + fields.join(", ") + " FROM " + table + qElms.m_clauses + qElms.m_order + qElms.m_limit;
  complete_query = complete_query.trimmed();
  complete_query = CUtils::removeDblSpaces(complete_query);
//    return {QString::fromUtf8(s.c_str()), qElms};
  return {complete_query, qElms};
}

PTRRes DbModel::prepareToDelete(const QString &table,
                                const ClausesT &clauses)
{
  QueryElements qElms = clauseQueryGenerator(clauses);
  QString complete_query = "DELETE FROM " + table + qElms.m_clauses + qElms.m_order + qElms.m_limit;
  complete_query = complete_query.trimmed();
  complete_query = CUtils::removeDblSpaces(complete_query);
  return {complete_query, qElms};
}

QueryElements DbModel::clauseQueryGenerator(
        const ClausesT &clauses,
        const OrderT &order,
        const int &limit)
{
  QVDicT _values = {};
  QString _clauses = "";
  std::vector<QVariant> valArr;

  if (clauses.size()>0)
  {
    QStringList tmp;

    for (ModelClause aClauseTuple: clauses)
    {
       QString key = aClauseTuple.m_fieldName;
       if (DbModel::s_single_operands.contains(aClauseTuple.m_clause_operand))
       {
         tmp << " (" + key + " " + aClauseTuple.m_clause_operand + " :" + key + ") ";
         _values.insert(key, aClauseTuple.m_fieldSingleValue);
       }
       else if ((aClauseTuple.m_clause_operand == "IN") || (aClauseTuple.m_clause_operand == "NOT IN"))
       {
         QStringList placeholders = {};
         for (int i=0; i < aClauseTuple.m_fieldMultiValues.size(); i++)
         {
           placeholders.push_back(":" + key + QString("%1").arg(i));
           _values.insert(key, aClauseTuple.m_fieldMultiValues[i]);
         }
         tmp << " (" + key + " " + aClauseTuple.m_clause_operand + " (" + placeholders.join(", ") + ")) ";
       }
       else if (aClauseTuple.m_clause_operand == "LIKE:OR")
       {

       }

    }

    if ((tmp.size() > 0) && (_values.keys().size() > 0)) {
      _clauses = " WHERE " + tmp.join(" AND ");
    }
  }

  QString _order = "";
  if (order.size() > 0)
  {
    QStringList orders;
    for (OrderModifier anOrder :  order)
    {
      orders << anOrder.m_field + " " + anOrder.m_order;
    }
    _order = " ORDER BY " + orders.join(", ");
  }


  QString _limit = "";
  if (limit > 0)
  {
    _limit = " LIMIT " + QString("%1").arg(limit) + " ";
  }


  return {
    _clauses,
    _values,
    _order,
    _limit
  };

}

QueryRes DbModel::sCustom(
  const QString &complete_query,
  const QStringList &fields,
  const int &field_count,
  const QVDicT &to_bind_values,
  const bool &is_transactional,
  const bool &do_log)
{
  Q_UNUSED(is_transactional);
  QSqlQuery query(DbHandler::getDB());
  query.setForwardOnly(true);
  if (do_log)
    CLog::log(complete_query, "sql", "trace");

  query.prepare(complete_query);

  for (QString aKey : to_bind_values.keys())
    query.bindValue(":"+aKey, to_bind_values[aKey]);

  QMap<QString, QVariant> sqlIterator(query.boundValues());
  QStringList boundList;
  for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
  {
    boundList.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
  }
  if (do_log)
    CLog::log("Query Values: [" + boundList.join(", ") + "]", "sql", "trace");


  QVDRecordsT records;
  if (!query.exec())
  {
    if (do_log)
      CLog::log(query.lastError().text(), "sql", "fatal");
    return {
      false,
      records
    };
  }

  while (query.next())
  {
    QVDicT aRow;
    if(fields.size() > 0)
    {
      for ( QString aFiled: fields)
      {
        aRow[aFiled] = query.value(aFiled);
      }

    }else{
      // return results by position
      for(int i=0; i<field_count; i++)
        aRow[QString::number(i)] = query.value(i);

    }
    records.push_back(aRow);

    QMap<QString, QVariant> sqlIterator(query.boundValues());
    QStringList boundList;
    for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
    {
      boundList.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
    }
  }

  if (query.isSelect())
  {
    CLog::log("Results: [" + QString("%1").arg(records.size()) + " rows] returned", "sql", "trace");
  }
  else if (query.numRowsAffected() > 0)
  {
    CLog::log("Affected: [" + QString("%1").arg(query.numRowsAffected()) + " rows] afected", "sql", "trace");
  }


  query.finish();

  return {
      true,
      records
  };
}

QueryRes DbModel::sQuery(
  const QString &complete_query,
  const ClausesT &clauses,
  const QStringList &fields,
  const QVDicT &upd_values,
  const bool &is_transactional,
  const bool &do_log)
{
  Q_UNUSED(is_transactional);
  QSqlQuery query(DbHandler::getDB());
  query.setForwardOnly(true);
  if (do_log)
    CLog::log(complete_query, "sql", "trace");

  query.prepare(complete_query);


  for (QString aKey : upd_values.keys())
  {
//    logValues.append(upd_values[aKey].toString());
    query.bindValue(":"+aKey, upd_values[aKey]);
  }


  for (ModelClause aClauseTuple: clauses)
  {
    QString fieldName = aClauseTuple.m_fieldName;
    QString fieldValue;
    if (DbModel::s_single_operands.contains(aClauseTuple.m_clause_operand))
    {
      fieldValue = aClauseTuple.m_fieldSingleValue.toString();
      query.bindValue(":"+fieldName, fieldValue);
    }
    else if ((aClauseTuple.m_clause_operand == "IN") || (aClauseTuple.m_clause_operand == "NOT IN"))
    {
      QStringList placeholders = {};
      for (int i=0; i < aClauseTuple.m_fieldMultiValues.size(); i++)
      {
        fieldValue = aClauseTuple.m_fieldMultiValues[i];
        query.bindValue(":"+fieldName + QString("%1").arg(i), fieldValue);
      }

    }
    else if (aClauseTuple.m_clause_operand == "LIKE:OR")
    {

    }

  }

  QMap<QString, QVariant> sqlIterator(query.boundValues());
  QStringList boundList;
  for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
  {
    boundList.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
  }
  if (do_log)
    CLog::log("Query Values: [" + boundList.join(", ") + "]", "sql", "trace");

  QVDRecordsT records;
  if (!query.exec())
  {
    if (do_log)
    {
      CLog::log("Thread failed: " + QString::number((quint64)QThread::currentThread(), 16), "sql", "fatal");
      CLog::log(query.lastError().text(), "sql", "fatal");
    }
    return {
      false,
      records
    };
  }
//    std::cout << std::endl << "res count" << query.size();
//    CLog::log("Result count: ["+static_cast<QString>(query.size()), "sql", "trace");

//    QSqlRecord rec = query.record();
//    std::cout << "Number of columns: " << rec.count();

  while (query.next())
  {

    QVDicT aRow;
    for ( QString aFiled: fields)
    {
      aRow[aFiled] = query.value(aFiled);
    }
    records.push_back(aRow);
//        QString kv_value = query.value(1).toString();
//        QString kv_value2 = query.value("kv_value").toString();
  }

  //    std::cout << std::endl << "kv_value2: " << kv_value.toStdString();



  if (query.isSelect())
  {
    CLog::log("Results: [" + QString("%1").arg(records.size()) + " rows] returned", "sql", "trace");
  }
  else if (query.numRowsAffected() >0 )
  {
    CLog::log("Affected: [" + QString("%1").arg(query.numRowsAffected()) + " rows] afected", "sql", "trace");
  }


  query.finish();

  return {
      true,
      records
  };

}


bool DbModel::insert(
  const QString &table,
  const QVDicT &values,
  const bool &is_transactional,
  const bool &do_log)
{
  Q_UNUSED(is_transactional);
  GenRes res;
  QList keys = values.keys();
  QString keysStr = keys.join(", ");
  QString keysHandlers = keys.join(", :");
  QString qStr = "INSERT INTO " + table + " (" + keysStr + ") VALUES (:" + keysHandlers + ")";
  if (do_log)
    CLog::log(qStr, "sql", "info");

  QSqlQuery query(DbHandler::getDB());
  query.prepare(qStr);
  QStringList logValues;
  for (QString aKey : keys)
  {
    query.bindValue(":"+aKey, values[aKey]);
//        logValues.push_back(values[aKey].toString());
  }
  if (!query.exec())
  {
    CLog::log(query.lastError().text(), "sql", "fatal");
    return false;
  }
//    CLog::log("values [" +logValues.join(", ") + "]", "sql", "info");
//    CLog::log("Query Values: [" + boundList.join(", ") + "]", "sql", "trace");
  QMap<QString, QVariant> sqlIterator(query.boundValues());
  QStringList boundList;
  for (auto i = sqlIterator.begin(); i != sqlIterator.end(); ++i)
  {
    boundList.push_back(i.key().toUtf8() + ": " + i.value().toString().toUtf8());
  }
  if (do_log)
    CLog::log("Query Values: [" + boundList.join(", ") + "]", "sql", "trace");

  query.finish();
  return true;
}

bool DbModel::upsert(
        const QString &table,
        const QString &controledField,
        const QString &controledValue,
        const QVDicT &values,
        const bool &is_transactional,
        const bool &do_log)
{

  const ClausesT clauses =
  {
    ModelClause(controledField, controledValue)
  };

  // controll if the record already existed
  QueryRes existed = DbModel::select(
    table,
    QStringList {controledField},     // fields
    clauses, //clauses
    {},   // order
    1,   // limit
    is_transactional,
    do_log
  );
  if (existed.records.size() > 0) {
    // update existed record
    DbModel::update(
          table,
          values, // update values
          clauses,  // update clauses
          is_transactional
          );

    return true;
  }

  // insert new entry
  QVDicT insValues = QHash(values);
  insValues[controledField] = controledValue;
  return DbModel::insert(
    table,     // table
    insValues, // values to insert
    true
  );

}

std::tuple<QString, QStringList> DbModel::prepareToUpdate(
    const QString &table,
    const QVDicT &upd_values,
    const ClausesT &clauses)
{
  QueryElements qElms = clauseQueryGenerator(clauses);
  QStringList field_clauses = {};
  QStringList fields = upd_values.keys();
  for (QString aKey : fields)
  {
    field_clauses.append(aKey+"=:"+aKey);
  }
  QString setFields = field_clauses.join(", ");
  QString complete_query = "UPDATE " + table + " SET " + setFields + qElms.m_clauses;
  complete_query = complete_query.trimmed();
  complete_query = CUtils::removeDblSpaces(complete_query);
//  complete_query = QString::fromUtf8(s.c_str());

  return {complete_query, fields};
}

bool DbModel::update(
  const QString &table,
  const QVDicT &update_values,
  const ClausesT &update_clauses,
  const bool &is_transactional,
  const bool &do_log)
{
  Q_UNUSED(is_transactional);
  auto[complete_query, fields] = prepareToUpdate(table, update_values, update_clauses);
  Q_UNUSED(fields);
  QueryRes updRes = DbModel::sQuery(complete_query, update_clauses, {}, update_values);//, lockDb, log);
  return true;
}


bool DbModel::ClauseContains(ClausesT &clauses, const QString &filedName)   //legacy queryHas
{
  for (ModelClause aClause : clauses)
  {
    if (aClause.m_fieldName == filedName)
      return true;
   }
   return false;
}









