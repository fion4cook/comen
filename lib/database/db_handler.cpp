#include "db_handler.h"


DbHandler DbHandler::s_instance;

DbHandler::~DbHandler()
{
    m_db.close();
}

std::tuple<bool, QString, QSqlDatabase> DbHandler::IconnectDB()
{
  auto thread_name = "CDB_" + QString::number((quint64)QThread::currentThread(), 16);
  CLog::log("thread_name to check(" + thread_name + ")", "sql", "trace");

  if (m_database_thread_connections.keys().contains(thread_name))
    return {true, thread_name + " Already connected to Database", m_database_thread_connections[thread_name]};

  CLog::log("thread_name to create(" + thread_name + ")", "sql", "trace");

  QSqlDatabase thread_db;

  const QString SQLITE_DRIVER {"QSQLITE"};
  if (QSqlDatabase::isDriverAvailable(SQLITE_DRIVER))
  {
    thread_db = QSqlDatabase::addDatabase(SQLITE_DRIVER, thread_name);
    QSqlQuery thread_q = QSqlQuery(thread_db);
    thread_db.setDatabaseName(QCoreApplication::applicationDirPath() + QDir::separator() + "comen.dat");
    if (!thread_db.open())
    {
      qDebug() << "Failed to open database" << thread_db.lastError();
      return
      {
        false,
        "Failed to open database",
        thread_db
      };
    }

//    if (m_database_thread_connections.keys().size() == 0)
//    {
      bool res = thread_q.exec("SELECT name FROM sqlite_master WHERE type='table' AND name='c_nodes_screenshots'");
      if (!res || !thread_q.last())
      {
        createTables(thread_q);
      }
//    }
  }
  else
  {
    return
    {
      false,
      "SQLITE_DRIVER Failed to open database",
      thread_db
    };
  }

  m_database_thread_connections.insert(thread_name, thread_db);

  return
  {
    true,
    "",
    m_database_thread_connections[thread_name]
  };

}

std::tuple<bool, QString> DbHandler::IinitDb()
{
  auto[status, msg, db] = connectDB();
  Q_UNUSED(db);
  return {status, msg};
}

QSqlDatabase DbHandler::IgetDB()
{
  auto[status, msg, db] = connectDB();
  Q_UNUSED(msg);
  if(status)
    return db;

//  if (m_database_thread_connections.keys().contains(thread_name))
//    return m_database_thread_connections[thread_name];

  auto thread_name = "CDB_" + QString::number((quint64)QThread::currentThread(), 16);
  CLog::log("Invalid thread database request " + thread_name, "sql", "fatal");
  QSqlDatabase t;
  return t;
};

int DbHandler::createTables(QSqlQuery thread_q)
{

    // Drop all tables
//    for (std::string aQuery : dbInitQueryDel)
//    {
//        QString qstr = QString::fromStdString(aQuery);
//        bool res = m_q.exec(qstr);
//        if (!res){
//            std::cout << std::endl << "Error! " << aQuery;
//        }
//    }

    // create essential tables
    for (std::string aQuery : dbInitQuery)
    {
        QString qstr = QString::fromStdString(aQuery);
        bool res = thread_q.exec(qstr);
        if (!res){
            std::cout << std::endl << "Error! " << aQuery;
        }
        thread_q.finish();
    }

    // create developers tables
    for (std::string aQuery : dbInitQueryDev)
    {
        QString qstr = QString::fromStdString(aQuery);
        bool res = thread_q.exec(qstr);
        if (!res){
            std::cout << std::endl << "Error! " << aQuery;
        }
        thread_q.finish();
    }

    return 0;
}

int DbHandler::initValues()
{
    return 1;
}










