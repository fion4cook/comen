#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <QtSql>
#include <QSqlDriver>
#include <QCoreApplication>
#include <QHash>
#include <QDir>
#include <QString>
#include <QDebug>

#include "constants.h"
#include "db_model.h"
#include "lib/clog.h"
//#include "lib/block/block_types/genesis.h"


#include "init_sql.h"


//class ProposalHandler;
////class DNAHandler;
//class Block;

#include "lib/services/dna/dna_handler.h"

class DbHandler
{

public:
  DbHandler(const DbHandler&) = delete;
  static DbHandler &get(){return s_instance;};
  ~DbHandler();

  QSqlDatabase m_db;
  QHash<QString, QSqlDatabase> m_database_thread_connections;
//  QSqlQuery m_q;

  static std::tuple<bool, QString> initDb(){ return get().IinitDb(); };
  static std::tuple<bool, QString, QSqlDatabase> connectDB(){ return get().IconnectDB(); }
//  static QSqlQuery getQ(){ return get().IgetQ(); };
  static QSqlDatabase getDB(){ return get().IgetDB(); };
  int createTables(QSqlQuery thread_q);
  int initValues();

private:
  DbHandler(){};
  static DbHandler s_instance;
  std::tuple<bool, QString, QSqlDatabase> IconnectDB();
  std::tuple<bool, QString> IinitDb();
//  QSqlQuery IgetQ(){ return m_q; };
  QSqlDatabase IgetDB();
};

#endif // DBHANDLER_H
