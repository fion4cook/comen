#ifndef MISSEDBLOCKSHANDLER_H
#define MISSEDBLOCKSHANDLER_H


#include "lib/dag/dag.h"
#include "lib/parsing_q_handler/parsing_q_handler.h"

class MissedBlocksHandler
{
public:
  static const QString stbl_missed_blocks;

  MissedBlocksHandler();

  static QStringList getMissedBlocksToInvoke(const uint64_t &limit = 0);

  static bool addMissedBlocksToInvoke(QStringList hashes = {});

  static bool removeFromMissedBlocks(const QString &block_hash);

  static bool increaseAttempNumber(const QString &block_hash);

  static bool refreshMissedBlock();


};

#endif // MISSEDBLOCKSHANDLER_H
