#ifndef FULLDAGHANDLER_H
#define FULLDAGHANDLER_H


class FullDAGHandler
{
public:
  FullDAGHandler();

  static std::tuple<QString , QString> invokeFullDAGDlRequest(const QString &start_from);

};

#endif // FULLDAGHANDLER_H
