#include "stable.h"

#include "super_control_til_coinbase_minting.h"

static int level = 0;
static QVDRecordsT coinTrack {};

SuperControlTilCoinbaseMinting::SuperControlTilCoinbaseMinting()
{

}

///**
//*
//* @param {*} spendBlock
//* @param {*} interestedDocs
//* going back all the way to find the Coinbase/RlBlock block for every spent inputs in given block
//*/
//SuperControlTilCoinbaseMinting::trackingBackTheCoins(
//  const QJsonObject &spendBlock,
//  const QStringList &interestedDocs = {},
//  const QStringList &interestedCoins = {})
//{
//  level = 0;
//  coinTrack = {};
//  if (CConsts::isSuperValidateDebug)
//  {
//    CLog::log(QString::number(level) + ".SCUUCM, Block(" + CUtils::hash8c(spendBlock.value("blockHash").toString()) + ")", "trx", "trace");
//    CLog::log(QString::number(level) + ".SCUUCM, spendBlock: " + CUtils::dumpIt(spendBlock), "trx", "trace");
//  }
//  let res = trackingBackTheCoinsRecursive(
//    spendBlock,
//    interestedDocs,
//    interestedCoins,
//    "Generated");
//  if (isSuperValidateDebug) {
//  clog.trx.info(`${level}.SCUUCM, coin Track: ${utils.stringify(coinTrack)}`);
//  }
//  res.coinTrack = coinTrack;
//  return res;
//}


//SuperControlTilCoinbaseMinting::trackingBackTheCoinsRecursive(
//  const QJsonObject &spendBlock,
//  const QStringList &interestedDocs = {},
//  const QStringList &interestedCoins = {},
//  const QString &descendent)
//{

//  level++;

//  QVDicT traceBack {
//    {"level", level},
//    {"bType", spendBlock.value("bType")},
//    {"creationDate", spendBlock.value("creationDate")},
//    {"blockHash", spendBlock.value("blockHash")},
//    {"descendent", descendent},
//    {"interestedDocs", interestedDocs},
//    {"interestedCoins", interestedCoins}
//  };
//  if ((level == 1) && (interestedDocs.size() == 0))
//  {
//    // log all docs as interested
//    traceBack["interestedDocs"] = QStringList{};
//    for(auto item: spendBlock.value("docs"))
//      traceBack["interestedDocs"].append(item.value("hash"));
//  }
//  if ((level == 1) && (interestedCoins.size() == 0))
//  {
//    // log all coins as interested
//    if (spendBlock.keys().contains("docs"))
//    {
//      for (auto aDoc: spendBlock.value("docs"))
//      {
//        if (aDoc.keys().contains("inputs"))
//        {
//          for (auto anInp: aDoc.value("inputs"))
//          {
//            traceBack["interestedCoins"].push_back(CUtils::packCoinRef(anInp[0], anInp[1]));
//          }
//        }
//      }
//    }
//  }
//  coinTrack.push_back(traceBack);


//  // retrieve spend block extra info
//  QJsonObject bExtInfo{};
//  if (spendBlock.keys().contains("bExtInfo"))
//  {
//    bExtInfo = spendBlock.value("bExtInfo").toObject();
//  } else {
//    auto[status, extExist, bExtInfo_] = Block::getBlockExtInfos(spendBlock.value("blockHash").toString());
//    bExtInfo = bExtInfo_;
//  }

//  QJsonArray documents = spendBlock.value("docs").toArray();
//  for (int docInx = 0; docInx < documents.size(); docInx++)
//  {
//    QJsonObject doc = documents[docInx].toObject();

//    if (CConsts::isSuperValidateDebug)
//      CLog::log(QString::number(level) + ".SCUUCM, spendBlock.blockHash(" + CUtils::hash8c(spendBlock.value("blockHash")) + " doc(" + CUtils::hash8c(doc.value("hash")) + ") docInx(" + QString::number(docInx) + "/" + doc.value("dType"), "trx", "info");

//    // if it is not first level, so it MUST have certain interested docHash to trace back
//    if ((level != 1) && !interestedDocs.contains(doc.value("hash").toString()))
//      continue;

//    /**
//     * in first level we can ask for control of all/some docs in block
//     * if is insisted to control certain docs and current doc is not one of mentioned docs,
//     * so contiue the loop
//     */
//    if ((level == 1) &&
//      (interestedDocs.size() > 0) &&
//      (!interestedDocs.contains(doc.value("hash").toString()))
//    ) {
//      continue;
//    }


//    if (CConsts::isSuperValidateDebug)
//        CLog::log(QString::number(level) + ".SCUUCM, control doc(" + CUtils::hash8c(doc.value("hash").toString()) + ")", "app", "trace");


//    if (!Document::trxHasInput(doc.value("dType")))
//    {
//        /**
//         * transaction has not input, so it must be trx in which the fresh output was created.
//         * either because of being Coinbase Trx or being the DPCostPay Trx
//         */
//        if (Document::trxHasNotInput(doc.value("dType")))
//        {
//          if (interestedDocs.contains(doc.value("hash") || ((level == 1) && (interestedDocs.size() == 0)))
//          {
//            // we found one interested fresh coin creating location
//            coinTrack.push_back(QVDicT {
//              {"level", level},
//              {"bType", spendBlock.value("bType")},
//              {"creationDate", spendBlock.value("creationDate")},
//              {"blockHash", spendBlock.value("blockHash")},
//              {"dType", doc.value("dType")},
//              {"descendent", doc.value("hash")},
//              {"interestedDocs", doc.value("hash")},
//              {"interestedCoins", QStringList{"fresh coin"}}});
//          }
//          continue;

//        } else {
//          CLog::log(QString::number(level) + ".SCUUCM, block(" + CUtils::hash8c(spendBlock.value("blockHash").toString()) + ") doc(" + CUtils::hash8c(doc.value("hash").toString()) +" must be Coinbase or DPCostPay and it is " + spendBlock.value("bType").toString() + "/" + doc.value("dType").toString(), "sec", "error");
//          return {false};
//        }
//    }

//    // console.log(`${level}.SCUUCM, ext Infos: ${JSON.stringify(ext Infos)} of block(${CUtils::hash8c(spendBlock.blockHash)})`);
//    // clog.trx.info(`${level}.SCUUCM, ext Infos: ${JSON.stringify(ext Infos)} of block(${CUtils::hash8c(spendBlock.blockHash)})`);
//    let docExtInfos = _.has(bExtInfo, docInx) ? bExtInfo[docInx] : null;

//    if (isSuperValidateDebug)
//        clog.trx.info(`${level}.SCUUCM, 1, the doc(${CUtils::hash8c(doc.hash)}) has some inputs(${doc.inputs.length}) to trace back`);

//    for (let inputIndex = 0; inputIndex < doc.inputs.length; inputIndex++) {
//        let input = doc.inputs[inputIndex];

//        let spendHash = input[0];
//        let spendIndex = input[1];

//        // mapping input ref to proper container block
//        let refBlockHashes = dagHandler.getBlockHashesByDocHashes({ docsHashes: [spendHash] }).blockHashes;
//        if (isSuperValidateDebug) {
//            clog.trx.info(`${level}.SCUUCM, docHash ${CUtils::hash8c(spendHash)}`);
//            clog.trx.info(`${level}.SCUUCM, refBlockHashes ${refBlockHashes.map(x => CUtils::hash8c(x))}`);
//        }
//        if (refBlockHashes.length == 0) {
//            msg = `${level}.SCUUCM the refloc ${input} does not mapped to it's container-block!`;
//            clog.sec.error(msg);
//            return { err: true, msg }
//        }


//        // another paranoic test
//        let controlRefInHirarechy = true;
//        if (controlRefInHirarechy) {
//            // go back to history by ancestors link to find reference location(which block creates this ref as output) for spendHash

//            levelAnc = 0;
//            SuperControlUntilCoinbaseMinting.lookForDocByAnc({
//                vSetter: setter1,
//                vGetter: getter1,
//                block: spendBlock,
//                docHash: spendHash,
//                level
//            });
//            clog.trx.info(`${level}.SCUUCM, getter1 finalresssssssssssssssss ${JSON.stringify(getter1())}`);

//            if (getter1('err') == true) {
//                return {
//                    err: getter1('err'),
//                    msg: getter1('msg')
//                };
//            }

//            if (refBlockHashes.indexOf(getter1('ref')) == -1) {
//                msg = `${level}.SCUUCM, the docHash(${CUtils::hash8c(spendHash)}) which is used in doc(${CUtils::hash8c(doc.hash)}) in block(${CUtils::hash8c(spendBlock.blockHash)})
//                    is referring to 2 diff blocks by Ancestors(${getter1('ref')}) &  i_docs_blocks_map(${refBlockHashes.map(x => CUtils::hash8c(x))})`;
//                clog.sec.error(msg);
//                return { err: true, msg }
//            }

//        }

//        // retrieve container block info
//        let refWBlocks = dagHandler.searchInDAGSync({
//            query: [
//                ['b_hash', ['IN', refBlockHashes]],
//            ]
//        });
//        if (refWBlocks.length != utils.arrayUnique(refBlockHashes).length) {
//            msg = `${level}.SCUUCM, some of the blocks(${refBlockHashes.map(x => CUtils::hash8c(x))}) do not exist in DAG (${refWBlocks.length})`;
//            clog.sec.error(msg);
//            return { err: true, msg }
//        }
//        for (let refWBlock of refWBlocks) {
//            // let refBlock = JSON.parse(refWBlock.body);
//            let refBlock = blockUtils.openDBSafeObject(refWBlock.bBody).content;

//            // clog.trx.info(`${level}.SCUUCM, minutessssssssssssssssssssssssssssssss: ${refBlock.creationDate}, ${spendBlock.creationDate} ${JSON.stringify(utils.timeDiff(refBlock.creationDate, spendBlock.creationDate))}`);
//            // defeniatelay refblock must be created AT LEAST one cycle before to be having spendable outputs
//            if (
//                (spendBlock.bType != iConsts.BLOCK_TYPES.RpBlock) &&
//                (utils.timeDiff(refBlock.creationDate, spendBlock.creationDate).asMinutes < iConsts.getCycleByMinutes())
//            ) {
//                msg = `${level}.SCUUCM, block(${CUtils::hash8c(spendBlock.blockHash)} ${spendBlock.creationDate}) `
//                msg += `uses an output of block(${CUtils::hash8c(refBlock.blockHash)} ${refBlock.creationDate}) before being maturated`;
//                clog.sec.error(msg);
//                return { err: true, msg }
//            }

//            // looking for refBlock.output = spendBlock.input
//            let inputExistInRefBlock = false;
//            for (let refDoc of refBlock.docs) {
//                if (refDoc.hash == spendHash) {
//                    inputExistInRefBlock = true;

//                    if (!iutils.trxHasNotInput(refDoc.dType)) {
//                        // also output address of ref controls!
//                        const mOfNHandler = require('../transaction/signature-structure-handler/general-m-of-n-handler');
//                        let isValidUnlock = mOfNHandler.validateSigStruct({
//                            address: refDoc.outputs[spendIndex][0],
//                            uSet: docExtInfos[inputIndex].uSet
//                        });
//                        if (isValidUnlock != true) {
//                            msg = `${level}.SCUUCM, Invalid given unlock structure for trx:${spendHash} input:${inputIndex}`;
//                            clog.sec.error(msg);
//                            return { err: true, msg }
//                        }
//                    }

//                    let matureCycles = iutils.getMatureCyclesCount(refDoc.dType)
//                    if (matureCycles.err != false)
//                        return matureCycles;
//                    matureCycles = matureCycles.number;
//                    if (matureCycles > 1) {
//                        // control maturity
//                        if (
//                            (![iConsts.BLOCK_TYPES.RpBlock].includes(spendBlock.bType)) &&
//                            (utils.timeDiff(refBlock.creationDate, spendBlock.creationDate).asMinutes < (matureCycles * iConsts.getCycleByMinutes()))
//                        ) {
//                            msg = `${level}.SCUUCM, block(${CUtils::hash8c(spendBlock.blockHash)} ${spendBlock.creationDate}) `;
//                            msg += `uses an output(${refDoc.dType} ${CUtils::hash8c(refDoc.hash)}) of block(${CUtils::hash8c(refBlock.blockHash)} ${refBlock.creationDate}) `;
//                            msg += `before being maturated by ${matureCycles} cycles`;
//                            clog.sec.error(msg);
//                            return { err: true, msg }
//                        }
//                    }

//                    // controlling the ref of ref
//                    return SuperControlUntilCoinbaseMinting.trackingBackTheCoinsRecursive({
//                        spendBlock: refBlock,
//                        descendent: doc.hash,
//                        interestedDocs: [refDoc.hash],
//                        interestedCoins: []
//                    });

//                }
//            }

//            if (!inputExistInRefBlock) {
//                msg = `${level}.SCUUCM, the input(${CUtils::hash8c(spendHash)}) which is used in block(${CUtils::hash8c(spendBlock.blockHash)}) do not exist in block(${CUtils::hash8c(refBlock.blockHash)})`;
//                clog.sec.error(msg);
//                return { err: true, msg }
//            }
//        }
//    }
//  }
//  return { err: false }
//  }

//  static lookForDocByAnc(args) {
//  let vSetter = args.vSetter;
//  vSetter({
//    ref: null,
//    calledAncestors: [],
//    err: false,
//    msg: ''
//  });
//  args.level = 0;
//  SuperControlUntilCoinbaseMinting.lookForDocByAncRecursive(args);
//  }

//  static lookForDocByAncRecursive(args) {
//  let vSetter = args.vSetter;
//  let vGetter = args.vGetter;
//  let block = args.block;
//  let docHash = args.docHash;
//  let level = _.has(args, 'level') ? args.level : 'u0';

//  if (vGetter('ref') != null)
//    clog.trx.info(`\n${level}.${levelAnc} SuperValidate, founded ref=${vGetter('ref')}`);


//  levelAnc++;
//  let msg;
//  if (isSuperValidateDebug)
//    clog.trx.info(`\n${level}.${levelAnc} SuperValidate, lookForDocByAnc Recursive: looking for docHash(${CUtils::hash8c(docHash)}) in block(${CUtils::hash8c(block.blockHash)})`);

//  // check if invoked doc exist in this block
//  if ((![iConsts.BLOCK_TYPES.FSign, iConsts.BLOCK_TYPES.FVote].includes(block.bType)) && _.has(block, 'docs') && (block.docs.map(x => x.hash).includes(docHash))) {
//    clog.trx.info(`\n${level}.${levelAnc} SuperValidate, OK. Found ref for docHash(${CUtils::hash8c(docHash)}) in block(${CUtils::hash8c(block.blockHash)})`);
//    vSetter({ ref: block.blockHash, err: false });
//    return;
//  }


//  let ancestors = block.ancestors;
//  if (isSuperValidateDebug)
//    clog.trx.info(`${level}.${levelAnc} SuperValidate, loopiong on ancestors(${utils.stringify(ancestors)})`);

//  if (ancestors.length == 0) {
//    msg = `${level}.SCUUCM, block(${CUtils::hash8c(block.blockHash)}) has no ancestors(${block.bType})`; // (${JSON.stringify(block)}) \n\n and still can not find the doc(${docHash})`;
//    clog.trx.info(msg);
//    return;
//  }
//  for (let ancHash of ancestors) {
//    let ancWBlocks = dagHandler.searchInDAGSync({
//        fields: ['b_body'],
//        query: [
//            ['b_hash', ancHash],
//        ],
//        log: false
//    });
//    if (ancWBlocks.length != 1) {
//        msg = `${level}.SCUUCM, block(${CUtils::hash8c(block.blockHash)}) has an ancestor(${ancHash}) which doesn't exist in DAG (${ancWBlocks.length})`;
//        clog.sec.error(msg);
//        vSetter({ err: true, msg });
//        return;
//    }
//    let ancWBlock = ancWBlocks[0];
//    let ancBlock = blockUtils.openDBSafeObject(ancWBlock.bBody).content;
//    if (isSuperValidateDebug)
//        clog.trx.info(`${level}.${levelAnc} SuperValidate, refBlockHashFoundByAncestors ============(${JSON.stringify(vGetter('ref'))})`);

//    if ((vGetter('ref') == null) && (vGetter('calledAncestors').indexOf(ancBlock.blockHash) == -1)) {
//        let tmp = vGetter('calledAncestors')
//        tmp.push(ancBlock.blockHash);
//        vSetter({ calledAncestors: tmp })
//        SuperControlUntilCoinbaseMinting.lookForDocByAncRecursive({
//            vSetter,
//            vGetter,
//            block: ancBlock,
//            docHash,
//            level
//        });
//    }
//  };

//  }
