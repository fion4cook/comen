#ifndef SOCIETYRULES_H
#define SOCIETYRULES_H


class SocietyRules
{
public:
  SocietyRules();
  static const QString stbl_administrative_refines_history;
  static const QStringList stbl_administrative_refines_history_fields;
  static const QString stbl_administrative_pollings;

  static QVariant getAdmValue(
    const QString &pollingKey,
    QString cDate = CUtils::getNow());

  static MPAIValueT getSingleIntegerValue(
    const QString &pollingKey,
    const QString &cDate = CUtils::getNow());

  static MPAIValueT getBasePricePerChar(const QString &cDate = CUtils::getNow());

  static MPAIValueT getBasicTxDPCost(
    const DocLenT &dLen,
    const QString &cDate = CUtils::getNow());

  static MPAIValueT getPollingDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getPledgeDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getClosePledgeDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getCloseDNAProposalDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getBallotDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getINameRegDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getINameBindDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getINameMsgDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getCPostDPCost(const QString &cDate = CUtils::getNow());
  static MPAIValueT getBlockFixCost(const QString &cDate = CUtils::getNow());

  static QVDicT prepareDocExpenseDict(
    const QString &cDate = CUtils::getNow(),
    const DocLenT &dLen = 0);

  static MPAIValueT getDocExpense(
    const QString &dType,
    const DocLenT &dLen,
    const QString &dClass = "",
    const QString &cDate = CUtils::getNow());

  static MPAIValueT getTransactionMinimumFee(const QString &cDate = CUtils::getNow());
};

#endif // SOCIETYRULES_H
