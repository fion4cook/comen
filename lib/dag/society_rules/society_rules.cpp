#include "stable.h"
#include "society_rules.h"

const QString SocietyRules::stbl_administrative_refines_history = "c_administrative_refines_history"; // tblAdmRefinesHist
const QStringList SocietyRules::stbl_administrative_refines_history_fields = {"arh_id", "arh_hash", "arh_subject", "arh_value", "arh_apply_date"};


const QString SocietyRules::stbl_administrative_pollings = "c_administrative_pollings"; // table

SocietyRules::SocietyRules()
{

}

/**
* func retrieves the last date before given cDate in which the value is refined
* @param {*} args
*/
QVariant SocietyRules::getAdmValue(
  const QString &pollingKey,
  QString cDate)
{
  QueryRes res = DbModel::select(
    stbl_administrative_refines_history,
    {"arh_value"},
    {{"arh_subject", pollingKey},
    {"arh_apply_date", cDate, "<="}},
    {{"arh_apply_date", "DESC"}},
    1);

  if (res.records.size() == 0)
    CUtils::exiter("invalid arh_apply_date for (" + pollingKey + ") on date(" + cDate + ")", 125);

  return res.records[0].value("arh_value");
}


MPAIValueT SocietyRules::getSingleIntegerValue(
  const QString &pollingKey,
  const QString &cDate)
{
  // fetch from DB the price for calculation Date
  QVariant value = getAdmValue(pollingKey, cDate);
  CLog::log("Retrieveing RFRf for pollingKey(" + pollingKey + ") on date(" + cDate + ") -> value(" + value.toString() + ")", "app", "trace");
  return value.toInt();
}

MPAIValueT SocietyRules::getBasicTxDPCost(
  const DocLenT &dLen,
  const QString &cDate)
{
  MPAIValueT TxBasePrice = getSingleIntegerValue("RFRfTxBPrice", cDate);

  /**
   * TODO: maybe can be modified in next version of transaction to be more fair
   * specially after implementing the indented bach32 unlockers(recursively unlimited unlockers which have another bech32 as an unlocker)
   */
  auto[x, y, gain, revGain] = CUtils::calcLog(dLen, CConsts::MAX_DOC_LENGTH_BY_CHAR);//(dLen * 1) / (iConsts.MAX_DOC_LENGTH_BY_CHAR * 1);
  Q_UNUSED(x);
  Q_UNUSED(y);
  Q_UNUSED(gain);
  MPAIValueT cost = CUtils::iFloorFloat(TxBasePrice * pow(100 * revGain, 20));
  return cost;
}

MPAIValueT SocietyRules::getPollingDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfPollingPrice", cDate);
}

MPAIValueT SocietyRules::getPledgeDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfPLedgePrice", cDate);
}

MPAIValueT SocietyRules::getClosePledgeDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfClPLedgePrice", cDate);
}

MPAIValueT SocietyRules::getCloseDNAProposalDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfDNAPropPrice", cDate);
}

MPAIValueT SocietyRules::getBallotDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfBallotPrice", cDate);
}

MPAIValueT SocietyRules::getINameRegDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfINameRegPrice", cDate);
}

MPAIValueT SocietyRules::getINameBindDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfINameBndPGPPrice", cDate);
}

MPAIValueT SocietyRules::getINameMsgDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfINameMsgPrice", cDate);
}

MPAIValueT SocietyRules::getCPostDPCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfCPostPrice", cDate);
}

MPAIValueT SocietyRules::getBlockFixCost(const QString &cDate)
{
  return getSingleIntegerValue("RFRfBlockFixCost", cDate);
}


QVDicT SocietyRules::prepareDocExpenseDict(
  const QString &cDate,
  const DocLenT &dLen)
{
  QVDicT servicePrices {
    {CConsts::DOC_TYPES::BasicTx, QVariant::fromValue(getBasicTxDPCost(dLen, cDate))},
    {CConsts::DOC_TYPES::AdmPolling, QVariant::fromValue(getPollingDPCost(cDate))},
    {CConsts::DOC_TYPES::Polling, QVariant::fromValue(getPollingDPCost(cDate))},
    {CConsts::DOC_TYPES::Pledge, QVariant::fromValue(getPledgeDPCost(cDate))},
    {CConsts::DOC_TYPES::ClosePledge, QVariant::fromValue(getClosePledgeDPCost(cDate))},
    {CConsts::DOC_TYPES::DNAProposal, QVariant::fromValue(getCloseDNAProposalDPCost(cDate))},
    {CConsts::DOC_TYPES::Ballot, QVariant::fromValue(getBallotDPCost(cDate))},
    {CConsts::DOC_TYPES::INameReg, QVariant::fromValue(getINameRegDPCost(cDate))},
    {CConsts::DOC_TYPES::INameBind, QVariant::fromValue(getINameBindDPCost(cDate))},
    {CConsts::DOC_TYPES::INameMsgTo, QVariant::fromValue(getINameMsgDPCost(cDate))},
    {CConsts::DOC_TYPES::CPost, QVariant::fromValue(getCPostDPCost(cDate))}};

  return servicePrices;
}

MPAIValueT SocietyRules::getBasePricePerChar(const QString &cDate)
{
  return getSingleIntegerValue("RFRfBasePrice", cDate);
}

MPAIValueT SocietyRules::getDocExpense(
  const QString &dType,
  const DocLenT &dLen,
  const QString &dClass,
  const QString &cDate)
{

  if (dLen > CConsts::MAX_DOC_LENGTH_BY_CHAR)
    return 0;

  QVDicT servicePrices = prepareDocExpenseDict(cDate, dLen);

  if (servicePrices.keys().contains(dType))
    return servicePrices[dType].toUInt();

  return 0;

  //TODO: implement plugin price
  // if type of documents is not defined, so accept it as a base feePerByte
//  let pluginPrice = listener.doCallSync('SASH_calc_service_price', args);
//  if (_.has(pluginPrice, 'err') && pluginPrice.err != false) {
//      utils.exiter(`wrong plugin price calc for ${utils.stringify(args)}`, 434);
//  }
//  if (!_.has(pluginPrice, 'fee')) {
//    utils.exiter(`missed price fee for dType(${dType})`, 34)
//  }
//  return pluginPrice.fee;
}


/**
 * returns minimum transaction fee by microPAI
 */
MPAIValueT SocietyRules::getTransactionMinimumFee(const QString &cDate)
{
  return CUtils::truncMPAI(static_cast<double>(
    CConsts::TRANSACTION_MINIMUM_LENGTH *
    getBasePricePerChar(cDate) *
    getDocExpense(
      CConsts::DOC_TYPES::BasicTx,
      CConsts::TRANSACTION_MINIMUM_LENGTH,
      cDate)
  ));
}
