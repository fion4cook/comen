#include "stable.h"
#include "rejected_transactions_handler.h"

const QString RejectedTransactionsHandler::stbl_trx_rejected_transactions = "c_trx_rejected_transactions";
const QStringList RejectedTransactionsHandler::stbl_trx_rejected_transactions_fields = {"rt_block_hash", "rt_doc_hash", "rt_coin", "rt_insert_date"};

RejectedTransactionsHandler::RejectedTransactionsHandler()
{

}


QVDRecordsT  RejectedTransactionsHandler::searchRejTrx(
  const ClausesT &clauses,
  const QStringList &fields,
  const OrderT &order,
  const int &limit)
{
  QueryRes res = DbModel::select(
    stbl_trx_rejected_transactions,
    fields,
    clauses,
    order,
    limit);

  return res.records;
}

//RejectedTransactionsHandler::addTransaction(args) {
//  model.sCreate({
//  table,
//  values: {
//  rt_block_hash: args.blockHash,
//  rt_doc_hash: args.docHash,
//  rt_coin: args.refLoc,
//  rt_insert_date: utils.getNow()
//  }
//  })
//}
