#ifndef NORMALUTXOHANDLER_H
#define NORMALUTXOHANDLER_H

class cUtils;


#include "lib/transactions/basic_transactions/utxo/utxo_handler.h"


class NormalUTXOHandler
{

public:
  NormalUTXOHandler();
  static void recursiveImportUTXOs();
  static void importNormalBlockUTXOs(QString cDate = "");
  static void doImportUTXOs(QString cDate = "");
};

#endif // NORMALUTXOHANDLER_H
