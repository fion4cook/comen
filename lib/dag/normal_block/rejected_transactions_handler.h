#ifndef REJECTEDTRANSACTIONSHANDLER_H
#define REJECTEDTRANSACTIONSHANDLER_H


class RejectedTransactionsHandler
{
public:
  RejectedTransactionsHandler();

  const static QString stbl_trx_rejected_transactions;
  const static QStringList stbl_trx_rejected_transactions_fields;

  static QVDRecordsT searchRejTrx(
    const ClausesT &clauses = {},
    const QStringList &fields = stbl_trx_rejected_transactions_fields,
    const OrderT &order = {},
    const int &limit = 0);


};

#endif // REJECTEDTRANSACTIONSHANDLER_H
