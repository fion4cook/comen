#include "stable.h"
#include "lib/messaging_protocol/graphql_handler.h"
#include "full_dag_handler.h"

FullDAGHandler::FullDAGHandler()
{

}

std::tuple<QString , QString> FullDAGHandler::invokeFullDAGDlRequest(const QString &start_from)
{
  return GraphQLHandler::makeAPacket(
    QJsonArray{ QJsonObject {
      {"cdType", CConsts::CARD_TYPES::FullDAGDownloadRequest},
      {"cdVer", "0.0.1"},
      {"startFrom", start_from}}});
}
