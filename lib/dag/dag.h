#ifndef DAG_H
#define DAG_H


class BlockRecord;
#include "lib/block/block_types/block.h"


class DAG
{

public:
  static QString stbl_blocks;
  static QStringList stbl_blocks_fields;

  static QString stbl_docs_blocks_map;
    DAG();


  /**
   *
   * @param {array} blockHashes
   * @param {array} descendents
   * adds given descendents to given blocks
   */
  static void appendDescendents(const QStringList &blockHashes, const std::vector<QString> &newDescendents);

  static QVDRecordsT searchInDAG(
    const ClausesT &clauses,
    const QStringList &fields = stbl_blocks_fields,
    const OrderT &order = {},
    const int &limit = 0);

  static std::tuple<QVDRecordsT, GRecordsT> getWBlocksByDocHash(
    const QStringList &doc_hashes,
    const QString &hashes_type = CConsts::COMPLETE);

  static QV2DicT getCoinsGenerationInfoViaSQL(const QStringList &coins);

  static std::tuple<QStringList, GRecordsT> getBlockHashesByDocHashes(
    const QStringList &doc_hashes,
    const QString &hashes_type = CConsts::COMPLETE);

  //  -  -  -  walkthrough DAG
  static std::tuple<bool, BlockRecord> getLatestBlockRecord();

  static std::tuple<bool, QString, QString> getLatestBlock();

  static QStringList getAncestors(
      QStringList blockHashes = {},
      uint level = 1);

  static void recursive_backwardInTime(
      const QStringList &blockHashes,
      const QString &date,
      QStringList &ancestors);

  static QStringList returnAncestorsYoungerThan(
      const QStringList &blockHashes,
      const QString &byDate = "",
      const int32_t &byMinutes = -1,
      const int32_t &cycle = -1);

};

#endif // DAG_H
