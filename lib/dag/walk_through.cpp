#include "stable.h"

#include "dag.h"



/**
 * returns latest block which is already recorded in DAG
 */
std::tuple<bool, BlockRecord> DAG::getLatestBlockRecord()
{
  QVDRecordsT lastBLockRecord = searchInDAG(
    {},
    {"b_hash", "b_creation_date"},
    {{"b_creation_date", "DESC"}},
    1
  );
  if (lastBLockRecord.size() == 0)
  {
    CLog::log("The DAG hasn't any node!", "sec", "fatal");
    BlockRecord b;
    return {false, b};
  }
  BlockRecord blockRecord(lastBLockRecord[0]);
  return {false, blockRecord};
}

std::tuple<bool, QString, QString> DAG::getLatestBlock()
{
  QVDRecordsT lastWBLock = searchInDAG(
    {},
    {"b_hash", "b_creation_date"},
    {{"b_creation_date", "DESC"}},
    1);

  if (lastWBLock.size() == 0)
  {
    CLog::log("The DAG hasn't any node!", "sc", "error");
    return {false, "", ""};
  }

  QVDicT wBlock = lastWBLock[0];
  return {
    true,
    wBlock.value("b_hash").toString(),
    wBlock.value("b_creation_date").toString()
  };
}

/**
 *
 * @param {*} blockHashes
 * @param {*} level
 * this is not aggregative and returns ONLY given level's ancestors and not the blocks in backing track way
 */
QStringList DAG::getAncestors(
  QStringList blockHashes,
  uint level)
{
  if (blockHashes.size() == 0)
    return {};

  QVDRecordsT res = searchInDAG(
    {{"b_hash", blockHashes, "IN"}},
    {"b_ancestors"});

  if (res.size()== 0)
    return {};

  QStringList out {};
  for (QVDicT aRes: res)
    out = CUtils::arrayAdd(out, aRes.value("b_ancestors").toString().split(","));

  out = CUtils::arrayUnique(out);

  if (level == 1)
    return out;

  return getAncestors(out, level - 1);
}
