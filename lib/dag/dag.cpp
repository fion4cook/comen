#include "stable.h"

#include "lib/block_utils.h"
#include "dag.h"


DAG::DAG()
{

}

QString DAG::stbl_blocks = "c_blocks";
QStringList DAG::stbl_blocks_fields = {"b_id", "b_hash", "b_type", "b_cycle", "b_confidence", "b_ext_root_hash", "b_docs_root_hash", "b_signals", "b_trxs_count", "b_docs_count", "b_ancestors_count", "b_ancestors", "b_descendents", "b_body", "b_creation_date", "b_receive_date", "b_confirm_date", "b_backer", "b_utxo_imported"};

QString DAG::stbl_docs_blocks_map = "c_docs_blocks_map";

void DAG::appendDescendents(const QStringList &block_hashes, const std::vector<QString> &newDescendents)
{
  if (newDescendents.size()>0)
  {
    for (QString aBlockHash : block_hashes)
    {
      QueryRes bExtInfo = DbModel::select(
          DAG::stbl_blocks,
          QStringList {"b_hash", "b_descendents"},     // fields
          {ModelClause("b_hash", aBlockHash)},
          {},
          1   // limit
      );
      if (bExtInfo.records.size() == 1)
      {
         QString serializedValue = bExtInfo.records[0].value("b_descendents").toString();
         // FIXME: continue to update descendents
         CLog::log("------appendDescendents ---------" + serializedValue );
         QJsonObject currentDescendents = CUtils::parseToJsonObj(serializedValue);


         //            let currentDescend = res[0].b_descendents;
         //            if (utils._nilEmptyFalse(currentDescend)) {
         //                currentDescend = [];
         //            } else {
         //                currentDescend = utils.parse(currentDescend);
         //            }
         //            if (utils._nilEmptyFalse(currentDescend) || (!Array.isArray(currentDescend)))
         //                currentDescend = [];

         //            let finalDescendents = utils.arrayUnique(utils.arrayAdd(newDescendents, currentDescend));
         //            model.sUpdate({
         //                table,
         //                query: [
         //                    ['b_hash', aBlockHash]
         //                ],
         //                updates: { b_descendents: utils.stringify(finalDescendents) },
         //                log: false
         //            });
         //        }


      }

    }
  }
}

std::tuple<QStringList, GRecordsT> DAG::getBlockHashesByDocHashes(
  const QStringList &doc_hashes,
  const QString &hashes_type)
{
  ClausesT clauses {};
  if (hashes_type == CConsts::SHORT)
  {
    QStringList tmp{};
    for(QString a_hash: doc_hashes)
      tmp.append(a_hash + "%");
    clauses.push_back({"dbm_doc_hash", tmp, "LIKE:OR"});
  } else {
    clauses.push_back({"dbm_doc_hash", doc_hashes, "IN"});
  }

  QueryRes res = DbModel::select(
    stbl_docs_blocks_map,
    {"dbm_block_hash", "dbm_doc_hash", "dbm_doc_index"},
    clauses);
  QStringList block_hashes{};
  GRecordsT map_doc_to_block{};
  for (QVDicT element: res.records)
  {
    block_hashes.append(element.value("dbm_block_hash").toString());
    // since we can have more than 1 coinbase block for each cycle, so mapping a document to its container block could be 1 to n mapping
    // also in lone transactions we have same psituation in which a certain transaction can take place in different blocks by different backers
    QString dbm_doc_hash = element.value("dbm_doc_hash").toString();
    if (!map_doc_to_block.keys().contains(dbm_doc_hash))
      map_doc_to_block[dbm_doc_hash] = QVDRecordsT{};
    map_doc_to_block[dbm_doc_hash].push_back(QVDicT{
      {"block_hash", element.value("dbm_block_hash")},
      {"doc_index", element.value("dbm_doc_index")}
    });
  }
  return {block_hashes, map_doc_to_block};
}

QVDRecordsT DAG::searchInDAG(
    const ClausesT &clauses,
    const QStringList &fields,
    const OrderT &order,
    const int &limit
    )
{
  QueryRes res = DbModel::select(stbl_blocks, fields, clauses, order, limit);
  return res.records;
}

std::tuple<QVDRecordsT, GRecordsT> DAG::getWBlocksByDocHash(
  const QStringList &doc_hashes,
  const QString &hashes_type)
{
  auto[block_hashes, map_doc_to_block] = getBlockHashesByDocHashes(doc_hashes, hashes_type);
  QVDRecordsT block_records = searchInDAG({{"b_hash", block_hashes, "IN"}});
  return {block_records, map_doc_to_block};
}

/**
*
* @param {*} coins
* finding the blocks in which were created given coins
*/
QV2DicT DAG::getCoinsGenerationInfoViaSQL(const QStringList &coins)
{
  QStringList docsHashes {};
  for(QString a_coin: coins)
  {
    auto[doc_hash_, output_index_] = CUtils::unpackCoinRef(a_coin);
    Q_UNUSED(output_index_);
    docsHashes.append(doc_hash_);
  }
  CLog::log("find Output Info By RefLocViaSQL for docs: " + docsHashes.join(", "), "trx", "trace");

  auto[block_records, map_] = DAG::getWBlocksByDocHash(docsHashes);
  Q_UNUSED(map_);
  // console.log('find Output Info By RefLocViaSQL', block_records);
  QV2DicT outputsDict {};
  for (QVDicT wBlock: block_records)
  {
    WrapRes block_ = BlockUtils::unwrapSafeContentForDB(wBlock.value("b_body").toString());
    QJsonObject block = CUtils::parseToJsonObj(block_.content);
    if (block.keys().contains("docs"))
    {
      for (QJsonValueRef doc_: block.value("docs").toArray())
      {
        QJsonObject doc = doc_.toObject();
        if (doc.keys().contains("outputs"))
        {
          QJsonArray outputs = doc.value("outputs").toArray();
          for (OutputIndexT outputInx = 0; outputInx < static_cast<OutputIndexT>(outputs.size()); outputInx++)
          {
            auto output = outputs[outputInx].toArray();
            QString aCoin = CUtils::packCoinRef(doc.value("hash").toString(), outputInx);
            outputsDict[aCoin] = QVDicT {
              {"coinGenCycle", wBlock.value("bCycle")},
              {"coinGenBlockHash", block.value("blockHash")},
              {"coinGenBType", block.value("bType")},
              {"coinGenCreationDate", block.value("creationDate")},
              {"coinGenDocType", doc.value("dType")},
              {"coinGenRefLoc", aCoin},
              {"coinGenOutputAddress", output[0].toString()},
              {"coinGenOutputValue", output[1].toDouble()}};
          }
        }
      }
    }
  }

  QV2DicT res {};
  for (auto aCoin: coins)
  {
    if (outputsDict.keys().contains(aCoin))
      res[aCoin] = outputsDict[aCoin];
  }
  return res;
}

void DAG::recursive_backwardInTime(
  const QStringList &blockHashes,
  const QString &date,
  QStringList &ancestors)
{
  // console.log(`::::::::::: recursive_backwardInTime args: ${utils.stringify(args)}`);
  if (blockHashes.size()== 0)
    return;

  QVDRecordsT res = searchInDAG(
    {{"b_hash", blockHashes, "IN"},
    {"b_creation_date", date, ">"}},
    {"b_ancestors"});

  if (res.size()== 0)
    return;

  QStringList out {};
  for (QVDicT aRes: res)
    out = CUtils::arrayAdd(out, aRes.value("b_ancestors").toString().split(","));

  out = CUtils::arrayUnique(out);
  ancestors = CUtils::arrayAdd(ancestors, out);

  if (out.size()> 0)
    recursive_backwardInTime(
      out,
      date,
      ancestors);
}

/**
 *
 * @param {*} args
 * returns all ancestors of given block(s), which are youngre than a certain age (byMinutes or cycle)
 */
QStringList DAG::returnAncestorsYoungerThan(
  const QStringList &blockHashes,
  const QString &byDate,
  const int32_t &byMinutes,
  const int32_t &cycle)
{
  CLog::log("return AncestorsYoungerThan: blockHashes: " + blockHashes.join(",") + "\nbyDate(" + byDate +")", "app", "trace");
  if (blockHashes.size() == 0)
    return {};

  QString date_;
  if (byDate != "")
  {
    date_ = byDate;
  } else if (byMinutes != -1)
  {
    date_ = CUtils::minutesBefore(byMinutes);
  } else if (cycle != -1)
  {
    date_ = CUtils::minutesBefore(CMachine::getCycleByMinutes() * cycle);
  }

  QStringList ancestors;
  recursive_backwardInTime(
    blockHashes,
    date_,
    ancestors
  );

  return ancestors;
}
