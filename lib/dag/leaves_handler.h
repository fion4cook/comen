#ifndef LEAVESHANDLER_H
#define LEAVESHANDLER_H

#include <tuple>

class QString;
class QJsonObject;



class LeavesHandler
{
public:
  static const QString stbl_kvalue;
  LeavesHandler();
  static QJsonObject getLeaveBlocks(const QString &maxCreationDate="");
  static std::tuple<bool, QString> removeFromLeaveBlocks(const QStringList &leaves);
  static std::tuple<bool, QString> addToLeaveBlocks(
    const QString &blockHash,
    const QString &creationDate,
    const QString &bType = "UB"
  );
  static QJsonObject getFreshLeaves();
  static bool hasFreshLeaves();

};

#endif // LEAVESHANDLER_H
