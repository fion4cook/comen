#ifndef GSSHANDLER_H
#define GSSHANDLER_H

#include "individual_signature.h"
#include "unlock_set.h"
#include "unlock_document.h"



namespace SignatureStructureHandler
{


QString calcUnlockHash(const UnlockSet &unlock_set, QString hashAlgorithm = "keccak256");

UnlockDocument createCompleteUnlockSets(
    const QHash<QString, IndividualSignature> &individuals_signing_sets,
    uint16_t neccessarySignaturesCount,
    QVDicT options = {});

UnlockDocument createMOfNMerkle(
    QVector<UnlockSet> unlock_sets,
    QV2DicT customTypes,
    QVDicT customSalts,
    QVDicT options = {});

std::tuple<bool, QString> validateSigStruct(const UnlockSet &uSet, const QString &address, const QVDicT &options = {});
bool validateStructureStrictions(const UnlockSet &unlock, const QVDicT &options = {});
QString customStringifySignatureSets(const QVector<IndividualSignature> &signature_sets);


};

#endif // GSSHANDLER_H
