#ifndef INDIVIDUALSIGNATURE_H
#define INDIVIDUALSIGNATURE_H

#include <QString>
#include <QJsonObject>


class IndividualSignature
{
public:
  QString m_signer_id = ""; // a dummy handler id
  QString m_signature_key = ""; // sKey
  QString m_permitted_to_pledge = ""; // pPledge
  QString m_permitted_to_delegate = ""; // pDelegate

  // a date for which, user can not spend this money before stated date
  QString m_input_time_lock_strict = "";
//    note for implementation. ther will be 2 differnt input lock time signatures.
//    free input lock time: either the user with lock time or other users with or without lock time, can escrow money
//    strict input lock time: ONLY and ONLY one signature is valid and the owner ONLY can scrow money after time expiration.


  IndividualSignature(){};

  IndividualSignature(
      const QString &signature_key,
      const QString &permitted_to_pledge = "",
      const QString &permitted_to_delegate = "",
      const QString &input_time_lock_strict = ""
      );

  QJsonObject exportJson()
  {
    return QJsonObject {
      {"m_signature_key", m_signature_key},
      {"m_permitted_to_pledge", m_permitted_to_pledge},
      {"m_permitted_to_delegate", m_permitted_to_delegate},
      {"m_input_time_lock_strict", m_input_time_lock_strict}
    };
  }

  void importJson(const QJsonObject &obj)
  {
    m_signature_key = obj.value("m_signature_key").toString();
    m_permitted_to_pledge = obj.value("m_permitted_to_pledge").toString();
    m_permitted_to_delegate = obj.value("m_permitted_to_delegate").toString();
    m_input_time_lock_strict = obj.value("m_input_time_lock_strict").toString();
  }
};


#endif // INDIVIDUALSIGNATURE_H
