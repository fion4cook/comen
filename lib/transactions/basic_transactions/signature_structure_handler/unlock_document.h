#ifndef UNLOCKDOCUMENT_H
#define UNLOCKDOCUMENT_H

#include<QHash>

#include "individual_signature.h"
#include "unlock_set.h"

class UnlockDocument
{
public:
  UnlockDocument();
  UnlockDocument(
    QString merkle_root,
    QString account_address = "",
    QVector<UnlockSet> unlock_sets = {},
    QString merkle_version = "0.0.0");

  QVector<UnlockSet> m_unlock_sets = {};
  QString m_merkle_root = "";
  QString m_account_address = "";
  QString m_merkle_version = "0.0.0";
  QHash<QString, QStringList> m_private_keys = {};

  QJsonObject exportJson() const;
  void importJson(const QJsonObject &obj);
};

#endif // UNLOCKDOCUMENT_H
