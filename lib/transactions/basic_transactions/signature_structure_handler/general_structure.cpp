#include "stable.h"
#include "lib/ccrypto.h"
#include "general_structure.h"



// -  -  -  -  -  -  UnlockSet -  -  -  -
QJsonObject UnlockSet::exportJson()
{
  QJsonArray signature_sets{};
  for (IndividualSignature aSig: m_signature_sets)
  {
    signature_sets.push_back(aSig.exportJson());
  }
  return QJsonObject {
    {"m_signature_type", m_signature_type},
    {"m_signature_ver", m_signature_ver},
    {"m_signature_sets", signature_sets},
    {"m_left_hash", m_left_hash},
    {"m_salt", m_salt}
  };
}

void UnlockSet::importJson(const QJsonObject &obj)
{
  m_signature_type = obj.value("m_signature_type").toString();
  m_signature_ver = obj.value("m_signature_ver").toString();
  m_left_hash = obj.value("m_left_hash").toString();
  m_salt = obj.value("m_salt").toString();
  m_signature_sets = {};
  for (auto a_signature_set: obj.value("m_signature_sets").toArray())
  {
    IndividualSignature sig_obj;
    sig_obj.importJson(a_signature_set.toObject());
    m_signature_sets.push_back(sig_obj);
  }

}






// -  -  -  -  -  -  SignatureStructureHandler -  -  -  -

namespace SignatureStructureHandler
{


UnlockDocument createCompleteUnlockSets(
      const QHash<QString, IndividualSignature> &individuals_signing_sets,
      uint16_t neccessarySignaturesCount,
      QVDicT options)
{
  QString signature_type = options.value("signature_type", "").toString();
  QString signature_version = options.value("signature_version", "").toString();
  QString customSalt = options.value("customSalt", "PURE_LEAVE").toString();

  QStringList signers_ids;
  QVDicT signersByKey;

  // generate permutation of signatures. later will be used as tree leaves
  QStringList leave_ids = individuals_signing_sets.keys();
  leave_ids.sort();
  PermutationHandler signPermutations(leave_ids, neccessarySignaturesCount);

  QV2DicT customTypes;
  QVector<UnlockSet> unlock_sets;
  QVDicT customSalts;
  for (QStringList an_unlock_individuals_combination: signPermutations.m_permutations)
  {
    QVector<IndividualSignature> a_signature_combination;
    for (QString an_individual_id: an_unlock_individuals_combination) {
      a_signature_combination.push_back(individuals_signing_sets.value(an_individual_id));
    }
    unlock_sets.push_back(a_signature_combination);


    QString customKey = CUtils::hash16c(CCrypto::keccak256(customStringifySignatureSets(a_signature_combination)));
    if ((signature_type != "") && (signature_version != ""))
    {
        customTypes[customKey] = QVDicT
        {
          {"signature_type", signature_type},
          {"signature_version", signature_version}
        };
    }

    if (customSalt != "")
    {
      if (customSalt == "PURE_LEAVE") {
        customSalts[customKey] = customKey;
      } else {
        customSalts[customKey] = customSalt;
      }
    }
  }

  CLog::log("createCompleteUnlockSets", "app" , "trace");
  CLog::log("createCompleteUnlockSets (unlock_sets):" + CUtils::dumpIt(unlock_sets), "app" , "trace");
  CLog::log("createCompleteUnlockSets (customTypes):" + CUtils::dumpIt(customTypes), "app" , "trace");
  CLog::log("createCompleteUnlockSets (customSalts):" + CUtils::dumpIt(customSalts), "app" , "trace");
  CLog::log("createCompleteUnlockSets (options):" + CUtils::dumpIt(options), "app" , "trace");

  return createMOfNMerkle(
    unlock_sets,
    customTypes,
    customSalts,
    options
  );

}


UnlockDocument createMOfNMerkle(
    QVector<UnlockSet> unlock_sets,
    QV2DicT customTypes,
    QVDicT customSalts,
    QVDicT options)
{
  CLog::log("createMOfNMerkle creating unlock_sets" + CUtils::dumpIt(unlock_sets), "app", "trace");

  QString hash_algorithm = options.value("hash_algorithm", "keccak256").toString();
  QString input_type = options.value("input_type", "hashed").toString();

  QStringList hashedUnlocks;
  QHash<QString, UnlockSet> tmpUnlockers;
  QString customKey, leave_hash;
  for (UnlockSet an_unlock_set: unlock_sets)
  {
    customKey = CUtils::hash16c(CCrypto::keccak256(customStringifySignatureSets(an_unlock_set.m_signature_sets)));
    if (customTypes.keys().contains(customKey))
    {
      an_unlock_set.m_signature_type = customTypes[customKey]["signature_type"].toString();
      an_unlock_set.m_signature_ver = customTypes[customKey]["signature_version"].toString();
    }
    else
    {
      an_unlock_set.m_signature_type = "Basic";
      an_unlock_set.m_signature_ver = "0.0.0";
    }

    // adding random salt to obsecure/unpredictable final address
    // TODO: maybe use crypto secure random generator
    if (customSalts.keys().contains(customKey))
    {
      an_unlock_set.m_salt = customSalts[customKey].toString();
    } else {
      an_unlock_set.m_salt = CUtils::hash16c(CCrypto::keccak256(CUtils::dumpIt(an_unlock_set) + CUtils::getNow() + QString::number(random())));
    }
    leave_hash = calcUnlockHash(an_unlock_set, hash_algorithm);
    hashedUnlocks.push_back(leave_hash);
    tmpUnlockers[leave_hash] = an_unlock_set;

  }

  auto[mRoot, mVerifies, mVersion, _levels, _leaves] = CMerkle::generate(hashedUnlocks, input_type, hash_algorithm);
  Q_UNUSED(_levels);
  Q_UNUSED(_leaves);
  UnlockDocument unlock_document(
    mRoot,
    CCrypto::bech32Encode(CCrypto::keccak256Dbl(mRoot)),   // because of securiy, MUST use double hash
    {},
    mVersion
  );

  // asign merkle proofs to sign_set itself
  for (QString key: tmpUnlockers.keys())
  {
    UnlockSet tmp(
      tmpUnlockers[key].m_signature_sets,
      tmpUnlockers[key].m_salt,
      tmpUnlockers[key].m_signature_type,
      tmpUnlockers[key].m_signature_ver,
      mVerifies.value(key).m_left_hash, // : null,
      mVerifies.value(key).m_proofs// : null,
    );
    unlock_document.m_unlock_sets.push_back(tmp);
  }
  return unlock_document;
}

QString calcUnlockHash(const UnlockSet &unlock_set, QString hash_algorithm)
{
//  std::string potential_strings = R"(
//  Basic:0.0.0:[{"sKey":"0353444374647c47d52534fb9e0c1d7767d1a143ac5511c1ea45f98bc321732a0c"}]:a1ca0290308ed6a3
//  Basic:0.0.0:[{"sKey":"0353444374647c47d52534fb9e0c1d7768d1a143ac5511c1ee45f98bc321732a0c"},{"sKey":"0257040e1bf22ab8785c56d21615b140b766dee38ec8f1a7db49a6ebf98977a6bc"}]:b84100a46012d51a
//  Strict:0.0.0:[{"sKey":"022968b10e02e2af51a5965b9735ac2c75c51c71207f87bec0bd49fa61902f8619","pPledge":"Y","pDelegate":"Y"},{"sKey":"0339129227adebcb49c89fdcbf036249b1e277727895b6803378a0364c33bc0b46","pPledge":"N","pDelegate":"N"},{"sKey":"03a797608e14ee87a93c0bf7d7d121593c5985030e9053e7d062bf081d59da956b","pPledge":"N","pDelegate":"N"},{"sKey":"03f4e4a46c160246e518c41c661b6eeae89aee2188e9dd454274bcca3414a2ed54","pPledge":"N","pDelegate":"N"},{"sKey":"03c146c6e887a1be14606d4a56a72905620064d30a0efa61bf99c1b4dcd10412ad","pPledge":"Y","pDelegate":"Y"},{"sKey":"02bcf7558f443691819af3d1ab6661f379efb8bbda9791f81156749f218ad2101a","pPledge":"N","pDelegate":"N"}]:a9de676dd54b672c
//  )";


  QString to_be_hashed = unlock_set.m_signature_type + ":" +unlock_set.m_signature_ver +":" + customStringifySignatureSets(unlock_set.m_signature_sets) + ":" + unlock_set.m_salt;//  hash_algorithm(${sType}:${sVer}:${JSON.stringify(sSet)}:${salt})
  CLog::log("Custom stringyfied unlock_struct: " + to_be_hashed, "app", "trace");
  if (hash_algorithm == "keccak256")
  {
    return CCrypto::keccak256(to_be_hashed);
  }

  CLog::log("Invalid hash algorythm!", "app", "fatal");
  exit(918);
}



std::tuple<bool, QString> validateSigStruct(const UnlockSet &unlockSet, const QString &address, const QVDicT &options)
{
  CLog::log("validateSigStruct: " + CUtils::dumpIt(unlockSet), "app", "debug");
  QString input_type = options.value("input_type", "hashed").toString();
  QString hash_algorithm = options.value("hash_algorithm", "keccak256").toString();
  bool do_permutation = options.value("do_permutation", false).toBool();

  // console.log(validate StructureOfAnUnlockMOfN.args: ${utils.stringify(args)});
  bool sStrict = validateStructureStrictions(unlockSet, options);
  if (!sStrict)
    return { sStrict, "Invalid strict address"};

  try {

     // normally the wallets SHOULD send the saved order of a sSets, so we do not need to Permutation
     QString leaveHash = calcUnlockHash(unlockSet);

     QString merkle_root = CMerkle::getRootByAProve(leaveHash, unlockSet.m_proofs, unlockSet.m_left_hash, input_type, hash_algorithm);
     QString bech32 = CCrypto::bech32Encode(CCrypto::keccak256Dbl(merkle_root));  // because of securiy, MUST use double hash
     if (address == bech32) {
       return { true, "Valid unlock structure" };
     }


    if (!do_permutation)
      return {false, "Invalid unlock structure"};
      // FIXME: implement it ASAP
     // in case of disordinating inside sSets
//     let hp = new utils.heapPermutation();
//     hp.heapP(unlockSet.sSets)
//     for (let premu of hp.premutions) {
//         leaveHash = hash_algorithm(${unlockSet.sType}:${unlockSet.sVer}:${JSON.stringify(premu)}:${unlockSet.salt});
//         merkle_root = crypto.merkleGetRootByAProve(leaveHash, unlockSet.proofs, unlockSet.lHash, input_type, hash_algorithm);
//         bech32 = crypto.bech32_encodePub(crypto.keccak256Dbl(merkle_root)).encoded;  // because of securiy, MUST use double hash
//         if (_.has(unlockSet, 'address')) {
//             if (unlockSet.address == bech32) {
//                 return true;
//             }
//         } else if (_.has(unlockSet, 'merkle_root')) {
//             if (unlockSet.merkle_root == merkle_root) {
//                 return true;
//             }
//         }
//     };
//     return false;
    return {false, "Invalid unlock structure, even after premutation!"};
  } catch (std::exception) {
    return {false, "Invalid unlock structure, even after premutation!"};
  }
}

QString customStringifySignatureSets(const QVector<IndividualSignature> &signature_sets)
{
  QStringList sSets_serial;
  for (IndividualSignature a_sig: signature_sets)
  {
    QString tmp = "{\"sKey\":\"" + a_sig.m_signature_key + "\"";

    if (a_sig.m_permitted_to_pledge != "")
    {
      tmp += ",\"pPledge\":\"" + a_sig.m_permitted_to_pledge + "\"";
    }

    if (a_sig.m_permitted_to_delegate != "")
    {
      tmp += ",\"pDelegate\":\"" + a_sig.m_permitted_to_delegate + "\"";
    }

    tmp += "}";

    sSets_serial.append(tmp);
  }
  QString custom_stringify = "[" + sSets_serial.join(",") + "]";  //  JSON.stringify(sSet)
  return custom_stringify;
}

bool validateStructureStrictions(const UnlockSet &unlock, const QVDicT &options)
{
    // console.log(validate StructureStrictions.args: ${utils.stringify(args)});
  QString hash_algorithm = options.value("hash_algorithm", "keccak256").toString();

  if (unlock.m_signature_type == CConsts::SIGNATURE_TYPES::Strict)
  {

    /**
     * this strict type of signature MUST have and ONLY have these 3 features
     * sKey: can be a public key(and later can be also another bech32 address, after implementing nested signature feature)
     * pPledge: means the signer Permitted to Pleadge this account
     * pDelegate: means the signer Permited to Delegate some rights (binded to this address) to others
     */

    if (CUtils::hash16c(CCrypto::keccak256(customStringifySignatureSets(unlock.m_signature_sets))) != unlock.m_salt)
    {
      CLog::log("invalid strict structure of signature of salt(" + unlock.m_salt + ") ");
      return false;
    }

    for (IndividualSignature aSignSet: unlock.m_signature_sets)
    {
        if ( (aSignSet.m_signature_key == "") || (aSignSet.m_permitted_to_pledge == "") ||
            (aSignSet.m_permitted_to_delegate == "")
            )
        {
            CLog::log("invalid strict structure of signature:" + CUtils::dumpIt(unlock));
            return false;
        }
    }

    return true;
  }

  return true;

}


};
