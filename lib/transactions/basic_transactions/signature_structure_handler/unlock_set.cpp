#include "stable.h"
#include "unlock_set.h"

UnlockSet::UnlockSet(
    QVector<IndividualSignature> signature_sets,
    QString salt,
    QString signature_type,
    QString signature_ver,
    QString left_hash,
    QStringList proofs)
{
  m_signature_sets = signature_sets;
  m_salt = salt;
  m_signature_ver = signature_ver;
  m_signature_type = signature_type;
  m_left_hash = left_hash;
  m_proofs = proofs;
}
