#include "stable.h"
#include "utxo_handler.h"

const QString UTXOHandler::stbl_trx_utxos = "c_trx_utxos";
const QStringList UTXOHandler::stbl_trx_utxos_fields = {"ut_id", "ut_creation_date", "ut_coin", "ut_o_address", "ut_o_value", "ut_visible_by", "ut_ref_creation_date"};

UTXOHandler::UTXOHandler()
{

}

void UTXOHandler::refreshVisibility()
{

}


ClausesT UTXOHandler::prepareUTXOQuery(
  const QStringList &coins,
  const QStringList &visible_by)
{
  ClausesT clauses = {};

  if (coins.size() > 0)
    clauses.push_back({"ut_coin", coins, "IN"});

  if (visible_by.size() > 0)
    clauses.push_back({"ut_visible_by", visible_by, "IN"});

  return clauses;
}


QVDRecordsT UTXOHandler::getCoinsInfo(
  const QStringList &coins,
  const QStringList &visible_by,
  const QStringList &fields)
{
  ClausesT clauses = prepareUTXOQuery(coins, visible_by);
  QueryRes res = DbModel::select(
    stbl_trx_utxos,
    fields,
    clauses,
    {},
    0,
    true,
    true);

  return res.records;
}

/**
 *
 * @param {*} args function clons entire entries are visibleby given block(ancestors)
 * to new entries which are visibleby new-block
 */
void UTXOHandler::inheritAncestorsVisbility(
  const QStringList &blockHashes,
  const QString &creationDate,
  const QString &newBlockHash)
{
  // clog.trx.info(`inherit AncestorsVisbility: ${JSON.stringify(args)}`)
  // clog.trx.info(`blockHashes==============================: ${blockHashes}`)
  QueryRes currentVisibility = DbModel::select(
    stbl_trx_utxos,
    {"ut_coin", "ut_o_address", "ut_o_value", "ut_ref_creation_date"},
    {{"ut_visible_by", blockHashes, "IN"}},
    {},
    0,
    true);
  // clog.trx.info(`currentVisibility: ${JSON.stringify(currentVisibility)}`);

  for (QVDicT aUTXO: currentVisibility.records)
  {
    addNewUTXO(
      creationDate,
      aUTXO.value("ut_coin").toString(),
      newBlockHash,
      aUTXO.value("ut_o_address").toString(),
      aUTXO.value("ut_o_value").toDouble(),
      aUTXO.value("ut_ref_creation_date").toString());
  }
}

void UTXOHandler::addNewUTXO(
  const QString &creationDate,
  const QString &the_coin,
  const QString &visibleBy,
  const QString &address,
  const MPAIValueT &coin_value,
  const QString &refCreationDate)
{
  QueryRes dblChk = DbModel::select(
    stbl_trx_utxos,
    {"ut_coin"},
    {{"ut_coin", the_coin},
    {"ut_visible_by", visibleBy}},
    {},
    0,
    true);

  if (dblChk.records.size() > 0)
    return;

  // clog.trx.info(`add NewUTXO maturated block(${utils.hash6c(args.visibleBy)}) cycle/cloneCode: ${args.cloneCode} ${utils.hash8c(args.address)} ${utils.microPAIToPAI(args.value)} pai`);
  DbModel::insert(
    stbl_trx_utxos,
    {
      {"ut_creation_date", creationDate},
      {"ut_coin", the_coin},
      {"ut_visible_by", visibleBy},
      {"ut_o_address", address},
      {"ut_o_value", QVariant::fromValue(coin_value).toDouble()},
      {"ut_ref_creation_date", refCreationDate}
    },
    true);
}

/**
 *
 * @param {*} hashes
 * Only used for mitigate table load
 */
bool UTXOHandler::removeVisibleOutputsByBlocks(const QStringList &blockHashes)
{
  QueryRes removeCandids = DbModel::select(
    stbl_trx_utxos,
    {"ut_coin"},
    {{"ut_visible_by", blockHashes, "IN"}},
    {},
    0,
    true);

  // sceptical tests before removing
  // TODO: take cae about repayment blocks, since they are created now but block creation date is one cycle before
  for (QVDicT aUtxo: removeCandids.records)
  {
    // control if the utxo already is visible by some newer blocks?
    QueryRes youngerVisibilityOfRefLoc = DbModel::select(
      stbl_trx_utxos,
      {"ut_coin"},
      {{"ut_coin", aUtxo.value("ut_coin")},
      {"ut_creation_date", aUtxo.value("ut_creation_date"), ">"}},
      {},
      0,
      true);

    if (youngerVisibilityOfRefLoc.records.size()== 0)
    {
      // security issue
      QString msg = "The ut_coin which want to remove can not be seen by newer entries! " + CUtils::dumpIt(aUtxo);
      CLog::log(msg, "sec", "error");
      return false;
    }
    // clog.trx.info(`youngerVisibilityOfRefLoc res: ${utils.stringify(youngerVisibilityOfRefLoc)}`);

    // if the newer block has the old one in his history?
    bool isVisByAnc = false;
    for (QVDicT anVis: youngerVisibilityOfRefLoc.records)
    {
      if (isVisByAnc)
        continue;

      // retrieve whole ancestors of the utxo
      QStringList allAncestorsOfAYoungerBlock = DAG::returnAncestorsYoungerThan(
          {anVis.value("ut_visible_by").toString()},
          aUtxo.value("ut_creation_date").toString());

      if (allAncestorsOfAYoungerBlock.size() == 0)
        continue;

      if (allAncestorsOfAYoungerBlock.contains(aUtxo.value("ut_visible_by").toString()))
          isVisByAnc = true;
    }
    if (!isVisByAnc) {
      // security issue
      QString msg = "The ut_coin which want to remove does not exist in history of newer entries! aUTXO: " + CUtils::dumpIt(aUtxo) + " younger Visibility Of RefLoc: " + CUtils::dumpIt(youngerVisibilityOfRefLoc.records);
      CLog::log(msg, "sec", "error");
      return false;
    }

    // finally remove utxo which is visible by his descendents
    DbModel::dDelete(
      stbl_trx_utxos,
      {{"ut_visible_by", blockHashes, "IN"},
      {"ut_coin", aUtxo.value("ut_coin").toString()}},
      true);
  }
  return true;
}
