#ifndef SPENTCOINSHANDLER_H
#define SPENTCOINSHANDLER_H


class SpentCoinsHandler
{
public:
  SpentCoinsHandler();

  static const QString stbl_trx_spend;
  static const QStringList stbl_trx_spend_filed;

  static QVDRecordsT searchInSpentCoins(
    const QStringList &coins,
    const OrderT &order = {});

  static GRecordsT makeSpentCoinsDict(const QStringList &coins);

  static std::tuple<bool, GRecordsT> findCoinsSpendLocations(const QStringList &coins);
};

#endif // SPENTCOINSHANDLER_H
