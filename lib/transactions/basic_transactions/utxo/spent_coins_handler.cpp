#include "stable.h"

#include "lib/block_utils.h"
#include "spent_coins_handler.h"

const QString SpentCoinsHandler::stbl_trx_spend = "c_trx_spend";
const QStringList SpentCoinsHandler::stbl_trx_spend_filed = {"sp_coin", "sp_spend_loc", "sp_spend_date"};


SpentCoinsHandler::SpentCoinsHandler()
{

}

std::tuple<bool, GRecordsT> SpentCoinsHandler::findCoinsSpendLocations(const QStringList &coins)
{
  // finding the block(s) which are used these coins and already are registerg in DAG(if they did)
  // this function just writes some logs and have not effect on block validation accept/denay
  GRecordsT recordedSpendInDAG {};
  for (QString a_coin: coins)
  {
    CLog::log("SCUDS: looking for a Coin(" + CUtils::shortCoinRef(a_coin) + ")", "trx", "trace");
    auto[doc_hash, inx_] = CUtils::unpackCoinRef(a_coin);
    Q_UNUSED(inx_);

    // find broadly already recorded block(s) which used(or referenced) this input-doc_hash
    auto[wBlocks, map_] = DAG::getWBlocksByDocHash(QStringList{doc_hash});
    Q_UNUSED(map_);

    CLog::log("SCUDS: looking for doc(" + CUtils::hash8c(doc_hash) + ") returned " + QString::number(wBlocks.size()) + " potentially blocks: " + CUtils::dumpIt(wBlocks), "trx", "trace");
    if (wBlocks.size()> 0)
    {
      for (QVDicT wBlock: wBlocks)
      {
        WrapRes refBlock_ = BlockUtils::unwrapSafeContentForDB(wBlock.value("b_body").toString());
        QJsonObject refBlock = CUtils::parseToJsonObj(refBlock_.content);
        CLog::log("SCUDS: controlling block(" + CUtils::hash8c(refBlock.value("blockHash").toString()) + ") ", "trx", "trace");
        if (refBlock.keys().contains("docs") && (refBlock.value("docs").toArray().size() > 0))
        {
          CLog::log("SCUDS: block(" + CUtils::hash8c(refBlock.value("blockHash").toString()) + " has " + QString::number(refBlock.value("docs").toArray().size())+ " docs", "trx", "info");
          for (auto doc_: refBlock.value("docs").toArray())
          {
            QJsonObject doc = doc_.toObject();
            if (doc.keys().contains("inputs"))
            {
              QJsonArray the_inputs = doc.value("inputs").toArray();
              CLog::log("SCUDS: doc has " + QString::number(the_inputs.size()) + " inputs", "trx", "info");
              for (int input_index = 0; input_index < the_inputs.size(); input_index++)
              {
                QJsonArray trxInput = the_inputs[input_index].toArray();
                // if the doc_hash is referenced as an input index, select id
                if (trxInput[0].toString() == doc_hash)
                {
                  QString tmp_coin = CUtils::packCoinRef(trxInput[0].toString(), trxInput[1].toInt());
                  CLog::log("SCUDS: controlling input(" + CUtils::shortCoinRef(tmp_coin) + ")", "trx", "info");
                  if (coins.contains(tmp_coin))
                  {
                    if (!recordedSpendInDAG.keys().contains(tmp_coin))
                      recordedSpendInDAG[tmp_coin] = QVDRecordsT {};
                    recordedSpendInDAG[tmp_coin].push_back(QVDicT {
                      {"block_hash", refBlock.value("blockHash")},
                      {"doc_hash", doc_hash},
                      {"input_index", input_index}
                    });
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return {true, recordedSpendInDAG};
}

QVDRecordsT SpentCoinsHandler::searchInSpentCoins(
  const QStringList &coins,
  const OrderT &order)
{
  ClausesT clauses {};

  if (coins.size() > 0)
    clauses.push_back({"sp_coin", coins, "IN"});

  QueryRes res = DbModel::select(
    stbl_trx_spend,
    stbl_trx_spend_filed,
    clauses,
    order,
    0,
    true);

  return res.records;
}


/**
* accepts given coins and prepares an ordered history of coins spent
*/
GRecordsT SpentCoinsHandler::makeSpentCoinsDict(const QStringList &coins)
{
  // TODO: maybe optimize it via bloom filters for daily(cyclic) spent coins
  GRecordsT coinsInSpentTable = {};
//  let spendsOrder = {};
  QVDRecordsT inDAGRecordedCoins = searchInSpentCoins(coins, {{"sp_spend_date", "ASC"}});
  if (inDAGRecordedCoins.size()> 0)
  {
    for (QVDicT sp: inDAGRecordedCoins)
    {
      QString the_coin = sp.value("spCoin").toString();
      if (!coinsInSpentTable.keys().contains(the_coin))
        coinsInSpentTable[the_coin] = {};
      auto[block_hash_, doc_hash_] = CUtils::unpackCoinSpendLoc(sp.value("spSpendLoc").toString());
      coinsInSpentTable[the_coin].push_back(QVDicT{
        {"spendDate", sp.value("spSpendDate").toString()},
        {"spendBlockHash", block_hash_},
        {"spendDocHash", doc_hash_}});

      /**
      * making a dictionary of history of spending each unique coin,
      * and ordering by spent time in machine's point of view.
      * later it will be used to vote about transactions priority.
      * it is totally possible in this step machine can not retrieve very old spending
      * (because the spent table periodicly truncated), in this case machine will vote a doublespended doc as a first document
      * later in "doGroupByCoinAndVoter" method we add second index(vote date) to securing the spend order
      *
      */
      // if (!_.has(spendsOrder, the_coin))
      //     spendsOrder[the_coin] = [];
      // spendsOrder[the_coin].push({
      //     date: sp.spSpendDate,
      //     blockHash: spendInfo.blockHash,
      //     docHash: spendInfo.docHash,
      // });
    }
    CLog::log("already Spent And Recorded Inputs Dict: " + CUtils::dumpIt(coinsInSpentTable), "trx", "error");
  }
  return coinsInSpentTable;
}
