#ifndef UTXOHANDLER_H
#define UTXOHANDLER_H


class UTXOHandler
{
public:
  UTXOHandler();

  const static QString stbl_trx_utxos;
  const static QStringList stbl_trx_utxos_fields;

  static void refreshVisibility();

  static ClausesT prepareUTXOQuery(
    const QStringList &coins = {},
    const QStringList &visible_by = {});

  static QVDRecordsT getCoinsInfo(
    const QStringList &coins = {},
    const QStringList &visible_by = {},
    const QStringList &fields = {"DISTINCT ut_coin", "ut_o_address", "ut_o_value", "ut_ref_creation_date"});

  static void inheritAncestorsVisbility(
      const QStringList &blockHashes,
      const QString &creationDate,
      const QString &newBlockHash);

  static void addNewUTXO(
      const QString &creationDate,
      const QString &the_coin,
      const QString &visibleBy,
      const QString &address,
      const MPAIValueT &coin_value,
      const QString &refCreationDate);

  static bool removeVisibleOutputsByBlocks(const QStringList &blockHashes);
};

#endif // UTXOHANDLER_H
