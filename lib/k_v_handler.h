#ifndef KVHANDLER_H
#define KVHANDLER_H



class KVHandler
{
public:
  static QString stbl_kvalue;
  KVHandler();

  static QString getValue(const QString &kvKey);
  static bool deleteKey(const QString &kvKey);
  static QVDRecordsT serach();
  static bool updateKValue(const QString &key, const QString &value);
  static bool upsertKValue(
      const QString &key,
      const QString &value,
      const bool &log = true);
  static bool setValue(
      const QString &key,
      const QString &value,
      const bool &log = true);

};

#endif // KVHANDLER_H
