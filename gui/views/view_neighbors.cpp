#include "mainwindow.h"
#include "ui_mainwindow.h"



enum ITEM_DATA{
  N_ID = 1,
  CONNECTION_TYPE = 3,
  EMAIL = 4
};

//void MainWindow::loadNeighborsInfo()
//{
//  CLog::log("public_neighbor_email: ==================");
//  ui->tableWidget_pulicNeighbors->setRowCount(0);
//  ui->tableWidget_pulicNeighbors->setColumnCount(6);
//  ui->tableWidget_pulicNeighbors->setEditTriggers(QAbstractItemView::NoEditTriggers);

//  ui->tableWidget_pulicNeighbors->setHorizontalHeaderLabels({
//    "Email",
//    "Public Key",
//    "Shakehand",
//    "Discover", //(Ask about her public neighbors list)
//    "Connect People", // (Present her to your neighbors)
//    "Disconnect"});

//  QTableWidgetItem *item {};
//  int row_number {};
//  QVDRecordsT public_neighbors = CMachine::getNeighbors(CConsts::PUBLIC);

//  for (QVDicT a_nei: public_neighbors)
//  {

//    row_number = ui->tableWidget_pulicNeighbors->rowCount();
//    ui->tableWidget_pulicNeighbors->insertRow(row_number);

//    item = new QTableWidgetItem { a_nei.value("n_email").toString() };
//    ui->tableWidget_pulicNeighbors->setItem(row_number, 0, item);

//    item = new QTableWidgetItem { a_nei.value("n_pgp_public_key").toString() };
//    ui->tableWidget_pulicNeighbors->setItem(row_number, 1, item);


////    QPushButton *button = new QPushButton("SSSS");

//    item = new QTableWidgetItem { "Shake Hand" };
//    item->setToolTip("Double click here to send a neighborhood request");
//    item->setData(ITEM_DATA::N_ID, a_nei.value("n_id"));
//    item->setData(ITEM_DATA::CONNECTION_TYPE, CConsts::PUBLIC);
//    item->setData(ITEM_DATA::EMAIL, a_nei.value("n_email"));
//    ui->tableWidget_pulicNeighbors->setItem(row_number, 2, item);

//    item = new QTableWidgetItem { "Disconnect" };
//    item->setToolTip("Double click here to delete neighbor");
//    item->setData(ITEM_DATA::N_ID, a_nei.value("n_id"));
//    item->setData(ITEM_DATA::CONNECTION_TYPE, CConsts::PUBLIC);
//    item->setData(ITEM_DATA::EMAIL, a_nei.value("n_email"));
//    ui->tableWidget_pulicNeighbors->setItem(row_number, 5, item);

//  }

//  connect(ui->tableWidget_pulicNeighbors, SIGNAL( cellDoubleClicked (int, int) ), this, SLOT( neighborDoubleClicked( int, int ) ) );
//////  QObject::connect(ui->tableWidget_pulicNeighbors, SIGNAL(itemClicked(QTableWidget*)), this, SLOT(itemClicked(QTableWidget*)));
//}

//  -  -  -  MainWindow

void MainWindow::neighborDoubleClicked(int nighbor_row, int nighbor_col)
{
  CLog::log("clicked on table neighbors (" + QString::number(nighbor_col) + "," + QString::number(nighbor_row) + ")");
  auto item = ui->tableWidget_pulicNeighbors->item(nighbor_row, nighbor_col);

  QString connection_type = item->data(ITEM_DATA::CONNECTION_TYPE).toString();
  QString n_id = item->data(ITEM_DATA::N_ID).toString();
  QString email = item->data(ITEM_DATA::EMAIL).toString();

  if (nighbor_col == 2)
  {
    // hand shake
    bool res = CMachine::handshakeNeighbor(n_id, connection_type);
    if(res)
    {
      informationMessage("Handshake to " + email +  " sent");
    }else{
      warningMessage("Handshake to " + email +  " was failed!");
    }
  }
  else if (nighbor_col == 5)
  {
    // delete neighbor
    bool res = CMachine::deleteNeighbors(n_id, connection_type);
    if(res)
    {
      informationMessage("Delete reqauest to " + email +  " sent");
    }else{
      warningMessage("Delete reqauest to " + email +  " was failed!");
    }
  }

    InitializeNode::refreshGUI();
}

bool MainWindow::on_pushButton_addANewNeighbor_clicked()
{
  QString public_neighbor_email = ui->lineEdit_publicNeighborEmail->text();
  QString public_neighbor_public_key = ui->textEdit_publicNeighborPubKey->toPlainText();
  CLog::log("public_neighbor_email: " + public_neighbor_email);
  CLog::log("public_neighbor_public_key: " + public_neighbor_public_key);
  if(public_neighbor_email == "")
  {
    warningMessage("The neighbor email is needed!", "Adding a new neighbor");
    return false;
  }
  auto[staut, msg] = CMachine::addANewNeighbor(public_neighbor_email, CConsts::PUBLIC, public_neighbor_public_key);
  if(!staut)
  {
    warningMessage(msg, "Adding a new neighbor");
    return false;
  }

  // refresh the neighbors list
//  loadNeighborsInfo();
  CGUI::signalUpdateNeighbors();
  informationMessage("New neighbor added", "Adding a new neighbor");

  return true;
}


void MainWindow::connectNeighborsToModel()
{
  ModelNeighbors *mdl = new ModelNeighbors(this);
  CGUI::setModelNeighbors(mdl);
  mdl->populateData();
  ui->tableView_pulicNeighbors->setModel(mdl);
  connect(ui->tableView_pulicNeighbors, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(handshakeClicked(const QModelIndex &)));

}

void MainWindow::handshakeDblClicked(const QModelIndex &index)
{
  if (index.isValid()) {
    QString cellText = index.data().toString();
  }
}

