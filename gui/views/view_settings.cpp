#include "mainwindow.h"
#include "ui_mainwindow.h"



//  -  -  -  MainWindow

void MainWindow::on_pushButton_saveEmailsSettings_clicked()
{
  if (!emailSettingsValidator())
    return;

  CLog::log("Updating settings");
  EmailSettings public_email = CMachine::getProfile().m_mp_settings.m_public_email;

  CMachine::setPublicEmailAddress(ui->lineEdit_publicEmailAddress->text());
  CMachine::setPublicEmailInterval(ui->lineEdit_publicEmailInterval->text());
  CMachine::setPublicEmailIncomeHost(ui->lineEdit_publicEmailIncomeHost->text());
  CMachine::setPublicEmailPassword(ui->lineEdit_publicEmailPassword->text());
  CMachine::setPublicEmailIncomeIMAP(ui->lineEdit_lineEdit_publicEmailIncomeIMAP->text());
  CMachine::setPublicEmailIncomePOP(ui->lineEdit_lineEdit_publicEmailIncomePOP->text());
  CMachine::setPublicEmailOutgoingSMTP(ui->lineEdit_lineEdit_publicEmailOutgoingSMTP->text());
  CMachine::setPublicEmailOutgoingHost(ui->lineEdit_publicEmailOutgoingHost->text());
  CMachine::setTermOfServices( ui->checkBox_ToS->isChecked()==true ? CConsts::YES : CConsts::NO );
  bool status = CMachine::saveSettings();
  if (!status)
    warningMessage("Failed to save setings!", "Save Email Settings");

  informationMessage("Setings updated", "Save Email Settings");

}

bool MainWindow::emailSettingsValidator()
{
  if (!ui->checkBox_ToS->isChecked())
  {
    warningMessage("Terms of Seervices not accepted!", "Save Email Settings");
    return false;
  }

  if ( (ui->lineEdit_publicEmailIncomeHost->text() == "") || (ui->lineEdit_publicEmailOutgoingHost->text() == ""))
  {
   warningMessage("The host name (Income or Outcome) is missed", "Save Email Settings");
    return false;
  }

  if (ui->lineEdit_publicEmailAddress->text() == "")
  {
   warningMessage("Public email address is missed", "Save Email Settings");
    return false;
  }

  return true;
}


