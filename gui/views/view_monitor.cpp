#include "stable.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib/messaging_protocol/dag_message_handler.h"
#include "lib/parsing_q_handler/parsing_q_handler.h"
#include "lib/sending_q_handler/sending_q_handler.h"
#include "lib/file_buffer_handler/file_buffer_handler.h"



#include <QtGui>
#include <QFileSystemModel>
#include <QAbstractTableModel>


enum ITEM_DATA{
  BLOCK_HASH = 1};



void MainWindow::on_pushButton_readHDManually_clicked()
{
  FileBufferHandler::readAndParseHardDiskInbox();
  refreshParsingQ();
}

void MainWindow::on_pushButton_refresDAGManually_clicked()
{
  refresDAG();
  refreshSendingQ();
  refreshParsingQ();
  refreshMissingBlocks();
  refreshLeavesInfo();
}

void MainWindow::refresDAG()
{
//  CLog::log("public_neighbor_email: ==================");

//  ui->tableWidget_DAGHistory->setRowCount(0);
//  ui->tableWidget_DAGHistory->setColumnCount(2);
//  ui->tableWidget_DAGHistory->setEditTriggers(QAbstractItemView::NoEditTriggers);

//  ui->tableWidget_DAGHistory->setHorizontalHeaderLabels({
//    "#",
//    "code"});

//  QTableWidgetItem *item {};
//  int row_number {};
//  QVDRecordsT blocks = DAG::searchInDAG(
//    {},
//    {"b_hash", "b_type", "b_creation_date"},
//    {{"b_creation_date", "ASC"}});


////  QHash<QString, QBrush> brushes = CGUI()::getBlocksBrushes();
//  for (QVDicT a_block: blocks)
//  {
//    row_number = ui->tableWidget_DAGHistory->rowCount();
//    ui->tableWidget_DAGHistory->insertRow(row_number);

//    item = new QTableWidgetItem { a_block.value("b_type").toString() };
//    ui->tableWidget_DAGHistory->setItem(row_number, 0, item);

//    item = new QTableWidgetItem { CUtils::hash8c(a_block.value("b_hash").toString()) };
////    item->setBackground(brushes[a_block.value("b_type").toString()]);
//    item->setToolTip(a_block.value("b_creation_date").toString() + " " + a_block.value("b_type").toString() + " Double click to see block details");
//    item->setData(ITEM_DATA::BLOCK_HASH, a_block.value("b_hash"));
//    ui->tableWidget_DAGHistory->setItem(row_number, 1, item);
//  }

//  ui->lbl_server_time->setText(CUtils::getNow());
//  ui->statusbar->showMessage("Syncroniziing...", 5000);

//  connect(ui->tableWidget_DAGHistory, SIGNAL( cellDoubleClicked (int, int) ), this, SLOT( neighborDoubleClicked( int, int ) ) );

}

void MainWindow::on_pushButton_invokeDescendents_clicked()
{
  DAGMessageHandler::invokeDescendents();
}


void MainWindow::refreshSendingQ()
{
  CLog::log("public_neighbor_email: ==================");

  ui->tableWidget_sendingQ->setRowCount(0);
  ui->tableWidget_sendingQ->setColumnCount(2);
  ui->tableWidget_sendingQ->setEditTriggers(QAbstractItemView::NoEditTriggers);

  ui->tableWidget_sendingQ->setHorizontalHeaderLabels({
    "Type",
    "Code"});
  ui->tableWidget_sendingQ->setColumnWidth(0, 55);
  ui->tableWidget_sendingQ->setColumnWidth(1, 101);

  QTableWidgetItem *item {};
  int row_number {};
  QVDRecordsT cpackets = SendingQHandler::fetchFromSendingQ();
  QHash<QString, QBrush> brushes = getCPacketBrushes();
  QStringList cpacket_types = brushes.keys();

  for (QVDicT a_cpacket: cpackets)
  {

    row_number = ui->tableWidget_sendingQ->rowCount();
    ui->tableWidget_sendingQ->insertRow(row_number);

    item = new QTableWidgetItem { a_cpacket.value("sq_type").toString() };
    ui->tableWidget_sendingQ->setItem(row_number, 0, item);


    item = new QTableWidgetItem { CUtils::hash8c(a_cpacket.value("sq_code").toString()) };

    QBrush the_brush;
    if (cpacket_types.contains(a_cpacket.value("sq_type").toString()))
    {
      the_brush = brushes[a_cpacket.value("sq_type").toString()];
    } else {
      the_brush = brushes[CConsts::DEFAULT];
    }
    item->setBackground(the_brush);
    item->setToolTip(
      a_cpacket.value("sq_connection_type").toString() + " " +
      a_cpacket.value("sq_creation_date").toString() + " " +
      a_cpacket.value("sq_title").toString() +
      ". Double click to see block details");
    item->setData(ITEM_DATA::BLOCK_HASH, a_cpacket.value("sq_code"));
    ui->tableWidget_sendingQ->setItem(row_number, 1, item);

  }

  connect(ui->tableWidget_sendingQ, SIGNAL( cellDoubleClicked (int, int) ), this, SLOT( neighborDoubleClicked( int, int ) ) );

}


void MainWindow::connectInboxFilesToModel()
{
  QString inbox = CConsts::HD_PATHES::INBOX;
  if (CMachine::getAppCloneId() > 0)
    inbox = inbox + QString::number(CMachine::getAppCloneId());

  QFileSystemModel *model = new QFileSystemModel;
  model->setRootPath(inbox);

  ui->listView_inboxFiles->setModel(model);
  ui->listView_inboxFiles->setRootIndex(model->index(inbox));
}

void MainWindow::connectOutboxFilesToModel()
{
  QString outbox = CConsts::HD_PATHES::OUTBOX;
  if (CMachine::getAppCloneId() > 0)
    outbox = outbox + QString::number(CMachine::getAppCloneId());

  QFileSystemModel *model = new QFileSystemModel;
  model->setRootPath(outbox);

  ui->listView_outboxFiles->setModel(model);
  ui->listView_outboxFiles->setRootIndex(model->index(outbox));
}

//void MainWindow::refreshOutboxFiles()
//{
//  CLog::log("public_neighbor_email: ==================");

//  ui->tableWidget_outboxFiles->setRowCount(0);
//  ui->tableWidget_outboxFiles->setColumnCount(1);
//  ui->tableWidget_outboxFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);

//  ui->tableWidget_outboxFiles->setHorizontalHeaderLabels({
//    "File Name"});
//  ui->tableWidget_outboxFiles->setColumnWidth(0, 291);

//  QTableWidgetItem *item {};
//  int row_number {};
//  QStringList files = FileBufferHandler::listHardDiskOutbox();
//  CLog::log("files: " + files.join(","));
//  QHash<QString, QBrush> brushes = getCPacketBrushes();
//  QStringList cpacket_types = brushes.keys();

//  for (QString a_file: files)
//  {

//    row_number = ui->tableWidget_outboxFiles->rowCount();
//    ui->tableWidget_outboxFiles->insertRow(row_number);

//    item = new QTableWidgetItem { a_file.split("@")[1] + a_file.split("@")[2] };
//    item->setToolTip(a_file);
//    ui->tableWidget_outboxFiles->setItem(row_number, 0, item);

//  }

////  connect(ui->tableWidget_outboxFiles, SIGNAL( cellDoubleClicked (int, int) ), this, SLOT( neighborDoubleClicked( int, int ) ) );

//}


void MainWindow::refreshParsingQ()
{
//  CLog::log("public_neighbor_email: ==================");

//  ui->tableWidget_parsingQ->setRowCount(0);
//  ui->tableWidget_parsingQ->setColumnCount(2);
//  ui->tableWidget_parsingQ->setEditTriggers(QAbstractItemView::NoEditTriggers);

//  ui->tableWidget_parsingQ->setHorizontalHeaderLabels({
//    "Type",
//    "Code"});
//  ui->tableWidget_parsingQ->setColumnWidth(0, 55);
//  ui->tableWidget_parsingQ->setColumnWidth(1, 101);

//  QTableWidgetItem *item {};
//  int row_number {};
//  QVDRecordsT cpackets = ParsingQHandler::searchParsingQ(
//    {},
//    {"pq_type", "pq_code", "pq_sender", "pq_connection_type", "pq_receive_date", "pq_prerequisites", "pq_parse_attempts", "pq_creation_date", "pq_insert_date"},
//    {{"pq_connection_type", "ASC"}, {"pq_creation_date", "ASC"}});

//  QHash<QString, QBrush> brushes = getCPacketBrushes();
//  QStringList cpacket_types = brushes.keys();

//  for (QVDicT a_cpacket: cpackets)
//  {
//    QStringList prerequisites = a_cpacket.value("pq_prerequisites").toString().split(",");
//    row_number = ui->tableWidget_parsingQ->rowCount();
//    ui->tableWidget_parsingQ->insertRow(row_number);

//    item = new QTableWidgetItem { a_cpacket.value("pq_type").toString() };
//    ui->tableWidget_parsingQ->setItem(row_number, 0, item);


//    item = new QTableWidgetItem { CUtils::hash8c(a_cpacket.value("pq_code").toString()) };

//    QBrush the_brush;
//    if (cpacket_types.contains(a_cpacket.value("pq_type").toString()))
//    {
//      the_brush = brushes[a_cpacket.value("pq_type").toString()];
//    } else {
//      the_brush = brushes[CConsts::DEFAULT];
//    }
//    item->setBackground(the_brush);
//    item->setToolTip(
//      a_cpacket.value("pq_sender").toString() + ": " +
//      a_cpacket.value("pq_connection_type").toString() + " " +
//      " prerequisites(" + QString::number(prerequisites.size()) + ") " +
//      a_cpacket.value("pq_creation_date").toString() + " " +
//      a_cpacket.value("pq_parse_attempts").toString() +
//      ". Double click to see block details");
//    item->setData(ITEM_DATA::BLOCK_HASH, a_cpacket.value("pq_code"));
//    ui->tableWidget_parsingQ->setItem(row_number, 1, item);

//  }

//  connect(ui->tableWidget_parsingQ, SIGNAL( cellDoubleClicked (int, int) ), this, SLOT( neighborDoubleClicked( int, int ) ) );

}

void MainWindow::refreshLeavesInfo()
{

}

void MainWindow::refreshMissingBlocks()
{

  ui->tableWidget_missedBlocks->setRowCount(0);
  ui->tableWidget_missedBlocks->setColumnCount(1);
  ui->tableWidget_missedBlocks->setEditTriggers(QAbstractItemView::NoEditTriggers);

  ui->tableWidget_missedBlocks->setHorizontalHeaderLabels({"Code"});
  ui->tableWidget_missedBlocks->setColumnWidth(0, 101);

  QTableWidgetItem *item {};
  int row_number {};
  QStringList missed_blocks = MissedBlocksHandler::getMissedBlocksToInvoke();
  CLog::log("refreshMissingBlocks: " + missed_blocks.join(","), "app", "trace");

  QHash<QString, QBrush> brushes = getCPacketBrushes();
  QStringList cpacket_types = brushes.keys();

  for (QString a_block: missed_blocks)
  {
    row_number = ui->tableWidget_missedBlocks->rowCount();
    ui->tableWidget_missedBlocks->insertRow(row_number);

    QBrush the_brush = brushes[CConsts::DEFAULT]; // TODO: implement color index for attemps

    item = new QTableWidgetItem { CUtils::hash8c(a_block) };
    item->setBackground(the_brush);
    item->setData(ITEM_DATA::BLOCK_HASH, a_block);
    ui->tableWidget_missedBlocks->setItem(row_number, 0, item);

  }

//  connect(ui->tableWidget_missedBlocks, SIGNAL( cellDoubleClicked (int, int) ), this, SLOT( neighborDoubleClicked( int, int ) ) );

}

void MainWindow::on_pushButton_broadcastManually_clicked()
{
  SendingQHandler::sendOutThePacket();
  refreshSendingQ();
}

void MainWindow::on_pushButton_invokeBlock_clicked()
{
  DAGMessageHandler::invokeBlock(ui->lineEdit_invokeBlock->text());
  refreshSendingQ();
}


void MainWindow::on_pushButton_controlMissedBlocks_clicked()
{
  MissedBlocksHandler::refreshMissedBlock();
  refreshMissingBlocks();
}

void MainWindow::on_pushButton_invokeAllMissedBlocks_clicked()
{
  DAGMessageHandler::recursiveMissedBlocksInvoker();
}

void MainWindow::on_pushButton_pullFromParsingQManually_clicked()
{
  ParsingQHandler::recursiveSmartPullFromParsingQ();
  refreshParsingQ();
  refreshMissingBlocks();
}




void MainWindow::connectDAGHistoryToModel()
{
  ui->tableView_dagHistory->setColumnWidth(0, 55);
  ui->tableView_dagHistory->setColumnWidth(1, 101);

  ModelBlocks *mdl = new ModelBlocks(this);
  CGUI::setModelBlocks(mdl);
  mdl->populateData();
  ui->tableView_dagHistory->setModel(mdl);
}

void MainWindow::connectParsingQToModel()
{
  ui->tableView_parsingQ->setColumnWidth(0, 55);
  ui->tableView_parsingQ->setColumnWidth(1, 101);

  ModelParsingQ *mdl = new ModelParsingQ(this);
  CGUI::setModelParsingQ(mdl);
  mdl->populateData();
  ui->tableView_parsingQ->setModel(mdl);
}

