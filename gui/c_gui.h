#ifndef CGUI_H
#define CGUI_H

class MainWindow;
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <thread>
#include <future>

#include <QFileDialog>
#include <QApplication>
#include <QTableView>
#include <QItemDelegate>
#include <QIdentityProxyModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QPixmap>
#include <QObject>

#include "lib/machine/machine_handler.h"

#include "gui/models/model_blocks.h"
#include "gui/models/model_neighbors.h"
#include "gui/models/model_parsing_q.h"

//#include "gui/models/blocks.h"
//#include "gui/models/parsing_q.h"
//#include "gui/models/neighbors.h"



class CGUI: public QObject
{
  Q_OBJECT
public:

  Ui::MainWindow* m_ui;
  bool m_checkBox_ToS;


  CGUI(const CGUI&) = delete;
  static CGUI &get();
  static void initGUI(Ui::MainWindow* ui){ get().IinitGUI(ui); };

  static QHash<QString, QBrush> getBlocksBrushes();

  static void setModelBlocks(ModelBlocks* model){ get().IsetModelBlocks(model); };
  static void signalUpdateBlocks(){ get().m_modelBlocks->signalUpdate(); };

  static void setModelParsingQ(ModelParsingQ* model){ get().IsetModelParsingQ(model); };
  static void signalUpdateParsingQ(){ get().m_modelParsingQ->signalUpdate(); };

  static void setModelNeighbors(ModelNeighbors* model){ get().IsetModelNeighbors(model); };
  static void signalUpdateNeighbors(){ get().m_modelNeighbors->signalUpdate(); };


signals:

private slots:




  void on_pushButton_10_clicked();

private:
  explicit CGUI(QObject *parent = nullptr);
//  CGUI(){};
  static CGUI s_instance;
  void IinitGUI(Ui::MainWindow* ui);

  ModelBlocks* m_modelBlocks;
  void IsetModelBlocks(ModelBlocks* model){
    m_modelBlocks = model;
    m_modelBlocks->m_brushes = getBlocksBrushes();
  };

  ModelParsingQ* m_modelParsingQ;
  void IsetModelParsingQ(ModelParsingQ* model){
    m_modelParsingQ = model;
    m_modelBlocks->m_brushes = getBlocksBrushes();
  };

  ModelNeighbors* m_modelNeighbors;
  void IsetModelNeighbors(ModelNeighbors* model){
    m_modelNeighbors = model;
    m_modelBlocks->m_brushes = getBlocksBrushes();
  };

};


#endif // CGUI_H
