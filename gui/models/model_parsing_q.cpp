#include "lib/parsing_q_handler/parsing_q_handler.h"

#include "model_parsing_q.h"

ModelParsingQ::ModelParsingQ(QObject *parent)
{
  Q_UNUSED(parent);
}


int ModelParsingQ::rowCount(const QModelIndex & /*parent*/) const
{
  return m_parsing_q.size();
}

int ModelParsingQ::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}

QVariant ModelParsingQ::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return m_parsing_q[index.row()].value("pq_type").toString();
        case 1:
          return CUtils::hash8c(m_parsing_q[index.row()].value("pq_code").toString());
        case 2:
          return m_parsing_q[index.row()].value("pq_creation_date").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return QBrush();
        case 1:
        case 2:
          return m_brushes[m_parsing_q[index.row()].value("pq_type").toString()];

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {


    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelParsingQ::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Type");
      case 1:
        return QString("Hash");
      case 2:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}

void ModelParsingQ::populateData()
{
  m_parsing_q = ParsingQHandler::searchParsingQ(
    {},
    {"pq_type", "pq_code", "pq_sender", "pq_connection_type", "pq_receive_date", "pq_prerequisites", "pq_parse_attempts", "pq_creation_date", "pq_insert_date"},
    {{"pq_connection_type", "ASC"}, {"pq_creation_date", "ASC"}});
}


void ModelParsingQ::signalUpdate()
{
  beginResetModel();
  populateData();
  endResetModel();
}

