#include "stable.h"

#include <QTimer>

#include "lib/dag/dag.h"
#include "model_blocks.h"

ModelBlocks::ModelBlocks(QObject *parent)
{
//  QTimer* timer = new QTimer(this);
//  timer->setInterval(3000);
//  connect(timer, SIGNAL(timeout()) , this, SLOT(timerHit()));
//  timer->start();
}

int ModelBlocks::rowCount(const QModelIndex & /*parent*/) const
{
  return m_blocks.size();
}

int ModelBlocks::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}

QVariant ModelBlocks::data(const QModelIndex &index, int role) const
{
  switch(role){
    case Qt::DisplayRole:
      switch(index.column())
      {
        case 0:
          return m_blocks[index.row()].value("b_type").toString();
        case 1:
          return CUtils::hash8c(m_blocks[index.row()].value("b_hash").toString());
        case 2:
          return m_blocks[index.row()].value("b_creation_date").toString();
      }
      break;

    case Qt::BackgroundRole:
      switch(index.column())
      {
        case 0:
          return QBrush();
        case 1:
        case 2:
          return m_brushes[m_blocks[index.row()].value("b_type").toString()];

      }
      break;


  }

  if (role == Qt::DisplayRole)
  {


    return QString("Row%1, Column%2")
    .arg(index.row() + 1)
    .arg(index.column() +1);
  }
  return QVariant();
}

QVariant ModelBlocks::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
    {
      switch (section)
      {
      case 0:
        return QString("Type");
      case 1:
        return QString("Hash");
      case 2:
        return QString("Create Date");
      }
    }
  }
  return QVariant();
}

//Qt::ItemFlags ModelBlocks::flags(const QModelIndex & /*index*/) const
//{
//  return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
//}

//void ModelBlocks::timerHit()
//{
//   //we identify the top left cell
//   QModelIndex topLeft = createIndex(0,0);
//   //emit a signal to make the view reread identified data
//   emit dataChanged(topLeft, topLeft);
//}

void ModelBlocks::populateData()
{
  m_blocks = DAG::searchInDAG({});    // TODO: optimize it ASAP
}


void ModelBlocks::signalUpdate()
{
  beginResetModel();
  m_blocks = DAG::searchInDAG({});    // TODO: optimize it ASAP
  endResetModel();
}

