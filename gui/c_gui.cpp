#include "mainwindow.h"
#include "ui_mainwindow.h"



#include "c_gui.h"

CGUI::CGUI(QObject *parent) : QObject(parent)
{
}


CGUI CGUI::s_instance;

CGUI& CGUI::get()
{
  return s_instance;
}

void CGUI::IinitGUI(Ui::MainWindow* ui)
{
  m_ui = ui;
  QPixmap c_logo(":/assets/comen_logo.jpg");
  m_ui->label_logo_comen->setPixmap(c_logo.scaled(333, 299, Qt::KeepAspectRatio));

}




void CGUI::on_pushButton_10_clicked()
{
    CLog::log("poooooooooooo");
}


//  -  -  -  MainWindow

void MainWindow::loadGUIData()
{
  CLog::log("--------------------pppp------");
  // (maybe init machin) and then read machine configs
  bool m_checkBox_ToS = (CMachine::getProfile().m_mp_settings.m_term_of_services == CConsts::YES);
  ui->checkBox_ToS->setChecked(m_checkBox_ToS);

  EmailSettings public_email = CMachine::getProfile().m_mp_settings.m_public_email;
  ui->lineEdit_publicEmailAddress->setText(public_email.m_address);
  ui->lineEdit_publicEmailInterval->setText(public_email.m_fetching_interval_by_minute);
  ui->lineEdit_publicEmailIncomeHost->setText(public_email.m_incoming_mail_server);
  ui->lineEdit_publicEmailPassword->setText(public_email.m_password);
  ui->lineEdit_lineEdit_publicEmailIncomeIMAP->setText(public_email.m_income_IMAP);
  ui->lineEdit_lineEdit_publicEmailIncomePOP->setText(public_email.m_income_POP3);
  ui->lineEdit_lineEdit_publicEmailOutgoingSMTP->setText(public_email.m_outgoing_SMTP);
  ui->lineEdit_publicEmailOutgoingHost->setText(public_email.m_outgoing_mail_server);



  // combo sample
  ui->comboBox_contributeUsefulness->addItem("level 1", 1);
  ui->comboBox_contributeUsefulness->addItem("level 2", 2);
  ui->comboBox_contributeUsefulness->addItem("level 3", 3);
  ui->comboBox_contributeUsefulness->addItem("level 4", 4);

  // table sample
//  ui->tableWidget_coinsList->addItem("Coin 1");
//  ui->tableWidget_coinsList->addItem("Coin 2");
//  ui->tableWidget_coinsList->addItem("Coin 3");
//  ui->tableWidget_coinsList->addItem("Coin 4");

  // bind signal slotsoccc
  connectInboxFilesToModel();
  connectOutboxFilesToModel();
  connectDAGHistoryToModel();
  connectParsingQToModel();

  // load neighbors info
  connectNeighborsToModel();
//  loadNeighborsInfo();

  // load blocks history and display DAG
  refresDAG();
  refreshSendingQ();
  refreshParsingQ();
  refreshMissingBlocks();
  refreshLeavesInfo();

}

void MainWindow::informationMessage(const QString &msg, const QString &title)
{
  CLog::log(msg);
  QMessageBox::information(this, title, msg);
}

void MainWindow::warningMessage(const QString &msg, const QString &title)
{
  CLog::log(msg);
  QMessageBox::warning(this, title, msg);
}

QHash<QString, QBrush> MainWindow::getCPacketBrushes()
{
  QV2DicT settings{
    { CConsts::DEFAULT, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::Coinbase, {
      {"r0" ,1},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::Normal, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,1},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::FSign, {
      {"r0" ,0},
      {"g0" ,0},
      {"b0" ,1},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::SusBlock, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::RpBlock, {
      {"r0" ,1},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::RlBlock, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::POW, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

  };

  QHash<QString, QBrush> brushes;
  for(QString a_block_type: settings.keys())
  {
    QVDicT setting = settings[a_block_type];
    QRadialGradient gradient(50, 50, 90, 50, 50);
    gradient.setColorAt(
      0,
      QColor::fromRgbF(
        setting["r0"].toInt(),
        setting["g0"].toInt(),
        setting["b0"].toInt(),
        setting["a0"].toInt()));

    gradient.setColorAt(
      1,
      QColor::fromRgbF(
        setting["r1"].toInt(),
        setting["g1"].toInt(),
        setting["b1"].toInt(),
        setting["a1"].toInt()));

    QBrush brush(gradient);

    brushes.insert(a_block_type, brush);
  }

  return brushes;
}

QHash<QString, QBrush> CGUI::getBlocksBrushes()
{
  QV2DicT settings{
    { CConsts::BLOCK_TYPES::Genesis, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::Coinbase, {
      {"r0" ,1},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::Normal, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,1},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::FSign, {
      {"r0" ,0},
      {"g0" ,0},
      {"b0" ,1},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::SusBlock, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::RpBlock, {
      {"r0" ,1},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::RlBlock, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

    { CConsts::BLOCK_TYPES::POW, {
      {"r0" ,0},
      {"g0" ,1},
      {"b0" ,0},
      {"a0" ,1},
      {"r1" ,0},
      {"g1" ,0},
      {"b1" ,0},
      {"a1" ,0}
    }},

  };

  QHash<QString, QBrush> brushes;
  for(QString a_block_type: settings.keys())
  {
    QVDicT setting = settings[a_block_type];
    QRadialGradient gradient(50, 50, 90, 50, 50);
    gradient.setColorAt(
      0,
      QColor::fromRgbF(
        setting["r0"].toInt(),
        setting["g0"].toInt(),
        setting["b0"].toInt(),
        setting["a0"].toInt()));

    gradient.setColorAt(
      1,
      QColor::fromRgbF(
        setting["r1"].toInt(),
        setting["g1"].toInt(),
        setting["b1"].toInt(),
        setting["a1"].toInt()));

    QBrush brush(gradient);

    brushes.insert(a_block_type, brush);
  }

  return brushes;
}
