#include "stable.h"
#include "tests_string_manipulations.h"

TestsStringmanipulations::TestsStringmanipulations()
{

}

bool TestsStringmanipulations::doTests()
{
  test1();

  return true;
}

bool TestsStringmanipulations::test1()
{
  // strip no hex chars
  if(CUtils::stripNonHex("12dj8in 0e8") != "12d80e8")
    CUtils::exiter("strip Non Hex failed 1", 0);

  if(CUtils::stripNonHex("Z12dj8-/in 0'kkk\"=+e8") != "12d80e8")
    CUtils::exiter("strip Non Hex failed 2", 0);

  if(!CUtils::isValidHash("12d80e8"))
    CUtils::exiter("is Valid Hash failed 1", 0);

  if(CUtils::isValidHash("Z1\2dj8-/in 0'kkk\"=+e8"))
    CUtils::exiter("is Valid Hash failed 2", 0);

  return true;
}
