#ifndef TESTSCYCLETIMES_H
#define TESTSCYCLETIMES_H


class TestsCycleTimes
{
public:
  TestsCycleTimes();
  static bool doTests();
  static bool test1();
  static bool test2();
  static bool test3();
  static bool test4();

};

#endif // TESTSCYCLETIMES_H
