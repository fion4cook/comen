#include "stable.h"
#include "tests_cycle_times.h"

TestsCycleTimes::TestsCycleTimes()
{

}

bool TestsCycleTimes::doTests()
{
  bool status;
  status = test1();
  if (!status)
    return status;

  status = test2();
  if (!status)
    return status;

  status = test3();
  if (!status)
    return status;

  status = test4();
  if (!status)
    return status;


}

bool TestsCycleTimes::test1()
{
 //Should control coinbase date range is valid (12 hour per cycle)
  if (CConsts::TIME_GAIN == 1)
  {

    if (CUtils::getCoinbaseCycleNumber("2012-11-05 00:00:01") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed1", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 01:08:00") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed2", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 07:08:00") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed3", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 11:59:59") != "00:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed4", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 12:00:00") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed5", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 12:00:01") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed6", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 13:08:00") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed7", 0);
    if (CUtils::getCoinbaseCycleNumber("2012-11-05 23:59:59") != "12:00:00")
      CUtils::exiter("get Coinbase Cycle Number failed8", 0);

    if (CUtils::getCycleElapsedByMinutes("2012-11-05 00:00:00") != 0)
      CUtils::exiter("get Cycle Elapsed By Minutes failed1", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 00:01:01") != 1)
      CUtils::exiter("get Cycle Elapsed By Minutes failed2", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 07:00:01") != 420)
      CUtils::exiter("get Cycle Elapsed By Minutes failed3", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 12:00:01") != 0)
      CUtils::exiter("get Cycle Elapsed By Minutes failed4", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 13:00:01") != 60)
      CUtils::exiter("get Cycle Elapsed By Minutes failed5", 0);

    if (CUtils::getCycleElapsedByMinutes("2012-11-05 11:59:59") != 719)
      CUtils::exiter("get Cycle Elapsed By Minutes failed6", 0);
    if (CUtils::getCycleElapsedByMinutes("2012-11-05 23:59:59") != 719)
      CUtils::exiter("get Cycle Elapsed By Minutes failed7", 0);

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 00:00:01");
      Q_UNUSED(from_);
      Q_UNUSED(to_);
      if (from_ != "2012-11-05 00:00:00")
        CUtils::exiter("get Coinbase Range failed1", 0);
    }

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 00:00:01");
      Q_UNUSED(from_);
      if (to_ != "2012-11-05 11:59:59")
        CUtils::exiter("get Coinbase Range failed2", 0);
    }

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 11:59:59");
      Q_UNUSED(to_);
      if (from_ != "2012-11-05 00:00:00")
        CUtils::exiter("get Coinbase Range failed3", 0);
    }

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 11:59:59");
      Q_UNUSED(from_);
      if (to_ != "2012-11-05 11:59:59")
        CUtils::exiter("get Coinbase Range failed4", 0);
    }

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 12:00:00");
      Q_UNUSED(to_);
      if (from_ != "2012-11-05 12:00:00")
        CUtils::exiter("get Coinbase Range failed5", 0);
    }

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 12:00:00");
      Q_UNUSED(to_);
      if (to_ != "2012-11-05 23:59:59")
        CUtils::exiter("get Coinbase Range failed6", 0);
    }

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 23:59:59");
      Q_UNUSED(to_);
      if (from_ != "2012-11-05 12:00:00")
        CUtils::exiter("get Coinbase Range failed7", 0);
    }

    {
      auto [from_, to_] = CUtils::getCoinbaseRange("2012-11-05 23:59:59");
      Q_UNUSED(from_);
      if (to_ != "2012-11-05 23:59:59")
        CUtils::exiter("get Coinbase Range failed8", 0);
    }



    if(CUtils::getCoinbaseCycleStamp("2012-11-05 00:00:00") != "2012-11-05 00:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed1 " + CUtils::getCoinbaseCycleStamp("2012-11-05 00:00:00"), 0);
    if(CUtils::getCoinbaseCycleStamp("2012-11-05 00:00:01") != "2012-11-05 00:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed2", 0);
    if(CUtils::getCoinbaseCycleStamp("2012-11-05 11:59:59") != "2012-11-05 00:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed3", 0);
    if(CUtils::getCoinbaseCycleStamp("2012-11-05 12:00:00") != "2012-11-05 12:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed4", 0);
    if(CUtils::getCoinbaseCycleStamp("2012-11-05 12:00:01") != "2012-11-05 12:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed5", 0);
    if(CUtils::getCoinbaseCycleStamp("2012-11-05 23:59:59") != "2012-11-05 12:00:00")
          CUtils::exiter("get Coinbase Cycle Stamp failed6", 0);

  }
  return true;
}


bool TestsCycleTimes::test2()
{

  // coinbase_plan
  // Should encode then decod and return same text
  if (CUtils::getCycleByMinutes() == 5)
  {
    // if(CoinbaseUTXOHandler.calcCoinbasedOutputMaturationDate("2019-05-10 00:00:00") != "2019-05-10 00:15:00");
    // if(CoinbaseUTXOHandler.calcCoinbasedOutputMaturationDate("2019-05-10 00:02:00") != "2019-05-10 00:15:00");
    // if(CoinbaseUTXOHandler.calcCoinbasedOutputMaturationDate("2019-05-10 00:04:59") != "2019-05-10 00:15:00");
  }


  if (CUtils::getCycleByMinutes() == 720)
  {
    if(CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 00:00:00") != "2019-05-11 00:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed1 " + CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 00:00:00"), 0);
    if(CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 11:59:59") != "2019-05-11 00:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed2 " + CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 11:59:59"), 0);

    if(CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 12:00:00") != "2019-05-11 12:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed3", 0);
    if(CoinbaseUTXOHandler::calcCoinbasedOutputMaturationDate("2019-05-10 23:59:59") != "2019-05-11 12:00:00")
      CUtils::exiter("calc Coinbased Output Maturation Date failed4", 0);
  }
  return true;
}

bool TestsCycleTimes::test3()
{
  if (CConsts::TIME_GAIN == 1)
  {
    {
     auto [from_, to_] = CUtils::getCbUTXOsDateRange("2017-07-22 00:00:00");
      if(from_ != "2017-07-21 00:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed1 " + from_, 0);
      if(to_ != "2017-07-21 11:59:59")
        CUtils::exiter("get Cb UTXOs Date Range failed2 " + to_, 0);
    }
    {
     auto [from_, to_] = CUtils::getCbUTXOsDateRange("2017-07-22 11:59:59");
      if(from_ != "2017-07-21 00:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed3", 0);
      if(to_ != "2017-07-21 11:59:59")
        CUtils::exiter("get Cb UTXOs Date Range failed4", 0);
    }
    {
     auto [from_, to_] = CUtils::getCbUTXOsDateRange("2017-07-22 12:00:00");
      if(from_ != "2017-07-21 12:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed5", 0);
      if(to_ != "2017-07-21 23:59:59")
          CUtils::exiter("get Cb UTXOs Date Range failed6", 0);
    }
    {
     auto [from_, to_] = CUtils::getCbUTXOsDateRange("2017-07-22 23:59:00");
      if(from_ != "2017-07-21 12:00:00")
        CUtils::exiter("get Cb UTXOs Date Range failed7", 0);
      if(to_ != "2017-07-21 23:59:59")
          CUtils::exiter("get Cb UTXOs Date Range failed8", 0);
    }

  }
  return true;
}

bool TestsCycleTimes::test4()
{

  // getCoinbaseInfo
  // Should getCoinbaseInfo
  {
    auto[cycleStamp, from_, to_, fromHour, toHour] = CUtils::getCoinbaseInfo("", "2016-01-01 00:00:00");
    if (cycleStamp != "2016-01-01 00:00:00")
      CUtils::exiter("get Coinbase Info failed1", 0);
    if (from_ != "2016-01-01 00:00:00")
      CUtils::exiter("get Coinbase Info failed2", 0);
    if (fromHour != "00:00:00")
      CUtils::exiter("get Coinbase Info failed3", 0);
    if (to_ != "2016-01-01 11:59:59")
      CUtils::exiter("get Coinbase Info failed4", 0);
    if (toHour != "11:59:59")
      CUtils::exiter("get Coinbase Info failed5", 0);
  }

  {
    auto[cycleStamp, from_, to_, fromHour, toHour] = CUtils::getCoinbaseInfo("", "2016-01-01 12:00:00");
    if (cycleStamp != "2016-01-01 12:00:00")
      CUtils::exiter("get Coinbase Info failed11", 0);
    if (from_ != "2016-01-01 12:00:00")
      CUtils::exiter("get Coinbase Info failed12", 0);
    if (fromHour != "12:00:00")
      CUtils::exiter("get Coinbase Info failed13", 0);
    if (to_ != "2016-01-01 23:59:59")
      CUtils::exiter("get Coinbase Info failed14", 0);
    if (toHour != "23:59:59")
      CUtils::exiter("get Coinbase Info failed15", 0);
  }

}
