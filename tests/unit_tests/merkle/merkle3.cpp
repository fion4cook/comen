#include <math.h>
#include <vector>

#include <QList>
#include <QVariant>

//#include "constants.h"
#include "stable.h"
#include "lib/ccrypto.h"

#include "lib/utils/cmerkle.h"

#include "merkle3.h"

using CCrypto::keccak256;

CMerkleTests3::CMerkleTests3()
{
}

void CMerkleTests3::doTests()
{
  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "hashed", "noHash");
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "1233")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_left_hash != "")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[0].midRef(0, 1) != "r")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[0].midRef(2) != "2")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[1].midRef(0, 1) != "r")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[1].midRef(2) != "33")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_left_hash != "1")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_left_hash != "3")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_proofs[0].midRef(2) != "33")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_proofs[0].midRef(0, 1) != "r")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_proofs[0].midRef(0, 1) != "l")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_proofs[0].midRef(2) != "12")
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("1", verifies["1"].m_proofs, verifies["1"].m_left_hash, "hashed", "noHash"))
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("2", verifies["2"].m_proofs, verifies["2"].m_left_hash, "hashed", "noHash"))
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("3", verifies["3"].m_proofs, verifies["3"].m_left_hash, "hashed", "noHash"))
    {
      CLog::log("ERROR in CMerkle::generate tree root for 1,2,3 " , "app", "fatal");
      exit(1098);
    }
  }



  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "hashed", "aliasHash");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "h(h(12)h(33))")
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
  }

  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "string", "aliasHash");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "h(h(h(1)h(2))h(h(3)h(3)))")
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
  }

  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "string");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != keccak256(keccak256(keccak256("1") + keccak256("2")) + keccak256(keccak256("3") + keccak256("3"))) )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies[keccak256("1")].m_proofs != QStringList{"r." + keccak256("2"), "r." + keccak256(keccak256("3") + keccak256("3"))} )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("1", verifies[keccak256("1")].m_proofs, verifies[keccak256("1")].m_left_hash, "string", "keccak256"))
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("2", verifies[keccak256("2")].m_proofs, verifies[keccak256("2")].m_left_hash, "string", "keccak256"))
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("3", verifies[keccak256("3")].m_proofs, verifies[keccak256("3")].m_left_hash, "string", "keccak256"))
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
  }


  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"});
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "04735db037244d0798e86e350ac5b51a913a3778a03c46899bbd5500fe6eec98" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
  }


  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate({"1", "2", "3"}, "hashed", "noHash");
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != "1233" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_left_hash != "" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[0].midRef(0, 1).toString() != "r" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[0].midRef(2).toString() != "2" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[1].midRef(0, 1).toString() != "r" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["1"].m_proofs[1].midRef(2).toString() != "33" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_left_hash != "1" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_proofs[0].midRef(2).toString() != "33" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["2"].m_proofs[0].midRef(0,1).toString() != "r" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_left_hash != "3" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_proofs[0].midRef(0, 1).toString() != "l" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (verifies["3"].m_proofs[0].midRef(2).toString() != "12" )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("1", verifies["1"].m_proofs, verifies["1"].m_left_hash, "hashed", "noHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("2", verifies["2"].m_proofs, verifies["2"].m_left_hash, "hashed", "noHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
    if (root != CMerkle::getRootByAProve("3", verifies["3"].m_proofs, verifies["3"].m_left_hash, "hashed", "noHash")
       )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
  }

  {
    auto[root, verifies, version, levels, leaves] = CMerkle::generate(
    {"98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0",
      "ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f",
      "3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d"});
    Q_UNUSED(verifies);
    Q_UNUSED(version);
    Q_UNUSED(levels);
    Q_UNUSED(leaves);
    if (root != keccak256(
         keccak256("98325468840887230d248330de2c99f76750d131aa6076dbd9e9a0ab20f09fd0ff1da71d8a78d13fd280d29c3f124e6e97b78a5c8317a2a9ff3d6c5f7294143f") +
         keccak256("3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d3b071f3d67e907ed5e2615ee904b9135e7ad4db666dad72aa63af1b04076eb9d"))
       )
    {
      CLog::log("ERROR in CMerkle::generate 3: " , "app", "fatal");
      exit(1098);
    }
  }

}

