#ifndef CMERKLETESTS_H
#define CMERKLETESTS_H

#include <math.h>
#include <vector>

#include <QList>
#include <QVariant>

#include "constants.h"
#include "lib/utils/cmerkle.h"
#include "merkle1.h"
#include "merkle2.h"
#include "merkle3.h"  // FIXME: implement ALL merkle tests

class CMerkleTests
{
public:
    CMerkleTests();
    static void doTests();
};

#endif // CMERKLETESTS_H
