#ifndef CUTILSTESTS_H
#define CUTILSTESTS_H

#include <math.h>
#include <vector>

#include <QList>
#include <QVariant>

#include "constants.h"

class CUtilsTests
{
public:
    CUtilsTests();
    static void doChunksTests();
    static void doChunkTests2();
    static void doTests();
};

#endif // CUTILSTESTS_H
