#ifndef CCRYPTOTESTS_H
#define CCRYPTOTESTS_H




class TestsCCrypto
{
public:
  TestsCCrypto();
  static void doTests();

  static bool test_b64();
  static void autoGenNativeKeyPairTests();
  static void autoGenECDSAKeyPairTests();
};

#endif // CCRYPTOTESTS_H
