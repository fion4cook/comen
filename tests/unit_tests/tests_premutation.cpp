#include "tests_premutation.h"

TestsPremutation::TestsPremutation()
{

}

void TestsPremutation::doTests()
{

  // FIXME: complete it in order to test more possibile combination in automatic style




  {
    PermutationHandler hp ({"a", "b", "c"}, 2);
    if (hp.m_permutations.size() != 3)
    {
      CLog::log("PermutationHandler failed on 1");
      exit(485);
    }
    hp.testAnalyze();
  }

  {
    PermutationHandler hp ({"a", "b"}, 1);
    if (hp.m_permutations.size() != 2)
    {
      CLog::log("PermutationHandler failed on 1");
      exit(485);
    }
    hp.testAnalyze();
  }

  {
    PermutationHandler hp ({"a"}, 1);
    if (hp.m_permutations.size() != 1)
    {
      CLog::log("PermutationHandler failed on 1");
      exit(485);
    }
  }


}
