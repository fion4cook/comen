#ifndef TESTSPREMUTATION_H
#define TESTSPREMUTATION_H

#include "lib/utils/permutation_handler.h"
#include "lib/clog.h"

class TestsPremutation
{
public:
  TestsPremutation();
  static void doTests();
};

#endif // TESTSPREMUTATION_H
