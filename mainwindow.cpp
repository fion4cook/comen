#include "stable.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "lib/file_buffer_handler/file_buffer_handler.h"


//#include <Poco/Net/MailMessage.h>
//#include <Poco/Net/MailRecipient.h>

#include <string>
//using namespace std;

#include "comen_email.hpp"



MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  CGUI::initGUI(ui);



//    m_wallet = new Wallet(this);

  CoinsT spendableCoins({{ "bbb", 400 }, { "aaa", 200 }});

//  Coin a_coin({ "aaa", 200 });
//  spendableCoins.push_back({ "bbb", 400 }, { "aaa", 200 });

//  = CoinsT{
//    ,
//    Coin { "bbb", 400 },
//    Coin { "ccc", 100 },
//    Coin { "fff", 50 }
//  };

//    m_wallet->setSpendableCoins(spendableCoins);

  ui->coinsTableWidget->setColumnCount(2);
  ui->coinsTableWidget->setHorizontalHeaderLabels(QStringList()
                                                  << "Refrence"
                                                  << "Value");

  QTableWidgetItem *item { };
  int              row { };

  for (const auto& c: spendableCoins)
  {
    row = ui->coinsTableWidget->rowCount();

    ui->coinsTableWidget->insertRow(row);

    item = new QTableWidgetItem { c.coinRef() };
    ui->coinsTableWidget->setItem(row, 0, item);

    item = new QTableWidgetItem { QString::number(c.value()) };
    ui->coinsTableWidget->setItem(row, 1, item);
  }

  connect(ui->sendPushButton, &QPushButton::clicked, [&](bool)
  {
//      if (sendEmail())
//      {
//        QMessageBox::critical(this, "Error", "Failed to send an email.");
//      }
//      else
//      {
//        QMessageBox::information(this, "Info", "Email sent successfully.");
//      }
  });
}


MainWindow::~MainWindow()
{
  delete ui;
}

int MainWindow::initMachineMain(const uint8_t &clone_id)
{
  CMachine::setClone(clone_id); // this
  CMachine::createFolders();

  GenRes res = InitializeNode::bootMachine();
  if (res.status != true)
  {
    CUtils::exiter(res.msg, 111);
  }

  InitializeNode::maybeInitDAG();

  ThreadsHandler::launchThreads();

  loadGUIData();

  return 1;
}

void MainWindow::loadConfigurationParameters()
{
//  restoreGeometry(g_cfg->m_mwGeometry);
//  restoreState(g_cfg->m_mwState);
}

void MainWindow::saveConfigurationParameters()
{
//  g_cfg->m_mwGeometry	= saveGeometry();
//  g_cfg->m_mwState		= saveState();
}

int MainWindow::sendEmail()
{
  using Poco::Net::MailMessage;
  using Poco::Net::MailRecipient;

  std::string host      { ui->hostLineEdit->text().toStdString() },
         sender    { ui->senderLineEdit->text().toStdString() },
         recipient { ui->recipientLineEdit->text().toStdString() },
         subject   { ui->subjectLineEdit->text().toStdString() },
         content   { ui->contentTextEdit->toPlainText().toStdString() },
         username  { ui->usernameLineEdit->text().toStdString() },
         password  { ui->passwordLineEdit->text().toStdString() };
  Poco::UInt16 port { static_cast<Poco::UInt16>(ui->portSpinBox->value()) };

  MailMessage msg;

  msg.setSender(sender);
  msg.addRecipient(MailRecipient(MailRecipient::PRIMARY_RECIPIENT, recipient));
  msg.setSubject(subject);
  msg.setContent(content);

  return ::sendEmail(msg, username, password, host, port);
}




void MainWindow::on_pushButton_dummyTrigger_clicked()
{
  FileBufferHandler::readAndParseHardDiskInbox();
}


