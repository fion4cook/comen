#include "mainwindow.h"

#include "stable.h"

#include "lib/ccrypto.h"

#include "global_funcs.hpp"

#include "lib/bech32.h"



int main(int argc, char *argv[])
{
  InitCCrypto::init();


  QApplication app(argc, argv);
    MainWindow w;
    w.show();

    QObject::connect(&app, &QCoreApplication::aboutToQuit, [&]()
    {
      onAboutToQuit(&w);
    });

    w.loadConfigurationParameters();

    if (true)
    {
      dummyTestsHandler();
    }

    uint8_t cloneId = 0;    // FIXME: this value must be defined by command line
    w.initMachineMain(cloneId);

  return app.exec();

}
