#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "stable.h"

#include "lib/machine/machine_handler.h"
#include "lib/services/initialize_node.h"
#include "lib/threads_handler.h"
#include "gui/c_gui.h"

#include "lib/wallet/wallet.h"
#include "lib/wallet/coin.h"

#include "global_vars.hpp"
#include "global_funcs.hpp"



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Wallet;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  void loadConfigurationParameters();
  void saveConfigurationParameters();

  int initMachineMain(const uint8_t &m_clone_id = 0);
  int sendEmail();

  //  -  -  -  -  GUI part
  void informationMessage(const QString &msg, const QString &title = "");
  void warningMessage(const QString &msg, const QString &title = "");
  void loadGUIData();
//  static QHash<QString, QBrush> getBlocksBrushes();
  static QHash<QString, QBrush> getCPacketBrushes();

  void refreshGUI();


  // -  -  -  -  -  -  -  settings tab
  bool emailSettingsValidator();

  // -  -  -  -  -  -  -  -  neighbors tab
//  void loadNeighborsInfo();

private slots:

  void on_pushButton_dummyTrigger_clicked();

  // - - - - - - Menu % Toolbar QActions
  void on_actionBackup_Machine_triggered();
  void on_actionExit_triggered();
  void on_pushButton_restoreMachineFromBackup_clicked();

  void on_pushButton_saveEmailsSettings_clicked();


  // - - - - - - Contributes
  void on_pushButton_prepareProposal_clicked();


  // - - - - - - Neighbors
  void connectNeighborsToModel();
  void handshakeDblClicked(const QModelIndex &index);
  bool on_pushButton_addANewNeighbor_clicked();
  void neighborDoubleClicked(int nRow, int nCol);
  void on_pushButton_readHDManually_clicked();



  // - - - - - - Monitor
  void connectInboxFilesToModel();
  void connectOutboxFilesToModel();
  void connectDAGHistoryToModel();
  void connectParsingQToModel();
  void refreshParsingQ();
  void refresDAG();
  void on_pushButton_refresDAGManually_clicked();
  void refreshSendingQ();
  void refreshMissingBlocks();
  void refreshLeavesInfo();

  void on_pushButton_invokeDescendents_clicked();

  void on_pushButton_broadcastManually_clicked();

  void on_pushButton_invokeBlock_clicked();

  void on_pushButton_pullFromParsingQManually_clicked();

  void on_pushButton_controlMissedBlocks_clicked();

  void on_pushButton_invokeAllMissedBlocks_clicked();

private:
  Ui::MainWindow *ui;

  Wallet *m_wallet { };
};
#endif // MAINWINDOW_H
