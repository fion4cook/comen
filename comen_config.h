#ifndef COMEN_CONFIG_H
#define COMEN_CONFIG_H

#include <QString>

class QSettings;

class Config
{
public:
	Config(const QString& path);
	Config();
	~Config();
	Config(const Config& other) = delete;

	Config& operator=(const Config& rhs) = delete;

	inline const QString& path() const { return m_path; }
	inline void setPath(const QString& path) { m_path	= path; }

	bool load();
	bool save();

  QByteArray m_mwGeometry,
             m_mwState;
  QString    m_emailHost;

private:
	QString		m_path;
};

#endif // COMEN_CONFIG_H
