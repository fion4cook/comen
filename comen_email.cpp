#include <Poco/Net/MailRecipient.h>
#include <Poco/Net/SecureSMTPClientSession.h>
#include <Poco/Net/StringPartSource.h>
#include <Poco/Net/SSLManager.h>
#include <Poco/Net/KeyConsoleHandler.h>
#include <Poco/Net/ConsoleCertificateHandler.h>
#include <Poco/SharedPtr.h>
#include <Poco/Exception.h>
//#include <iostream>

using Poco::Net::MailMessage;
using Poco::Net::MailRecipient;
using Poco::Net::SMTPClientSession;
using Poco::Net::SecureSMTPClientSession;
using Poco::Net::StringPartSource;
using Poco::Net::SSLManager;
using Poco::Net::Context;
using Poco::Net::KeyConsoleHandler;
using Poco::Net::PrivateKeyPassphraseHandler;
using Poco::Net::InvalidCertificateHandler;
using Poco::Net::ConsoleCertificateHandler;
using Poco::SharedPtr;
using Poco::Exception;

class SSLInitializer
{
public:
  SSLInitializer()
  {
    Poco::Net::initializeSSL();
  }

  ~SSLInitializer()
  {
    Poco::Net::uninitializeSSL();
  }
};

int sendEmail(const Poco::Net::MailMessage& mailMsg,
              const std::string& username, const std::string& password,
              const std::string& host, Poco::UInt16 port)
{
  SSLInitializer sslInitializer;

  try
  {
    SharedPtr<InvalidCertificateHandler> pCert = new ConsoleCertificateHandler(false);
    Context::Ptr pContext = new Context(Context::CLIENT_USE, "", "", "", Context::VERIFY_RELAXED, 9, true, "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
    SSLManager::instance().initializeClient(0, pCert, pContext);

    SecureSMTPClientSession session(host, port);
    session.login();
    session.startTLS();
    if (!username.empty())
    {
      session.login(SMTPClientSession::AUTH_LOGIN, username, password);
    }
    session.sendMessage(mailMsg);
    session.close();
  }
  catch (Exception& e)
  {
//    std::cerr << e.message() << std::endl;

    return -1;
  }

  return 0;
}
