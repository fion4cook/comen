#ifndef WALLET_H
#define WALLET_H

#include "coinentry.h"

#include <QObject>
#include <QList>

typedef QList<CoinEntry> TheCoins;

class Wallet : public QObject
{
    Q_OBJECT
public:



    explicit Wallet(QObject *parent = nullptr);



    const TheCoins& spendableCoins() const;
    void setSpendableCoins(const TheCoins &spendableCoins);

signals:

private:
    TheCoins m_spendableCoins;
    TheCoins m_waitingCoins;
    TheCoins m_spentCoins;

};

#endif // WALLET_H
