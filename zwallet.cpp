#include "wallet.h"
#include "coinentry.h"

typedef QList<CoinEntry> TheCoins;

Wallet::Wallet(QObject *parent) : QObject(parent)
{

}

const TheCoins& Wallet::spendableCoins() const
{
    return m_spendableCoins;
}

void Wallet::setSpendableCoins(const TheCoins &spendableCoins)
{
    m_spendableCoins = spendableCoins;
}
