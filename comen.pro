QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += BOOST_LOG_DYN_LINK

QMAKE_CXXFLAGS += -Wno-unused-parameter

INCLUDEPATH += /usr/include/cryptopp \

PRECOMPILED_HEADER = stable.h

SUBDIRS += \
    gui/ \
    gui/models/ \
    gui/views/ \
    lib \
    lib/address \
    lib/block \
    lib/block/block_types \
    lib/block/block_types/block_coinbase \
    lib/block/document_types \
    lib/block/document_ext_info_types \
    lib/dag \
    lib/dag/normal_block \
    lib/database \
    lib/documents_in_related_block \
    lib/file_handler \
    lib/machine \
    lib/network/email \
    lib/network/i2p \
    lib/network/onion \
    lib/network/tcp \
    lib/network \
    lib/pgp \
    lib/sending_q_handler \
    lib/services \
    lib/services/dna \
    lib/services/contracts \
    lib/services/contracts/flens \
    lib/services/contracts/loan \
    lib/services/contracts/pledge \
    lib/services/contracts/reputation \
    lib/services/free_docs \
    lib/services/free_docs/demos \
    lib/services/free_docs/demos/init_contents \
    lib/services/free_docs/wiki \
    lib/services/free_docs/wiki/init_contents \
    lib/services/polling \
    lib/utils \
    lib/transactions \
    lib/transactions/basic_transactions \
    lib/transactions/basic_transactions/signature_structure_handler \
    lib/transactions/basic_transactions/utxo \
    lib/wallet \
    tests/unit_tests \
    tests/unit_tests/merkle


# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    gui/c_gui.cpp \
    gui/dddd.cpp \
    gui/models/model_blocks.cpp \
    gui/models/model_neighbors.cpp \
    gui/models/model_parsing_q.cpp \
    gui/views/view_contributes.cpp \
    gui/views/view_monitor.cpp \
    gui/views/view_neighbors.cpp \
    gui/views/view_settings.cpp \
    gui/qmenu.cpp \
    lib/block/block_types/block_coinbase/coinbase_issuer.cpp \
    lib/block/block_types/block_coinbase/coinbase_block.cpp \
    lib/block/block_types/block_coinbase/coinbase_utxo_handler.cpp \
    lib/block/block_types/block_floating_signature/floating_signature_block.cpp \
    lib/block/document_types/basic_tx_document.cpp \
    lib/block/document_types/coinbase_document.cpp \
    lib/block/document_types/document_factory.cpp \
    lib/block/document_types/rl_docdocument.cpp \
    lib/block/document_types/rp_docdocument.cpp \
    lib/block/documents_in_related_block/transactions/coins_visibility_handler.cpp \
    lib/block/documents_in_related_block/transactions/equations_controls.cpp \
    lib/block/documents_in_related_block/transactions/transactions_in_related_block.cpp \
    lib/dag/full_dag_handler.cpp \
    lib/dag/normal_block/rejected_transactions_handler.cpp \
    lib/dag/society_rules/society_rules.cpp \
    lib/dag/super_control_til_coinbase_minting.cpp \
    lib/file_buffer_handler/file_buffer_handler.cpp \
    lib/messaging_protocol/dag_message_handler.cpp \
    lib/messaging_protocol/dispatcher.cpp \
    lib/messaging_protocol/graphql_handler.cpp \
    lib/messaging_protocol/message_handler.cpp \
    lib/messaging_protocol/greeting.cpp \
    lib/network/cpacket_handler.cpp \
    lib/network/network_handler.cpp \
    lib/services/treasury/treasury_handler.cpp \
    lib/transactions/basic_transactions/utxo/spent_coins_handler.cpp \
    pch.cpp \
    comen_config.cpp \
    comen_email.cpp \
    comen_threads.cpp \
    constants.cpp \
    global_funcs.cpp \
    global_vars.cpp \
    lib/address/address_handler.cpp \
    lib/address/basic_addresses.cpp \
    lib/address/bitcoin_like_addresses.cpp \
    lib/address/complex_addresses.cpp \
    lib/address/indented_addresses.cpp \
    lib/address/strict_addresses.cpp \
    lib/block/block_ext_info.cpp \
    lib/block/block_types/block_factory.cpp \
    lib/block/block_types/block_normal/normal_block.cpp \
    lib/block/document_types/arbitrary_doc.cpp \
    lib/block/document_types/dna_proposal.cpp \
    lib/block_utils.cpp \
    lib/ccrypto.cpp \
    lib/clog.cpp \
    lib/dag/leaves_handler.cpp \
    lib/dag/missed_blocks_handler.cpp \
    lib/dag/sceptical_dag_integrity_control.cpp \
    lib/dag/walk_through.cpp \
    lib/dag/normal_block/normal_utxo_handler.cpp \
    lib/k_v_handler.cpp \
    lib/network/broadcast_logger.cpp \
    lib/network/email/email.cpp \
    lib/machine/machine_backup.cpp \
    lib/machine/machine_handler.cpp \
    lib/machine/machine_neighbor.cpp \
    lib/machine/machine_services_interests.cpp \
    lib/parsing_q_handler/parsing_q_handler.cpp \
    lib/parsing_q_handler/queue_picker.cpp \
    lib/parsing_q_handler/queue_utils.cpp \
    lib/pgp/cpgp.cpp \
    lib/sending_q_handler/sending_q_handler.cpp \
    lib/services/contracts/flens/iname_binder.cpp \
    lib/services/contracts/flens/iname_handler.cpp \
    lib/services/contracts/flens/iname_messaging.cpp \
    lib/services/contracts/flens/iname_register_handler.cpp \
    lib/services/contracts/flens/reserved_inames.cpp \
    lib/services/dna/dna_handler.cpp\
    lib/services/free_docs/demos/demos_handler.cpp \
    lib/services/free_docs/demos/initialize_agoras.cpp \
    lib/services/free_docs/demos/init_contents/agoras_init_contents.cpp \
    lib/services/free_docs/wiki/wiki_handler.cpp \
    lib/services/free_docs/wiki/init_contents/wikis_init_contents.cpp \
    lib/services/initialize_node.cpp \
    lib/services/polling/polling_handler.cpp \
    lib/transactions/basic_transactions/signature_structure_handler/general_structure.cpp \
    lib/threads_handler.cpp \
    lib/transactions/basic_transactions/signature_structure_handler/individual_signature.cpp \
    lib/transactions/basic_transactions/signature_structure_handler/unlock_document.cpp \
    lib/transactions/basic_transactions/signature_structure_handler/unlock_set.cpp \
    lib/transactions/basic_transactions/utxo/utxo_handler.cpp \
    lib/utils/compressor.cpp \
    lib/utils/permutation_handler.cpp \
    lib/version_handler.cpp \
    lib/wallet/wallet_address_handler.cpp \
    lib/bech32.cpp \
    lib/dag/dag.cpp \
    lib/block/block_types/block.cpp \
    lib/block/document_types/document.cpp \
    lib/block/document_ext_info_types/document_ext_info.cpp \
    lib/block/block_types/genesis.cpp \
    lib/block/signal.cpp \
    lib/database/db_model.cpp \
    lib/database/init_sql.cpp \
    lib/database/query_creator.cpp \
    lib/file_handler/file_read_res.cpp \
    lib/wallet/signature_set.cpp \
    lib/wallet/signature_unlcok_set.cpp \
    lib/file_handler/file_handler.cpp \
    lib/database/db_handler.cpp \
    lib/wallet/coin.cpp \
    lib/utils/cmerkle.cpp \
    lib/utils/cutils.cpp \
    lib/utils/cutils_dumpers.cpp \
    lib/wallet/wallet.cpp \
    main.cpp \
    mainwindow.cpp \
    tests/unit_tests/cutils/tests_cycle_times.cpp \
    tests/unit_tests/cutils/tests_string_manipulations.cpp \
    tests/unit_tests/merkle/merkle.cpp \
    tests/unit_tests/merkle/merkle1.cpp \
    tests/unit_tests/merkle/merkle2.cpp \
    tests/unit_tests/merkle/merkle3.cpp \
    tests/unit_tests/ccryptotests.cpp \
    tests/unit_tests/cutils_tests.cpp \
    tests/unit_tests/sql_query_generator.cpp \
    tests/unit_tests/tests_pgp.cpp \
    tests/unit_tests/tests_premutation.cpp

HEADERS += \
    gui/c_gui.h \
    gui/dddd.h \
    gui/models/model_blocks.h \
    gui/models/model_neighbors.h \
    gui/models/model_parsing_q.h \
    lib/block/block_types/block_coinbase/coinbase_issuer.h \
    lib/block/block_types/block_coinbase/coinbase_block.h \
    lib/block/block_types/block_coinbase/coinbase_utxo_handler.h \
    lib/block/block_types/block_floating_signature/floating_signature_block.h \
    lib/block/document_types/basic_tx_document.h \
    lib/block/document_types/coinbase_document.h \
    lib/block/document_types/document_factory.h \
    lib/block/document_types/rl_docdocument.h \
    lib/block/document_types/rp_docdocument.h \
    lib/block/documents_in_related_block/transactions/coins_visibility_handler.h \
    lib/block/documents_in_related_block/transactions/equations_controls.h \
    lib/block/documents_in_related_block/transactions/transactions_in_related_block.h \
    lib/dag/full_dag_handler.h \
    lib/dag/normal_block/rejected_transactions_handler.h \
    lib/dag/society_rules/society_rules.h \
    lib/dag/super_control_til_coinbase_minting.h \
    lib/file_buffer_handler/file_buffer_handler.h \
    lib/messaging_protocol/dag_message_handler.h \
    lib/messaging_protocol/dispatcher.h \
    lib/messaging_protocol/graphql_handler.h \
    lib/messaging_protocol/message_handler.h \
    lib/network/cpacket_handler.h \
    lib/network/network_handler.h \
    lib/services/treasury/treasury_handler.h \
    lib/transactions/basic_transactions/utxo/spent_coins_handler.h \
    pch.h \
    constants.h \
    comen_config.h \
    comen_email.hpp \
    comen_threads.hpp \
    global_funcs.hpp \
    global_vars.hpp \
    lib/address/address_handler.h \
    lib/block/block_ext_info.h \
    lib/block/block_types/block_factory.h \
    lib/block/block_types/block_normal/normal_block.h \
    lib/block/document_types/arbitrary_doc.h \
    lib/block/document_types/dna_proposal.h \
    lib/block_utils.h \
    lib/dag/missed_blocks_handler.h \
    lib/k_v_handler.h \
    lib/network/broadcast_logger.h \
    lib/parsing_q_handler/parsing_q_handler.h \
    lib/pgp/cpgp.h \
    lib/sending_q_handler/sending_q_handler.h \
    lib/transactions/basic_transactions/signature_structure_handler/individual_signature.h \
    lib/transactions/basic_transactions/signature_structure_handler/unlock_document.h \
    lib/transactions/basic_transactions/signature_structure_handler/unlock_set.h \
    lib/transactions/basic_transactions/utxo/utxo_handler.h \
    lib/utils/cmerkle.h \
    lib/utils/compressor.h \
    lib/utils/cutils.h \
    lib/dag/dag.h \
    lib/block/block_types/block.h \
    lib/block/document_types/document.h \
    lib/block/document_ext_info_types/document_ext_info.h \
    lib/block/block_types/genesis.h \
    lib/block/signal.h \
    lib/ccrypto.h \
    lib/dag/leaves_handler.h \
    lib/dag/sceptical_dag_integrity_control.h \
    lib/dag/normal_block/normal_utxo_handler.h \
    lib/network/email/email.h \
    lib/machine/machine_handler.h \
    lib/services/contracts/flens/iname_handler.h \
    lib/services/contracts/flens/reserved_inames.h \
    lib/services/dna/dna_handler.h \
    lib/services/free_docs/demos/demos_handler.h \
    lib/services/free_docs/demos/initialize_agoras.h \
    lib/services/free_docs/demos/init_contents/agoras_init_contents.h \
    lib/services/free_docs/wiki/wiki_handler.h \
    lib/services/free_docs/wiki/init_contents/wikis_init_contents.h \
    lib/services/initialize_node.h \
    lib/services/polling/polling_handler.h \
    lib/transactions/basic_transactions/signature_structure_handler/general_structure.h \
    lib/threads_handler.h \
    lib/utils/permutation_handler.h \
    lib/version_handler.h \
    lib/wallet/wallet_address_handler.h \
    tests/unit_tests/cutils/tests_cycle_times.h \
    tests/unit_tests/cutils/tests_string_manipulations.h \
    tests/unit_tests/merkle/merkle.h \
    tests/unit_tests/merkle/merkle1.h \
    tests/unit_tests/merkle/merkle2.h \
    tests/unit_tests/merkle/merkle3.h \
    tests/unit_tests/ccryptotests.h \
    tests/unit_tests/cutils_tests.h \
    tests/unit_tests/sql_query_generator.h \
    lib/clog.h \
    lib/database/db_model.h \
    lib/database/init_sql.h \
    lib/database/query_creator.h \
    lib/file_handler/file_read_res.h \
    lib/wallet/signature_set.h \
    lib/wallet/signature_unlcok_set.h \
    mainwindow.h \
    lib/vector.h \
    lib/bech32.h \
    lib/file_handler/file_handler.h \
    lib/database/db_handler.h \
    lib/wallet/coin.h \ \
    tests/unit_tests/tests_pgp.h \
    tests/unit_tests/tests_premutation.h

    lib/wallet/wallet.h

FORMS += \
    gui/mainwindow.ui \


LIBS += -lPocoFoundation -lPocoCrypto -lPocoNet -lPocoNetSSL -lpthread -lboost_log_setup -lboost_log -lboost_regex -lboost_system -lboost_thread \
    -lcryptopp


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
