#ifndef GLOBAL_VARS_HPP
#define GLOBAL_VARS_HPP

#include "comen_config.h"

#include <QFileInfo>
#include <QFileSystemWatcher>

extern Config             *g_cfg;
extern QString            g_monDir;
extern QFileInfoList      g_monFiList;
extern QFileSystemWatcher *g_fsWatch;

#endif // GLOBAL_VARS_HPP
